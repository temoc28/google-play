@isTest
public class TestRestCustomerRecords 
{
    @isTest static void testRestCustomerRecords0()
    {
        //createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    @isTest static void testRestCustomerRecords1()
    {
        //Map<String,Id> idsMap = createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', 'test@levtest.com');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = [SELECT Id FROM Account WHERE FirstName='test'][0].Id;
        rma.GoogleAsset__c = [SELECT Id FROM Google_Asset__c WHERE Name = 'TestGA'][0].Id;
        rma.ServiceModel__c='Remorse + DOA + Warranty';
        insert rma;
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    @isTest static void testRestCustomerRecords2()
    {
        //createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', 'testOppt');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    @isTest static void testRestCustomerRecords3()
    {
        //createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', 'TestGA');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    @isTest static void testRestCustomerRecords4()
    {
        //Map<String,Id> idsMap = createTestData();
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = [SELECT Id FROM Account WHERE FirstName='test'][0].Id;
        rma.GoogleAsset__c = [SELECT Id FROM Google_Asset__c WHERE Name = 'TestGA'][0].Id;
        rma.ServiceModel__c='Remorse + DOA + Warranty + Repair';
        insert rma;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', rma.Name);
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    @testSetup static void createTestData(){
        
        Account acct = new Account();
        acct.PersonEmail = 'test@levtest.com';
        acct.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'GoogleCustomer'].id;
        acct.FirstName = 'test';
        acct.LastName = 'levtest';
        acct.PersonMailingCountry = 'US';
        acct.PersonMailingState = 'AZ';
        insert acct;
        
        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c();
        codeSet.Name = 'CHromecaseTest';
        insert codeSet;
        
        Product_Family__c prodFam = new Product_Family__c();
        prodFam.Name = 'Chromecast Test';
        insert prodFam;
        
        Document__c doc = new Document__c();
        doc.Name = 'Chromecast Test';
        doc.Product_Family__c = prodFam.Id;
        doc.Return_Reason_Code_Set__c = codeSet.Id;
        insert doc;
        
        Country_Variant__c cv = new Country_Variant__c();
        cv.Document__c = doc.Id;
        cv.Name = 'US';
        insert cv;
        
        Product2 prod = new Product2();
        prod.Name = 'Chromecast US Test';
        prod.SKU__c = '86002596-01-Test';
        prod.Sale_Country__c = 'US';
        prod.Unique_ID_Type__c = 'Serial Number';
        prod.RMARecordType__c = 'Chromecast';
        prod.Document__c = doc.Id;
        insert prod;
        
        Opportunity oppt = new Opportunity();
        oppt.AccountId = acct.Id;
        oppt.OrderIDExternalID__c = '32434902134BNMBd';
        oppt.TransactionDate__c = dateTime.now();
        oppt.Country__c = 'US';
        oppt.State_Province__c = 'AZ';
        oppt.Name = 'testOppt';
        oppt.Type = 'Standard Order';
        oppt.CloseDate = Date.today().addDays(30);
        oppt.StageName = 'Prospecting';
        insert oppt;
        
        Google_Asset__c ga = new Google_Asset__c();
        ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga.AssetOwner__c = acct.Id;
        ga.SKU__c = '86002596-01-Test';
        ga.Name = 'TestGA';
        ga.Order_Opportunity__c = oppt.Id;
        ga.Line_Number__c = 1;
        ga.ProductID__c = prod.Id;
        insert ga;
    }
}