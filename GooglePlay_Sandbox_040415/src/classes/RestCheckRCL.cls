@RestResource(urlMapping='/RCLVerify/*')
global class RestCheckRCL {

		@HttpGet
		global static RestResponseModel.RCLCheck checkRCL(){
			RestResponseModel.RCLCheck response = new RestResponseModel.RCLCheck();
			Map<String,String> paramMap = RestContext.request.params;
			
			
			String rmaId = paramMap.get('rma_id');
			String orderId = paramMap.get('order_id');
			
			
			Rma__c rma;
			
			try{
				rma = [SELECT Type__c,Id,Name,RCL_Verify_ID__c,Replacement_Order_Status__c,Status__c FROM Rma__c WHERE Name = :rmaId];
			}
			catch(QueryException qe){
				response.failure_reason = 'RMA does not exist.';
				response.is_valid_request = false;
				return response;
			}
			
			
			//Check Warranty Type
			if(!rma.Type__c.equalsIgnoreCase('Warranty DOA') && !rma.Type__c.equalsIgnoreCase('Warranty Regular')){
				response.failure_reason = 'Warranty type not eligible for replacement order.';
				response.is_valid_request = false;
				return response;
			}
			
			if(rma.RCL_Verify_ID__c != null && !rma.RCL_Verify_ID__c.equalsIgnoreCase(orderId)){
				response.failure_reason = 'Replacement link already exists for this RMA with OrderId: ' + rma.RCL_Verify_ID__c;
				response.is_valid_request = false;
				return response;
			}
			

			if(rma.Status__c.equals('Closed') || rma.Status__c.equals('Cancel Auth Pending')){
				response.failure_reason = 'RMA has been closed/cancelled.';
				response.is_valid_request = false;
				return response;
			}
			
			if((rma.RCL_Verify_ID__c == null || rma.RCL_Verify_ID__c.trim().equals('')) || (rma.RCL_Verify_ID__c != null && rma.RCL_Verify_ID__c.equalsIgnoreCase(orderId))){
				rma.RCL_Verify_ID__c = orderId;
				rma.Replacement_Order_Status__c = 'Pending';
				try{
					update rma;
				}
				catch(DMLException dmle){
					response.failure_reason = 'Replacement link cannot be set for this RMA. Failed to update record in Salesforce.' + dmle;
					response.is_valid_request = false;
					return response;
				}
				
				response.failure_reason = 'N/A#';
				response.is_valid_request = true;
				return response;
			}
			
			//
			response.failure_reason = 'Unknown failure.';
			response.is_valid_request = false;
			return response;
		}
}