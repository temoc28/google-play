global class MainRESTWebServiceMock implements HttpCalloutMock{
    
	global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><sessionId>00DU0000000KYL9!AQYAQB2mY685UHNSFPFaqOrg7PxetD7FqwBMK.eKVqB4mesH_F3cV7nZWONPO9VREtflKD0a35yNZPIrRplgFJllHTseTwXT</sessionId></result></loginResponse></soapenv:Body></soapenv:Envelope>');
        res.setStatusCode(200);
        return res;
    }
	
}