public with sharing class RefundShipping 
{
	private Opportunity opp;	
	public RefundShipping(ApexPages.StandardController con)
	{
		opp = (Opportunity)con.getRecord();
	}
	public PageReference createShippingRefund()
	{
		Id shippingRefundRt = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Shipping Refund').getRecordTypeId();
		List<Refund__c> refundList = new List<Refund__c>([SELECT Id FROM Refund__c WHERE OrderNumber__c=:opp.Id AND RecordTypeId=:shippingRefundRt]);
		if(refundList.size()>0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'This order already has a shipping refund'));
			return null;
		}
		Refund__c refund = new Refund__c();
		refund.OrderNumber__c=opp.Id;
		refund.PercentShippingItemRefund__c=100;
		refund.PercentShippingTaxRefund__c=100;
		refund.RefundStatus__c='Pending';
		refund.RefundReason__c='Shipping';
		//check to see which date is further out
		Date plusBusinessDays = null;
		Date plusOneDay = Date.today().addDays(1);
		if(opp.Ship_By_Date__c!=null)
			plusBusinessDays = BusinessDays.addBusinessDays(opp.Ship_By_Date__c,3);
		else
			plusBusinessDays = Date.today();
		refund.ExpectedRefundDate__c = plusBusinessDays > plusOneDay ? plusBusinessDays : plusOneDay;
		refund.RecordTypeId = shippingRefundRt;
		try
		{
			insert refund;
		}
		catch(DMLException e)
		{
			ApexPages.addMessages(e);
			return null;
		}
		return new PageReference('/'+opp.Id);
	}	
}