public with sharing class RefundTotalTax 
{
	Opportunity opp;
	public Refund__c refund {get;set;}
	public RefundTotalTax(ApexPages.StandardController con)
	{
		opp=(Opportunity)con.getRecord();
		refund = new Refund__c();
		refund.RefundReason__c='Tax Exempt';
	}
	public PageReference createTotalTaxRefund()
	{
		Id tTaxRefundRt = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Tax Refund').getRecordTypeId();
		List<Refund__c> refundList = new List<Refund__c>([SELECT Id FROM Refund__c WHERE OrderNumber__c=:opp.Id AND RecordTypeId=:tTaxRefundRt]);
		if(refundList.size()>0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'This order already has a total tax refund'));
			return null;
		}		
		refund.OrderNumber__c=opp.Id;		
		refund.RefundStatus__c='Pending';
		refund.ExpectedRefundDate__c=Date.today().addDays(1);
		refund.RecordTypeId=tTaxRefundRt;		
		insert refund;
		return new PageReference('/'+refund.Id);
	}
}