/*
    Description: Retrieves all the google asset information using the assetId sent in and assigns it to the variables of the response object.
    returns response object with Google asset Information.
 */
@RestResource(urlMapping='/Owner/*')
global class RestGetOwner
{
	@HttpGet
	global static RestResponseModel.owner_details_t getAssetOwner()
	{
		Map<String,String> paramMap = RestContext.request.params;
		String assetId = paramMap.get('assetId');
		RestResponseModel.owner_details_t od = new RestResponseModel.owner_details_t();
		if(String.isNotEmpty(assetId))
		{
			List<Google_Asset__c> assetList = new List<Google_Asset__c>([SELECT Id,Name,AssetOwner__r.FirstName,AssetOwner__r.LastName,AssetOwner__r.PersonEmail,AssetOwner__r.PersonMailingStreet,AssetOwner__r.PersonMailingCity,AssetOwner__r.PersonMailingState,AssetOwner__r.PersonMailingCountry,AssetOwner__r.PersonMailingPostalCode FROM Google_Asset__c WHERE Name=:assetId]);
			if(assetList.size()==1)
			{
				Google_Asset__c ga = assetList.get(0);
				od.unique_id=ga.Name;
				od.new_owner_first_name=ga.AssetOwner__r.FirstName;
				od.new_owner_last_name=ga.AssetOwner__r.LastName;
				od.new_owner_email=ga.AssetOwner__r.PersonEmail;
				od.new_owner_address = new RestResponseModel.address_t();
				od.new_owner_address.address_street=ga.AssetOwner__r.PersonMailingStreet;
				od.new_owner_address.address_city=ga.AssetOwner__r.PersonMailingCity;
				od.new_owner_address.address_state_province=ga.AssetOwner__r.PersonMailingState;
				od.new_owner_address.address_country=ga.AssetOwner__r.PersonMailingCountry;
				od.new_owner_address.address_postal_code=ga.AssetOwner__r.PersonMailingPostalCode;
			}
		}
		return od;
	}
}