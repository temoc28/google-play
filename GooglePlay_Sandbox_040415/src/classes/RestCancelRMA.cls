@RestResource(urlMapping='/CancelRMA/*')
global class RestCancelRMA
{
	//do we need to pass unique_id and rma_id?
	/*	Description: Gets the RMA object details based on the RMA Id that was sent in. returns success or failure. success if RMA was retrieved successfully and status was set "closed".
		Else it returns Failure . For example in the case of not finding the RMA.
	*/
	global class RestCancelRMARequest{
		global String rmaId;
	}

	@HttpPost
	global static RestResponseModel.submit_rma_response_t doCancelRMA(RestCancelRMARequest request)
	{
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
		if(request != null && request.rmaId != null && String.isNotEmpty(request.rmaId))
		{
			String rmaId= request.rmaId;
			List<RMA__c> rmaList = new List<RMA__c>([SELECT Id, RCL_Verify_ID__c, Status__c  FROM RMA__c WHERE Name=:rmaId]);
			if(rmaList.size()==1)
			{
				RMA__c rma = rmaList.get(0);
				if(rma.RCL_Verify_ID__c != null && String.isNotEmpty(rma.RCL_Verify_ID__c))
				{
					submit_rma_response.failure_reason='RCL Verify ID is populated cannot close RMA: '+ rmaId;
					submit_rma_response.is_success=false;
					return submit_rma_response;
				}
				rma.Status__c='Closed';
				try
				{
					update rma;
				}
				catch(Exception e)
				{
					submit_rma_response.failure_reason=e.getMessage();
					submit_rma_response.is_success=false;
				}
				submit_rma_response.is_success=true;
			}
			else
			{
				submit_rma_response.failure_reason='No RMA was found for: '+rmaId;
				submit_rma_response.is_success=false;
			}
		}
		else
		{
			submit_rma_response.failure_reason='rmaId parameter was not passed';
			submit_rma_response.is_success=false;
		}
		return submit_rma_response;
	}
}