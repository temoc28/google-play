@isTest
private class TestRestGoogleAccount {
 	@testSetup static void setUp() {
        Account a = new Account();
        a.FirstName = 'First';
        a.LastName = 'Last';
        a.PersonEmail='test@test.com';        
        a.PersonMailingCountry='GB';
        insert a;
        
	}

	@isTest static void testGetObjectByName() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAccountTest/';
        rReq.httpMethod = 'POST';
        rReq.addParameter('accountName','TestName');

        RestContext.request = rReq;
        RestContext.response = rRes;

		Account a = new Account();
        a.Name = 'TestName';
        insert a;

        // TEST
        GoogleAccountModel ggAccountmodel = (GoogleAccountModel) RestGoogleAccount.getObject();

        // Check
        System.assert(ggAccountmodel != null);
        System.assertEquals('TestName', ggAccountmodel.Name);

    }

	@isTest static void testGetObjectByEmail() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAccountTest/';
        rReq.httpMethod = 'POST';
        rReq.addParameter('email','test@test.com');

        RestContext.request = rReq;
        RestContext.response = rRes;

         // TEST
        GoogleAccountModel ggAccountmodel = (GoogleAccountModel) RestGoogleAccount.getObject();

        // Check
        System.assert(ggAccountmodel != null);
        System.assertEquals('test@test.com', ggAccountmodel.personEmail);
    }

    @isTest static void testGetObjectById() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAccountTest/';
        rReq.httpMethod = 'POST';

         // TEST
        GoogleAccountModel ggAccountmodel = (GoogleAccountModel) RestGoogleAccount.getObject();

        // Check
        System.assert(ggAccountmodel != null);
    }

    @isTest static void testUpdateObject() {
    	// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAccountTest/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        GoogleAccountModel ggAcc = (GoogleAccountModel) RestGoogleAccount.getObject();
        String oldName = ggAcc.Name;

        ggAcc.Name = 'NEWNAME';
		GoogleAccountModel updatedModel = (GoogleAccountModel) RestGoogleAccount.updateObject(ggAcc);        

		// CHECK
		System.assertNotEquals(oldName, updatedModel.Name);
		System.assertEquals('NEWNAME', updatedModel.Name);
    }

    @isTest static void testUpdateModel() {
    	// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAccountTest/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        Account acc = [SELECT Id, PersonEmail FROM Account WHERE Account.PersonEmail = 'test@test.com' limit 1];
        String oldEmail = acc.PersonEmail;

        acc.PersonEmail = 'newmail2@test.com';
		GoogleAccountModel updatedModel = (GoogleAccountModel) RestGoogleAccount.updateModel(acc);        

		// CHECK
		System.assertNotEquals(oldEmail, updatedModel.PersonEmail);
		System.assertEquals('newmail2@test.com', updatedModel.PersonEmail);
    }


}