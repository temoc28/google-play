@RestResource(urlMapping='/CreateExtendedWarranty/*')
global class RestCreateExtendedWarranty {
	public static final String CLOSED_WON = 'Closed/Won';
	public static final String ERROR_DUPLICATE = 'Duplicate Contract Id.';
	public static final String STANDARD_ORDER = 'Standard Order';
	public static final String GOOGLE_PLAY_ASSET = 'Google Play Asset';
    @HttpPost
    global static RestResponseModel.extended_warranty_response_t createExtendedWarranty(RestResponseModel.extended_warranty_request_detail_t extendedDetail){
    	RestResponseModel.extended_warranty_response_t extended_warranty_response = new RestResponseModel.extended_warranty_response_t();
    	if(extendedDetail.asset_id == null){
    		extended_warranty_response.failure_reason= 'Asset id parameter was not found';
            extended_warranty_response.is_success=false;
    	}else if(extendedDetail.order_id == null){
			extended_warranty_response.failure_reason= 'Order id parameter was not found';
            extended_warranty_response.is_success=false;
		}else if(extendedDetail.contract_id == null){
    		extended_warranty_response.failure_reason= 'Contract id parameter was not found';
            extended_warranty_response.is_success=false;
    	}else if(extendedDetail.warranty_expiration_date == null){
			extended_warranty_response.failure_reason= 'Warranty expiration date parameter was not found';
            extended_warranty_response.is_success=false;
    	}else{
	    	List<Opportunity> orderList = new List<Opportunity>();
	    	List<Google_Asset__c> assetList = new List<Google_Asset__c>();
	    	List<Extended_Warranty__c> extendList = new List<Extended_Warranty__c>();

	    	Opportunity orderObj;
	    	Google_Asset__c assetObj;
	    	Extended_Warranty__c extendObj;
	    	String defaultCountry = 'US';

	    	Savepoint sp = Database.setSavepoint();
	        try{
		    	orderList = [SELECT Id,ShipmentDate__c FROM Opportunity WHERE Name=:extendedDetail.order_id LIMIT 1];
		    	if(orderList.size() == 0){//create new order
	    			orderObj = new Opportunity();
	    			orderObj.Name = extendedDetail.order_id;
	    			orderObj.StageName = RestCreateExtendedWarranty.CLOSED_WON;
	    			orderObj.CloseDate = System.Today();
	    			orderObj.ShipmentDate__c = System.Today();
	    			orderObj.Type = RestCreateExtendedWarranty.STANDARD_ORDER;
	    			if(extendedDetail.country == null || extendedDetail.country == ''){
	    				orderObj.Country__c = defaultCountry;
	    			}else{
	    				orderObj.Country__c = extendedDetail.country;
	    			}
		    		insert orderObj;
		    	}else {
		    		orderObj = orderList.get(0);
		    		if(orderObj.ShipmentDate__c==null){
		    			orderObj.ShipmentDate__c = System.Today();
		    			update orderObj;
		    		}
		    	}
		    	assetList = [SELECT Id,Extended_Warranty__c,Order_Opportunity__c,Line_Number__c FROM Google_Asset__c WHERE Name=:extendedDetail.asset_id LIMIT 1];
		    	if(assetList.size() == 0){//create new assert
					assetObj = new Google_Asset__c();
					assetObj.Name = extendedDetail.asset_id;
					assetObj.SKU__c = extendedDetail.sku;
					assetObj.RecordTypeId = Schema.SObjectType.Google_Asset__c.getRecordTypeInfosByName().get(RestCreateExtendedWarranty.GOOGLE_PLAY_ASSET).getRecordTypeId();
					insert assetObj;
		    	}else assetObj = assetList.get(0);
		    	extendList = [SELECT Id,Name,Line_Number__c,Current_Active_Asset__r.Name,Google_Order__r.Name,Expiration_Date__c FROM Extended_Warranty__c WHERE Name=:extendedDetail.contract_id LIMIT 1];
		    	if(extendList.size() == 0){//new extended warranty
		    		extendObj = new Extended_Warranty__c();
		    		extendObj.Name = extendedDetail.contract_id;
		    		extendObj.Current_Active_Asset__c = assetObj.Id;
		    		extendObj.Google_Order__c = orderObj.Id;
		    		extendObj.Expiration_Date__c = extendedDetail.warranty_expiration_date;
		    		if(extendedDetail.line_number!=null)
		    			extendObj.Line_Number__c = Decimal.valueOf(extendedDetail.line_number);

		    		insert extendObj;
		    	}else{
		    		throw new RestCreateExtendedWarranty.ExtendedWarrantyException(RestCreateExtendedWarranty.ERROR_DUPLICATE, extendList.get(0));
		    	}
		    	assetObj.Order_Opportunity__c = orderObj.Id;
		    	assetObj.Extended_Warranty__c = extendObj.Id;
		    	try{
		    		if(extendedDetail.asset_line_number!=null){
		    			assetObj.Line_Number__c = Decimal.valueOf(extendedDetail.asset_line_number);
		    		}
		    	}catch (Exception ex){
		    		System.debug('Error with transform :'+ex.getMessage());
		    	}
		    	update assetObj;
	    	
	        	extended_warranty_response.is_success=true;
	        }catch(RestCreateExtendedWarranty.ExtendedWarrantyException ew){
	        	extended_warranty_response.failure_reason=ew.getMessage();
	            extended_warranty_response.is_success=false;
	            RestResponseModel.extended_warranty_detail_t duplicateDetail = new RestResponseModel.extended_warranty_detail_t();

	            if(ew.extended.Google_Order__r!=null)
	            	duplicateDetail.order_id = ew.extended.Google_Order__r.Name;
	            if(ew.extended.Current_Active_Asset__r!=null)
	            	duplicateDetail.asset_id = ew.extended.Current_Active_Asset__r.Name;

				duplicateDetail.contract_id = ew.extended.Name;
				duplicateDetail.warranty_expiration_date = ew.extended.Expiration_Date__c;
				if(ew.extended.Line_Number__c!=null)
					duplicateDetail.line_number = String.valueOf(ew.extended.Line_Number__c);

	            extended_warranty_response.extended_warranty_detail = duplicateDetail;

				Database.rollBack(sp);
	        }catch(Exception e){
	        	extended_warranty_response.failure_reason=e.getMessage();
	            extended_warranty_response.is_success=false;
				Database.rollBack(sp);
	        }
	    }
    	return extended_warranty_response;
    }
    global class ExtendedWarrantyException extends Exception{
    	private Extended_Warranty__c extended;
    	global ExtendedWarrantyException(String message, Extended_Warranty__c extended){
    		this(message);
    		this.extended = extended;
    	}
    }
}