/** 
  * @author:		Cuauhtemoc Munoz
  * @date:			04/21/15	
  * @email: 		cmunoz@levementum.com
  * @description: 	Creates a model based on the RMA__c model in Salesforce
  */ 
global class RMAModel extends ObjectModel
{
	public id OwnerId {get; set;}// Reference field type in Salesforce
	public string Name {get; set;}
	public id RecordTypeId {get; set;}// Reference field type in Salesforce
	public date LastActivityDate {get; set;}
	public datetime LastViewedDate {get; set;}
	public datetime LastReferencedDate {get; set;}
	public String Status {get; set;}// PickList field type in Salesforce
	public string Replacement_Outbound_UPS {get; set;}
	public string Return_Shipping_Tracking_Number {get; set;}
	public String Notes {get; set;}// TextArea field type in Salesforce
	public String Type {get; set;}// PickList field type in Salesforce
	public String RMA_Category {get; set;}// PickList field type in Salesforce
	public id CaseObj {get; set;}// Reference field type in Salesforce
	public Decimal Amount_Refunded {get; set;}// Currency field type in Salesforce
	public id GoogleCustomer {get; set;}// Reference field type in Salesforce
	public id Google_Account {get; set;}// Reference field type in Salesforce
	public id Order {get; set;}// Reference field type in Salesforce
	public String RMA_Sub_Category {get; set;}// PickList field type in Salesforce
	public String Barcode_Type {get; set;}// PickList field type in Salesforce
	public String Barcode_Subtype {get; set;}// PickList field type in Salesforce
	public Decimal Amount_Authorized {get; set;}// Currency field type in Salesforce
	public Decimal Amount_Charged {get; set;}// Currency field type in Salesforce
	public String Return_Condition_Notes {get; set;}// TextArea field type in Salesforce
	public string RMA_ID_External_ID {get; set;}
	public String Condition {get; set;}// PickList field type in Salesforce
	public string external_RMA_num {get; set;}
	public String rma_country {get; set;}// PickList field type in Salesforce
	public boolean Customer_changed_from_DOA_to_Remorse {get; set;}
	public id GoogleAsset {get; set;}// Reference field type in Salesforce
	public string AssetType_Legacy {get; set;}
	public string IMEI {get; set;}
	public date LastDateBuyersRemorse {get; set;}
	public date LastDateDOA {get; set;}
	public date LastDateSpareParts {get; set;}
	public string RepairPartner {get; set;}
	public string SKUDescription {get; set;}
	public string SKU {get; set;}
	public string SerialNumber {get; set;}
	public string UniqueIDType {get; set;}
	public date WarrantyExpirationDate {get; set;}
	public id GPN {get; set;}// Reference field type in Salesforce
	public double Line_Number {get; set;}
	public string Operating_System_Code {get; set;}
	public String Order_Type {get; set;}// PickList field type in Salesforce
	public String Return_Code_Reason {get; set;}// PickList field type in Salesforce
	public string Return_Product_Instruction_Code {get; set;}
	public string Shipping_Service_Level_Code {get; set;}
	public String Warranty_Identifier {get; set;}// PickList field type in Salesforce
	public String Warranty_Provider {get; set;}// PickList field type in Salesforce
	public string DataMigrationKey {get; set;}
	public boolean CTDataMigration {get; set;}
	public datetime CreatedDateHold {get; set;}
	public string WarrantyCountries {get; set;}
	public id OpportunityObj {get; set;}// Reference field type in Salesforce
	public string PersonAcctEmailAddress {get; set;}
	public date Date_Received {get; set;}
	public String Customer_Induced_Damage {get; set;}// PickList field type in Salesforce
	public string CSSN {get; set;}
	public date VendorAssetReceivedDate {get; set;}
	public date VendorAssetReceivedNotificationDate {get; set;}
	public string Final_Asset_Disposition_Status {get; set;}
	public date ReturnConditionNotificationDate {get; set;}
	public string suggested_category {get; set;}
	public string MTR_ID {get; set;}
	public string RMA_Country_Formula {get; set;}
	public String ServiceModel {get; set;}// PickList field type in Salesforce
	public string Account_Street {get; set;}
	public string Account_City {get; set;}
	public string Account_State {get; set;}
	public string Account_Postal_Code {get; set;}
	public string Account_Country {get; set;}
	public string Incomplete {get; set;}
	public String Warranty_Repair_Status {get; set;}// PickList field type in Salesforce
	public string Google_Asset_Unique_ID {get; set;}
	public date Shipment_Date {get; set;}
	public String Triage_Code {get; set;}// PickList field type in Salesforce
	public string Device_Condition {get; set;}
	public string Google_Order_ID {get; set;}
	public string Customer_Name {get; set;}
	public date Needs_Shipping_Label {get; set;}
	public String Failure_Action_Code {get; set;}// PickList field type in Salesforce
	public String Failure_Modified_Code {get; set;}// Multipicklist field type in Salesforce
	public String Failure_Free_Form_Notes {get; set;}// TextArea field type in Salesforce
	public String Failure_Observed_Condition_Code {get; set;}// Multipicklist field type in Salesforce
	public String Condition_Code {get; set;}// PickList field type in Salesforce
	public boolean Created_by_logged_in_user {get; set;}
	public string Replacement_Cart_ID {get; set;}
	public string Item_Description {get; set;}
	public string Repair_Partner_2 {get; set;}
	public String RMA_Type {get; set;}// PickList field type in Salesforce
	public datetime RMA_Category_Change_Date {get; set;}
	public string Previous_RMA_Category {get; set;}
	public String RMA_Auditor_Notes {get; set;}// TextArea field type in Salesforce
	public boolean RMA_Audit {get; set;}
	public boolean RMA_Needs_Review {get; set;}
	public String Router_Brand {get; set;}// PickList field type in Salesforce
	public string Router_Model {get; set;}
	public boolean Ready_to_Submit {get; set;}
	public boolean CustomerInducedDamage_Legacy {get; set;}
	public boolean Query_Status_Processed {get; set;}
	public boolean Created_on_Google_Server {get; set;}
	public string Truncated_Sub_Category {get; set;}
	public String why_other {get; set;}// TextArea field type in Salesforce
	public boolean acknowledge_review {get; set;}
	public string Google_Order_Type_Display {get; set;}
	public string Upserted_Status {get; set;}
	public date Ready_to_send {get; set;}
	public string gCases_ID {get; set;}
	public String Upserted_Triage_Code {get; set;}// PickList field type in Salesforce
	public String Upserted_Customer_Induced_Damage {get; set;}// PickList field type in Salesforce
	public string Customer_Induced_Damage_Formula {get; set;}
	public double Google_Asset_Line_Number {get; set;}
	public string GoogleOrderStateProvince {get; set;}
	public string GoogleOrderType {get; set;}
	public string GoogleOrderCountry {get; set;}
	public boolean ensured_N7 {get; set;}
	public double Time_between_created_and_last_modified {get; set;}
	public double in_warranty {get; set;}
	public String RMAReceivedReason {get; set;}// PickList field type in Salesforce
	public String RMAReceivedReasonNotes {get; set;}// TextArea field type in Salesforce
	public id MarkedReceivedBy {get; set;}// Reference field type in Salesforce
	public string Reason_Code {get; set;}
	public string Asset_Sales_Channel {get; set;}
	public boolean SendtoBizApps {get; set;}
	public string TV_Brand {get; set;}
	public string TV_Model {get; set;}
	public string AVR_Brand_Model {get; set;}
	public string Asset_Type {get; set;}
	public string ASUS_Problem_Code {get; set;}
	public String Exception_Notes {get; set;}// TextArea field type in Salesforce
	public String Exception_Reason {get; set;}// PickList field type in Salesforce
	public String Exception_Type {get; set;}// PickList field type in Salesforce
	public boolean I_understand_Exception_RMAs_are_reviewed {get; set;}
	public string LG_Reason_Code {get; set;}
	public string Manufacturer {get; set;}
	public string RMA_Country_Workflow {get; set;}
	public String CloseRMAReceivedReason {get; set;}// PickList field type in Salesforce
	public string CreatedByLDAP {get; set;}
	public string CreatedByLocation {get; set;}
	public String MarkRMAReceivedReason {get; set;}// PickList field type in Salesforce
	public id ReplacementOrder {get; set;}// Reference field type in Salesforce
	public id ManuallyClosedBy {get; set;}// Reference field type in Salesforce
	public id ManuallyReceivedBy {get; set;}// Reference field type in Salesforce
	public date RMA_In_Transit_Date {get; set;}
	public date RMA_Delivered_Date {get; set;}
	public string GlassRmaNumber {get; set;}
	public string GlassRmaId {get; set;}
	public boolean GlassError {get; set;}
	public string GlassErrorMessage {get; set;}
	public String Primary_CID_Exception_Reason {get; set;}// PickList field type in Salesforce
	public String RMA_Action {get; set;}// PickList field type in Salesforce
}