public with sharing class CancelExtendedWarranty {
	public Extended_Warranty__c extendedWarranty{get;set;}
	public Refund__c refundObj{get;set;}
	public CancelExtendedWarranty(ApexPages.StandardController con) {
		extendedWarranty = [SELECT Id, Line_Number__c, Google_Order__c FROM Extended_Warranty__c WHERE Id = :con.getId()];
		refundObj = new Refund__c();
	}

	public PageReference cancelWarranty() {
		refundObj.RefundReason__c = 'Extended Warranty Cancellation'; 
		refundObj.RefundStatus__c = 'Pending'; 
		if (extendedWarranty.Line_Number__c != null)
		refundObj.LineNumber__c = String.valueOf(extendedWarranty.Line_Number__c); 
		refundObj.OrderNumber__c = extendedWarranty.Google_Order__c; 
		refundObj.ExtendedWarranty__c = extendedWarranty.Id;
		refundObj.ExpectedRefundDate__c = System.today() + 1;
		refundObj.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Line-Item Refund').getRecordTypeId();
		refundObj.PercentItemRefund__c = 100;
		refundObj.PercentTaxRefund__c = 100;

		insert refundObj;

		return new PageReference('/' + extendedWarranty.Id);
	}
}