public interface RestServiceModel 
{
	ObjectModel updateObject(ObjectModel obj);
	ObjectModel getObject();
	void deleteObject();
	ObjectModel updateModel(sObject udpatedsObject);
}