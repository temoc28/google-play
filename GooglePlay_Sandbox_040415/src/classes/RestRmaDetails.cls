@RestResource(urlMapping='/RmaDetails/*')
global class RestRmaDetails
{
    //Description: It gets all the information needed (PersonAccount, RMA, Opportunity, Google_Asset__c) based on the email sent in to build the response object.
    //The method returns the response as a list RestResponseModel.rma_details_t. Gets RMA details.
    @HttpGet
    global static List<RestResponseModel.rma_details_t> getRmaDetails()
    {
        List<RestResponseModel.rma_details_t> rmas = new List<RestResponseModel.rma_details_t>();
        try
        {
            Map<String,String> paramMap = RestContext.request.params;
            String orderNumber = paramMap.get('orderNumber');
            String assetId = paramMap.get('assetId');
            String rmaId = paramMap.get('rmaId');
            String email = paramMap.get('email');
            //add all the Ids to this set so we can query and get all the field history for all the RMAs
            Set<String> rmaIdSet = new Set<String>();           
            Set<String> docIdSet = new Set<String>();
            Set<String> rmaCountryWorkflowSet = new Set<String>();
            Map<String,String> rmaToReplacementOrderMap = new Map<String,String>();
            Set<String> aRmaIdSet = new Set<String>();
            Set<String> aOppIdSet = new Set<String>();
            Set<String> aGaIdSet = new Set<String>();
            if(String.isNotEmpty(email))
            {
                for(Account a : [Select Id,(Select Id From RMA__r),(Select Id From Opportunities),(Select Id From Google_Assets__r) From Account WHERE PersonEmail=:email])
                {
                    for(RMA__c rma : a.RMA__r)
                        aRmaIdSet.add(rma.Id);
                    for(Opportunity opp : a.Opportunities)
                        aOppIdSet.add(opp.Id);
                    for(Google_Asset__c ga : a.Google_Assets__r)
                        aGaIdSet.add(ga.Id);
                }
            }
            if(String.isNotEmpty(orderNumber) || !aOppIdSet.isEmpty())
            {
                for(Opportunity opp : [SELECT Id,(SELECT Id,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,ReplacementOrder__c FROM RMA__r) FROM Opportunity WHERE Name=:orderNumber OR Id IN :aOppIdSet])
                {
                    for(RMA__c rma : opp.RMA__r)
                    {
                        rmaIdSet.add(rma.Id);
                        if(String.isNotEmpty(rma.GoogleAsset__r.ProductId__r.Document__c))
                            docIdSet.add(rma.GoogleAsset__r.ProductId__r.Document__c);
                        if(String.isNotEmpty(rma.RMA_Country_Workflow__c))
                            rmaCountryWorkflowSet.add(rma.RMA_Country_Workflow__c);
                        if(String.isNotEmpty(rma.ReplacementOrder__c))
                            rmaToReplacementOrderMap.put(rma.Id,rma.ReplacementOrder__c);
                    }
                }
            }
            if(String.isNotEmpty(assetId)|| !aGaIdSet.isEmpty())
            {
                for(Google_Asset__c ga : [SELECT Id,SKU_Description__c,(SELECT Id,SKUDescription__c,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,ReplacementOrder__c FROM RMA__r) FROM Google_Asset__c WHERE Name=:assetId OR Id IN :aGaIdSet])
                {
                    for(RMA__c rma : ga.RMA__r)
                    {
                        rmaIdSet.add(rma.Id);
                        if(String.isNotEmpty(rma.GoogleAsset__r.ProductId__r.Document__c))
                            docIdSet.add(rma.GoogleAsset__r.ProductId__r.Document__c);
                        if(String.isNotEmpty(rma.RMA_Country_Workflow__c))
                            rmaCountryWorkflowSet.add(rma.RMA_Country_Workflow__c);
                        if(String.isNotEmpty(rma.ReplacementOrder__c))
                            rmaToReplacementOrderMap.put(rma.Id,rma.ReplacementOrder__c);
                    }
                }
            }
            if(String.isNotEmpty(rmaId)|| !aRmaIdSet.isEmpty())
            {
                for(RMA__c rma : [SELECT Id,SKUDescription__c,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,ReplacementOrder__c,Exception_Type__c,Exception_Reason__c,Exception_notes__c,I_understand_exception_RMAS_are_reviewed__c FROM RMA__c WHERE Name=:rmaId OR Id IN :aRmaIdSet])
                {
                    rmaIdSet.add(rma.Id);
                    if(String.isNotEmpty(rma.GoogleAsset__r.ProductId__r.Document__c))
                        docIdSet.add(rma.GoogleAsset__r.ProductId__r.Document__c);
                    if(String.isNotEmpty(rma.RMA_Country_Workflow__c))
                        rmaCountryWorkflowSet.add(rma.RMA_Country_Workflow__c);
                    if(String.isNotEmpty(rma.ReplacementOrder__c))
                        rmaToReplacementOrderMap.put(rma.Id,rma.ReplacementOrder__c);
                }
            }
            if(!rmaIdSet.isEmpty())
            {
                RestResponseModel.rma_details_t rma_details;
                /*Map<String,Country_Variant__c> cvMap = new Map<String,Country_Variant__c>();
                Set<String> rcIdSet = new Set<String>();
                Map<String,List<Shipping_Label_Method_Rule__c>> rcToRuleMap = new Map<String,List<Shipping_Label_Method_Rule__c>>();
                for(Country_Variant__c cv : [SELECT Id,Name,Requires_Shipping_Label__c,Buyer_s_Remorse_Article_ID__c,DOA_Article_ID__c,DOA_Replacement_Doc_ID__c,DOA_Replacement_GPN__c,Warranty_Article_ID__c,Warranty_Replacement_Doc_ID__c,Warranty_Replacement_GPN__c,Warranty_Refund_Article_ID__c,AR_Method__c,Custom_Return_Center__r.Address_Line_1__c,Custom_Return_Center__c,Custom_Return_Center__r.Address_Line_2__c,Custom_Return_Center__r.Address_Line_3__c,Custom_Return_Center__r.City__c,Custom_Return_Center__r.State__c,Custom_Return_Center__r.Postal_Code__c,Custom_Return_Center__r.Country__c,Custom_Return_Center__r.Shipping_Label_Service_Code__c,Custom_Return_Center__r.Name,Document__c,Country__c,Country__r.Return_Center__c,Country__r.Return_Center__r.Address_Line_1__c,Country__r.Return_Center__r.Address_Line_2__c,Country__r.Return_Center__r.Address_Line_3__c,Country__r.Return_Center__r.City__c,Country__r.Return_Center__r.State__c,Country__r.Return_Center__r.Country__c,Country__r.Return_Center__r.Postal_Code__c,Return_Center_ID__c,Return_Center_Name__c FROM Country_Variant__c WHERE Name in :rmaCountryWorkflowSet AND Document__c=:docIdSet])
                {
                    cvMap.put(cv.Name+cv.Document__c,cv);
                    rcIdSet.add(cv.Custom_Return_Center__c);
                }
                for(Shipping_Label_Method_Rule__c sRule : [SELECT Id,Return_Center__c,Shipping_Label_Method__c, RMA_Method__c, Action__c FROM Shipping_Label_Method_Rule__c WHERE Return_Center__c IN :rcIdSet])
                {
                    if(!rcToRuleMap.containsKey(sRule.Return_Center__c))
                        rcToRuleMap.put(sRule.Return_Center__c,new List<Shipping_Label_Method_Rule__c>());
                    rcToRuleMap.get(sRule.Return_Center__c).add(sRule);
                }*/
                Map<String,Opportunity> oppMap = new Map<String,Opportunity>([SELECT Id,(SELECT Id,Serial__c,Name,Manufacture_Date__c,RepairPartner__c,ProductSKUDescription__c,ProductSKU__c,ProductID__r.Name FROM Google_Assets__r) FROM Opportunity WHERE Id in :rmaToReplacementOrderMap.values()]);

                RmaDetailsBuilder rmaDetailsBuilder = new RmaDetailsBuilder()
                        .setCV(rmaCountryWorkflowSet,docIdSet);

                for(RMA__c rma : [SELECT RCL_Verify_ID__c,Upserted_Status__c,Id,ReplacementOrder__c,Opportunity__r.Name,Type__c,GoogleAsset__r.Serial__c,GoogleAsset__r.Name,Name,CreatedDate,CreatedBy.Name,CreatedBy.Email,RMA_Category__c,RMA_Sub_Category__c,RMA_Type__c,Status__c,Extended_Warranty_Claim_ID__c,
                                  Return_Shipping_Tracking_Number__c,GoogleCustomer__r.PersonEmail,GoogleCustomer__r.FirstName,GoogleCustomer__r.LastName,GoogleCustomer__r.PersonMailingStreet,Notes__c,RMA_Action__c,
                                  gCases_ID__c,GoogleAsset__r.SKU_Description__c,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,GoogleAsset__r.Carrier__c,GoogleAsset__r.Tracking_URL__c,GoogleAsset__r.RepairPartner__c,GoogleAsset__r.ProductSKUDescription__c,GoogleAsset__r.ProductSKU__c,GoogleAsset__r.ProductID__r.Name,
                                  GoogleCustomer__r.PersonMailingCity,GoogleCustomer__r.PersonMailingState,GoogleCustomer__r.PersonMailingCountry,GoogleCustomer__r.PersonMailingPostalCode,GoogleAsset__r.Manufacture_Date__c,
                                  Exception_Type__c,Exception_Reason__c,Exception_notes__c,I_understand_exception_RMAS_are_reviewed__c,Extended_Warranty_Contract_ID_Backup__c,Replacement_Order_Status__c, ReplacementOrder__r.Name, GoogleAsset__r.SKU__c,
                                  (Select CreatedBy.Name, CreatedDate, Field, OldValue, NewValue From Histories),
                                  (Select Currency__c, ExpectedRefundDate__c, ItemRefundAmount__c, RefundStatus__c, TaxRefundAmount__c, Total_Refund__c From Refunds__r)
                                  FROM RMA__c WHERE Id in :rmaIdSet AND GoogleAsset__c!=''])
                {

                    rma_details = new RestResponseModel.rma_details_t();
                    rma_details.replacement_status = rma.Replacement_Order_Status__c;
                    rma_details.email_id=rma.GoogleCustomer__r.PersonEmail;
                    rma_details.unique_id=rma.GoogleAsset__r.Name;
                    rma_details.rma_number=rma.Name;
                    if(rma.Upserted_Status__c == null){
                        rma_details.return_status = 'Pending';
                    }
                    else
                        rma_details.return_status = rma.Upserted_Status__c;
                    rma_details.order_id=rma.Opportunity__r.Name;
                    rma_details.rma_initiation_details = new RestResponseModel.rma_initiation_details_t();
                    rma_details.rma_initiation_details.unique_id=rma.GoogleAsset__r.Name;
                    rma_details.rma_initiation_details.sku = rma.GoogleAsset__r.SKU__c;
                    rma_details.rma_initiation_details.rma_requester_email=rma.GoogleCustomer__r.PersonEmail;
                    rma_details.rma_initiation_details.rma_requester_first_name=rma.GoogleCustomer__r.FirstName;
                    rma_details.rma_initiation_details.rma_requester_last_name=rma.GoogleCustomer__r.LastName;
                    RestResponseModel.address_t rma_requester_address = new RestResponseModel.address_t();
                    rma_requester_address.address_street=rma.GoogleCustomer__r.PersonMailingStreet;
                    rma_requester_address.address_city=rma.GoogleCustomer__r.PersonMailingCity;
                    rma_requester_address.address_state_province=rma.GoogleCustomer__r.PersonMailingState;
                    rma_requester_address.address_country=rma.GoogleCustomer__r.PersonMailingCountry;
                    rma_requester_address.address_postal_code=rma.GoogleCustomer__r.PersonMailingPostalCode;
                    rma_details.rma_initiation_details.rma_requester_address=rma_requester_address;
                    rma_details.rma_initiation_details.rma_type=rma.Type__c;
                    rma_details.rma_initiation_details.rma_category=rma.RMA_Category__c;
                    rma_details.rma_initiation_details.rma_sub_category=rma.RMA_Sub_Category__c;
                    rma_details.rma_initiation_details.rma_notes=rma.Notes__c;
                    rma_details.rma_initiation_details.rma_action=rma.RMA_Action__c;
                    rma_details.rma_initiation_details.case_id=rma.gCases_ID__c;
                    rma_details.rma_initiation_details.rma_creation_date=rma.CreatedDate.date();
                    rma_details.rma_initiation_details.rma_creator_name=rma.CreatedBy.Name;
                    rma_details.rma_initiation_details.rma_creator_email=rma.CreatedBy.Email;
                    //rma_details.rma_initiation_details.rma_creator_site='';
                    rma_details.rma_initiation_details.warranty_exceptions='';
                    rma_details.rma_initiation_details.rma_override='';
                    rma_details.rma_initiation_details.rma_overrider_email='';
                    rma_details.rma_initiation_details.extended_warranty_claim_id=rma.Extended_Warranty_Claim_ID__c;

                    rma_details.rma_success_details = rmaDetailsBuilder.setRMA(rma)
                            .stampParameter()
                            .getRMASuccessDetail();
                    rma_details.rma_success_details.extended_warranty_contract_id = rma.Extended_Warranty_Contract_ID_Backup__c;
                    /*rma_details.rma_success_details = new RestResponseModel.rma_success_details_t();
                    rma_details.rma_success_details.rma_id=rma.Name;
                    rma_details.rma_success_details.shipping_destination_address=new RestResponseModel.address_t();
                    if(cvMap.containsKey(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c))
                    {
                        Country_Variant__c cv = cvMap.get(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c);
                        if(cv.Custom_Return_Center__c != null){
                            rma_details.rma_success_details.shipping_destination_address.address_street=cv.Custom_Return_Center__r.Address_Line_1__c;
                            rma_details.rma_success_details.shipping_destination_address.address_street_2=cv.Custom_Return_Center__r.Address_Line_2__c;
                            rma_details.rma_success_details.shipping_destination_address.address_street_3=cv.Custom_Return_Center__r.Address_Line_3__c;
                            rma_details.rma_success_details.shipping_destination_address.address_city=cv.Custom_Return_Center__r.City__c;
                            rma_details.rma_success_details.shipping_destination_address.address_state_province=cv.Custom_Return_Center__r.State__c;
                            rma_details.rma_success_details.shipping_destination_address.address_country=cv.Custom_Return_Center__r.Country__c;
                            rma_details.rma_success_details.shipping_destination_address.address_postal_code=cv.Custom_Return_Center__r.Postal_Code__c;
                            rma_details.rma_success_details.shipping_center_id=cv.Custom_Return_Center__r.Shipping_Label_Service_Code__c;
                            rma_details.rma_success_details.shipping_center_name=cv.Custom_Return_Center__r.Name;

                            if(cv.Requires_Shipping_Label__c && rcToRuleMap.containsKey(cv.Custom_Return_Center__c))
                            {
                                for(Shipping_Label_Method_Rule__c slRule : rcToRuleMap.get(cv.Custom_Return_Center__c))
                                {
                                    if(rma.RMA_Action__c == 'ADVANCED_EXCHANGE')
                                    {
                                        if(slRule.RMA_Method__c==cv.AR_Method__c && slRule.Action__c==rma.RMA_Action__c)
                                            rma_details.rma_success_details.shipping_method=slRule.Shipping_Label_Method__c;
                                    }
                                    else
                                    {
                                        if(slRule.Action__c==rma.RMA_Action__c)
                                            rma_details.rma_success_details.shipping_method=slRule.Shipping_Label_Method__c;
                                    }
                                }
                            }
                        }else{
                            rma_details.rma_success_details.shipping_destination_address.address_street=cv.Country__r.Return_Center__r.Address_Line_1__c;
                            rma_details.rma_success_details.shipping_destination_address.address_street_2=cv.Country__r.Return_Center__r.Address_Line_2__c;
                            rma_details.rma_success_details.shipping_destination_address.address_street_3=cv.Country__r.Return_Center__r.Address_Line_3__c;
                            rma_details.rma_success_details.shipping_destination_address.address_city=cv.Country__r.Return_Center__r.City__c;
                            rma_details.rma_success_details.shipping_destination_address.address_state_province=cv.Country__r.Return_Center__r.State__c;
                            rma_details.rma_success_details.shipping_destination_address.address_country=cv.Country__r.Return_Center__r.Country__c;
                            rma_details.rma_success_details.shipping_destination_address.address_postal_code=cv.Country__r.Return_Center__r.Postal_Code__c;
                            rma_details.rma_success_details.shipping_center_id=cv.Return_Center_ID__c;
                            rma_details.rma_success_details.shipping_center_name=cv.Return_Center_Name__c;

                            if(cv.Requires_Shipping_Label__c && rcToRuleMap.containsKey(cv.Country__r.Return_Center__c))
                            {
                                for(Shipping_Label_Method_Rule__c slRule : rcToRuleMap.get(cv.Country__r.Return_Center__c))
                                {
                                    if(rma.RMA_Action__c == 'ADVANCED_EXCHANGE')
                                    {
                                        if(slRule.RMA_Method__c==cv.AR_Method__c && slRule.Action__c==rma.RMA_Action__c)
                                            rma_details.rma_success_details.shipping_method=slRule.Shipping_Label_Method__c;
                                    }
                                    else
                                    {
                                        if(slRule.Action__c==rma.RMA_Action__c)
                                            rma_details.rma_success_details.shipping_method=slRule.Shipping_Label_Method__c;
                                    }
                                }
                            }
                        }
                        rma_details.rma_success_details.shipping_label_needed=cv.Requires_Shipping_Label__c;
                        if(rma.Type__c=='Buyer\'s Remorse'){
                            rma_details.rma_success_details.canned_response=cv.Buyer_s_Remorse_Article_ID__c;
                        }
                        else if(rma.Type__c=='Warranty DOA'){
                            rma_details.rma_success_details.canned_response=cv.DOA_Article_ID__c;
                            rma_details.rma_success_details.exchange_gpn=cv.DOA_Replacement_GPN__c;
                            rma_details.rma_success_details.exchange_doc_id = cv.DOA_Replacement_Doc_ID__c;
                        }
                        else if(rma.Type__c=='Warranty Regular'){
                            rma_details.rma_success_details.canned_response=cv.Warranty_Article_ID__c;
                            rma_details.rma_success_details.exchange_gpn=cv.Warranty_Replacement_GPN__c;
                            rma_details.rma_success_details.exchange_doc_id = cv.Warranty_Replacement_Doc_ID__c;
                        }
                        else if(rma.Type__c=='Warranty Refund'){
                            rma_details.rma_success_details.canned_response=cv.Warranty_Refund_Article_ID__c;
                        }
                        rma_details.rma_success_details.ar_method=cv.AR_Method__c;
                    }*/

                    rma_details.rma_state=rma.Status__c;
                    rma_details.rma_tracking_number=rma.GoogleAsset__r.Tracking_URL__c;
                    rma_details.rma_carrier=rma.GoogleAsset__r.Carrier__c;
                    rma_details.sku_description = rma.GoogleAsset__r.ProductSkuDescription__c;
                    if(String.isNotEmpty(rma.ReplacementOrder__c) && oppMap.containsKey(rma.ReplacementOrder__c))
                    {
                        Opportunity replacementOrder = oppMap.get(rma.ReplacementOrder__c);
                        if(replacementOrder.Google_Assets__r!=null && replacementOrder.Google_Assets__r.size()==1)
                        {
                            Google_Asset__c replacementAsset = replacementOrder.Google_Assets__r.get(0);
                            rma_details.replacement_order=new RestResponseModel.device_details_t();
                            rma_details.replacement_order.serial_number=replacementAsset.Serial__c;
                            rma_details.replacement_order.unique_id=replacementAsset.Name;
                            rma_details.replacement_order.original_gpn_sku=new RestResponseModel.gpn_sku_t();
                            rma_details.replacement_order.original_gpn_sku.gpn_sku=replacementAsset.ProductSku__c;
                            rma_details.replacement_order.original_gpn_sku.gpn_sku_description=replacementAsset.ProductSkuDescription__c;
                            rma_details.replacement_order.substitute_gpns=null;
                            rma_details.replacement_order.manufacture_date=replacementAsset.Manufacture_Date__c;
                            rma_details.replacement_order.repair_partner=replacementAsset.RepairPartner__c;
                            rma_details.replacement_order.components=null;
                            rma_details.replacement_order.device_users=null;
                            rma_details.replacement_order.activation_date=null;
                            rma_details.replacement_order.device_warranty_details=null;
                            rma_details.replacement_order.device_description=replacementAsset.ProductSKUDescription__c;
                            rma_details.replacement_order.device_sku_description=replacementAsset.ProductID__r.Name;
                            rma_details.replacement_order.order_id = rma.ReplacementOrder__r.Name;
                        }
                    
                    }
                    else if((rma.ReplacementOrder__c == null || !oppMap.containsKey(rma.ReplacementOrder__c) || oppMap.get(rma.replacementOrder__c).Google_Assets__r==null || oppMap.get(rma.replacementOrder__c).Google_Assets__r.size()==0) && rma.RCL_Verify_ID__c != null){
                        rma_details.replacement_order=new RestResponseModel.device_details_t();
                        rma_details.replacement_order.order_id = rma.RCL_Verify_ID__c;
                        rma_details.replacement_order.replacement_status = rma.Replacement_Order_Status__c;
                    }
                    //per Jordan - only one refund is possible
                    for(Refund__c ref : rma.Refunds__r)
                    {
                        rma_details.refund_charge_status=ref.RefundStatus__c;
                        rma_details.currency_name=ref.Currency__c;
                        rma_details.item_refund=String.valueOf(ref.ItemRefundAmount__c);
                        rma_details.tax_refund=String.valueOf(ref.TaxRefundAmount__c);
                        rma_details.total_refund=String.valueOf(ref.Total_Refund__c);
                        rma_details.expected_refund_date=ref.ExpectedRefundDate__c;
                    }
                    for(RMA__History history : rma.Histories)
                    {
                        RestResponseModel.rma_events_t rmaEvent = new RestResponseModel.rma_events_t();
                        rmaEvent.rma_event_name=history.Field;
                        rmaEvent.rma_past_value=String.valueOf(history.OldValue);
                        rmaEvent.rma_current_value=String.valueOf(history.NewValue);
                        rmaEvent.rma_event_change_date=history.CreatedDate.date();
                        rmaEvent.rma_event_changer=history.CreatedBy.Name;
                        rma_details.rma_events.add(rmaEvent);
                    }
                    rmas.add(rma_details);
                }
            }
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return rmas;
    }
}