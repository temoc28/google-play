global class RestResponseModel 
{           
    public static List<warranty_exception_t> wExceptions
    {
        get
        {
            if(wExceptions==null)
            {
                wExceptions=new List<warranty_exception_t>();
                Schema.DescribeFieldResult F = RMA__c.Exception_Type__c.getDescribe();
                List<Schema.PicklistEntry> P = F.getPicklistValues();
                warranty_exception_t we;
                for(Schema.PicklistEntry pe : p)
                {
                    we = new warranty_exception_t();
                    we.warranty_exception=pe.getValue();
                    wExceptions.add(we);
                }
            }
            return wExceptions;
        }
        set;
    }
    global class customer_records_t
    {
        public String email {get;set;}
        public String cid_exception_granted_datetime {get;set;}
        public String cid_exception_granted_asset {get;set;}
        public String cid_exception_on_this_asset {get;set;}
        public String cid_exception_granted {get;set;}
        public String cid_exception_granted_asset_description {get;set;}
        public String cid_exception_granted_asset_name {get;set;}
        
        public customer_records_t(String email)
        {
            this.email=email;
        }
        public List<device_details_t> device_details 
        {
            get
            {
                if(device_details==null)
                    device_details = new List<device_details_t>();
                return device_details;
            }
            set;
        }       
    }
    global class device_details_t
    {
        public String serial_number {get;set;}
        public String unique_id {get;set;}
        //public String what_is_unique_id {get;set;}
        public String device_description {get;set;}
        public String device_sku_description {get;set;}
        public String device_type {get;set;}
        public String replacement_device_description {get; set;}
        public gpn_sku_t original_gpn_sku {get;set;}
        public Date manufacture_date {get;set;}
        public String repair_partner {get;set;}
        public List<String> device_users {get;set;}
        public Date activation_date {get;set;}
        public country_variant_details_t country_variant_details {get;set;}  
        public device_warranty_details_t device_warranty_details {get;set;}     
        public device_purchase_details_t device_purchase_details {get;set;}
        public String replacement_status {get;set;}
        public String order_id {get;set;}
        public List<device_component_details_t> components 
        {
            get
            {
                if(components==null)
                    components=new List<device_component_details_t>();
                return components;
            }
            set;
        }
        public List<gpn_sku_t> substitute_gpns 
        {
            get
            {
                if(substitute_gpns==null)
                    substitute_gpns=new List<gpn_sku_t>();
                return substitute_gpns;
            }
            set;
        }
        public String device_return_reason_code_set {get;set;}
        /*public List<device_purchase_details_t> device_purchase_details 
        {
            get
            {
                if(device_purchase_details==null)
                    device_purchase_details = new List<device_purchase_details_t>();
                return device_purchase_details;
            }
            set;
        }       
        public List<rma_details_t> rma_details 
        {
            get
            {
                if(rma_details==null)
                    rma_details = new List<rma_details_t>();
                return rma_details;
            }
            set;
        }*/
    }
    global class rma_details_t
    {
        public String email_id {get;set;}
        public String unique_id {get;set;}
        public String rma_number {get;set;}
        public String order_id {get;set;}
        public rma_initiation_details_t rma_initiation_details {get;set;}
        public rma_success_details_t rma_success_details {get;set;}
        public String rma_state {get;set;}
        public String rma_tracking_number {get;set;}
        public String rma_carrier {get;set;}
        public String refund_charge_status {get;set;}
        public String currency_name {get;set;}
        public String item_refund {get;set;}
        public String tax_refund {get;set;}
        public String total_refund {get;set;}
        public Date expected_refund_date {get;set;}
        public device_details_t replacement_order {get;set;}
        public String replacement_status {get;set;}
        public String return_status {get;set;}
        public String sku_description {get; set;}
        public List<rma_events_t> rma_events 
        {
            get
            {
                if(rma_events==null)
                    rma_events=new List<rma_events_t>();
                return rma_events;
            }
            set;
        }
    }
    /*
    global class rma_t
    {
        public String rma_number {get;set;}
        public Date rma_creation_date {get;set;}
        public String rma_creator_name {get;set;}
        public String rma_creator_email {get;set;}
        public String rma_creator_site {get;set;}
        public String rma_category {get;set;}
        public String rma_subcategory {get;set;}
        public String rma_type {get;set;}
        public String rma_state {get;set;}
        public String rma_tracking_number {get;set;}
        public String rma_carrier {get;set;}
        public String refund_charge_status {get;set;}
        public String currency_name {get;set;}
        public String item_refund {get;set;}
        public String tax_refund {get;set;}
        public String total_refund {get;set;}
        public Date expected_refund_date {get;set;}
        public rma_initiation_details_t rma_request {get;set;}
        public device_details_t replacement_order {get;set;}
        public List<rma_events_t> rma_events 
        {
            get
            {
                if(rma_events==null)
                    rma_events=new List<rma_events_t>();
                return rma_events;
            }
            set;
        }
    }
    */
    global class warranty_model_t
    {       
        public String warranty_model {get;set;}
        public List<String> rma_action_list {get;set;}
    }
    global class warranty_exception_t
    {
        public String warranty_exception {get;set;}
    }
    global class device_warranty_details_t
    {
        public Date buyers_remorse_end_date {get;set;}
        public String buyers_remorse_days_left {get;set;}
        public Date doa_end_date {get;set;}
        public String doa_days_left {get;set;}
        public Date warranty_end_date {get;set;}
        public String warranty_days_left {get;set;}     
        public Date extended_warranty_end_date {get;set;}
        public String extended_warranty_days_left {get;set;}
        public String extended_warranty_contract_id {get;set;}
        public List<String> warranty_countries 
        {
            get
            {
                if(warranty_countries==null)
                    warranty_countries=new List<String>();
                return warranty_countries;
            }
            set;
        }
        //public String warranty_models {get;set;}      
        public List<warranty_model_t> warranty_models {get;set;}
        public List<warranty_exception_t> warranty_exceptions {get;set;}
    }
    global class device_purchase_details_t
    {
        public String original_purchaser {get;set;}
        public Date purchase_date {get;set;}
        public String purchase_country {get;set;}
        public String point_of_purchase {get;set;}
        public String google_order_id {get;set;}
        public Date shipment_date {get;set;}
        public String shipping_tracking_number {get;set;}
        public String shipping_carrier {get;set;}
        public String purchase_type {get;set;}
    }
    global class gpn_sku_t
    {
        public String gpn_sku {get;set;}
        public String gpn_sku_description {get;set;}
    }
    global class device_component_details_t
    {
        
    }
    global class device_support_reasons_t
    {
        public String rma_category {get;set;}
        public String rma_sub_category {get;set;}
    }
    global class device_rma_reasons_t
    {
        public String unique_id {get;set;}
        public List<rma_type_with_reasons_t> rma_type_with_reasons
        {   
            get
            {
                if(rma_type_with_reasons==null)
                    rma_type_with_reasons = new List<rma_type_with_reasons_t>();
                return rma_type_with_reasons;
            }
            set;
        }
    }
    /*global class device_rma_t
    {
        public String unique_id {get;set;}
        public List<rma_type_with_reasons_t> rma_type_with_reasons 
        {
            get
            {
                if(rma_type_with_reasons==null)
                    rma_type_with_reasons=new List<rma_type_with_reasons_t>();
                return rma_type_with_reasons;
            }
            set;
        }
    }*/
    global class rma_type_with_reasons_t
    {
        public String rma_type {get;set;}
        public List<rma_reasons_t> rma_reason 
        {
            get
            {
                if(rma_reason==null)
                    rma_reason=new List<rma_reasons_t>();
                return rma_reason;
            }
            set;
        }
    }

    // dlu@google.com restored version:
    //global class rma_reasons_t
    //{
    //    public String rma_category {get;set;}
    //    public List<String> rma_sub_category 
    //    {
    //        get
    //        {
    //            if(rma_sub_category==null)  rma_sub_category=new List<String>();
    //            return rma_sub_category;
    //        }
    //        set;
    //    }
    //}

// dlu@google.com original version:
    global class rma_reasons_t
    {
        public String rma_category {get;set;}
        //public List<rma_return_reason_code_t> rma_sub_category 
        public List<String> rma_sub_category 
        {
            get
            {
                //if(rma_sub_category==null)  rma_sub_category=new List<rma_return_reason_code_t>();
                if(rma_sub_category==null)  rma_sub_category=new List<String>();
                return rma_sub_category;
            }
            set;
        }
    }

    global class rma_return_reason_code_t
    {
        public String Name {get; set; }
        public Boolean CID {get; set;}
    }

    global class rma_initiation_details_t
    {
        public String unique_id {get;set;}
        public String rma_requester_email {get;set;}
        public String rma_requester_first_name {get;set;}
        public String rma_requester_last_name {get;set;}
        /*public String rma_requester_address_street {get;set;}
        public String rma_requester_address_city {get;set;}
        public String rma_requester_address_state_province {get;set;}
        public String rma_requester_address_country {get;set;}
        public String rma_requester_address_postal_code {get;set;}*/
        public String rma_type {get;set;}
        public String rma_category {get;set;}
        public String rma_sub_category {get;set;}
        public String rma_notes {get;set;}
        public String case_id {get;set;}
        public String rma_creator {get;set;}
        public String warranty_exceptions {get;set;}
        public String rma_override {get;set;}
        public String rma_overrider_email {get;set;}
        public String google_order_id {get;set;}
        public address_t rma_requester_address {get;set;}
        public String rma_action {get;set;}
        public String rma_creator_name {get;set;}
        public Date rma_creation_date {get;set;}
        public String rma_creator_email {get;set;}
        public String rma_creator_site {get;set;}
        public String extended_warranty_claim_id {get;set;}
        public String sku {get;set;}
    }
    global class submit_rma_response_t
    {
        public String failure_reason {get;set;}
        public String warning_reason {get;set;}
        public boolean is_success {get;set;}
        public rma_success_details_t rma_success_details {get;set;}
    }
    global class rma_request_details_t
    {
        public String unique_id {get;set;}
        public String rma_id {get;set;}
        public String replacement_cart_link {get;set;}
        public String replacement_gpn {get;set;}
        public String rma_requester_first_name {get;set;}
        public String rma_requester_last_name {get;set;}
        public address_t rma_requester_address {get;set;}
    }
    global class rma_success_details_t
    {
        public String rma_id {get;set;}     
        public boolean shipping_label_needed  {get;set;}        
        public String canned_response {get;set;}
        public String ar_method {get;set;}
        public address_t shipping_destination_address {get;set;}
        public String shipping_method {get;set;}
        //public String shipping_label_method {get;set;}
        public String exchange_sku_description {get;set;}
        public String exchange_gpn {get;set;}
        public String gpn {get; set;}
        public String exchange_doc_id {get;set;}
        public String shipping_center_id {get;set;}
        public String shipping_center_name {get;set;}
        public String need_rma_label {get;set;}
        //public String extended_warranty_claim_id {get;set;}
        public String extended_warranty_contract_id {get;set;}
        public String sku {get;set;}
    }
    global class update_owner_details_t
    {
        public String unique_id {get;set;}
        public String new_owner_first_name {get;set;}
        public String new_owner_last_name {get;set;}
        public String new_owner_email {get;set;}
        public address_t new_requester_address {get;set;}
        /*public String new_requester_address_street {get;set;}
        public String new_requester_address_city {get;set;}
        public String new_requester_address_state_province {get;set;}
        public String new_requester_address_country {get;set;}
        public String new_requester_address_postal_code {get;set;}*/
    }
    global class update_owner_details_response_t
    {
        public boolean is_success {get;set;}
        public String failure_reason {get;set;}
    }
    
    global class rma_events_t
    {
        public String rma_event_name {get;set;}
        public String rma_past_value {get;set;}
        public String rma_current_value {get;set;}
        public Date rma_event_change_date {get;set;}
        public String rma_event_changer {get;set;}
    }
    global class owner_details_t
    {
        public String unique_id {get;set;}
        public String new_owner_first_name {get;set;}
        public String new_owner_last_name {get;set;}
        public String new_owner_email {get;set;}
        public address_t new_owner_address {get;set;}
        /*public String new_requester_address_street {get;set;}
        public String new_requester_address_city {get;set;}
        public String new_requester_address_state_province {get;set;}
        public String new_requester_address_country {get;set;}
        public String new_requester_address_postal_code {get;set;}*/
    }
    global class customer_details_t
    {
        public String customer_first_name {get;set;}
        public String customer_last_name {get;set;}
        public String customer_email {get;set;}
        public address_t customer_address {get;set;}
        /*public String customer_address_street {get;set;}
        public String customer_address_city {get;set;}
        public String customer_address_state_province {get;set;}
        public String customer_address_country {get;set;}
        public String customer_address_postal_code {get;set;}*/
    }
    global class address_t
    {
        public String address_street {get;set;}
        public String address_street_2 {get;set;}
        public String address_street_3 {get;set;}
        public String address_city {get;set;}
        public String address_state_province {get;set;}
        public String address_country {get;set;}
        public String address_postal_code {get;set;}
    }
    global class device_types_t
    {
        public String doc_id{get;set;}
        public String sku_gpn {get;set;}
        public String device_family {get;set;}
        public String device_sub_family {get;set;}
        public String doc_description {get;set;}   
    }
    global class country_variant_model_t
    {       
        public String country_variant_country {get;set;}
        public String country_variant_eligible_for_cid {get;set;}
        public String country_variant_ar_method {get;set;}
        public String country_variant_doa_replacement_sku {get;set;}
        public String country_variant_doa_replacement_sku_description {get;set;}
        public String country_variant_replacement_sku {get;set;}
        public String country_variant_replacement_sku_description {get;set;}
    }
    global class country_variant_details_t
    {
        public List<country_variant_model_t> country_variant_models {get;set;}
    }
    global virtual class extended_warranty_detail_t
    {
        public String asset_id {get;set;}
        public String order_id {get;set;}
        public String contract_id {get;set;}
        public Date warranty_expiration_date {get;set;}
        public String line_number {get;set;}
    }
    global class extended_warranty_request_detail_t extends extended_warranty_detail_t
    {
        public String sku {get;set;}
        public String asset_line_number {get;set;}
        public String country {get;set;}
    }
    global class extended_warranty_response_t
    {
        public String failure_reason {get;set;}
        public boolean is_success {get;set;}
        public extended_warranty_detail_t extended_warranty_detail {get;set;}
    }
    global class lost_stolen_history_t
    {
        public String order_id {get;set;}
        public String country {get;set;}
        public String rma_no {get;set;}
        public String asset_id {get;set;}
        public String sku {get;set;}
        public String customer_email {get; set;}
        public String agent_ldap {get; set;}
        public String case_id {get; set;}
    }
    global class lost_stolen_history_response_t
    {
        public String failure_reason {get;set;}
        public boolean is_success {get;set;}
        public String customer_id {get; set;}
        public Integer number_lost_stolen_refunds {get; set;}
    }

    global class warranty_transfer_detail_t
    {
        public String order_id {get;set;}
        public String country {get;set;}
        public String rma_no {get;set;}
        public String asset_id {get;set;}
        public String sku {get;set;}
        public String customer_email {get; set;}
        public String agent_ldap {get; set;}
        public String cases_id {get; set;}
    }
    global class warranty_transfer_response_t
    {
        public String failure_reason {get;set;}
        public boolean is_success {get;set;}
        public String customer_id {get; set;}
        public Integer number_lost_stolen_refunds {get; set;}
    }

    global class cancel_warranty_detail_t
    {
        public String contract_id {get;set;}
        public Date cancel_date {get;set;}
    }
    global class cancel_warranty_response_t
    {
        public String failure_reason {get;set;}
        public boolean is_success {get;set;}
    }
    
    global class lostStolenRefundResponse{
        public String refund_id;
        public String failure_reason;
    }
    
    global class RCLCheck{
        public String failure_reason {get;set;}
        public Boolean is_valid_request {get;set;}
    }
}