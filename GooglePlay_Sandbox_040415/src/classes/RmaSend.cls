public without sharing class RmaSend 
{
    public RMA__c rma {get;set;}
    public String jsonString {get;set;}
    public String cverror {get;set;}
    public RmaSend(ApexPages.StandardController con)
    {
        rma = [SELECT Name,Type__c, RMA_Country_Formula__c,RMA_Country_Workflow__c,Google_Order_ID__c,
        GoogleAsset__r.Name,GoogleAsset__r.SKU__c,GoogleAsset__r.ProductSKUDescription__c,
        GoogleAsset__r.ProductID__r.Document__r.Unique_ID_Type__c,
        GoogleAsset__r.ProductID__r.RepairPartner__c,GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c,
        Opportunity__r.Address__c,Opportunity__r.City__c,Opportunity__r.State_Province__c,Opportunity__r.PostalCode__c,
        GoogleCustomer__r.Name,RMA_Sub_Category__c,RMA_Category__c,Notes__c,CreatedDate,
        gCases_ID__c,GoogleCustomer__r.PersonMailingStreet,GoogleCustomer__r.PersonMailingCity,
        GoogleCustomer__r.PersonMailingState,GoogleCustomer__r.PersonMailingCountry,GoogleCustomer__r.PersonMailingPostalCode,
        GoogleCustomer__r.PersonEmail,Incomplete__c,GoogleAsset__r.ProductID__r.DOA_Doc_ID__c,
        is_nova__c, is_extended_warranty__c,
        Extended_Warranty_Contract_ID_Backup__c
        FROM RMA__c WHERE Id=:con.getId()];
        if(rma.Incomplete__c=='TRUE')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'This RMA is incomplete. Please finish filling out the RMA details.'));
            return;
        }
        if (string.isBlank(rma.RMA_Country_Workflow__c))
            rma.RMA_Country_Workflow__c = rma.RMA_Country_Formula__c;
        if (rma.RMA_Country_Workflow__c == 'UK')
            rma.RMA_Country_Workflow__c = 'GB';
        if (rma.Type__c == 'Extended Warranty')
            rma.Type__c = 'Warranty Regular';
        if (string.isBlank(rma.RMA_Country_Workflow__c))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is no Country selected. Please make sure the related Order has the Country field set correctly.'));
            cverror = 'TRUE';
            return;
        }
        if (rma.GoogleAsset__r.SKU__c == '970-00001-01' || rma.GoogleAsset__r.SKU__c == '970-00006-01')
            rma.GoogleAsset__r.SKU__c += '-RA';
        List<Country_Variant__c> cvsList = new List<Country_Variant__c>([SELECT DOA_Replacement_Doc_ID__c, Warranty_Replacement_Doc_ID__c, Document__r.Finsky_Doc_ID__c, Document__r.Product_Family__r.Name FROM Country_Variant__c WHERE Name = :rma.RMA_Country_Workflow__c AND Document__c = :rma.GoogleAsset__r.ProductID__r.Document__c]);
        cverror = 'FALSE';
        if (cvsList.isEmpty())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is no Country Variant configured for this Product/Country combination. Please escalate this case to a supervisor.'));
            cverror = 'TRUE';
            return;
        }
        Country_Variant__c cv = cvsList[0];
        String isglass = '';
        if (cv.Document__r.Product_Family__r.Name == 'Glass')
            isglass = '_r';
        if (rma.Type__c == 'Warranty DOA')
            rma.GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c = cv.DOA_Replacement_Doc_ID__c + isglass;
        else if (rma.Type__c == 'Warranty Regular')
            rma.GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c = cv.Warranty_Replacement_Doc_ID__c + isglass;
        else
            rma.GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c = cv.Document__r.Finsky_Doc_ID__c + isglass;    
        jsonString = String.escapeSingleQuotes(JSON.serialize(rma));        
    }   
}