@isTest
private class TestRestSubmitRMA {
	public static void setUptest()
	{
		Account a = new Account();
		a.FirstName='Test';
		a.LastName='Account';
		a.PersonEmail='Test@test.com';
		a.PersonMailingCountry='GB';
		a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
		insert a;

		Opportunity opp = new Opportunity();
		opp.Name='Opportunity1';
		opp.CloseDate=Date.today();
		opp.StageName='Closed Won';
		opp.AccountId=a.Id;
		opp.Type='Standard Order';
		opp.Country__c='US';
		opp.State_Province__c='TX';
		insert opp;

		Product_Family__c pf = new Product_Family__c(Name='Test');
		insert pf;

		Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
		insert d;

		Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
		insert cv;

		Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
		insert p;


		Google_Asset__c gAsset = new Google_Asset__c();
		gAsset.Name='TestGA';
		gAsset.ProductId__c=p.Id;
		gAsset.SKU__c='SSSS';
		gAsset.Order_Opportunity__c=opp.Id;
		gAsset.Line_Number__c=1;
		gAsset.Retail_Country__c = 'Set by SKU';
		gAsset.ProductId__c=p.Id;
		insert gAsset;
	}
	@isTest static void doUpdateNewRmaWithanExistingAcct() {
		TestRestSubmitRMA.setUptest();
		Opportunity opp = [SELECT Id, Country__c FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];

		Google_Asset__c gAsset = [SELECT Id FROM Google_Asset__c WHERE Name = 'TestGA' LIMIT 1];

		RMA__c rma = new RMA__c();
		rma.Status__c='Pending Return';
		rma.Triage_Code__c='C';
		rma.Customer_Induced_Damage__c='Y';
		rma.Type__c='Buyer\'s Remorse';
		rma.Opportunity__c = opp.Id;
		rma.GoogleAsset__c=gAsset.Id;
		rma.RMA_Category__c='No Longer Wants product';
		rma.RMA_Sub_Category__c='Test';
		rma.Notes__c='Test';
		rma.Extended_Warranty_Claim_ID__c = '123544984';
		insert rma;


		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		RestContext.request = rReq;
		RestContext.response = rRes;

		//BEGIN TEST FOR RestSubmitRMA.cls
		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
		response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'Test@test.com';
		requestDetails.rma_type = 'Extended Warranty';
		testRestUtils.createOpportunity();
		requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

		testRestUtils.createLevUser();
		requestDetails.rma_creator = 'standarduser@levementum.com';
		requestDetails.extended_warranty_claim_id = '123544984';
		requestDetails.sku = '256156918591';

		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		Profile pr = ([SELECT Id FROM Profile WHERE Name =: 'System Administrator']);
		User testUser = new User();
		testUser.LastName = 'testName';
		testUser.UserName = 'test123165@test.com';
		testUser.ProfileId = pr.Id;
		testUser.Alias = 'testName';
		testUser.Localesidkey='en_US';
		testUser.EmailEncodingKey = 'UTF-8';
		testUser.TimeZoneSidKey = 'America/Los_Angeles';
		testUser.Email = 'Test1231@test.com';
		testUser.LanguageLocaleKey = 'en_US';
		insert testUser;
		RestSubmitRMA.updateUser(testUser.Id);
		//END TEST FOR RestSubmitRMA.cls
		System.assertEquals(response.is_success, false);
		System.assertEquals(response.rma_success_details, null);

	}
	@isTest static void doUpdateNewRma() {
		TestRestSubmitRMA.setUptest();
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Google_Asset__c gAsset = [SELECT Id FROM Google_Asset__c WHERE Name = 'TestGA' LIMIT 1];

		RMA__c rma = new RMA__c();
		rma.Status__c='Pending Return';
		rma.Triage_Code__c='C';
		rma.Customer_Induced_Damage__c='Y';
		rma.Type__c='Buyer\'s Remorse';
		rma.Opportunity__c = opp.Id;
		rma.GoogleAsset__c=gAsset.Id;
		rma.RMA_Category__c='No Longer Wants product';
		rma.RMA_Sub_Category__c='Test';
		rma.Notes__c='Test';
		insert rma;

		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;


		//BEGIN TEST FOR RestSubmitRMA.cls
		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
		response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values
		System.assertEquals(response.is_success, false);

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test@levtest.com';

		testRestUtils.createOpportunity();
		requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

		testRestUtils.createLevUser();
		requestDetails.rma_creator = 'standarduser@levementum.com';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		//END TEST FOR RestSubmitRMA.cls
		//suppose to have a is succes response equals to false.
		System.assertEquals(response.is_success, false);
	}
	@isTest static void doUpdateWarrantyRMA() {
		TestRestSubmitRMA.setUptest();
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		//BEGIN TEST FOR RestSubmitRMA.cls
		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
		response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test@levtest.com';

		testRestUtils.createOpportunity();
		requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

		testRestUtils.createLevUser();
		requestDetails.rma_creator = 'standarduser@levementum.com';

		requestDetails.rma_requester_first_name = 'John';
		requestDetails.rma_requester_last_name = 'Wick';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		requestDetails.rma_type = 'Extended Warranty';
		requestDetails.sku = 'SSSS';
		requestDetails.extended_warranty_claim_id = 'Extended Claim1';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		System.assertEquals(response.is_success, true);
		List<Account> testAccounts = ([SELECT Id, FirstName, LastName FROM Account WHERE LastName =: 'Wick' ]);
		System.assertEquals(testAccounts.size() > 0, true);
		System.assertEquals(testAccounts[0].FirstName, requestDetails.rma_requester_first_name);
		System.assertEquals(testAccounts[0].LastName, requestDetails.rma_requester_last_name);
	}
	@isTest static void doUpdateWarrantyRMANoOrder() {
		TestRestSubmitRMA.setUptest();
		TestRestUtilities testRestUtils = new TestRestUtilities();


		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA2'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test2@levtest.com';

		//testRestUtils.createOpportunity();
		requestDetails.google_order_id = 'ordertestnew';

		//testRestUtils.createLevUser();
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

		User u = new User(IsActive=false,Alias = 'stdlev', Email='standarduser22@levementum.com',
				EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
				LocaleSidKey='en_US', ProfileId = p.Id,
				TimeZoneSidKey='America/Los_Angeles', UserName='standarduser22@levementum.com',LDAP__c='standarduser22@levementum.com');
		insert u;

		requestDetails.rma_creator = 'standarduser22@levementum.com';

		requestDetails.rma_requester_first_name = 'John';
		requestDetails.rma_requester_last_name = 'Wick';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		requestDetails.rma_type = 'Extended Warranty';
		requestDetails.sku = 'SKU';
		requestDetails.extended_warranty_claim_id = 'Extended Claim1';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		System.runAs([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]){
			Test.startTest();

			response = RestSubmitRMA.doSubmitRMAs(requestDetails);

			Test.stopTest();
			System.assertEquals(response.is_success, false);
		}
	}
	@isTest static void doUpdateWarrantyWitgNoClaimId() {
		TestRestSubmitRMA.setUptest();
		TestRestUtilities testRestUtils = new TestRestUtilities();


		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA2'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test2@levtest.com';

		//testRestUtils.createOpportunity();
		requestDetails.google_order_id = 'ordertestnew';

		testRestUtils.createLevUser();

		requestDetails.rma_creator = 'standarduser@levementum.com';

		requestDetails.rma_requester_first_name = 'John';
		requestDetails.rma_requester_last_name = 'Wick';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		requestDetails.rma_type = 'Extended Warranty';
		requestDetails.sku = 'SKU';
		//requestDetails.extended_warranty_claim_id = 'Extended Claim1';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		System.runAs([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]){
			Test.startTest();

			response = RestSubmitRMA.doSubmitRMAs(requestDetails);
			System.assertEquals(response.is_success, false);
			Test.stopTest();
		}
	}
	@isTest static void doUpdateWarrantyAlreadyHaveRMA() {
		TestRestSubmitRMA.setUptest();
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;


		//BEGIN TEST FOR RestSubmitRMA.cls
		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
		response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA567'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test@levtest.com';

		testRestUtils.createOpportunity();
		requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

		testRestUtils.createLevUser();
		requestDetails.rma_creator = 'standarduser@levementum.com';

		requestDetails.rma_requester_first_name = 'John';
		requestDetails.rma_requester_last_name = 'Wick';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		requestDetails.rma_type = 'Extended Warranty';
		requestDetails.sku = 'SSSS';
		requestDetails.extended_warranty_claim_id = 'Extended Claim1';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		System.assertEquals(response.is_success, true);
		List<Account> testAccounts = ([SELECT Id, FirstName, LastName FROM Account WHERE LastName =: 'Wick' ]);
		System.assertEquals(testAccounts.size() > 0, true);
		System.assertEquals(testAccounts[0].FirstName, requestDetails.rma_requester_first_name);
		System.assertEquals(testAccounts[0].LastName, requestDetails.rma_requester_last_name);
		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		System.assertEquals(response.is_success, false);
	}
	@isTest static void doUpdateWarrantyNoSKU() {
		TestRestSubmitRMA.setUptest();
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
		response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;

		requestDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		requestDetails.rma_requester_email = 'test@levtest.com';

		testRestUtils.createOpportunity();
		requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

		testRestUtils.createLevUser();
		requestDetails.rma_creator = 'standarduser@levementum.com';

		requestDetails.rma_requester_first_name = 'John';
		requestDetails.rma_requester_last_name = 'Wick';
		requestDetails.rma_requester_address = new RestResponseModel.address_t();
		requestDetails.rma_requester_address.address_state_province = 'TX';
		requestDetails.rma_requester_address.address_country = 'US';
		requestDetails.rma_requester_address.address_postal_code = '79938';

		requestDetails.rma_type = 'Extended Warranty';
		requestDetails.extended_warranty_claim_id = 'Extended Claim1';

		testRestUtils.createCodeSet();
		//testRestUtils.createReasonCode();
		testRestUtils.createProductFamily();
		testRestUtils.createDocument();
		testRestUtils.createCountryVariant();
		testRestUtils.createProduct1();

		System.debug('############### requestDetails:' + requestDetails);

		response = RestSubmitRMA.doSubmitRMAs(requestDetails);
		//Asserts to verify this fails. it has to fail because it has no SKU
		System.assertEquals(response.is_success, false);
		System.assertEquals(response.failure_reason, 'No SKU');
	}

}