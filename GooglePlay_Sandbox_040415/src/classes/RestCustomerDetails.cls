@RestResource(urlMapping='/CustomerDetails/*')
global class RestCustomerDetails
{
	//Description: Gets the person details based on the email sent and copies the data into the customer details of the response object. Example: address, Name and email.
	@HttpGet
	global static RestResponseModel.customer_details_t getCustomerDetails()
	{
		Map<String,String> paramMap = RestContext.request.params;
		String email = paramMap.get('email');
		RestResponseModel.customer_details_t customer_details = new RestResponseModel.customer_details_t();
		if(String.isNotEmpty(email))
		{
			List<Account> aList = new List<Account>([SELECT Id,FirstName,LastName,PersonEmail,PersonMailingStreet,PersonMailingCity,PersonMailingState,PersonMailingCountry,PersonMailingPostalCode FROM Account WHERE PersonEmail=:email]);
			if(aList.size()==1)
			{
				Account a = aList.get(0);
				customer_details.customer_first_name=a.FirstName;
				customer_details.customer_last_name=a.LastName;
				customer_details.customer_email=a.PersonEmail;
				customer_details.customer_address = new RestResponseModel.address_t();
				customer_details.customer_address.address_street=a.PersonMailingStreet;
				customer_details.customer_address.address_city=a.PersonMailingCity;
				customer_details.customer_address.address_state_province=a.PersonMailingState;
				customer_details.customer_address.address_country=a.PersonMailingCountry;
				customer_details.customer_address.address_postal_code=a.PersonMailingPostalCode;
			}
		}
		return customer_details;
	}
}