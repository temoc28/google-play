/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/20/15
 * @email:	cmunoz@levementum.com
 * @description:	General DML Operations for RMA__c custom object
 */
public class RMADataAccess implements SObjectDataAccess
{
	public static String objectStatus = 'OK';
	
	public sObject selectObject(Id rmaId)
	{
		RMA__c rmaSelected = null;
		try
		{
			rmaSelected = [SELECT ASUS_Problem_Code__c, Reason_Code__c, AVR_Brand_Model__c, Account_City__c, Account_Country__c, 
									Account_Postal_Code__c, Account_State__c, Account_Street__c, Amount_Authorized__c, Amount_Charged__c, 
									Amount_Refunded__c, Asset_Sales_Channel__c, Asset_Type__c, AssetType_Legacy__c, Barcode_Subtype__c, 
									Barcode_Type__c, Upserted_Customer_Induced_Damage__c, Customer_Induced_Damage__c, CSSN__c, 
									CTDataMigration__c, Case__c, CloseRMAReceivedReason__c, Condition__c, Condition_Code__c, CreatedById, 
									CreatedDate, Created_by_logged_in_user__c, Created_on_Google_Server__c, CreatedByLDAP__c, 
									CreatedByLocation__c, CreatedDateHold__c, Customer_Induced_Damage_Formula__c, CustomerInducedDamage_Legacy__c, 
									Customer_Name__c, Customer_changed_from_DOA_to_Remorse__c, DataMigrationKey__c, Date_Received__c, 
									IsDeleted, Device_Condition__c, PersonAcctEmailAddress__c, ensured_N7__c, Exception_Notes__c, 
									Exception_Reason__c, Exception_Type__c, external_RMA_num__c, Failure_Action_Code__c, Failure_Free_Form_Notes__c, 
									Failure_Modified_Code__c, Failure_Observed_Condition_Code__c, Final_Asset_Disposition_Status__c, GPN__c, 
									GlassRmaNumber__c, GlassError__c, GlassErrorMessage__c, GlassRmaId__c, GoogleCustomer__c, Google_Account__c, 
									GoogleAsset__c, Google_Asset_Line_Number__c, Google_Asset_Unique_ID__c, Opportunity__c, Google_Order_ID__c, 
									GoogleOrderCountry__c, GoogleOrderStateProvince__c, GoogleOrderType__c, acknowledge_review__c, 
									I_understand_Exception_RMAs_are_reviewed__c, IMEI__c, suggested_category__c, why_other__c, Incomplete__c, 
									Item_Description__c, LG_Reason_Code__c, LastActivityDate, LastDateBuyersRemorse__c, LastDateDOA__c, 
									LastDateSpareParts__c, LastModifiedById, LastModifiedDate, Line_Number__c, MTR_ID__c, ManuallyClosedBy__c, 
									ManuallyReceivedBy__c, Manufacturer__c, MarkRMAReceivedReason__c, MarkedReceivedBy__c, Needs_Shipping_Label__c, 
									Notes__c, Operating_System_Code__c, Order__c, Google_Order_Type_Display__c, Order_Type__c, OwnerId, 
									Previous_RMA_Category__c, Primary_CID_Exception_Reason__c, Query_Status_Processed__c, RMA_Action__c, 
									RMA_Audit__c, RMA_Auditor_Notes__c, RMA_Category__c, RMA_Country_Formula__c, rma_country__c, RMA_Country_Workflow__c, 
									RMA_Delivered_Date__c, RMA_ID_External_ID__c, RMA_In_Transit_Date__c, RMA_Needs_Review__c, Name, 
									RMAReceivedReason__c, RMAReceivedReasonNotes__c, RMA_Category_Change_Date__c, RMA_Sub_Category__c, 
									RMA_Type__c, Ready_to_Submit__c, Ready_to_send__c, Id, RecordTypeId, Repair_Partner_2__c, RepairPartner__c, 
									Replacement_Cart_ID__c, ReplacementOrder__c, Replacement_Outbound_UPS__c, Return_Code_Reason__c, 
									Return_Condition_Notes__c, ReturnConditionNotificationDate__c, Return_Product_Instruction_Code__c, 
									Return_Shipping_Tracking_Number__c, Router_Brand__c, Router_Model__c, SKU__c, SKUDescription__c, SendtoBizApps__c, 
									SerialNumber__c, ServiceModel__c, Shipment_Date__c, Shipping_Service_Level_Code__c, Status__c, SystemModstamp, 
									TV_Brand__c, TV_Model__c, Time_between_created_and_last_modified__c, Upserted_Triage_Code__c, Triage_Code__c, 
									Truncated_Sub_Category__c, Type__c, UniqueIDType__c, Upserted_Status__c, VendorAssetReceivedDate__c, 
									VendorAssetReceivedNotificationDate__c, WarrantyCountries__c, WarrantyExpirationDate__c, Warranty_Identifier__c, 
									Warranty_Provider__c, Warranty_Repair_Status__c, gCases_ID__c, in_warranty__c 
							 FROM RMA__c
							 WHERE id =: rmaId limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return rmaSelected;
	}
	
	public sObject selectObject(String rmaName)
	{
		RMA__c rmaSelected = null;
		try
		{
			rmaSelected = [SELECT ASUS_Problem_Code__c, Reason_Code__c, AVR_Brand_Model__c, Account_City__c, Account_Country__c, 
									Account_Postal_Code__c, Account_State__c, Account_Street__c, Amount_Authorized__c, Amount_Charged__c, 
									Amount_Refunded__c, Asset_Sales_Channel__c, Asset_Type__c, AssetType_Legacy__c, Barcode_Subtype__c, 
									Barcode_Type__c, Upserted_Customer_Induced_Damage__c, Customer_Induced_Damage__c, CSSN__c, 
									CTDataMigration__c, Case__c, CloseRMAReceivedReason__c, Condition__c, Condition_Code__c, CreatedById, 
									CreatedDate, Created_by_logged_in_user__c, Created_on_Google_Server__c, CreatedByLDAP__c, 
									CreatedByLocation__c, CreatedDateHold__c, Customer_Induced_Damage_Formula__c, CustomerInducedDamage_Legacy__c, 
									Customer_Name__c, Customer_changed_from_DOA_to_Remorse__c, DataMigrationKey__c, Date_Received__c, 
									IsDeleted, Device_Condition__c, PersonAcctEmailAddress__c, ensured_N7__c, Exception_Notes__c, 
									Exception_Reason__c, Exception_Type__c, external_RMA_num__c, Failure_Action_Code__c, Failure_Free_Form_Notes__c, 
									Failure_Modified_Code__c, Failure_Observed_Condition_Code__c, Final_Asset_Disposition_Status__c, GPN__c, 
									GlassRmaNumber__c, GlassError__c, GlassErrorMessage__c, GlassRmaId__c, GoogleCustomer__c, Google_Account__c, 
									GoogleAsset__c, Google_Asset_Line_Number__c, Google_Asset_Unique_ID__c, Opportunity__c, Google_Order_ID__c, 
									GoogleOrderCountry__c, GoogleOrderStateProvince__c, GoogleOrderType__c, acknowledge_review__c, 
									I_understand_Exception_RMAs_are_reviewed__c, IMEI__c, suggested_category__c, why_other__c, Incomplete__c, 
									Item_Description__c, LG_Reason_Code__c, LastActivityDate, LastDateBuyersRemorse__c, LastDateDOA__c, 
									LastDateSpareParts__c, LastModifiedById, LastModifiedDate, Line_Number__c, MTR_ID__c, ManuallyClosedBy__c, 
									ManuallyReceivedBy__c, Manufacturer__c, MarkRMAReceivedReason__c, MarkedReceivedBy__c, Needs_Shipping_Label__c, 
									Notes__c, Operating_System_Code__c, Order__c, Google_Order_Type_Display__c, Order_Type__c, OwnerId, 
									Previous_RMA_Category__c, Primary_CID_Exception_Reason__c, Query_Status_Processed__c, RMA_Action__c, 
									RMA_Audit__c, RMA_Auditor_Notes__c, RMA_Category__c, RMA_Country_Formula__c, rma_country__c, RMA_Country_Workflow__c, 
									RMA_Delivered_Date__c, RMA_ID_External_ID__c, RMA_In_Transit_Date__c, RMA_Needs_Review__c, Name, 
									RMAReceivedReason__c, RMAReceivedReasonNotes__c, RMA_Category_Change_Date__c, RMA_Sub_Category__c, 
									RMA_Type__c, Ready_to_Submit__c, Ready_to_send__c, Id, RecordTypeId, Repair_Partner_2__c, RepairPartner__c, 
									Replacement_Cart_ID__c, ReplacementOrder__c, Replacement_Outbound_UPS__c, Return_Code_Reason__c, 
									Return_Condition_Notes__c, ReturnConditionNotificationDate__c, Return_Product_Instruction_Code__c, 
									Return_Shipping_Tracking_Number__c, Router_Brand__c, Router_Model__c, SKU__c, SKUDescription__c, SendtoBizApps__c, 
									SerialNumber__c, ServiceModel__c, Shipment_Date__c, Shipping_Service_Level_Code__c, Status__c, SystemModstamp, 
									TV_Brand__c, TV_Model__c, Time_between_created_and_last_modified__c, Upserted_Triage_Code__c, Triage_Code__c, 
									Truncated_Sub_Category__c, Type__c, UniqueIDType__c, Upserted_Status__c, VendorAssetReceivedDate__c, 
									VendorAssetReceivedNotificationDate__c, WarrantyCountries__c, WarrantyExpirationDate__c, Warranty_Identifier__c, 
									Warranty_Provider__c, Warranty_Repair_Status__c, gCases_ID__c, in_warranty__c 
							 FROM RMA__c
							 WHERE Name =: rmaName limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return rmaSelected;
	}
	
	public void updateObject(sObject objectToUpdate, ObjectModel updatingData)
	{
		Savepoint sp = Database.setSavepoint();
		try
		{
			if(objectToUpdate == null)
			{
				insert insertObject(updatingData);
			}
			else
			{
				upsert updateModel(objectToUpdate, updatingData);
			}
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			objectStatus = ex.getMessage();
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(String objectName)
	{
		try
		{
			RMA__c rmaToDelete = [SELECT id
										 FROM RMA__c
										 WHERE Name =: objectName limit 1];
			delete rmaToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(Id objectId)
	{
		try
		{
			RMA__c rmaToDelete = [SELECT id
										 FROM RMA__c
										 WHERE id =: objectId];
			delete rmaToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public sObject insertObject(ObjectModel objectModel)
	{
		RMA__c rmaToInsert = new RMA__c();
		RMAModel rmaModel = (RMAModel)objectModel; 
		try
		{
			// REQUIRED FIELDS
			rmaToInsert.GoogleCustomer__c = rmaModel.GoogleCustomer;
			rmaToInsert.GoogleAsset__c = rmaModel.GoogleAsset;
			rmaToInsert.Opportunity__c = rmaModel.OpportunityObj;
			rmaToInsert.Notes__c = rmaModel.Notes;
			rmaToInsert.gCases_ID__c = rmaModel.gCases_ID;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return rmaToInsert;
	}
	
	public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
	{
		RMA__c rmaToUpdate = (RMA__c)sObjectToUpdate;
		RMAModel updatingModel = (RMAModel)updatingData;
		try
		{
			if(rmaToUpdate != null && updatingData != null)
			{
				rmaToUpdate.GoogleCustomer__c = updatingModel.GoogleCustomer;
				rmaToUpdate.GoogleAsset__c = updatingModel.GoogleAsset;
				rmaToUpdate.Opportunity__c = updatingModel.OpportunityObj;
				rmaToUpdate.Notes__c = updatingModel.Notes;
				rmaToUpdate.gCases_ID__c = updatingModel.gCases_ID;
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage(); 
		}
		return rmaToUpdate;
	}
}