/*
 * @author:	Levementum LLC
 * @date:	04/13/15
 * @description:	Google Asset model based on 
 * custom fields that may be added in Sandbox instance. Update this model
 * as necessary if new fields are added.
 */
global class GoogleAssetModel 
{
	public Date Last_Date_Buyer_s_Remorse {get; set;}
	public Date Last_Date_DOA {get; set;}
	public String Purchaser_Email {get; set;}
	public String active_device {get; set;}
	public String alternateserialnumber1 {get; set;}
	public String alternateserialnumber2 {get; set;} 
	public String alternateserialnumber3 {get; set;}
	public String alternateserialnumber4 {get; set;}
	public String cssn {get; set;}
	public String assetOwner {get; set;}
	public String asset_type {get; set;}
	public Boolean associated_with_a_safety_report {get; set;}
	public String buyers_remorse_destination {get; set;}
	public String csn {get; set;}
	public String carrier {get; set;}
	public String chrome_s_n {get; set;}
	public Boolean converted_to_chromecast_r {get; set;}
	public String country_sold {get; set;}
	public String createdbyid {get; set;} 
	public DateTime createddate {get; set;}
	public Decimal doa_period {get; set;}
	public String rma_exceptions {get; set;}
	public Decimal expected_sale_days {get; set;}
	public Boolean glasserror {get; set;}
	public String glasserrormessage {get; set;}
	public Boolean glasssent2 {get; set;}
	public String name {get; set;} 
	public String google_asset_id_external_id {get; set;}
	public String order_opportunity {get; set;}
	public String imei {get; set;}
	public String lastmodifiedbyid {get; set;} 
	public DateTime lastmodifieddate {get; set;}
	public Decimal line_number {get; set;}
	public String mac_address {get; set;}
	public Date manufacture_date {get; set;}
	public Decimal minimum_replacement_warranty_period {get; set;}
	public Boolean no_warranty_support {get; set;}
	public String orderlegacy {get; set;}
	public String original_retail_store_name {get; set;}
	public String ownerid {get; set;}
	public String productid {get; set;}
	public String proof_of_purchase_gcases_id {get; set;}
	public Id id {get; set;}
	public String recordtypeid {get; set;}
	public Decimal remorse_period {get; set;}
	public String bricking {get; set;}
	public String repair_partner_legacy {get; set;}
	public Decimal replacement_warranty {get; set;}
	public String retail_country {get; set;}
	public Date retail_sale_date {get; set;}
	public String sku {get; set;}
	public String sku_description {get; set;}
	public String skuupsert {get; set;}
	public Date sale_date {get; set;}
	public Date sale_datevalue {get; set;}
	public String serial {get; set;}
	public Boolean shopping_express {get; set;}
	public String tracking_number {get; set;}
	public String tracking_url {get; set;}
	public String unique_id_type {get; set;}
	public Boolean vip {get; set;}
	public String warranty_countries_legacy {get; set;}
	public Decimal warranty_period {get; set;}
	public Date warranty_expiration_date {get; set;}
	public Date Last_Date_Spare_Parts {get; set;}
	public String ProductSKUDescription {get; set;}
	public String ProductSKU {get; set;}
	public String RemoteDisable {get; set;}
	public String RepairPartner {get; set;}
	public String WarrantyCountries {get; set;}
	public Date SaleDateOrder {get; set;}
	public Id Unique_ID_Type_from_product {get; set;}
	public DateTime SaleDateOpportunity {get; set;}
	public Date Shipment_Date {get; set;}
	public String Sale_Country {get; set;}
	public String Tracking_Link {get; set;}
	public String Service_Model {get; set;}
	public double Shipping_Adjustment {get; set;}
	public double Warranty_Countdown {get; set;}
	public double Remorse_Days_Left {get; set;}
	public double DOA_Days_Left {get; set;}
	public String Order_ID_Name {get; set;}
	public boolean Recently_modified_by_user {get; set;}
	public String MTR_ID {get; set;}
	public String Order_Type {get; set;}
	public String ProductFamily {get; set;}
	
	public String status {get; set;}
}