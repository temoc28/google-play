global interface SObjectDataAccess 
{
	void updateObject(sObject obj, ObjectModel objModel);
	sObject selectObject(Id objId);
	sObject selectObject(String objName);
	sObject selectObject(ObjectModel model);
	void deleteObject(Id objId);
	void deleteObject(String objName);
	void deleteObject(ObjectModel objectModel);
	sObject insertObject(ObjectModel objectModel);
	sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData);
}