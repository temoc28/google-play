public without sharing class UserTrgHandler 
{
	public static void onAfterInsert(User[] trgNew)
	{
		List<Profile> pList = new List<Profile>([SELECT Id FROM Profile WHERE Name='Special Agent (SSO)']);
		if(pList.size()==1)
		{
			List<Group> gList = new List<Group>([SELECT Id FROM Group WHERE DeveloperName='Special_Agent']);
			Id gId;
			if(gList.size()==1)
				gId=gList.get(0).Id;
			if(gId==null)
				return;
			List<GroupMember> gmList = new List<GroupMember>();
			Id pId = pList.get(0).Id;
			for(User u : trgNew)
			{
				if(u.ProfileId==pId)
				{
					gmList.add(new GroupMember(GroupId=gId,UserOrGroupId=u.Id));
				}
			}
			if(gmList.size()>0)
				insert gmList;
		}
	}
}