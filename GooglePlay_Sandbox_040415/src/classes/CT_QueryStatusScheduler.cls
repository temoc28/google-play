global class CT_QueryStatusScheduler implements Schedulable{

    global void execute(SchedulableContext SC){    
        try{
            String QueryString = 'SELECT Id, Opportunity__r.Id, Name, Ready_to_Submit__c, Created_on_Google_Server__c, Status__c, Line_Number__c FROM RMA__c WHERE Created_on_Google_Server__c = true AND Status__c = \'Pending Return\'';
            CT_QueryStatusBatch qsb = new CT_QueryStatusBatch(QueryString);
            ID batchprocessid = Database.executeBatch(qsb,100);
            
        }catch(Exception e){
            System.debug('Exception: ' + e.getMessage());
        }
    }
      
    public static testmethod void testQueryStatusScheduler() {
        Test.startTest();                   
        CT_QueryStatusScheduler usm = new CT_QueryStatusScheduler();
        String sch = '10 23 * * * ?';
        system.schedule('Test CT_QueryStatusScheduler', sch, usm);            
        Test.stopTest();
    }
}