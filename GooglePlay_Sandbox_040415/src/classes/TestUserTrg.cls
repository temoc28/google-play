/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestUserTrg 
{
    static testMethod void myUnitTest() 
    {
        Profile p = [SELECT p.Name,p.Id from Profile p where p.Name='Special Agent (SSO)' limit 1];
    	Group g;
    	List<Group> gList = new List<Group>([SELECT Id FROM Group WHERE DeveloperName='Special_Agent']);
    	if(gList.size()!=1)
    	{
    		g = new Group(Type='Regular',Name='Special Agent',DeveloperName='Special_Agent');
    		insert g;
    	}
    	else
    		g = gList.get(0);
    	
    	User agent = new User(UserName='testMexyz@google.com',LastName='Test',email='test@google.com',alias='testMeXy',communitynickname='testMex',
		TimeZoneSidKey='America/New_York',LocaleSidKey='en_US',EmailEncodingKey='ISO-8859-1',ProfileId=p.Id,LanguageLocaleKey='en_US');
    	
    	insert agent;
    }    
}