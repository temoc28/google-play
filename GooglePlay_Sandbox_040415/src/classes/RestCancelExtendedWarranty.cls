@RestResource(urlMapping='/CancelExtendedWarranty/*')
global class RestCancelExtendedWarranty {

    @HttpPost
    global static RestResponseModel.cancel_warranty_response_t cancelWarranty(RestResponseModel.cancel_warranty_detail_t cancelWarrantyDetail) {
        RestResponseModel.cancel_warranty_response_t cancel_warranty_response = new RestResponseModel.cancel_warranty_response_t();
        if(cancelWarrantyDetail.contract_id == null){
            cancel_warranty_response.failure_reason= 'Contract id parameter was not found';
            cancel_warranty_response.is_success=false;
        }else if(cancelWarrantyDetail.cancel_date == null){
            cancel_warranty_response.failure_reason= 'Cancel Date parameter was not found';
            cancel_warranty_response.is_success=false;
        }else{
            try {
                List<Extended_Warranty__c> extendWarranty = [SELECT Id, Cancelled_Date__c FROM Extended_Warranty__c WHERE Name=:cancelWarrantyDetail.contract_id LIMIT 1];
                if(extendWarranty.size() == 0) {
                    throw new RestCancelExtendedWarranty.CancelWarrantyException('Can not find Contract Id with that number');
                } else extendWarranty.get(0).Cancelled_Date__c = cancelWarrantyDetail.cancel_date;

                update extendWarranty;
                cancel_warranty_response.is_success = true;
            } catch(CancelWarrantyException ce) {
                cancel_warranty_response.failure_reason = ce.getMessage();
                cancel_warranty_response.is_success = false;
            } catch(Exception e) {
                cancel_warranty_response.failure_reason = e.getMessage();
                cancel_warranty_response.is_success = false;
            }
        }

        return cancel_warranty_response;
    }

    global class CancelWarrantyException extends Exception{}

}