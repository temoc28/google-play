public class LookupProductBySKU {
	public static void LookupProduct(List<Google_Asset__c> TNew){
		
		Set<String> skus = new Set<String>();
		for(Google_Asset__c GA : TNew){
			GA.Google_Asset_ID_External_ID__c = GA.Name;
			
			//get a list of skus
			if(GA.SKU__c != null)
			{
				skus.add(GA.SKU__c);
			}
		}
	
		//try to populate the ProductID__c
		Map<String, Product2> pMap = new Map<String,Product2>();
		for(Product2 p:[Select Id, SKU__c from Product2 where SKU__c in :skus])
		{
			pMap.put(p.SKU__c, p);
		}
		for(Google_Asset__c GA : TNew){
				
			if(pMap.get(GA.SKU__c) != null)
			{
				GA.ProductID__c = pMap.get(GA.SKU__c).Id;
			}
		}
	}
	
	
	
	@isTest
    static void myUnitTest() {
        Google_Asset__c GA = new Google_Asset__c (Name = 'test',SKU__c='asdf');
        insert GA;
        List<Google_Asset__c> gaList = new List<Google_Asset__c>();
        gaList.add(ga);
        LookupProductBySKU.LookupProduct(gaList);
    }
}