@RestResource(urlMapping='/WarrantyTransfer/*')
global with sharing class RestWarrantyTransfer {
    public static final String CLOSED_WON = 'Closed/Won';
    public static String ADVANCED_REPLACEMENT = 'Advanced Replacement';
    @HttpPost
    global static RestResponseModel.warranty_transfer_response_t updateWarrantyTransfer(RestResponseModel.warranty_transfer_detail_t warrantyTransfer){
        RestResponseModel.warranty_transfer_response_t warranty_transfer_response = new RestResponseModel.warranty_transfer_response_t();
        if(warrantyTransfer.order_id == null){
            warranty_transfer_response.failure_reason= 'Order id parameter was not found';
            warranty_transfer_response.is_success=false;
        }else if(warrantyTransfer.rma_no == null){
            warranty_transfer_response.failure_reason= 'RMA no parameter was not found';
            warranty_transfer_response.is_success=false;
        }else if(warrantyTransfer.country == null){
            warranty_transfer_response.failure_reason= 'Country parameter was not found';
            warranty_transfer_response.is_success=false;
        }else if(warrantyTransfer.asset_id == null){
            warranty_transfer_response.failure_reason= 'Asset parameter was not found';
            warranty_transfer_response.is_success=false;
        }else if(warrantyTransfer.sku == null){
            warranty_transfer_response.failure_reason= 'SKU parameter was not found';
            warranty_transfer_response.is_success=false;
        }else{
            Savepoint sp = Database.setSavepoint();
            try{

                List<Opportunity> orderList = new List<Opportunity>();
                List<RMA__c> rmaList = new List<RMA__c>();
                List<Google_Asset__c> assertList = new List<Google_Asset__c>();
                
                Google_Asset__c assetObj;
                Opportunity orderObj = new Opportunity();
                RMA__c rmaObj = new RMA__c();

                assertList = [SELECT Id,Extended_Warranty__c,Line_Number__c,Order_Opportunity__c FROM Google_Asset__c WHERE Name=:warrantyTransfer.asset_id LIMIT 1];
                if(assertList.size() == 0){//create new assert
                    assetObj = new Google_Asset__c();
                    assetObj.Name = warrantyTransfer.asset_id;
                    assetObj.SKU__c = warrantyTransfer.sku;
                    assetObj.RecordTypeId = Schema.SObjectType.Google_Asset__c.getRecordTypeInfosByName().get(RestCreateExtendedWarranty.GOOGLE_PLAY_ASSET).getRecordTypeId();

                    insert assetObj;
                }else assetObj = assertList.get(0);

                rmaList = [SELECT Id,ReplacementOrder__c FROM RMA__c WHERE Name=:warrantyTransfer.rma_no LIMIT 1];
                if(rmaList.size() == 0){//create new order
                    throw new RestWarrantyTransfer.WarrantyTransferException('Can not find RMA with that number');
                }else rmaObj = rmaList.get(0);

                orderList = [SELECT Id,Name,Country__c,StageName,CloseDate,ShipmentDate__c,Type FROM Opportunity WHERE Name=:warrantyTransfer.order_id LIMIT 1];
                //Name
                if(orderList.size() == 0){//create new order
                    orderObj = new Opportunity();
                }else orderObj = orderList.get(0);

                orderObj.Type = RestWarrantyTransfer.ADVANCED_REPLACEMENT;
                orderObj.Name = warrantyTransfer.order_id;
                orderObj.Country__c = warrantyTransfer.country;
                orderObj.StageName = RestWarrantyTransfer.CLOSED_WON;
                orderObj.CloseDate = System.Today();
                if(orderObj.ShipmentDate__c==null){
                    orderObj.ShipmentDate__c = System.Today();
                }
                upsert orderObj Id;

                rmaObj.ReplacementOrder__c = orderObj.Id;
                rmaObj.Replacement_Order_Status__c = 'Shipped';
                update rmaObj;

                assetObj.Line_Number__c = 1;
                assetObj.Order_Opportunity__c = orderObj.Id;
                update assetObj;

                warranty_transfer_response.is_success = true;
            }catch(RestWarrantyTransfer.WarrantyTransferException wt){
                warranty_transfer_response.failure_reason=wt.getMessage();
                warranty_transfer_response.is_success=false;
                Database.rollBack(sp);
            }catch(Exception e){
                warranty_transfer_response.failure_reason=e.getMessage();
                warranty_transfer_response.is_success=false;
                Database.rollBack(sp);
            }
        }
        return warranty_transfer_response;
    }
    global class WarrantyTransferException extends Exception{}
}