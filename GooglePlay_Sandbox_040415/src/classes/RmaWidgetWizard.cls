public with sharing class RmaWidgetWizard 
{
    //url parameter passed for the search
    public String searchParam {get;set;}
    //box on the page for searching
    public String searchText {get;set;}
    public String selectedAssetId {get;set;}
    public Google_Asset__c ga {get;set;}
    public RMA__c rma {get;set;}
    public RMA__c fullRma {get;set;}
    public Date dToday {get;set;}
    public String rmaType {get;set;}    
    public boolean isExceptionRma {get;set;}
    public String jsonString {get;set;}
    public List<Account> accountList {get;set;}
    public List<Google_Asset__c> gaList {get;set;}
    public List<Opportunity> oppList {get;set;}
    String gCasesId;
    Id newRmaId;
    public RmaWidgetWizard()
    {
        searchParam=ApexPages.currentPage().getParameters().get('searchParam');     
        gCasesId=ApexPages.currentPage().getParameters().get('gCasesId');   
        newRmaId=ApexPages.currentPage().getParameters().get('newRmaId');
        if(String.isNotEmpty(newRmaId))
        {
            fullRma=[SELECT Name,Type__c, RMA_Country_Formula__c,
            GoogleAsset__r.Name,GoogleAsset__r.SKU__c,GoogleAsset__r.ProductSKUDescription__c,
            GoogleAsset__r.ProductID__r.Document__r.Unique_ID_Type__c,
            GoogleAsset__r.ProductID__r.RepairPartner__c,GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c,
            Opportunity__r.Address__c,Opportunity__r.City__c,Opportunity__r.State_Province__c,Opportunity__r.PostalCode__c,
            GoogleCustomer__r.Name,RMA_Sub_Category__c,RMA_Category__c,Notes__c,CreatedDate,
            gCases_ID__c,GoogleCustomer__r.PersonMailingStreet,GoogleCustomer__r.PersonMailingCity,
            GoogleCustomer__r.PersonMailingState,GoogleCustomer__r.PersonMailingCountry,GoogleCustomer__r.PersonMailingPostalCode,
            GoogleCustomer__r.PersonEmail,Incomplete__c,GoogleAsset__r.ProductID__r.DOA_Doc_ID__c,
            ServiceModel__c
            FROM RMA__c WHERE Id=:newRmaId];
            
            if(fullRma.Type__c=='Warranty DOA')
                fullRma.GoogleAsset__r.ProductID__r.Finsky_Doc_ID__c=fullRma.GoogleAsset__r.ProductID__r.DOA_Doc_ID__c;
            jsonString = String.escapeSingleQuotes(JSON.serialize(fullRma));
        }   
        dToday=Date.today();
        isExceptionRma=false;
        gaList = new List<Google_Asset__c>();
        accountList = new List<Account>();
        oppList = new List<Opportunity>();
        rmaList = new List<RMA__c>();
    }   
    public PageReference doParameterSearch()
    {       
        String emailPat = '[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+';
        String orderPat='[0-9]*\\.[0-9]*';
        gaList.clear();
        accountList.clear();
        oppList.clear();
        rmaList.clear();
        if(String.isNotEmpty(searchParam))
        {
            if(Pattern.matches(emailPat,searchParam))
            {
                //email search              
                accountList=new List<Account>([Select PersonEmail, Id, (Select Id,Name,Warranty_Expiration_Date__c, Last_Date_DOA__c, Last_Date_Buyer_s_Remorse__c From Google_Assets__r ORDER BY Last_Date_Buyer_s_Remorse__c) From Account WHERE PersonEmail=:searchParam]);
                if(!accountList.isEmpty())
                {
                    if(accountList.size()==1)
                    {
                        if(accountList.get(0).Google_Assets__r!=null && accountList.get(0).Google_Assets__r.size()==1)
                        {
                            selectedAssetId=accountList.get(0).Google_Assets__r.get(0).Id;
                            return goToAsset();
                        }
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Account matching email: '+searchParam));
                    rmaList=null;
                }               
            }
            else if(Pattern.matches(orderPat,searchParam))
            {
                oppList = new List<Opportunity>([Select Id, (Select Id,Name,Warranty_Expiration_Date__c, Last_Date_DOA__c, Last_Date_Buyer_s_Remorse__c From Google_Assets__r) From Opportunity WHERE Name=:searchParam]);
                if(!oppList.isEmpty())
                {
                    if(oppList.size()==1)
                    {
                        if(oppList.get(0).Google_Assets__r!=null && oppList.get(0).Google_Assets__r.size()==1)
                        {
                            selectedAssetId=oppList.get(0).Google_Assets__r.get(0).Id;
                            return goToAsset();
                        }
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Order matching: '+searchParam));
                    rmaList=null;
                }
            }
            else if(searchParam.startsWithIgnoreCase('RMA'))
            {
                rmaList = new List<RMA__c>([SELECT Id,GoogleAsset__r.Id,GoogleAsset__r.Name,GoogleAsset__r.Warranty_Expiration_Date__c, GoogleAsset__r.Last_Date_DOA__c, GoogleAsset__r.Last_Date_Buyer_s_Remorse__c,SKUDescription__c,Name FROM RMA__c WHERE Name=:searchParam]);
                if(!rmaList.isEmpty())
                {
                    if(rmaList.size()==1)
                    {
                        selectedAssetId=rmaList.get(0).GoogleAsset__r.Id;
                        return goToAsset();
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No RMA matching: '+searchParam));
                    rmaList=null;
                }
            }
            else
            {
                gaList = new List<Google_Asset__c>([SELECT Id,Name,Last_Date_Buyer_s_Remorse__c,Last_Date_DOA__c,Warranty_Expiration_Date__c FROM Google_Asset__c WHERE Name=:searchParam]);
                if(!gaList.isEmpty())
                {
                    if(gaList.size()==1)
                    {
                        selectedAssetId=gaList.get(0).Id;
                        return goToAsset();
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Asset matching: '+searchParam));
                    rmaList=null;
                }
            }
        }
        else
        {   
            rmaList=null;
        }
        return null;
    }
    public List<RMA__c> rmaList
    {       
        get
        {
            if(rmaList==null)
            {               
                //DateTime d = System.now().addDays(-2);
                Date d = Date.today();
                DateTime dT=DateTime.newInstance(d.year(), d.month(), d.day(), 0, 0, 0);
                rmaList = new List<RMA__c>([SELECT Id,GoogleAsset__r.Id,GoogleAsset__r.Name,SKUDescription__c,Name FROM RMA__c WHERE CreatedDate>=:dT AND CreatedById=:UserInfo.getUserId() order by CreatedDate desc limit 100]);                              
            }
            return rmaList;
        }
        set;
    }
    public PageReference goToAsset()
    {
        if(String.isNotEmpty(selectedAssetId))
        {
            ga=[SELECT Id,Name,ProductID__r.Name,AssetOwner__c,AssetOwner__r.Name,AssetOwner__r.PersonEmail,Service_Model__c,Remorse_Days_Left__c,Last_Date_Buyer_s_Remorse__c,Last_Date_DOA__c,DOA_Days_Left__c,Warranty_Expiration_Date__c,Warranty_Countdown__c,Order_Opportunity__c,(Select Id, Name From RMA__r WHERE Status__c != 'Closed') FROM Google_Asset__c WHERE Id=:selectedAssetId];
            return Page.RmaWidgetWizard2;
        }
        return null;
    }
    public PageReference goToRma()
    {
        rma = new RMA__c();
        rma.GoogleCustomer__c=ga.AssetOwner__c;
        rma.ServiceModel__c=ga.Service_Model__c;    
        if(rmaType=='BR')
            rmaType='Buyer\'s Remorse'; 
        rma.Type__c=rmaType;
        rma.GoogleAsset__c=ga.Id;
        rma.Opportunity__c=ga.Order_Opportunity__c;
        rma.gCases_ID__c=gCasesId;
        if(rma.Type__c=='Buyer\'s Remorse' && ga.Last_Date_Buyer_s_Remorse__c!=null && ga.Last_Date_Buyer_s_Remorse__c<=dToday)
            isExceptionRma=true;
        else if(rma.Type__c=='Warranty DOA' && ga.Last_Date_DOA__c!=null && ga.Last_Date_DOA__c<=dToday)
            isExceptionRma=true;
        else if(rma.Type__c=='Warranty Regular' && ga.Warranty_Expiration_Date__c!=null && ga.Warranty_Expiration_Date__c<=dToday)
            isExceptionRma=true;
            
        return Page.RmaWidgetWizard3;       
    }
    public PageReference saveRma()
    {
        try
        {
            if(isExceptionRma && !rma.I_understand_Exception_RMAs_are_reviewed__c)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You must accept Exception RMA\'s are reviewed before saving.'));
                return null;
            }
            insert rma;         
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Error: '+e));
            return null;
        }       
        PageReference pr = Page.RmaWidgetWizard4;       
        pr.getParameters().put('newRmaId',rma.Id);
        pr.setRedirect(true);
        return pr;
    }
    public PageReference updateRma()
    {
        try
        {
            update fullRma;         
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Error: '+e));
            return null;
        }       
        PageReference pr = Page.RmaWidgetWizard4;       
        pr.getParameters().put('newRmaId',fullRma.Id);
        pr.setRedirect(true);
        return pr;
    }
    public PageReference cancel()
    {       
        //if(rma!=null && String.isNotEmpty(rma.Id) && )
        PageReference pr = Page.RmaWidgetSearch;        
        pr.setRedirect(true);
        return pr;
    }
}