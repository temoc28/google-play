/**
 * @author:	Levementum LLC
 * @date:	01/21/16
 * @description:	Test Class for RestMarkRMAReceived web service
 */
 @isTest
public class TestRestMarkRMAReceived 
{
	public static RestRequest req;
	public static RestResponse res;

	public static void setup()
	{
		try
		{
			req = new RestRequest();
    		res = new RestResponse();
    			
    		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/MarkRMAReceived';
    		req.httpMethod = 'POST';
    			
    		RestContext.request = req;
    		RestContext.response = res;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
	}
	
    public static testMethod void testNullRMAId()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();

				 tr.createAccount();
				 tr.createGoogleAsset();

  				 tr.createRMA1();
  				 Id rmaId = tr.idsMap.get('RMA1');  
  				 
  				 RestMarkRMAReceived.RestMarkRMARequest request = new RestMarkRMAReceived.RestMarkRMARequest();
				 request.rmaId = null;
				 
				 RestResponseModel.submit_rma_response_t response = RestMarkRMAReceived.markRMAReceived(request); 
				 system.assertEquals(response.is_success, false);
  				   		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testValdidRMAId()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();

				 tr.createAccount();
				 tr.createGoogleAsset();

  				 tr.createRMA1();
  				 String rmaName = tr.namesMap.get('RMA1_NAME');  system.debug('???' + rmaName);
  				 
  				 RestMarkRMAReceived.RestMarkRMARequest request = new RestMarkRMAReceived.RestMarkRMARequest();
				 request.rmaId = rmaName;
				 
				 RestResponseModel.submit_rma_response_t response = RestMarkRMAReceived.markRMAReceived(request); 
				 //system.assertEquals(response.is_success, true);
  				   		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
}