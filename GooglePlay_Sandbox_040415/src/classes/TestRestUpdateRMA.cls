// jescamilla@levementum.com 12/2/14 Description: Test Coverage for RestUpdateRMA.cls.
@isTest
private class TestRestUpdateRMA {
	public static void setUptest()
	{
		Account a = new Account();
		a.FirstName='Test';
		a.LastName='Account';
		a.PersonEmail='Test@test.com';
		a.PersonMailingCountry='GB';
		a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
		insert a;

		Opportunity opp = new Opportunity();
		opp.Name='Opportunity1';
		opp.CloseDate=Date.today();
		opp.StageName='Closed Won';
		opp.AccountId=a.Id;
		opp.Type='Standard Order';
		opp.Country__c='US';
		opp.State_Province__c='TX';
		insert opp;

		Product_Family__c pf = new Product_Family__c(Name='Test');
		insert pf;

		Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
		insert d;

		Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
		insert cv;

		Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
		insert p;


		Google_Asset__c gAsset = new Google_Asset__c();
		gAsset.Name='TestGA';
		gAsset.ProductId__c=p.Id;
		gAsset.SKU__c='SSSS';
		gAsset.Order_Opportunity__c=opp.Id;
		gAsset.Line_Number__c=1;
		gAsset.AssetOwner__c = a.Id;
		gAsset.Retail_Country__c = 'Set by SKU';
		gAsset.ProductId__c=p.Id;
		insert gAsset;
	}
	@isTest static void doUpdateNewRma() {
		TestRestUpdateRMA.setUptest();
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Google_Asset__c gAsset = [SELECT Id, Name FROM Google_Asset__c WHERE Name = 'TestGA' LIMIT 1];


		RMA__c rma = new RMA__c();
		rma.Status__c='Pending Return';
		rma.Triage_Code__c='C';
		rma.Customer_Induced_Damage__c='Y';
		rma.Type__c='Buyer\'s Remorse';
		rma.Opportunity__c = opp.Id;
		rma.GoogleAsset__c=gAsset.Id;
		rma.RMA_Category__c='No Longer Wants product';
		rma.RMA_Sub_Category__c='Test';
		rma.Notes__c='Test';
		insert rma;
		rma = ([SELECT Status__c, Triage_Code__c, Customer_Induced_Damage__c, Name, Type__c, Opportunity__c, GoogleAsset__c, RMA_Category__c, RMA_Sub_Category__c, Notes__c, Id FROM RMA__c WHERE Id =: rma.Id ]);
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		rma_request_details.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		rma_request_details.rma_id = rma.Name;
		rma_request_details.replacement_cart_link = 'replacement_cart_link';
		rma_request_details.replacement_gpn = 'replacement_gpn';
		rma_request_details.rma_requester_first_name = 'John';
		rma_request_details.rma_requester_last_name = 'Wick';
		rma_request_details.rma_requester_address = new RestResponseModel.address_t();
		rma_request_details.rma_requester_address.address_state_province = 'TX';
		rma_request_details.rma_requester_address.address_country = 'US';
		rma_request_details.rma_requester_address.address_postal_code = '79938';
		rma_request_details.rma_requester_address.address_street = '1092 main';
		rma_request_details.rma_requester_address.address_city = 'phoenix';

		response = RestUpdateRMA.doUpdateRMA(rma_request_details);//Call with no matching Account

		//BEGIN TEST FOR RestGetDeviceFamily.cls
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelRMA';
		rReq.httpMethod = 'GET';

		RestContext.request = rReq;
		RestResponseModel.device_types_t  response_device_types = RestGetDeviceFamily.getDeviceFamily();//Test with Null Values
		//END TEST FOR RestGetDeviceFamily.cls
		//assertion checking for false positives:
		Account acctForAssert = ([SELECT Id, PersonMailingStreet, PersonMailingCity FROM Account WHERE PersonMailingStreet =: rma_request_details.rma_requester_address.address_street ]);
		System.assertNotEquals(acctForAssert, null);
		System.assertEquals(acctForAssert.PersonMailingCity, rma_request_details.rma_requester_address.address_city);
	}
	@isTest static void doUpdateWarrantyRMA() {
		TestRestUpdateRMA.setUptest();
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Google_Asset__c gAsset = [SELECT Id FROM Google_Asset__c WHERE Name = 'TestGA' LIMIT 1];
		RMA__c rma = new RMA__c();
		rma.Status__c='Pending Return';
		rma.Triage_Code__c='C';
		rma.Customer_Induced_Damage__c='Y';
		rma.Type__c='Buyer\'s Remorse';
		rma.Opportunity__c = opp.Id;
		rma.GoogleAsset__c=gAsset.Id;
		rma.RMA_Category__c='No Longer Wants product';
		rma.RMA_Sub_Category__c='Test';
		rma.Notes__c='Test';
		insert rma;
		rma = ([SELECT Status__c, Triage_Code__c, Customer_Induced_Damage__c, Name, Type__c, Opportunity__c, GoogleAsset__c, RMA_Category__c, RMA_Sub_Category__c, Notes__c, Id FROM RMA__c WHERE Id =: rma.Id ]);
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		rma_request_details.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		rma_request_details.rma_id = testRestUtils.namesMap.get('RMA1_NAME');
		rma_request_details.replacement_cart_link = 'replacement_cart_link';
		rma_request_details.replacement_gpn = 'replacement_gpn';
		rma_request_details.rma_requester_first_name = 'John';
		rma_request_details.rma_requester_last_name = 'Wick';
		rma_request_details.rma_requester_address = new RestResponseModel.address_t();
		rma_request_details.rma_requester_address.address_state_province = 'TX';
		rma_request_details.rma_requester_address.address_country = 'US';
		rma_request_details.rma_requester_address.address_postal_code = '79938';
		rma_request_details.rma_requester_address.address_street = '1092 main';
		rma_request_details.rma_requester_address.address_city = 'phoenix';

		response = RestUpdateRMA.doUpdateRMA(rma_request_details);//Call with no matching Account
		//assertion checking for false positives:
		List<Account> testAccount = new List<Account>();
		testAccount = ([SELECT Id, PersonMailingStreet, PersonMailingCity FROM Account WHERE PersonMailingStreet =: rma_request_details.rma_requester_address.address_street ]);
		//this one doesnt suppose to create an account
		System.assertEquals(testAccount.size() == 0, true);
	}
	@isTest static void doUpdateWarrantyAlreadyHaveRMA() {
		TestRestUpdateRMA.setUptest();
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Google_Asset__c gAsset = [SELECT Id, AssetOwner__c FROM Google_Asset__c WHERE Name = 'TestGA' LIMIT 1];

		RMA__c rma = new RMA__c();
		rma.Status__c='Pending Return';
		rma.Triage_Code__c='C';
		rma.Customer_Induced_Damage__c='Y';
		rma.Type__c='Buyer\'s Remorse';
		rma.Opportunity__c = opp.Id;
		rma.GoogleAsset__c=gAsset.Id;
		rma.RMA_Category__c='No Longer Wants product';
		rma.RMA_Sub_Category__c='Test';
		rma.Notes__c='Test';
		rma.GoogleCustomer__c = gAsset.AssetOwner__c;
		insert rma;
		rma = ([SELECT Status__c, Triage_Code__c, Customer_Induced_Damage__c, Name, Type__c, Opportunity__c, GoogleAsset__c, RMA_Category__c, RMA_Sub_Category__c, Notes__c, Id FROM RMA__c WHERE Id =: rma.Id ]);
		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();

		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
		rReq.httpMethod = 'POST';

		RestContext.request = rReq;
		RestContext.response = rRes;

		rma_request_details.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
		rma_request_details.rma_id = rma.Name;
		rma_request_details.replacement_cart_link = 'replacement_cart_link';
		rma_request_details.replacement_gpn = 'replacement_gpn';
		rma_request_details.rma_requester_first_name = 'John';
		rma_request_details.rma_requester_last_name = 'Wick';
		rma_request_details.rma_requester_address = new RestResponseModel.address_t();
		rma_request_details.rma_requester_address.address_state_province = 'TX';
		rma_request_details.rma_requester_address.address_country = 'US';
		rma_request_details.rma_requester_address.address_postal_code = '79938';
		rma_request_details.rma_requester_address.address_street = '1092 main';
		rma_request_details.rma_requester_address.address_city = 'phoenix';

		response = RestUpdateRMA.doUpdateRMA(rma_request_details);//Call with no matching Account
		//assertion checking for false positives:it should failed this method forces entering a catch and rolllback datrabase
		System.assertEquals(response.is_success, false);
	}
}