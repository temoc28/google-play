public with sharing class BizAppsHelper 
{
	private static boolean processed = false;
	public static Set<Id> processedRmaIdSet 
	{
		get
		{
			if(processedRmaIdSet==null)
				processedRmaIdSet = new Set<Id>();
			return processedRmaIdSet;
		}
	}
	public static boolean hasProcessed() 
	{
        return processed;
    }    
    public static void setProcessed() 
    {
        processed = true;
    }
}