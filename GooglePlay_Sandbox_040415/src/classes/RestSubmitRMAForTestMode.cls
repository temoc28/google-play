/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/17/15
 * @email:	cmunoz@levementum.com
 * @description:	Expose REST services for RMAs to the Cases Testing Team at Google Inc.
 */
 @RestResource(urlMapping='/SubmitRMATestMode/*')
 global class RestSubmitRMATestMode
 {
		@HttpPost
	    global static RestResponseModel.submit_rma_response_t doSubmitRMAs(RestResponseModel.rma_initiation_details_t requestDetails)
	    {
	    	RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
	    	try
	    	{
		        List<Google_Asset__c> gaList = new List<Google_Asset__c>([SELECT Id,RecordType.Name,Order_Opportunity__c,Line_Number__c FROM Google_Asset__c WHERE Name=:requestDetails.unique_id]);
		        
		        boolean queryOpp=true;
		        Google_Asset__c ga;
		        Id oppId;
		        if(gaList.size()!=1)
		        {
		            submit_rma_response.failure_reason='Device not found';
		            return submit_rma_response;
		        }
		        else 
		        {
		            ga = gaList.get(0);
		            oppId=ga.Order_Opportunity__c;
		            if(ga.RecordType.Name=='Google Play Asset' && oppId==null)
		            {
		                submit_rma_response.failure_reason='This is a Google Play Asset but there is not a related order.';
		                submit_rma_response.is_success=false;
		                return submit_rma_response;
		            }
		        }

		        List<Account> aList = new List<Account>([SELECT Id FROM Account WHERE PersonEmail=:requestDetails.rma_requester_email]);
		        if(aList.size()!=1)
		        {
		            submit_rma_response.failure_reason='Customer not found';
		            submit_rma_response.is_success=false;
		            return submit_rma_response;
		        }
   
		        String ownerId;
		        List<User> uList = new List<User>([SELECT Id FROM User WHERE IsActive=true AND LDAP__c=:requestDetails.rma_creator]);
		        
		        if(uList.size()==1)
		            ownerId=uList.get(0).Id;
		        if(ownerId==null)
		        {
		            submit_rma_response.failure_reason='No user found for '+requestDetails.rma_creator;
		            submit_rma_response.is_success=false;
		            return submit_rma_response;
		        }
		        System.savePoint sp;
		        try
		        {
		            //validation against line number to update          
		            if(ga.RecordType.Name!='Retail Asset' && ga.Line_Number__c==null && (requestDetails.rma_type=='Buyer\'s Remorse'||requestDetails.rma_type=='Warranty Refund'))
		            {
		                ga.Line_Number__c=0;               
		                update ga;              
		            }
		            RMA__c rma = new RMA__c();
		            rma.GoogleCustomer__c=aList[0].Id;
		            rma.Opportunity__c=oppId;
		            rma.GoogleAsset__c=gaList[0].Id;
		            rma.Type__c=requestDetails.rma_type;
		            rma.RMA_Category__c=requestDetails.rma_category;
		            rma.RMA_Sub_Category__c=requestDetails.rma_sub_category;
		            rma.Notes__c=requestDetails.rma_notes;
		            rma.gCases_ID__c=requestDetails.case_id;
		            rma.OwnerId=ownerId;
		            rma.RMA_Action__c=requestDetails.rma_action;
		            rma.RMA_Country_Workflow__c=requestDetails.rma_requester_address.address_country;
		            sp=Database.setSavePoint();
		            insert rma;

		            rma = [SELECT Name,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,Type__c,RMA_Action__c,GoogleAsset__c FROM RMA__c WHERE Id=:rma.Id];

		            submit_rma_response.rma_success_details = new RmaDetailsBuilder()
		                    .setCV(rma.RMA_Country_Workflow__c,rma.GoogleAsset__r.ProductID__r.Document__c)
		                    .setRMA(rma)
		                    .stampParameter()
		                    .getRMASuccessDetail();
		
		            submit_rma_response.rma_success_details.rma_id=rma.Name;

		            Account a = aList.get(0);           
		            a.FirstName=requestDetails.rma_requester_first_name;
		            a.LastName=requestDetails.rma_requester_last_name;
		            a.PersonMailingStreet=requestDetails.rma_requester_address.address_street;
		            a.PersonMailingCity=requestDetails.rma_requester_address.address_city;
		            a.PersonMailingState=requestDetails.rma_requester_address.address_state_province;
		            a.PersonMailingCountry=requestDetails.rma_requester_address.address_country;
		            a.PersonMailingPostalCode=requestDetails.rma_requester_address.address_postal_code;
		            //Date rmaCreateDate = null;
		            //Datetime exceptionDateTime = null;
		            //if(requestDetails.rma_creation_date != null && String.valueOf(requestDetails.rma_creation_date) != ''){
		            //    List<String> dateSplit = String.valueOf(requestDetails.rma_creation_date).split('-');
		            //    rmaCreateDate = Date.parse(dateSplit[2]+'/'+dateSplit[1]+'/'+dateSplit[0]);
		            //}
		            //if(a.CID_Exception_Granted_Date_Time__c!=null){
		            //    exceptionDateTime = a.CID_Exception_Granted_Date_Time__c;
		            //}
		            //if(requestDetails.rma_category=='1x Exception' && rmaCreateDate != null && (exceptionDateTime==null || exceptionDateTime.date()<rmaCreateDate)){
		            //    a.CID_Exception_Granted__c=true;
		            //    a.CID_Exception_Granted_Asset__c=rma.GoogleAsset__c;
		            //    a.CID_Exception_Granted_Date_Time__c=datetime.now();
		            //    a.CID_Exception_on_This_Asset__c=requestDetails.rma_category;
		            //}
		            update a;
		            submit_rma_response.is_success=true;
		        }
		        catch(Exception e)
		        {
		            submit_rma_response.failure_reason=e.getMessage();
		            submit_rma_response.is_success=false;
		            submit_rma_response.rma_success_details=null;
		            Database.rollBack(sp);
		        }
		    }
		    catch(Exception ex)
		    {
		    	system.debug(ex.getMessage());
		    }
	        return submit_rma_response;
	    }
}