@isTest
private class TestRestCreateExtendedWarranty {
    static testmethod void noCriteriaMatch(){
    	RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.extended_warranty_request_detail_t extendedDetail = new RestResponseModel.extended_warranty_request_detail_t();

        RestResponseModel.extended_warranty_response_t result = RestCreateExtendedWarranty.createExtendedWarranty(extendedDetail);
        System.assert(result != null);
        System.assert(!result.is_success);
        String expectedAssertId = '1111';
        String expectedOrderId = '2222';
        String expectedContractId = '3333';
        Date expectedExpireDate = System.Today()+10;
        String expectedLineNumber = '4444';
        String expectedSKU = '5555';
        String expectedCountry = 'US';

        extendedDetail.asset_id = expectedAssertId;
		extendedDetail.order_id = expectedOrderId;
		extendedDetail.contract_id = expectedContractId;
		extendedDetail.warranty_expiration_date = expectedExpireDate;
		extendedDetail.line_number = expectedLineNumber;
		extendedDetail.sku = expectedSKU;
		extendedDetail.country = null;
		
		RestResponseModel.extended_warranty_response_t result2 = RestCreateExtendedWarranty.createExtendedWarranty(extendedDetail);

		System.assert(result2 != null);
        System.assert(result2.is_success);

		Opportunity resultOpportunity = [SELECT Id,Name,StageName,CloseDate,Country__c FROM Opportunity LIMIT 1];
		
		Google_Asset__c resultAsset = [SELECT Id,Extended_Warranty__c,Name,SKU__c FROM Google_Asset__c LIMIT 1];

		Extended_Warranty__c resultExtendedWarranty = [SELECT Id,Name,Line_Number__c,Current_Active_Asset__c,Google_Order__c,Expiration_Date__c FROM Extended_Warranty__c LIMIT 1];

		System.assert(resultOpportunity!=null);
		System.assert(resultExtendedWarranty!=null);
		System.assert(resultAsset!=null);

		System.assertEquals(expectedOrderId,resultOpportunity.Name);
		System.assertEquals(RestCreateExtendedWarranty.CLOSED_WON,resultOpportunity.StageName);
		System.assertEquals(System.Today(),resultOpportunity.CloseDate);

		System.assertEquals(resultExtendedWarranty.Id,resultAsset.Extended_Warranty__c);
		System.assertEquals(expectedAssertId,resultAsset.Name);
		System.assertEquals(expectedSKU,resultAsset.SKU__c);

		System.assertEquals(expectedContractId,resultExtendedWarranty.Name);
		System.assertEquals(expectedLineNumber,String.valueOf(resultExtendedWarranty.Line_Number__c));
		System.assertEquals(resultAsset.Id,resultExtendedWarranty.Current_Active_Asset__c);
		System.assertEquals(resultOpportunity.Id,resultExtendedWarranty.Google_Order__c);
		System.assertEquals(expectedExpireDate,resultExtendedWarranty.Expiration_Date__c);
		System.assertEquals(expectedCountry,resultOpportunity.Country__c);
    }
    static testmethod void duplicateWarranty(){
    	RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.extended_warranty_request_detail_t extendedDetail = new RestResponseModel.extended_warranty_request_detail_t();

        RestResponseModel.extended_warranty_response_t result = RestCreateExtendedWarranty.createExtendedWarranty(extendedDetail);
        System.assert(result != null);
        System.assert(!result.is_success);
        String expectedAssertId = '1111';
        String expectedOrderId = '2222';
        String expectedContractId = '3333';
        Date expectedExpireDate = System.Today()+10;
        String expectedLineNumber = '4444';
        String expectedSKU = '5555';

        insert new Extended_Warranty__c(
        	Name = expectedContractId,
			Line_Number__c = Decimal.valueOf(expectedLineNumber),
			Expiration_Date__c = expectedExpireDate
        );

        extendedDetail.asset_id = expectedAssertId;
		extendedDetail.order_id = expectedOrderId;
		extendedDetail.contract_id = expectedContractId;
		extendedDetail.warranty_expiration_date = expectedExpireDate;
		extendedDetail.line_number = expectedLineNumber;
		extendedDetail.sku = expectedSKU;
		
		RestResponseModel.extended_warranty_response_t result2 = RestCreateExtendedWarranty.createExtendedWarranty(extendedDetail);

		System.assert(result2 != null);
        System.assert(!result2.is_success);
        System.assert(result2.extended_warranty_detail!=null);

		System.assertEquals(expectedContractId,result2.extended_warranty_detail.contract_id);
		System.assertEquals(expectedLineNumber,String.valueOf(result2.extended_warranty_detail.line_number));
		System.assertEquals(expectedExpireDate,result2.extended_warranty_detail.warranty_expiration_date);

		List<Opportunity> resultOpportunity = new List<Opportunity>();
		resultOpportunity = [SELECT Id,Name,StageName,CloseDate FROM Opportunity LIMIT 1];
		
		List<Google_Asset__c> resultAsset = new List<Google_Asset__c>();
		resultAsset = [SELECT Id,Extended_Warranty__c,Name,SKU__c FROM Google_Asset__c LIMIT 1];

		List<Extended_Warranty__c> resultExtendedWarranty = new List<Extended_Warranty__c>();
		resultExtendedWarranty = [SELECT Id,Name,Line_Number__c,Current_Active_Asset__c,Google_Order__c,Expiration_Date__c FROM Extended_Warranty__c LIMIT 1];

		System.assert(resultOpportunity.size() == 0);
		System.assert(resultExtendedWarranty.size() == 1);
		System.assert(resultAsset.size() == 0);

		System.assertEquals(expectedContractId,resultExtendedWarranty.get(0).Name);
		System.assertEquals(expectedLineNumber,String.valueOf(resultExtendedWarranty.get(0).Line_Number__c));
		System.assertEquals(expectedExpireDate,resultExtendedWarranty.get(0).Expiration_Date__c);
    }
}