public class CopyRMAIdentifier
{
	public static void UpdateValues(List<RMA__c> TNew)
	{
		/*Set<Id> RMAIds = new Set<Id>();
		for(RMA__c RMA: TNew)
			RMAIds.add(RMA.Id);
		
		List<RMA__c> NewRMAs = [SELECT Id, Name, RMA_ID_External_ID__c FROM RMA__c WHERE Id IN: RMAIds];
		for(RMA__c R : NewRMAs)
			R.RMA_ID_External_ID__c = R.Name;*/
		List<RMA__c> rmaList = new List<RMA__c>();
		for(RMA__c rma : TNew)
			rmaList.add(new RMA__c(Id=rma.Id,RMA_ID_External_ID__c=rma.Name));
		update rmaList;
		//update NewRMAs;
	}
	
	@isTest
    static void myUnitTest() {
 		Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c(
            Name = 'CHromecaseTest');
        insert codeSet;

        Product_Family__c prodFam = new Product_Family__c(Name = 'Chromecast Test');
        insert prodFam;

        Document__c doc = new Document__c(
            Name = 'Chromecast Test'
            ,Product_Family__c = prodFam.Id
            ,Return_Reason_Code_Set__c = codeSet.Id);
        insert doc;

        Product2 prod = new Product2(Name = 'Chromecast US Test'
            ,SKU__c = '86002596-01-Test'
            ,Sale_Country__c = 'US'
            ,Unique_ID_Type__c = 'Serial Number'
            ,RMARecordType__c = 'Chromecast'
            ,Document__c = doc.Id);
        insert prod;

		Google_Asset__c ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
        										, Asset_Type__c = 'New'
        										, Active_Device__c = 'Active'
        										,ProductID__c = prod.Id);
		insert ga;
        RMA__c rma = new RMA__c (GoogleAsset__c = ga.Id);        
        insert rma;        
    }
}