/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/13/15
 * @email:	cmunoz@levementum.com
 * @description:	Created REST services to expose Google Assets to Google Cases Testing Team
 */
@RestResource(urlMapping='/GoogleAssetTester/*')
global class RESTESDELETEAFTER
{
	@HttpPost
    global static GoogleAssetModel updateGoogleAsset(GoogleAssetModel googleAssetModel) 
    {
    	GoogleAssetModel response = new GoogleAssetModel();
    	try
    	{
    		if(googleAssetModel != null)
    		{
    			if(googleAssetModel.Name != null)
    			{
    				List<Google_Asset__c> currentGoogleAsset = GoogleAssetDataAccess.selectGoogleAsset(googleAssetModel.Name);  				
    				GoogleAssetDataAccess.updateGoogleAsset(currentGoogleAsset, googleAssetModel);
    				List<Google_Asset__c> updatedGoogleAssets = GoogleAssetDataAccess.selectGoogleAsset(googleAssetModel.Name); 
    				response = updateModel(updatedGoogleAssets);
    			}
    		}
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    		response.Status = GoogleAssetDataAccess.objectStatus;
    	}
        return response;
    }

    private static GoogleAssetModel updateModel(List<Google_Asset__c> updatedGoogleAssets)
    {
    	GoogleAssetModel gam = new GoogleAssetModel();
    	try
    	{
    		for(Google_Asset__c ga : updatedGoogleAssets)
    		{
    			gam.warranty_countries_legacy = ga.warranty_countries_legacy__c;
				gam.warranty_period = ga.warranty_period__c;
				gam.warranty_expiration_date = ga.warranty_expiration_date__c;
    		}	
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	return gam;
    }
}