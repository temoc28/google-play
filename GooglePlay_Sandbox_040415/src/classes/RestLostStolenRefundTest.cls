@isTest
private class RestLostStolenRefundTest {

	@isTest static void RestLostAndStolen() {
		// Create test Data needed:
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createOpportunity();
		Opportunity testOpp = new Opportunity();
		testOpp = ([SELECT AccountId, OrderIDExternalID__c, TransactionDate__c, Country__c, State_Province__c, Name, Type, CloseDate, StageName, Ship_By_Date__c
			FROM Opportunity WHERE Id =: utils.idsMap.get('Opportunity1')]);
		testOpp.Ship_By_Date__c = Date.today();
		update testOpp;
		Test.startTest();
		RestLostStolenRefund.LostStolenRMARequest refund = new RestLostStolenRefund.LostStolenRMARequest();
		refund.orderId = testOpp.Name;
		refund.notes = 'refund claim';
		RestResponseModel.lostStolenRefundResponse testResponse = RestLostStolenRefund.createLostStolenRMA(refund);
		Test.stopTest();
		System.assertEquals(refund.orderId, testOpp.Name);
		//To assert that a refund was created:
		System.assertNotEquals(testResponse.refund_id, null);
		//To assert there was no failure;
		System.assertEquals(testResponse.failure_reason, null);
		//Retrive the created refund from database and assert to make sure it created the one that it suppose to:
		Refund__c refundToAssert = ([SELECT OrderNumber__c FROM Refund__c WHERE OrderNumber__c =: testOpp.Id]);
		System.assertEquals(refundToAssert.OrderNumber__c, testOpp.Id);
	}
}