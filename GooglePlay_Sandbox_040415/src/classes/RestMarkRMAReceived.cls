/**
* @author:	Levementum LLC
* @description:	  POST API service that accepts an optional reason category and optional reason string, 
				  and agent ldap, and marks an RMA as Received.
*/
@RestResource(urlMapping='/MarkRMAReceived/*')
global class RestMarkRMAReceived 
{
	/**
	 * @description:	Request Model for markRMAReceived()
	 */
	 global class RestMarkRMARequest
	 {
		global String rmaId;
		global String reason_category;
		global String reason;
		global String agent_ldap;
	 }

	/**
	 * @description:	Marks an RMA as received based on the following params:
	 * @params:			rmaId, reason_category, reason, and agent_ldap. All these parameters
	 *					are optional except rmaId.
	 */
	 @HttpPost
	 global static RestResponseModel.submit_rma_response_t markRMAReceived(RestMarkRMARequest markRMAReceived)
	 {
		RestResponseModel.submit_rma_response_t response = new RestResponseModel.submit_rma_response_t();
		try 
		{	        		
			if(markRMAReceived != null && markRMAReceived.rmaId != null && String.isNotEmpty(markRMAReceived.rmaId))
			{
				String rmaId= markRMAReceived.rmaId;
				List<RMA__c> rmas = new List<RMA__c>([SELECT Id, RMA_Category__c, RCL_Verify_ID__c, Status__c  FROM RMA__c WHERE Name=:rmaId]);
				if(rmas.size() > 0)
				{
					RMA__c rma = rmas.get(0);
					if(rma.Status__c != 'Pending Return')
					{
						response.failure_reason = 'You can only mark an RMA received, if it is Pending Return.';
						response.is_success = false;
						return response;
					}
					rma.MarkedReceivedBy__c = UserInfo.getUserId();
					rma.Status__c = 'Received';
					rma.RMAReceivedReason__c = rma.MarkRMAReceivedReason__c;	
					rma.ManuallyReceivedBy__c = UserInfo.getUserId();	
					update rma;
					response.is_success = true;
				}
				else
				{
					response.failure_reason = 'No RMA was found for: ' + rmaId;
					response.is_success = false;
				}
			}
			else
			{
				response.failure_reason = 'rmaId parameter was not passed';
				response.is_success = false;
			}
			return response;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
		return response;
	 }
}