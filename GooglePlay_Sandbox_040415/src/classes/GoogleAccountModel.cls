/*
 * @author:	Levementum LLC
 * @date:	04/12/15
 * @description:	Google account model based on both Account standard object
 * and custom fields that may be added in Sandbox instance. Update this model
 * as necessary if new fields are added.
 * @update:	04/24/15. Added all remainig fields for Account
 */
global class GoogleAccountModel extends ObjectModel
{
	public id MasterRecordId {get; set;}// Reference field type in Salesforce
	public string Name {get; set;}
	public string LastName {get; set;}
	public string FirstName {get; set;}
	public String Salutation {get; set;}// PickList field type in Salesforce
	public String Type {get; set;}// PickList field type in Salesforce
	public id RecordTypeId {get; set;}// Reference field type in Salesforce
	public id ParentId {get; set;}// Reference field type in Salesforce
	public String BillingStreet {get; set;}// TextArea field type in Salesforce
	public string BillingCity {get; set;}
	public string BillingState {get; set;}
	public string BillingPostalCode {get; set;}
	public string BillingCountry {get; set;}
	public double BillingLatitude {get; set;}
	public double BillingLongitude {get; set;}
	public String ShippingStreet {get; set;}// TextArea field type in Salesforce
	public string ShippingCity {get; set;}
	public string ShippingState {get; set;}
	public string ShippingPostalCode {get; set;}
	public string ShippingCountry {get; set;}
	public double ShippingLatitude {get; set;}
	public double ShippingLongitude {get; set;}
	public String Phone {get; set;}// Phone field type in Salesforce
	public String Fax {get; set;}// Phone field type in Salesforce
	public string AccountNumber {get; set;}
	public String Website {get; set;}// URL field type in Salesforce
	public string Sic {get; set;}
	public String Industry {get; set;}// PickList field type in Salesforce
	public Decimal AnnualRevenue {get; set;}// Currency field type in Salesforce
	public Integer NumberOfEmployees {get; set;}// AnyType field type in Salesforce
	public String Ownership {get; set;}// PickList field type in Salesforce
	public string TickerSymbol {get; set;}
	public String Description {get; set;}// TextArea field type in Salesforce
	public String Rating {get; set;}// PickList field type in Salesforce
	public string Site {get; set;}
	public id OwnerId {get; set;}
	public date LastActivityDate {get; set;}
	public datetime LastViewedDate {get; set;}
	public datetime LastReferencedDate {get; set;}
	public id PersonContactId {get; set;}// Reference field type in Salesforce
	public boolean IsPersonAccount {get; set;}
	public String PersonMailingStreet {get; set;}// TextArea field type in Salesforce
	public string PersonMailingCity {get; set;}
	public string PersonMailingState {get; set;}
	public string PersonMailingPostalCode {get; set;}
	public string PersonMailingCountry {get; set;}
	public double PersonMailingLatitude {get; set;}
	public double PersonMailingLongitude {get; set;}
	public String PersonOtherStreet {get; set;}// TextArea field type in Salesforce
	public string PersonOtherCity {get; set;}
	public string PersonOtherState {get; set;}
	public string PersonOtherPostalCode {get; set;}
	public string PersonOtherCountry {get; set;}
	public double PersonOtherLatitude {get; set;}
	public double PersonOtherLongitude {get; set;}
	public String PersonMobilePhone {get; set;}// Phone field type in Salesforce
	public String PersonHomePhone {get; set;}// Phone field type in Salesforce
	public String PersonOtherPhone {get; set;}// Phone field type in Salesforce
	public String PersonAssistantPhone {get; set;}// Phone field type in Salesforce
	public String PersonEmail {get; set;}// Email field type in Salesforce
	public string PersonTitle {get; set;}
	public string PersonDepartment {get; set;}
	public string PersonAssistantName {get; set;}
	public String PersonLeadSource {get; set;}// PickList field type in Salesforce
	public date PersonBirthdate {get; set;}
	public datetime PersonLastCURequestDate {get; set;}
	public datetime PersonLastCUUpdateDate {get; set;}
	public string PersonEmailBouncedReason {get; set;}
	public datetime PersonEmailBouncedDate {get; set;}
	public string Jigsaw {get; set;}
	public string JigsawCompanyId {get; set;}
	public String AccountSource {get; set;}// PickList field type in Salesforce
	public string SicDesc {get; set;}
	public string Last_Name {get; set;}
	public string First_Name {get; set;}
	public String Mobile {get; set;}// Phone field type in Salesforce
	public String Other_Phone {get; set;}// Phone field type in Salesforce
	public string LegacySFContactID {get; set;}
	public string PersonAcctIDExternalID {get; set;}
	public boolean MIgration {get; set;}
	public boolean DupFlag {get; set;}
	public id PersonAcctContactLookup {get; set;}// Reference field type in Salesforce
	public string Customer_Country {get; set;}
	public boolean CID_Exception_Granted {get; set;}
	public datetime CID_Exception_Granted_Date_Time {get; set;}
	public string CID_Exception_on_This_Asset {get; set;}
	public id CID_Exception_Granted_Asset {get; set;}// Reference field type in Salesforce
	public string CID_Exception_Granted_Asset_Description {get; set;}
	public string CID_Exception_Granted_Asset_Name {get; set;}
	public boolean Primary_Email_pc {get; set;}
	public String Notes_pc {get; set;}// TextArea field type in Salesforce
	public string Email_pc {get; set;}
	public string OtherEmail_pc {get; set;}
	public boolean Migration_pc {get; set;}
	
	public String status {get; set;}
}