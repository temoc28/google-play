/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/13/15
 * @email:	cmunoz@levementum.com
 * @description:	Created REST services to expose Google Assets to Google Cases Testing Team
 */
@RestResource(urlMapping='/GoogleAsset/*')
global class RestGoogleAsset
{
	@HttpPost
    global static GoogleAssetModel updateGoogleAsset(GoogleAssetModel googleAssetModel) 
    {
    	GoogleAssetModel response = new GoogleAssetModel();
    	try
    	{
    		if(googleAssetModel != null)
    		{
    			List<Google_Asset__c> currentGoogleAsset = new List<Google_Asset__c>();
    			List<Google_Asset__c> updatedGoogleAssets = new List<Google_Asset__c>();
    			if(googleAssetModel.Id != null)
				{
					currentGoogleAsset = GoogleAssetDataAccess.selectGoogleAsset(googleAssetModel.Id); 
					Id assetId = GoogleAssetDataAccess.updateGoogleAsset(currentGoogleAsset, googleAssetModel).get(0).Id;
    				updatedGoogleAssets = GoogleAssetDataAccess.selectGoogleAsset(assetId);
				}
				else
				{
					currentGoogleAsset = GoogleAssetDataAccess.selectGoogleAsset(googleAssetModel.Name); 
					String name = GoogleAssetDataAccess.updateGoogleAsset(currentGoogleAsset, googleAssetModel).get(0).Name;
    				updatedGoogleAssets = GoogleAssetDataAccess.selectGoogleAsset(name);
				}	 				
    			response = updateModel(updatedGoogleAssets);
    		}
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	response.Status = GoogleAssetDataAccess.objectStatus;
        return response;
    }
	
	@HttpGet
    global static GoogleAssetModel retrieveGoogleAsset()
    {
    	GoogleAssetModel response = null;
    	try
    	{
    		Map<String, String> paramMap = RestContext.request.params;
			
			String assetName = paramMap.get('assetName');
			String assetId = paramMap.get('id');

			List<Google_Asset__c> currentGoogleAssets = new List<Google_Asset__c>();
			if(assetName != null)
			{
				currentGoogleAssets = GoogleAssetDataAccess.selectGoogleAsset(assetName);			
			}
			else if(assetId != null)
			{
				Id rawId = RestServiceContext.getId(assetId);
				if(rawId != null)
				{
					currentGoogleAssets = GoogleAssetDataAccess.selectGoogleAsset(rawId);	
				}
			}
			response = updateModel(currentGoogleAssets);
    	}		
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' Line: ' + ex.getLineNumber());
    	}
    	if(response.status != null)
    	{
    		response.Status = GoogleAssetDataAccess.objectStatus;
    	}
        return response;
    }
    
    @HttpDelete
	global static String deleteObject()
	{
		String status = 'Delete succeeded';
		try
		{
    		Map<String, String> paramMap = RestContext.request.params;
			String assetName = paramMap.get('assetId');	
			GoogleAssetDataAccess.deleteObject(assetName);
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			status = ex.getMessage();
		}
		return status;
	}
    
    private static GoogleAssetModel updateModel(List<Google_Asset__c> updatedGoogleAssets)
    {
    	GoogleAssetModel gam = new GoogleAssetModel();
    	try
    	{
    		for(Google_Asset__c ga : updatedGoogleAssets)
    		{
    			if(ga != null && gam != null)
    			{
					gam.Id = ga.Id;
					gam.OwnerId = ga.OwnerId;
					gam.Name = ga.Name;
					gam.RecordTypeId = ga.RecordTypeId;
					gam.CreatedDate = ga.CreatedDate;
					gam.CreatedById = ga.CreatedById;
					gam.LastModifiedDate = ga.LastModifiedDate;
					gam.LastModifiedById = ga.LastModifiedById;
					gam.OrderLegacy = ga.OrderLegacy__c;
					gam.Purchaser_Email = ga.Purchaser_Email__c;
					gam.Serial = ga.Serial__c;
					gam.Manufacture_Date = ga.Manufacture_Date__c;
					gam.Warranty_Expiration_Date = ga.Warranty_Expiration_Date__c;
					gam.Sale_Date = ga.Sale_Date__c;
					gam.IMEI = ga.IMEI__c;
					gam.SKU = ga.SKU__c;
					gam.Last_Date_DOA = ga.Last_Date_DOA__c;
					gam.Last_Date_Spare_Parts = ga.Last_Date_Spare_Parts__c;
					gam.Last_Date_Buyer_s_Remorse = ga.Last_Date_Buyer_s_Remorse__c;
					gam.Active_Device = ga.Active_Device__c;
					gam.Asset_Type = ga.Asset_Type__c;
					gam.Unique_ID_Type = ga.Unique_ID_Type__c;
					gam.SKU_Description = ga.SKU_Description__c;
					gam.Repair_Partner_Legacy = ga.Repair_Partner_Legacy__c;
					gam.Warranty_Countries_Legacy = ga.Warranty_Countries_Legacy__c;
					gam.bricking = ga.bricking__c;
					gam.Tracking_Number = ga.Tracking_Number__c;
					gam.Google_Asset_ID_External_ID = ga.Google_Asset_ID_External_ID__c;
					gam.cssn = ga.cssn__c;
					gam.country_sold = ga.country_sold__c;
					gam.Buyers_Remorse_destination = ga.Buyers_Remorse_destination__c;
					gam.ProductID = ga.ProductID__c;
					gam.AssetOwner = ga.AssetOwner__c;
					gam.Chrome_S_N = ga.Chrome_S_N__c;
					gam.Order_Opportunity = ga.Order_Opportunity__c;
					gam.ProductSKUDescription = ga.ProductSKUDescription__c;
					gam.ProductSKU = ga.ProductSKU__c;
					gam.RemoteDisable = ga.RemoteDisable__c; 
					gam.RepairPartner = ga.RepairPartner__c; 
					gam.WarrantyCountries = ga.WarrantyCountries__c; 
					gam.SaleDateOrder = ga.SaleDateOrder__c;
					gam.AlternateSerialNumber1 = ga.AlternateSerialNumber1__c;
					gam.AlternateSerialNumber2 = ga.AlternateSerialNumber2__c;
					gam.AlternateSerialNumber3 = ga.AlternateSerialNumber3__c;
					gam.AlternateSerialNumber4 = ga.AlternateSerialNumber4__c;
					gam.SKUUpsert = ga.SKUUpsert__c;system.debug('?? ' + ga.Unique_ID_Type_from_product__c);
					gam.SaleDateOpportunity = ga.SaleDateOpportunity__c;
					gam.Shipment_Date = ga.Shipment_Date__c;
					gam.Sale_Country = ga.Sale_Country__c; 
					gam.VIP = ga.VIP__c;
					gam.Tracking_URL = ga.Tracking_URL__c;
					gam.Carrier = ga.Carrier__c;
					gam.Tracking_Link = ga.Tracking_Link__c;
					gam.RMA_Exceptions = ga.RMA_Exceptions__c;
					gam.Service_Model = ga.Service_Model__c; 
					gam.Line_Number = ga.Line_Number__c;
					gam.Shipping_Adjustment = ga.Shipping_Adjustment__c;
					gam.Remorse_Period = ga.Remorse_Period__c; 
					gam.DOA_Period = ga.DOA_Period__c;
					gam.Warranty_Countdown = ga.Warranty_Countdown__c;
					gam.Remorse_Days_Left = ga.Remorse_Days_Left__c;
					gam.DOA_Days_Left = ga.DOA_Days_Left__c;
					gam.No_Warranty_Support = ga.No_Warranty_Support__c;
					gam.Expected_Sale_Days = ga.Expected_Sale_Days__c;
					gam.Minimum_Replacement_Warranty_Period = ga.Minimum_Replacement_Warranty_Period__c;
					gam.Proof_of_Purchase_gCases_ID = ga.Proof_of_Purchase_gCases_ID__c;
					gam.Replacement_Warranty = ga.Replacement_Warranty__c;
					gam.Retail_Country = ga.Retail_Country__c;
					gam.Retail_Sale_Date = ga.Retail_Sale_Date__c;
					gam.Warranty_Period = ga.Warranty_Period__c;
					gam.Original_Retail_Store_Name = ga.Original_Retail_Store_Name__c;
					gam.Mac_Address = ga.Mac_Address__c;
					gam.Order_ID_Name = ga.Order_ID_Name__c;
					gam.Recently_modified_by_user = ga.Recently_modified_by_user__c;
					gam.MTR_ID = ga.MTR_ID__c;
					gam.Order_Type = ga.Order_Type__c;
					gam.Sale_Datevalue = ga.Sale_Datevalue__c;
					gam.Associated_with_a_Safety_report = ga.Associated_with_a_Safety_report__c;
					gam.Shopping_Express = ga.Shopping_Express__c;
					gam.ProductFamily = ga.ProductFamily__c;
					gam.GlassError = ga.GlassError__c; 
					gam.GlassErrorMessage = ga.GlassErrorMessage__c;
					gam.GlassSent2 = ga.GlassSent2__c;
					gam.Converted_to_Chromecast_R = ga.Converted_to_Chromecast_R__c;
					gam.CSN = ga.CSN__c;
					gam.Unique_ID_Type_from_product = ga.Unique_ID_Type_from_product__c;
    			}
    			else
    			{
    				GoogleAssetDataAccess.objectStatus = 'Requested Google Asset is either empty or does not exist.';
    			}
    		}	
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' Line: ' + ex.getLineNumber());
    	}
    	return gam;
    }
}