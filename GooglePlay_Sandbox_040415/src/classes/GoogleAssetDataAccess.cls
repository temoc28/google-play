/**
 * @author:	Levementum LLC
 * @date:	04/13/15
 * @description:	General DML Operations for Google Asset custom object
 */
public with sharing class GoogleAssetDataAccess 
{
	public static String objectStatus = 'OK';
	private static String namePrefix = 'Google_Asset_';
	private static String serialPrefix = 'Serial_';
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/13/15
	 * @description:	Retrieves a Google Asset based on an asset id
	 */
	public static List<Google_Asset__c> selectGoogleAsset(Id assetId)
	{
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			googleAssets = [SELECT Active_Device__c, AlternateSerialNumber1__c, AlternateSerialNumber2__c, AlternateSerialNumber3__c, AlternateSerialNumber4__c, cssn__c, AssetOwner__c, Asset_Type__c, Associated_with_a_Safety_report__c, Buyers_Remorse_destination__c, CSN__c, Carrier__c, Chrome_S_N__c, Converted_to_Chromecast_R__c, country_sold__c, CreatedById, CreatedDate, DOA_Days_Left__c, DOA_Period__c, IsDeleted, RMA_Exceptions__c, Expected_Sale_Days__c, GlassError__c, GlassErrorMessage__c, GlassSent2__c, Name, Google_Asset_ID_External_ID__c, Order_Opportunity__c, IMEI__c, Last_Date_Buyer_s_Remorse__c, Last_Date_DOA__c, Last_Date_Spare_Parts__c, LastModifiedById, LastModifiedDate, Line_Number__c, MTR_ID__c, Mac_Address__c, Manufacture_Date__c, Minimum_Replacement_Warranty_Period__c, No_Warranty_Support__c, OrderLegacy__c, Order_ID_Name__c, Order_Type__c, Original_Retail_Store_Name__c, OwnerId, ProductID__c, ProductSKUDescription__c, ProductSKU__c, ProductFamily__c, Proof_of_Purchase_gCases_ID__c, Purchaser_Email__c, Recently_modified_by_user__c, Id, RecordTypeId, Remorse_Days_Left__c, Remorse_Period__c, bricking__c, RemoteDisable__c, Repair_Partner_Legacy__c, RepairPartner__c, Replacement_Warranty__c, Retail_Country__c, Retail_Sale_Date__c, SKU__c, SKU_Description__c, SKUUpsert__c, Sale_Country__c, Sale_Date__c, SaleDateOpportunity__c, SaleDateOrder__c, Sale_Datevalue__c, Serial__c, Service_Model__c, Shipment_Date__c, Shipping_Adjustment__c, Shopping_Express__c, SystemModstamp, Tracking_Number__c, Tracking_Link__c, Tracking_URL__c, Unique_ID_Type_from_product__c, Unique_ID_Type__c, VIP__c, Warranty_Countries_Legacy__c, WarrantyCountries__c, Warranty_Countdown__c, Warranty_Expiration_Date__c, Warranty_Period__c FROM Google_Asset__c WHERE id =: assetId];
		}
		catch(Exception ex)
		{
			objectStatus = 'Google Asset not found: ' + assetId;
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/13/15
	 * @description:	Retrieves a Google Asset based on an asset id
	 */
	public static List<Google_Asset__c> selectGoogleAsset(String assetId)
	{
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			if(assetId == null)
			{
				return googleAssets;
			}
			googleAssets = [SELECT Active_Device__c, AlternateSerialNumber1__c, AlternateSerialNumber2__c, AlternateSerialNumber3__c, AlternateSerialNumber4__c, cssn__c, AssetOwner__c, Asset_Type__c, Associated_with_a_Safety_report__c, Buyers_Remorse_destination__c, CSN__c, Carrier__c, Chrome_S_N__c, Converted_to_Chromecast_R__c, country_sold__c, CreatedById, CreatedDate, DOA_Days_Left__c, DOA_Period__c, IsDeleted, RMA_Exceptions__c, Expected_Sale_Days__c, GlassError__c, GlassErrorMessage__c, GlassSent2__c, Name, Google_Asset_ID_External_ID__c, Order_Opportunity__c, IMEI__c, Last_Date_Buyer_s_Remorse__c, Last_Date_DOA__c, Last_Date_Spare_Parts__c, LastModifiedById, LastModifiedDate, Line_Number__c, MTR_ID__c, Mac_Address__c, Manufacture_Date__c, Minimum_Replacement_Warranty_Period__c, No_Warranty_Support__c, OrderLegacy__c, Order_ID_Name__c, Order_Type__c, Original_Retail_Store_Name__c, OwnerId, ProductID__c, ProductSKUDescription__c, ProductSKU__c, ProductFamily__c, Proof_of_Purchase_gCases_ID__c, Purchaser_Email__c, Recently_modified_by_user__c, Id, RecordTypeId, Remorse_Days_Left__c, Remorse_Period__c, bricking__c, RemoteDisable__c, Repair_Partner_Legacy__c, RepairPartner__c, Replacement_Warranty__c, Retail_Country__c, Retail_Sale_Date__c, SKU__c, SKU_Description__c, SKUUpsert__c, Sale_Country__c, Sale_Date__c, SaleDateOpportunity__c, SaleDateOrder__c, Sale_Datevalue__c, Serial__c, Service_Model__c, Shipment_Date__c, Shipping_Adjustment__c, Shopping_Express__c, SystemModstamp, Tracking_Number__c, Tracking_Link__c, Tracking_URL__c, Unique_ID_Type_from_product__c, Unique_ID_Type__c, VIP__c, Warranty_Countries_Legacy__c, WarrantyCountries__c, Warranty_Countdown__c, Warranty_Expiration_Date__c, Warranty_Period__c FROM Google_Asset__c WHERE Name =: assetId.trim()];
		}
		catch(Exception ex)
		{
			objectStatus = 'Google Asset not found: ' + assetId;
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/13/15
	 * @description:	Updates current Google Asset
	 * @update:	04/24/15. Autogenerate both Google Asset name and serial if not provided in body
	 */
	public static List<Google_Asset__c> updateGoogleAsset(List<Google_Asset__c> googleAssetModelsToBeUpdated, GoogleAssetModel updatingData)
	{
		Savepoint sp = Database.setSavepoint();
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			if(googleAssetModelsToBeUpdated.size() <= 0)
			{
				Google_Asset__c googleAssetToInsert = insertGoogleAsset(updatingData);
				insert googleAssetToInsert;
				
				// autogenerate both Name and serial for Google Asset
				googleAssetToInsert.Name = namePrefix + googleAssetToInsert.id;
				googleAssetToInsert.Serial__c = serialPrefix + googleAssetToInsert.id;
				update googleAssetToInsert;
				googleAssets.add(googleAssetToInsert);
				
				objectStatus = 'Record provided in the body was not found. Therefore, a new record was created.';
			}
			else
			{
				for(Google_Asset__c ga : googleAssetModelsToBeUpdated)
				{
					googleAssets.add(updateGoogleAssetFields(ga, updatingData));
				}
				upsert googleAssets;
			}
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			objectStatus = ex.getMessage();
			system.debug(ex.getMessage());
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/17/15
	 * @description:	Inserts a brand new Google Asset absed on the response sent via @HttpPost
	 */
	private static Google_Asset__c insertGoogleAsset(GoogleAssetModel googleAssetModel)
	{
		Google_Asset__c googleAssetToInsert = new Google_Asset__c();
		try
		{
			if(googleAssetModel.ownerId!= null){googleAssetToInsert.OwnerId = googleAssetModel.OwnerId;}
			googleAssetToInsert.Name = namePrefix + System.currentTimeMillis();
			if(googleAssetModel.RecordTypeId!= null){googleAssetToInsert.RecordTypeId = googleAssetModel.RecordTypeId;}
			if(googleAssetModel.OrderLegacy!= null){googleAssetToInsert.OrderLegacy__c = googleAssetModel.OrderLegacy;}
			googleAssetToInsert.Serial__c = serialPrefix + System.currentTimeMillis();
			if(googleAssetModel.Manufacture_Date!= null){googleAssetToInsert.Manufacture_Date__c = googleAssetModel.Manufacture_Date;}
			if(googleAssetModel.Sale_Date!= null){googleAssetToInsert.Sale_Date__c = googleAssetModel.Sale_Date;}
			if(googleAssetModel.IMEI!= null){googleAssetToInsert.IMEI__c = googleAssetModel.IMEI;}
			if(googleAssetModel.SKU!= null){googleAssetToInsert.SKU__c = googleAssetModel.SKU;}
			if(googleAssetModel.Active_Device!= null){googleAssetToInsert.Active_Device__c = googleAssetModel.Active_Device;}
			if(googleAssetModel.Asset_Type!= null){googleAssetToInsert.Asset_Type__c = googleAssetModel.Asset_Type;}
			if(googleAssetModel.Unique_ID_Type!= null){googleAssetToInsert.Unique_ID_Type__c = googleAssetModel.Unique_ID_Type;}
			if(googleAssetModel.SKU_Description!= null){googleAssetToInsert.SKU_Description__c = googleAssetModel.SKU_Description;}
			if(googleAssetModel.Repair_Partner_Legacy!= null){googleAssetToInsert.Repair_Partner_Legacy__c = googleAssetModel.Repair_Partner_Legacy;}
			if(googleAssetModel.Warranty_Countries_Legacy!= null){googleAssetToInsert.Warranty_Countries_Legacy__c = googleAssetModel.Warranty_Countries_Legacy;}
			if(googleAssetModel.bricking!= null){googleAssetToInsert.bricking__c = googleAssetModel.bricking;}
			if(googleAssetModel.Tracking_Number!= null){googleAssetToInsert.Tracking_Number__c = googleAssetModel.Tracking_Number;}
			if(googleAssetModel.Google_Asset_ID_External_ID!= null){googleAssetToInsert.Google_Asset_ID_External_ID__c = googleAssetModel.Google_Asset_ID_External_ID;}
			if(googleAssetModel.cssn!= null){googleAssetToInsert.cssn__c = googleAssetModel.cssn;}
			if(googleAssetModel.country_sold!= null){googleAssetToInsert.country_sold__c = googleAssetModel.country_sold;}
			if(googleAssetModel.Buyers_Remorse_destination!= null){googleAssetToInsert.Buyers_Remorse_destination__c = googleAssetModel.Buyers_Remorse_destination;}
			if(googleAssetModel.ProductID!= null){googleAssetToInsert.ProductID__c = googleAssetModel.ProductID;}
			if(googleAssetModel.AssetOwner!= null){googleAssetToInsert.AssetOwner__c = googleAssetModel.AssetOwner;}
			if(googleAssetModel.Chrome_S_N!= null){googleAssetToInsert.Chrome_S_N__c = googleAssetModel.Chrome_S_N;}
			if(googleAssetModel.Order_Opportunity!= null){googleAssetToInsert.Order_Opportunity__c = googleAssetModel.Order_Opportunity;}
			if(googleAssetModel.AlternateSerialNumber1!= null){googleAssetToInsert.AlternateSerialNumber1__c = googleAssetModel.AlternateSerialNumber1;}
			if(googleAssetModel.AlternateSerialNumber2!= null){googleAssetToInsert.AlternateSerialNumber2__c = googleAssetModel.AlternateSerialNumber2;}
			if(googleAssetModel.AlternateSerialNumber3!= null){googleAssetToInsert.AlternateSerialNumber3__c = googleAssetModel.AlternateSerialNumber3;}
			if(googleAssetModel.AlternateSerialNumber4!= null){googleAssetToInsert.AlternateSerialNumber4__c = googleAssetModel.AlternateSerialNumber4;}
			if(googleAssetModel.SKUUpsert!= null){googleAssetToInsert.SKUUpsert__c = googleAssetModel.SKUUpsert;}
			if(googleAssetModel.VIP!= null){googleAssetToInsert.VIP__c = googleAssetModel.VIP;}
			if(googleAssetModel.Tracking_URL!= null){googleAssetToInsert.Tracking_URL__c = googleAssetModel.Tracking_URL;}
			if(googleAssetModel.Carrier!= null){googleAssetToInsert.Carrier__c = googleAssetModel.Carrier;}
			if(googleAssetModel.Line_Number!= null){googleAssetToInsert.Line_Number__c = googleAssetModel.Line_Number;}
			if(googleAssetModel.Remorse_Period!= null){googleAssetToInsert.Remorse_Period__c = googleAssetModel.Remorse_Period;}
			if(googleAssetModel.DOA_Period!= null){googleAssetToInsert.DOA_Period__c = googleAssetModel.DOA_Period;}
			if(googleAssetModel.No_Warranty_Support!= null){googleAssetToInsert.No_Warranty_Support__c = googleAssetModel.No_Warranty_Support;}
			if(googleAssetModel.Expected_Sale_Days!= null){googleAssetToInsert.Expected_Sale_Days__c = googleAssetModel.Expected_Sale_Days;}
			if(googleAssetModel.Minimum_Replacement_Warranty_Period!= null){googleAssetToInsert.Minimum_Replacement_Warranty_Period__c = googleAssetModel.Minimum_Replacement_Warranty_Period;}
			if(googleAssetModel.Proof_of_Purchase_gCases_ID!= null){googleAssetToInsert.Proof_of_Purchase_gCases_ID__c = googleAssetModel.Proof_of_Purchase_gCases_ID;}
			if(googleAssetModel.Replacement_Warranty!= null){googleAssetToInsert.Replacement_Warranty__c = googleAssetModel.Replacement_Warranty;}
			if(googleAssetModel.Retail_Country!= null){googleAssetToInsert.Retail_Country__c = googleAssetModel.Retail_Country;}
			if(googleAssetModel.Retail_Sale_Date!= null){googleAssetToInsert.Retail_Sale_Date__c = googleAssetModel.Retail_Sale_Date;}
			if(googleAssetModel.Warranty_Period!= null){googleAssetToInsert.Warranty_Period__c = googleAssetModel.Warranty_Period;}
			if(googleAssetModel.Original_Retail_Store_Name!= null){googleAssetToInsert.Original_Retail_Store_Name__c = googleAssetModel.Original_Retail_Store_Name;}
			if(googleAssetModel.Mac_Address!= null){googleAssetToInsert.Mac_Address__c = googleAssetModel.Mac_Address;}
			if(googleAssetModel.Associated_with_a_Safety_report!= null){googleAssetToInsert.Associated_with_a_Safety_report__c = googleAssetModel.Associated_with_a_Safety_report;}
			if(googleAssetModel.Shopping_Express!= null){googleAssetToInsert.Shopping_Express__c = googleAssetModel.Shopping_Express;}
			if(googleAssetModel.GlassError!= null){googleAssetToInsert.GlassError__c = googleAssetModel.GlassError;}
			if(googleAssetModel.GlassErrorMessage!= null){googleAssetToInsert.GlassErrorMessage__c = googleAssetModel.GlassErrorMessage;}
			if(googleAssetModel.GlassSent2!= null){googleAssetToInsert.GlassSent2__c = googleAssetModel.GlassSent2;}
			if(googleAssetModel.Converted_to_Chromecast_R!= null){googleAssetToInsert.Converted_to_Chromecast_R__c = googleAssetModel.Converted_to_Chromecast_R;}
			if(googleAssetModel.CSN!= null){googleAssetToInsert.CSN__c = googleAssetModel.CSN;}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}
		return googleAssetToInsert;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/13/15
	 * @description:	Updates current Google Asset
	 * @update:	04/23/15: Addded following fields: doa_period__c, expected_sale_days__c, remorse_period__c, 
	 *					  minimum_replacement_warranty_period__c, and replacement_warranty__c.
	 */
	private static Google_Asset__c updateGoogleAssetFields(Google_Asset__c googleAssetModelToUpdate, GoogleAssetModel updatingData)
	{
		Google_Asset__c ga = googleAssetModelToUpdate;
		try
		{
			if(ga != null && updatingData != null)
			{
				if(updatingData.Id!= null){ga.Id = updatingData.Id;}
				if(updatingData.ownerId!= null){ga.OwnerId = updatingData.OwnerId;}
				ga.Name = updatingData.Name;
				if(updatingData.RecordTypeId!= null){ga.RecordTypeId = updatingData.RecordTypeId;}
				if(updatingData.CreatedDate!= null){ga.CreatedDate = updatingData.CreatedDate;}
				if(updatingData.CreatedById!= null){ga.CreatedById = updatingData.CreatedById;}
				if(updatingData.LastModifiedDate!= null){ga.LastModifiedDate = updatingData.LastModifiedDate;}
				if(updatingData.LastModifiedById!= null){ga.LastModifiedById = updatingData.LastModifiedById;}
				if(updatingData.OrderLegacy!= null){ga.OrderLegacy__c = updatingData.OrderLegacy;}
				if(updatingData.serial != null){ga.Serial__c = updatingData.serial;}
				if(updatingData.Manufacture_Date!= null){ga.Manufacture_Date__c = updatingData.Manufacture_Date;}
				if(updatingData.Sale_Date!= null){ga.Sale_Date__c = updatingData.Sale_Date;}
				if(updatingData.IMEI!= null){ga.IMEI__c = updatingData.IMEI;}
				if(updatingData.SKU!= null){ga.SKU__c = updatingData.SKU;}
				if(updatingData.Active_Device!= null){ga.Active_Device__c = updatingData.Active_Device;}
				if(updatingData.Asset_Type!= null){ga.Asset_Type__c = updatingData.Asset_Type;}
				if(updatingData.Unique_ID_Type!= null){ga.Unique_ID_Type__c = updatingData.Unique_ID_Type;}
				if(updatingData.SKU_Description!= null){ga.SKU_Description__c = updatingData.SKU_Description;}
				if(updatingData.Repair_Partner_Legacy!= null){ga.Repair_Partner_Legacy__c = updatingData.Repair_Partner_Legacy;}
				if(updatingData.Warranty_Countries_Legacy!= null){ga.Warranty_Countries_Legacy__c = updatingData.Warranty_Countries_Legacy;}
				if(updatingData.bricking!= null){ga.bricking__c = updatingData.bricking;}
				if(updatingData.Tracking_Number!= null){ga.Tracking_Number__c = updatingData.Tracking_Number;}
				if(updatingData.Google_Asset_ID_External_ID!= null){ga.Google_Asset_ID_External_ID__c = updatingData.Google_Asset_ID_External_ID;}
				if(updatingData.cssn!= null){ga.cssn__c = updatingData.cssn;}
				if(updatingData.country_sold!= null){ga.country_sold__c = updatingData.country_sold;}
				if(updatingData.Buyers_Remorse_destination!= null){ga.Buyers_Remorse_destination__c = updatingData.Buyers_Remorse_destination;}
				if(updatingData.ProductID!= null){ga.ProductID__c = updatingData.ProductID;}
				if(updatingData.AssetOwner!= null){ga.AssetOwner__c = updatingData.AssetOwner;}
				if(updatingData.Chrome_S_N!= null){ga.Chrome_S_N__c = updatingData.Chrome_S_N;}
				if(updatingData.Order_Opportunity!= null){ga.Order_Opportunity__c = updatingData.Order_Opportunity;}
				if(updatingData.AlternateSerialNumber1!= null){ga.AlternateSerialNumber1__c = updatingData.AlternateSerialNumber1;}
				if(updatingData.AlternateSerialNumber2!= null){ga.AlternateSerialNumber2__c = updatingData.AlternateSerialNumber2;}
				if(updatingData.AlternateSerialNumber3!= null){ga.AlternateSerialNumber3__c = updatingData.AlternateSerialNumber3;}
				if(updatingData.AlternateSerialNumber4!= null){ga.AlternateSerialNumber4__c = updatingData.AlternateSerialNumber4;}
				if(updatingData.SKUUpsert!= null){ga.SKUUpsert__c = updatingData.SKUUpsert;}
				if(updatingData.VIP!= null){ga.VIP__c = updatingData.VIP;}
				if(updatingData.Tracking_URL!= null){ga.Tracking_URL__c = updatingData.Tracking_URL;}
				if(updatingData.Carrier!= null){ga.Carrier__c = updatingData.Carrier;}
				if(updatingData.Line_Number!= null){ga.Line_Number__c = updatingData.Line_Number;}
				if(updatingData.Remorse_Period!= null){ga.Remorse_Period__c = updatingData.Remorse_Period;}
				if(updatingData.DOA_Period!= null){ga.DOA_Period__c = updatingData.DOA_Period;}
				if(updatingData.No_Warranty_Support!= null){ga.No_Warranty_Support__c = updatingData.No_Warranty_Support;}
				if(updatingData.Expected_Sale_Days!= null){ga.Expected_Sale_Days__c = updatingData.Expected_Sale_Days;}
				if(updatingData.Minimum_Replacement_Warranty_Period!= null){ga.Minimum_Replacement_Warranty_Period__c = updatingData.Minimum_Replacement_Warranty_Period;}
				if(updatingData.Proof_of_Purchase_gCases_ID!= null){ga.Proof_of_Purchase_gCases_ID__c = updatingData.Proof_of_Purchase_gCases_ID;}
				if(updatingData.Replacement_Warranty!= null){ga.Replacement_Warranty__c = updatingData.Replacement_Warranty;}
				if(updatingData.Retail_Country!= null){ga.Retail_Country__c = updatingData.Retail_Country;}
				if(updatingData.Retail_Sale_Date!= null){ga.Retail_Sale_Date__c = updatingData.Retail_Sale_Date;}
				if(updatingData.Warranty_Period!= null){ga.Warranty_Period__c = updatingData.Warranty_Period;}
				if(updatingData.Original_Retail_Store_Name!= null){ga.Original_Retail_Store_Name__c = updatingData.Original_Retail_Store_Name;}
				if(updatingData.Mac_Address!= null){ga.Mac_Address__c = updatingData.Mac_Address;}
				if(updatingData.Associated_with_a_Safety_report!= null){ga.Associated_with_a_Safety_report__c = updatingData.Associated_with_a_Safety_report;}
				if(updatingData.Shopping_Express!= null){ga.Shopping_Express__c = updatingData.Shopping_Express;}
				if(updatingData.GlassError!= null){ga.GlassError__c = updatingData.GlassError;}
				if(updatingData.GlassErrorMessage!= null){ga.GlassErrorMessage__c = updatingData.GlassErrorMessage;}
				if(updatingData.GlassSent2!= null){ga.GlassSent2__c = updatingData.GlassSent2;}
				if(updatingData.Converted_to_Chromecast_R!= null){ga.Converted_to_Chromecast_R__c = updatingData.Converted_to_Chromecast_R;}
				if(updatingData.CSN!= null){ga.CSN__c = updatingData.CSN;}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage(); 
		}
		return ga;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/23/15
	 * @description:	Deletes a Google Asset by name
	 */
	public static void deleteObject(String objectName)
	{
		try
		{
			Google_Asset__c assetToDelete = [SELECT id
										 	 FROM Google_Asset__c
										 	 WHERE Name =: objectName limit 1];
			delete assetToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = 'Google Asset ' + objectName + ' could not be deleted. Please check the name and try again.';
		}
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/23/15
	 * @description:	Deletes a Google Asset by id
	 */
	public static void deleteObject(Id objectId)
	{
		try
		{
			Google_Asset__c assetToDelete = [SELECT id
										 	 FROM Google_Asset__c
										 	 WHERE id =: objectId];
			delete assetToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = 'Google Asset ' + objectId + ' could not be deleted. Please check the id and try again.';
		}
	}
}