public with sharing class RmaCloseCancel 
{
	 RMA__c rma;
	 
	 public RmaCloseCancel(ApexPages.StandardController con)
	 {
	    rma = (RMA__c)con.getRecord();	    
	 }
	 
	 public PageReference markClosed()
	 {	    
	    rma.Status__c='Closed';
	    //write the value to RMA Received Reason but we are showing the other field on the VF page
	    rma.RMAReceivedReason__c = rma.CloseRMAReceivedReason__c;	
	    rma.ManuallyClosedBy__c=UserInfo.getUserId();    
	    update rma;
	    
	    return new PageReference('/'+rma.Id);
	 }
}