/**
 * @author:	Levementum LLC
 * @date:	04/20/15
 * @description:	General DML Operations for Order custom object
 */
public class OpportunityDataAccess implements SObjectDataAccess
{
	public static String objectStatus = 'OK';
	public static String namePrefix = 'Order_';
	public static Opportunity opp;
	
	public sObject selectObject(ObjectModel model)
	{
		return null;
	}
	
	public sObject selectObject(Id orderId)
	{
		Opportunity orderSelected = null;
		try
		{
			orderSelected = [SELECT AHNumber__c, Amount, AttentionTo__c, CampaignId, City__c, CloseDate, IsClosed, Country__c, 
								    CreatedById, CreatedDate, IsDeleted, Description, Tax_Holiday__c, Email__c, Fiscal, 
								    FiscalQuarter, FiscalYear, ForecastCategoryName, ForecastCategory, GoogleAccountLegacy__c, 
								    AccountId, GoogleAsset__c, Name, HasOpportunityLineItem, LastActivityDate, LastModifiedById, 
								    LastModifiedDate, LeadSource, MoveOrder__c, NextStep, Id, OrderIDExternalID__c, OwnerId, 
								    RecordTypeId, Type, Telephone__c, Pricebook2Id, Probability, SFDCLegacyOrderID__c, 
								    Ship_By_Date__c, ShipServiceLevel__c, ShipmentDate__c, ShippingProblem__c, StageName, 
								    State_Province__c, Address__c, SystemModstamp, TrackingNumber__c, TrackingNumberLink__c, 
								    TransactionDate__c, Warehouse__c, IsWon, PostalCode__c, temp_future_date_catcher__c 
							FROM Opportunity
							WHERE id =: orderId limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = 'Order not found: ' + orderId; 
		}
		return orderSelected;
	}
	
	public sObject selectObject(String orderName)
	{
		Opportunity orderSelected = null;
		try
		{
			orderSelected = [SELECT AHNumber__c, Amount, AttentionTo__c, CampaignId, City__c, CloseDate, IsClosed, Country__c, 
								    CreatedById, CreatedDate, IsDeleted, Description, Tax_Holiday__c, Email__c, Fiscal, 
								    FiscalQuarter, FiscalYear, ForecastCategoryName, ForecastCategory, GoogleAccountLegacy__c, 
								    AccountId, GoogleAsset__c, Name, HasOpportunityLineItem, LastActivityDate, LastModifiedById, 
								    LastModifiedDate, LeadSource, MoveOrder__c, NextStep, Id, OrderIDExternalID__c, OwnerId, 
								    RecordTypeId, Type, Telephone__c, Pricebook2Id, Probability, SFDCLegacyOrderID__c, 
								    Ship_By_Date__c, ShipServiceLevel__c, ShipmentDate__c, ShippingProblem__c, StageName, 
								    State_Province__c, Address__c, SystemModstamp, TrackingNumber__c, TrackingNumberLink__c, 
								    TransactionDate__c, Warehouse__c, IsWon, PostalCode__c, temp_future_date_catcher__c 
							FROM Opportunity
							WHERE Name =: orderName.trim() limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = 'Order not found: ' + orderName; 
		}
		return orderSelected;
	}
	
	public void updateObject(sObject objectToUpdate, ObjectModel updatingData)
	{
		Savepoint sp = Database.setSavepoint();
		try
		{
			if(objectToUpdate == null)
			{
				opp = (Opportunity)insertObject(updatingData);
				insert opp;
				
				// autogenerate Order name
				opp.Name = namePrefix + opp.id;
				update opp;
			}
			else
			{
				upsert updateModel(objectToUpdate, updatingData);
			}
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			objectStatus = ex.getMessage();
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(String objectName)
	{
		try
		{
			Opportunity orderToDelete = [SELECT id
										 FROM Opportunity
										 WHERE Name =: objectName limit 1];
			delete orderToDelete;
		}
		catch(Exception ex)
		{
			objectStatus = 'Order could not be deleted: <Name>'; 
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(Id objectId)
	{
		try
		{
			Opportunity orderToDelete = [SELECT id
										 FROM Opportunity
										 WHERE id =: objectId];
			delete orderToDelete;
		}
		catch(Exception ex)
		{
			objectStatus = 'Order could not be deleted: <id>'; 
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(ObjectModel objectModel)
	{
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/19/15
	 * @description:	Inserts a new order/Opportunity
	 * @update:	04/23/15: Addded following fields: all non-required fields
	 */
	public sObject insertObject(ObjectModel objectModel)
	{
		Opportunity orderToInsert = new Opportunity();
		OpportunityModel oppModel = (OpportunityModel)objectModel; 
		try
		{
			if(orderToInsert != null && oppModel != null)
			{
				// REQUIRED FIELDS
				if(oppModel.Name != null){orderToInsert.Name = oppModel.Name;}else{orderToInsert.Name = namePrefix + System.currentTimeMillis();}
				if(oppModel.StageName != null){orderToInsert.StageName = oppModel.StageName;}
				if(oppModel.CloseDate != null){orderToInsert.CloseDate = oppModel.CloseDate;}
				if(oppModel.Type != null){orderToInsert.Type = oppModel.Type;}
				if(oppModel.Country != null){orderToInsert.Country__c = oppModel.Country;}
				if(oppModel.State_Province != null){orderToInsert.State_Province__c = oppModel.State_Province;}
				
				if(oppModel.AccountId != null){orderToInsert.AccountId = oppModel.AccountId;}
				if(oppModel.RecordTypeId != null){orderToInsert.RecordTypeId = oppModel.RecordTypeId;}
				if(oppModel.Description != null){orderToInsert.Description = oppModel.Description;}
				if(oppModel.Amount != null){orderToInsert.Amount = oppModel.Amount;}
				if(oppModel.Probability != null){orderToInsert.Probability = oppModel.Probability;}
				if(oppModel.NextStep != null){orderToInsert.NextStep = oppModel.NextStep;}
				if(oppModel.LeadSource != null){orderToInsert.LeadSource = oppModel.LeadSource;}
				if(oppModel.ForecastCategoryName != null){orderToInsert.ForecastCategoryName = oppModel.ForecastCategoryName;}
				if(oppModel.CampaignId != null){orderToInsert.CampaignId = oppModel.CampaignId;}
				if(oppModel.Pricebook2Id != null){orderToInsert.Pricebook2Id = oppModel.Pricebook2Id;}
				if(oppModel.OwnerId != null){orderToInsert.OwnerId = oppModel.OwnerId;}
				if(oppModel.CreatedDate != null){orderToInsert.CreatedDate = oppModel.CreatedDate;}
				if(oppModel.CreatedById != null){orderToInsert.CreatedById = oppModel.CreatedById;}
				if(oppModel.LastModifiedDate != null){orderToInsert.LastModifiedDate = oppModel.LastModifiedDate;}
				if(oppModel.LastModifiedById != null){orderToInsert.LastModifiedById = oppModel.LastModifiedById;}
				if(oppModel.AHNumber != null){orderToInsert.AHNumber__c = oppModel.AHNumber;}
				if(oppModel.Address != null){orderToInsert.Address__c = oppModel.Address;}
				if(oppModel.AttentionTo != null){orderToInsert.AttentionTo__c = oppModel.AttentionTo;}
				if(oppModel.City != null){orderToInsert.City__c = oppModel.City;}
				if(oppModel.GoogleAccountLegacy != null){orderToInsert.GoogleAccountLegacy__c = oppModel.GoogleAccountLegacy;}
				if(oppModel.GoogleAsset != null){orderToInsert.GoogleAsset__c = oppModel.GoogleAsset;}
				if(oppModel.MoveOrder != null){orderToInsert.MoveOrder__c = oppModel.MoveOrder;}
				if(oppModel.OrderIDExternalID != null){orderToInsert.OrderIDExternalID__c = oppModel.OrderIDExternalID;}
				if(oppModel.PostalCode != null){orderToInsert.PostalCode__c = oppModel.PostalCode;}
				if(oppModel.SFDCLegacyOrderID != null){orderToInsert.SFDCLegacyOrderID__c = oppModel.SFDCLegacyOrderID;}
				if(oppModel.ShipServiceLevel != null){orderToInsert.ShipServiceLevel__c = oppModel.ShipServiceLevel;}
				if(oppModel.ShipmentDate != null){orderToInsert.ShipmentDate__c = oppModel.ShipmentDate;}
				if(oppModel.ShippingProblem != null){orderToInsert.ShippingProblem__c = oppModel.ShippingProblem;}
				if(oppModel.Telephone != null){orderToInsert.Telephone__c = oppModel.Telephone;}
				if(oppModel.TrackingNumber != null){orderToInsert.TrackingNumber__c = oppModel.TrackingNumber;}
				if(oppModel.TransactionDate != null){orderToInsert.TransactionDate__c = oppModel.TransactionDate;}
				if(oppModel.Ship_By_Date != null){orderToInsert.Ship_By_Date__c = oppModel.Ship_By_Date;}
				if(oppModel.Warehouse != null){orderToInsert.Warehouse__c = oppModel.Warehouse;}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return orderToInsert;
	}
	
	/**
	 * @author:	Levementum LLC
	 * @date:	04/19/15
	 * @description:	Updates current Order/Opportunity
	 * @update:	04/23/15: Addded following fields: TransactionDate__c, ShipmentDate__c, and StageName.
	 */
	public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
	{
		Opportunity orderToUpdate = (Opportunity)sObjectToUpdate;
		OpportunityModel updatingModel = (OpportunityModel)updatingData;
		try
		{
			if(orderToUpdate != null && updatingData != null)
			{
				if(updatingModel.Id != null){orderToUpdate.Id = updatingModel.Id;}
				if(updatingModel.AccountId != null){orderToUpdate.AccountId = updatingModel.AccountId;}
				if(updatingModel.RecordTypeId != null){orderToUpdate.RecordTypeId = updatingModel.RecordTypeId;}
				if(updatingModel.Name != null){orderToUpdate.Name = updatingModel.Name;}
				if(updatingModel.Description != null){orderToUpdate.Description = updatingModel.Description;}
				if(updatingModel.StageName != null){orderToUpdate.StageName = updatingModel.StageName;}
				if(updatingModel.Amount != null){orderToUpdate.Amount = updatingModel.Amount;}
				if(updatingModel.Probability != null){orderToUpdate.Probability = updatingModel.Probability;}
				if(updatingModel.CloseDate != null){orderToUpdate.CloseDate = updatingModel.CloseDate;}
				if(updatingModel.Type != null){orderToUpdate.Type = updatingModel.Type;}
				if(updatingModel.NextStep != null){orderToUpdate.NextStep = updatingModel.NextStep;}
				if(updatingModel.LeadSource != null){orderToUpdate.LeadSource = updatingModel.LeadSource;}
				if(updatingModel.ForecastCategoryName != null){orderToUpdate.ForecastCategoryName = updatingModel.ForecastCategoryName;}
				if(updatingModel.CampaignId != null){orderToUpdate.CampaignId = updatingModel.CampaignId;}
				if(updatingModel.Pricebook2Id != null){orderToUpdate.Pricebook2Id = updatingModel.Pricebook2Id;}
				if(updatingModel.OwnerId != null){orderToUpdate.OwnerId = updatingModel.OwnerId;}
				if(updatingModel.CreatedDate != null){orderToUpdate.CreatedDate = updatingModel.CreatedDate;}
				if(updatingModel.CreatedById != null){orderToUpdate.CreatedById = updatingModel.CreatedById;}
				if(updatingModel.LastModifiedDate != null){orderToUpdate.LastModifiedDate = updatingModel.LastModifiedDate;}
				if(updatingModel.LastModifiedById != null){orderToUpdate.LastModifiedById = updatingModel.LastModifiedById;}
				if(updatingModel.AHNumber != null){orderToUpdate.AHNumber__c = updatingModel.AHNumber;}
				if(updatingModel.Address != null){orderToUpdate.Address__c = updatingModel.Address;}
				if(updatingModel.AttentionTo != null){orderToUpdate.AttentionTo__c = updatingModel.AttentionTo;}
				if(updatingModel.City != null){orderToUpdate.City__c = updatingModel.City;}
				if(updatingModel.GoogleAccountLegacy != null){orderToUpdate.GoogleAccountLegacy__c = updatingModel.GoogleAccountLegacy;}
				if(updatingModel.GoogleAsset != null){orderToUpdate.GoogleAsset__c = updatingModel.GoogleAsset;}
				if(updatingModel.MoveOrder != null){orderToUpdate.MoveOrder__c = updatingModel.MoveOrder;}
				if(updatingModel.OrderIDExternalID != null){orderToUpdate.OrderIDExternalID__c = updatingModel.OrderIDExternalID;}
				if(updatingModel.PostalCode != null){orderToUpdate.PostalCode__c = updatingModel.PostalCode;}
				if(updatingModel.SFDCLegacyOrderID != null){orderToUpdate.SFDCLegacyOrderID__c = updatingModel.SFDCLegacyOrderID;}
				if(updatingModel.ShipServiceLevel != null){orderToUpdate.ShipServiceLevel__c = updatingModel.ShipServiceLevel;}
				if(updatingModel.ShipmentDate != null){orderToUpdate.ShipmentDate__c = updatingModel.ShipmentDate;}
				if(updatingModel.ShippingProblem != null){orderToUpdate.ShippingProblem__c = updatingModel.ShippingProblem;}
				if(updatingModel.Telephone != null){orderToUpdate.Telephone__c = updatingModel.Telephone;}
				if(updatingModel.TrackingNumber != null){orderToUpdate.TrackingNumber__c = updatingModel.TrackingNumber;}
				if(updatingModel.TransactionDate != null){orderToUpdate.TransactionDate__c = updatingModel.TransactionDate;}
				if(updatingModel.State_Province != null){orderToUpdate.State_Province__c = updatingModel.State_Province;}
				if(updatingModel.Country != null){orderToUpdate.Country__c = updatingModel.Country;}
				if(updatingModel.Ship_By_Date != null){orderToUpdate.Ship_By_Date__c = updatingModel.Ship_By_Date;}
				if(updatingModel.Warehouse != null){orderToUpdate.Warehouse__c = updatingModel.Warehouse;}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage(); 
		}
		return orderToUpdate;
	}
}