@isTest
private class TestRestResponseModel {
	
	@isTest static void testClassInst() {
		RestResponseModel restRespMod = new RestResponseModel();

		RestResponseModel.customer_records_t customer_records = new RestResponseModel.customer_records_t('johnwick@levementum.com');
		RestResponseModel.device_details_t device_details = new RestResponseModel.device_details_t();
		System.assertNotEquals(device_details.components, null);
		System.assertNotEquals(device_details.substitute_gpns, null);
				
		RestResponseModel.rma_details_t rma_details = new RestResponseModel.rma_details_t();
		
		RestResponseModel.device_warranty_details_t device_warranty_details = new RestResponseModel.device_warranty_details_t();
		RestResponseModel.device_purchase_details_t device_purchase_details = new RestResponseModel.device_purchase_details_t();
		RestResponseModel.device_rma_reasons_t device_rma_reasons = new RestResponseModel.device_rma_reasons_t();

		RestResponseModel.rma_type_with_reasons_t rma_type_with_reasons = new RestResponseModel.rma_type_with_reasons_t();
		System.assertNotEquals(rma_type_with_reasons.rma_reason, null);

		RestResponseModel.rma_initiation_details_t rma_initiation_details = new RestResponseModel.rma_initiation_details_t();
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.rma_success_details_t rma_success_details = new RestResponseModel.rma_success_details_t();
		RestResponseModel.update_owner_details_t update_owner_details = new RestResponseModel.update_owner_details_t();
		RestResponseModel.update_owner_details_response_t update_owner_details_response = new RestResponseModel.update_owner_details_response_t();
		RestResponseModel.rma_events_t rma_events = new RestResponseModel.rma_events_t();
		RestResponseModel.owner_details_t owner_details = new RestResponseModel.owner_details_t();
		RestResponseModel.customer_details_t customer_details = new RestResponseModel.customer_details_t();
		RestResponseModel.address_t address = new RestResponseModel.address_t();
		RestResponseModel.device_types_t device_types = new RestResponseModel.device_types_t();
				
		RestResponseModel.warranty_model_t warranty_model = new RestResponseModel.warranty_model_t();
		warranty_model.warranty_model = 'Remorse Only';
		warranty_model.rma_action_list = new List<String>();	
		
		customer_records.cid_exception_granted_datetime = String.valueOf(Date.today());
		customer_records.cid_exception_granted_asset = 'NA';
		customer_records.cid_exception_on_this_asset = 'NA';
		customer_records.cid_exception_granted = 'NA';
		customer_records.cid_exception_granted_asset_description = 'NA';
		customer_records.cid_exception_granted_asset_name = 'NA';
		customer_records.device_details = null;
		
		device_details.serial_number = '000';
		device_details.unique_id = '0000';
		device_details.device_description = 'NA';
		device_details.device_sku_description = 'aaaa';
		device_details.device_type = 'NA';
		device_details.replacement_device_description = 'NA';
		device_details.manufacture_date = Date.today();
		device_details.repair_partner = 'NA';
		device_details.device_users = null;
		device_details.activation_date = Date.today();
		device_details.device_return_reason_code_set = 'NA';
		device_details.original_gpn_sku = null;
		device_details.country_variant_details = null;
		device_details.device_warranty_details = null;
		device_details.device_purchase_details = null;
		
		rma_details.email_id = 'NA';
		rma_details.unique_id = 'aaa';
		rma_details.rma_number = '000';
		rma_details.order_id = '555';
		rma_details.rma_state = 'TX';
		rma_details.rma_tracking_number = '233';
		rma_details.rma_carrier = 'US';
		rma_details.refund_charge_status = 'NA';
		rma_details.currency_name = 'Dollar';
		rma_details.item_refund = 'NA';
		rma_details.tax_refund = '45';
		rma_details.total_refund = '10';
		rma_details.expected_refund_date = Date.today();
		rma_details.rma_events = null;
		rma_details.rma_initiation_details = null;
		rma_details.rma_success_details = null;
		rma_details.replacement_order = null;
		
		device_warranty_details.buyers_remorse_end_date = Date.today();
		device_warranty_details.buyers_remorse_days_left = '10';
		device_warranty_details.doa_end_date = Date.today();
		device_warranty_details.doa_days_left = '5';
		device_warranty_details.warranty_end_date = Date.today();
		device_warranty_details.warranty_days_left = '6';
		device_warranty_details.warranty_countries = null;
		device_warranty_details.warranty_models = null;
		device_warranty_details.warranty_exceptions = null;
		
		device_purchase_details.original_purchaser = 'NA';
		device_purchase_details.purchase_date = Date.today();
		device_purchase_details.purchase_country = 'US';
		device_purchase_details.point_of_purchase = 'NA';
		device_purchase_details.google_order_id = '666';
		device_purchase_details.shipment_date = Date.today();
		device_purchase_details.shipping_tracking_number = '111';
		device_purchase_details.shipping_carrier = 'UPS';
		device_purchase_details.purchase_type = 'NA';
		
		RestResponseModel.wExceptions = null;
		
		device_rma_reasons.unique_id = 'NA';
		device_rma_reasons.rma_type_with_reasons = null;
		
		rma_type_with_reasons.rma_type = 'NA';
		rma_type_with_reasons.rma_reason = null;
		
		rma_initiation_details.unique_id = 'NA';
		rma_initiation_details.rma_requester_email = 'NA';
		rma_initiation_details.rma_requester_first_name = 'NA';
		rma_initiation_details.rma_requester_last_name = 'NA';
		rma_initiation_details.rma_type = 'NA';
		rma_initiation_details.rma_category = 'NA';
		rma_initiation_details.rma_sub_category = 'NA';
		rma_initiation_details.rma_notes = 'NA';
		rma_initiation_details.case_id = 'NA';
		rma_initiation_details.rma_creator = 'NA';
		rma_initiation_details.warranty_exceptions = 'NA';
		rma_initiation_details.rma_override = 'NA';
		rma_initiation_details.rma_overrider_email = 'NA';
		rma_initiation_details.google_order_id = 'NA';
		rma_initiation_details.rma_requester_address = null;
		rma_initiation_details.rma_action = 'NA';
		rma_initiation_details.rma_creator_name = 'NA';
		rma_initiation_details.rma_creation_date = date.today();
		rma_initiation_details.rma_creator_email = 'NA';
		rma_initiation_details.rma_creator_site = 'NA';
		
		submit_rma_response.failure_reason = 'NA';
		submit_rma_response.is_success = true;
		submit_rma_response.rma_success_details = null;
		
		rma_request_details.unique_id = 'NA';
		rma_request_details.rma_id = '00';
		rma_request_details.replacement_cart_link = 'NA';
		rma_request_details.replacement_gpn = 'NA';
		rma_request_details.rma_requester_first_name = 'NA';
		rma_request_details.rma_requester_last_name = 'NA';
		rma_request_details.rma_requester_address = null;
		
		rma_success_details.rma_id = 'NA';
		rma_success_details.shipping_label_needed = true;
		rma_success_details.canned_response = 'NA';
		rma_success_details.ar_method = 'NA';
		rma_success_details.shipping_destination_address = null;
		rma_success_details.shipping_method = 'NA';
		rma_success_details.exchange_sku_description = 'NA';
		rma_success_details.exchange_gpn = 'NA';
		rma_success_details.gpn = 'NA';
		rma_success_details.exchange_doc_id = 'NA';
		rma_success_details.shipping_center_id = 'NA';
		rma_success_details.shipping_center_name = 'NA';
		rma_success_details.need_rma_label = 'NA';
		
		update_owner_details.unique_id = 'NA';
		update_owner_details.new_owner_first_name = 'NA';
		update_owner_details.new_owner_last_name = 'NA';
		update_owner_details.new_owner_email = 'NA';
		update_owner_details.new_requester_address = null;
		
		update_owner_details_response.is_success = true;
		update_owner_details_response.failure_reason = 'NA';
		
		rma_events.rma_event_name = 'NA';
		rma_events.rma_past_value = 'NA';
		rma_events.rma_current_value = 'NA';
		rma_events.rma_event_change_date = date.today();
		rma_events.rma_event_changer = 'NA';
		
		owner_details.unique_id = 'NA';
		owner_details.new_owner_first_name = 'NA';
		owner_details.new_owner_last_name = 'NA';
		owner_details.new_owner_email = 'NA';
		owner_details.new_owner_address = null;
		
		customer_details.customer_first_name = 'NA';
		customer_details.customer_last_name = 'NA';
		customer_details.customer_email = 'NA';
		customer_details.customer_address = null;
		
		address.address_street = 'NA';
		address.address_street_2 = 'NA';
		address.address_street_3 = 'NA';
		address.address_city = 'NA';
		address.address_state_province = 'NA';
		address.address_country = 'NA';
		address.address_postal_code = 'NA';
		
		device_types.doc_id = 'NA';
		device_types.sku_gpn = 'NA';
		device_types.device_family = 'NA';
		device_types.device_sub_family = 'NA';
		device_types.doc_description = 'NA';
		
		RestResponseModel.gpn_sku_t gpn_sku = new RestResponseModel.gpn_sku_t();
		gpn_sku.gpn_sku = 'NA';
		gpn_sku.gpn_sku_description = 'NA';
		
		RestResponseModel.device_support_reasons_t device_sup = new RestResponseModel.device_support_reasons_t();
		device_sup.rma_sub_category = 'NA';
		device_sup.rma_category = 'NA';
		
		RestResponseModel.rma_reasons_t rma_reasons = new RestResponseModel.rma_reasons_t();
		rma_reasons.rma_category = 'NA';
		rma_reasons.rma_sub_category = null;
		
		RestResponseModel.rma_return_reason_code_t rma_return_reason_code = new RestResponseModel.rma_return_reason_code_t();
		rma_return_reason_code.Name = 'NA';
		rma_return_reason_code.CID = true;
		
		RestResponseModel.country_variant_model_t country_var = new RestResponseModel.country_variant_model_t();
		country_var.country_variant_country = 'NA';
		country_var.country_variant_eligible_for_cid = 'NA';
		country_var.country_variant_ar_method = 'NA';
		country_var.country_variant_doa_replacement_sku = 'NA';
		country_var.country_variant_doa_replacement_sku_description = 'NA';
		country_var.country_variant_replacement_sku = 'NA';
		country_var.country_variant_replacement_sku_description = 'NA';
		
		RestResponseModel.country_variant_details_t cvD = new RestResponseModel.country_variant_details_t();
		cvD.country_variant_models = null;
		
		// RESPONSE MODEL 2
		RestResponseModel2 restRespMod2 = new RestResponseModel2();

		RestResponseModel2.customer_records_t customer_records2 = new RestResponseModel2.customer_records_t('johnwick@levementum.com');
		RestResponseModel2.device_details_t device_details2 = new RestResponseModel2.device_details_t();
				
		RestResponseModel2.rma_details_t rma_details2 = new RestResponseModel2.rma_details_t();
		
		RestResponseModel2.device_warranty_details_t device_warranty_details2 = new RestResponseModel2.device_warranty_details_t();
		RestResponseModel2.device_purchase_details_t device_purchase_details2 = new RestResponseModel2.device_purchase_details_t();
		RestResponseModel2.device_rma_reasons_t device_rma_reasons2 = new RestResponseModel2.device_rma_reasons_t();

		RestResponseModel2.rma_type_with_reasons_t rma_type_with_reasons2 = new RestResponseModel2.rma_type_with_reasons_t();

		RestResponseModel2.rma_initiation_details_t rma_initiation_details2 = new RestResponseModel2.rma_initiation_details_t();
		RestResponseModel2.submit_rma_response_t submit_rma_response2 = new RestResponseModel2.submit_rma_response_t();
		RestResponseModel2.rma_request_details_t rma_request_details2 = new RestResponseModel2.rma_request_details_t();
		RestResponseModel2.rma_success_details_t rma_success_details2 = new RestResponseModel2.rma_success_details_t();
		RestResponseModel2.update_owner_details_t update_owner_details2 = new RestResponseModel2.update_owner_details_t();
		RestResponseModel2.update_owner_details_response_t update_owner_details_response2 = new RestResponseModel2.update_owner_details_response_t();
		RestResponseModel2.rma_events_t rma_events2 = new RestResponseModel2.rma_events_t();
		RestResponseModel2.owner_details_t owner_details2 = new RestResponseModel2.owner_details_t();
		RestResponseModel2.customer_details_t customer_details2 = new RestResponseModel2.customer_details_t();
		RestResponseModel2.address_t address2 = new RestResponseModel2.address_t();
		RestResponseModel2.device_types_t device_types2 = new RestResponseModel2.device_types_t();
				
		RestResponseModel2.warranty_model_t warranty_model2 = new RestResponseModel2.warranty_model_t();
		warranty_model.warranty_model = 'Remorse Only';
		warranty_model.rma_action_list = new List<String>();		
		
		customer_records2.cid_exception_granted_datetime = String.valueOf(Date.today());
		customer_records2.cid_exception_granted_asset = 'NA';
		customer_records2.cid_exception_on_this_asset = 'NA';
		customer_records2.cid_exception_granted = 'NA';
		customer_records2.cid_exception_granted_asset_description = 'NA';
		customer_records2.cid_exception_granted_asset_name = 'NA';
		customer_records2.device_details = null;
		
		device_details2.serial_number = '000';
		device_details2.unique_id = '0000';
		device_details2.device_description = 'NA';
		device_details2.device_sku_description = 'aaaa';
		device_details2.device_type = 'NA';
		device_details2.replacement_device_description = 'NA';
		device_details2.manufacture_date = Date.today();
		device_details2.repair_partner = 'NA';
		device_details2.device_users = null;
		device_details2.activation_date = Date.today();
		device_details2.device_return_reason_code_set = 'NA';
		device_details2.original_gpn_sku = null;
		device_details2.country_variant_details = null;
		device_details2.device_warranty_details = null;
		device_details2.device_purchase_details = null;
		device_details2.components = null;
		device_details2.substitute_gpns = null;
		
		rma_details2.email_id = 'NA';
		rma_details2.unique_id = 'aaa';
		rma_details2.rma_number = '000';
		rma_details2.order_id = '555';
		rma_details2.rma_state = 'TX';
		rma_details2.rma_tracking_number = '233';
		rma_details2.rma_carrier = 'US';
		rma_details2.refund_charge_status = 'NA';
		rma_details2.currency_name = 'Dollar';
		rma_details2.item_refund = 'NA';
		rma_details2.tax_refund = '45';
		rma_details2.total_refund = '10';
		rma_details2.expected_refund_date = Date.today();
		rma_details2.rma_events = null;
		rma_details2.rma_initiation_details = null;
		rma_details2.rma_success_details = null;
		rma_details2.replacement_order = null;
		
		device_warranty_details2.buyers_remorse_end_date = Date.today();
		device_warranty_details2.buyers_remorse_days_left = '10';
		device_warranty_details2.doa_end_date = Date.today();
		device_warranty_details2.doa_days_left = '5';
		device_warranty_details2.warranty_end_date = Date.today();
		device_warranty_details2.warranty_days_left = '6';
		device_warranty_details2.warranty_countries = null;
		device_warranty_details2.warranty_models = null;
		device_warranty_details2.warranty_exceptions = null;
		
		device_purchase_details2.original_purchaser = 'NA';
		device_purchase_details2.purchase_date = Date.today();
		device_purchase_details2.purchase_country = 'US';
		device_purchase_details2.point_of_purchase = 'NA';
		device_purchase_details2.google_order_id = '666';
		device_purchase_details2.shipment_date = Date.today();
		device_purchase_details2.shipping_tracking_number = '111';
		device_purchase_details2.shipping_carrier = 'UPS';
		device_purchase_details2.purchase_type = 'NA';
		
		RestResponseModel2.wExceptions = null;
		
		device_rma_reasons2.unique_id = 'NA';
		device_rma_reasons2.rma_type_with_reasons = null;
		
		rma_type_with_reasons2.rma_type = 'NA';
		rma_type_with_reasons2.rma_reason = null;
		
		rma_initiation_details2.unique_id = 'NA';
		rma_initiation_details2.rma_requester_email = 'NA';
		rma_initiation_details2.rma_requester_first_name = 'NA';
		rma_initiation_details2.rma_requester_last_name = 'NA';
		rma_initiation_details2.rma_type = 'NA';
		rma_initiation_details2.rma_category = 'NA';
		rma_initiation_details2.rma_notes = 'NA';
		rma_initiation_details2.case_id = 'NA';
		rma_initiation_details2.rma_creator = 'NA';
		rma_initiation_details2.warranty_exceptions = 'NA';
		rma_initiation_details2.rma_override = 'NA';
		rma_initiation_details2.rma_overrider_email = 'NA';
		rma_initiation_details2.google_order_id = 'NA';
		rma_initiation_details2.rma_requester_address = null;
		rma_initiation_details2.rma_action = 'NA';
		rma_initiation_details2.rma_creator_name = 'NA';
		rma_initiation_details2.rma_creation_date = date.today();
		rma_initiation_details2.rma_creator_email = 'NA';
		rma_initiation_details2.rma_creator_site = 'NA';
		rma_initiation_details2.rma_sub_category_cid = 'NA';
		
		submit_rma_response2.failure_reason = 'NA';
		submit_rma_response2.is_success = true;
		submit_rma_response2.rma_success_details = null;
		
		rma_request_details2.unique_id = 'NA';
		rma_request_details2.rma_id = '00';
		rma_request_details2.replacement_cart_link = 'NA';
		rma_request_details2.replacement_gpn = 'NA';
		rma_request_details2.rma_requester_first_name = 'NA';
		rma_request_details2.rma_requester_last_name = 'NA';
		rma_request_details2.rma_requester_address = null;
		
		rma_success_details2.rma_id = 'NA';
		rma_success_details2.shipping_label_needed = true;
		rma_success_details2.canned_response = 'NA';
		rma_success_details2.ar_method = 'NA';
		rma_success_details2.shipping_destination_address = null;
		rma_success_details2.shipping_method = 'NA';
		rma_success_details2.exchange_sku_description = 'NA';
		rma_success_details2.exchange_gpn = 'NA';
		rma_success_details2.gpn = 'NA';
		rma_success_details2.exchange_doc_id = 'NA';
		rma_success_details2.shipping_center_id = 'NA';
		rma_success_details2.shipping_center_name = 'NA';
		rma_success_details2.need_rma_label = 'NA';
		
		update_owner_details2.unique_id = 'NA';
		update_owner_details2.new_owner_first_name = 'NA';
		update_owner_details2.new_owner_last_name = 'NA';
		update_owner_details2.new_owner_email = 'NA';
		update_owner_details2.new_requester_address = null;
		
		update_owner_details_response2.is_success = true;
		update_owner_details_response2.failure_reason = 'NA';
		
		rma_events2.rma_event_name = 'NA';
		rma_events2.rma_past_value = 'NA';
		rma_events2.rma_current_value = 'NA';
		rma_events2.rma_event_change_date = date.today();
		rma_events2.rma_event_changer = 'NA';
		
		owner_details2.unique_id = 'NA';
		owner_details2.new_owner_first_name = 'NA';
		owner_details2.new_owner_last_name = 'NA';
		owner_details2.new_owner_email = 'NA';
		owner_details2.new_owner_address = null;
		
		customer_details2.customer_first_name = 'NA';
		customer_details2.customer_last_name = 'NA';
		customer_details2.customer_email = 'NA';
		customer_details2.customer_address = null;
		
		address2.address_street = 'NA';
		address2.address_street_2 = 'NA';
		address2.address_street_3 = 'NA';
		address2.address_city = 'NA';
		address2.address_state_province = 'NA';
		address2.address_country = 'NA';
		address2.address_postal_code = 'NA';
		
		device_types2.doc_id = 'NA';
		device_types2.sku_gpn = 'NA';
		device_types2.device_family = 'NA';
		device_types2.device_sub_family = 'NA';
		device_types2.doc_description = 'NA';
		
		RestResponseModel2.gpn_sku_t gpn_sku2 = new RestResponseModel2.gpn_sku_t();
		gpn_sku2.gpn_sku = 'NA';
		gpn_sku2.gpn_sku_description = 'NA';
		
		RestResponseModel2.device_support_reasons_t device_sup2 = new RestResponseModel2.device_support_reasons_t();
		device_sup2.rma_sub_category = 'NA';
		device_sup2.rma_category = 'NA';
		
		RestResponseModel2.rma_reasons_t rma_reasons2 = new RestResponseModel2.rma_reasons_t();
		rma_reasons2.rma_category = 'NA';
		
		RestResponseModel2.rma_return_reason_code_t rma_return_reason_code2 = new RestResponseModel2.rma_return_reason_code_t();
		rma_return_reason_code2.Name = 'NA';
		rma_return_reason_code2.CID = true;
		
		RestResponseModel2.country_variant_model_t country_var2 = new RestResponseModel2.country_variant_model_t();
		country_var2.country_variant_country = 'NA';
		country_var2.country_variant_eligible_for_cid = 'NA';
		country_var2.country_variant_ar_method = 'NA';
		country_var2.country_variant_doa_replacement_sku = 'NA';
		country_var2.country_variant_doa_replacement_sku_description = 'NA';
		country_var2.country_variant_replacement_sku = 'NA';
		country_var2.country_variant_replacement_sku_description = 'NA';
		
		RestResponseModel2.country_variant_details_t cvD2 = new RestResponseModel2.country_variant_details_t();
		cvD2.country_variant_models = null;
		
		RestResponseModel2.warranty_model_t wM = new RestResponseModel2.warranty_model_t();
		wM.warranty_model = 'NA';
		wM.rma_action_list = null;
	}
}