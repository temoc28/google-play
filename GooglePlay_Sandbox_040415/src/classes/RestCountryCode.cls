/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/14/15
 * @email:	cmunoz@levementum.com
 * @description:	Returns a list of all country codes for Country__c
 */
@RestResource(urlMapping='/CountryCode/*')
global class RestCountryCode
{	
	@HttpGet
    global static RestResponseModel.country_model_t retrieveCountryCodes()
    {
    	RestResponseModel.country_model_t response = null;
    	try
    	{
    		List<Country__c> currentCountries = CountryDataAccess.selectCountries();
    		response = updateModel(currentCountries);			
    	}		
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
        return response;
    }
    
    /**
 	 * @author:	Cuauhtemoc Munoz
 	 * @date:	04/14/15
 	 * @email:	cmunoz@levementum.com
 	 * @description:	Updates Country Model based on Country__c.
 	 * @update 1:	Return all country codes from Country__c
 	 */
    private static RestResponseModel.country_model_t updateModel(List<Country__c> updatedCountries)
    {
    	RestResponseModel.country_model_t countryModel = new RestResponseModel.country_model_t();
    	try
    	{
    		for(Country__c country : updatedCountries)
    		{
    			countryModel.country_codes.add(country.Name);
    		}	
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	return countryModel;
    }
}