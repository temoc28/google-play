@isTest
private class TestRestCancelExtendedWarranty {
	@testSetup static void setUp() {
		String expectedContractId = '111';
        Date expectedExpireDate = System.Today()+10;
        String expectedLineNumber = '4444';

        insert new Extended_Warranty__c(
        	Name = expectedContractId,
			Line_Number__c = Decimal.valueOf(expectedLineNumber),
			Expiration_Date__c = expectedExpireDate
        );
	}

	@isTest static void noParameters(){
		RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelExtendedWarranty';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.cancel_warranty_detail_t cancelWarranty = new RestResponseModel.cancel_warranty_detail_t();

        RestResponseModel.cancel_warranty_response_t result = RestCancelExtendedWarranty.cancelWarranty(cancelWarranty);

        System.assert(result != null);
        System.assert(!result.is_success);
	}

    @isTest static void noCancelDate(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelExtendedWarranty';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.cancel_warranty_detail_t cancelWarranty = new RestResponseModel.cancel_warranty_detail_t();
        cancelWarranty.cancel_date = System.today();
        RestResponseModel.cancel_warranty_response_t result = RestCancelExtendedWarranty.cancelWarranty(cancelWarranty);

        System.assert(result != null);
        System.assert(!result.is_success);
    }

	@isTest static void noContractId(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelExtendedWarranty';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.cancel_warranty_detail_t cancelWarranty = new RestResponseModel.cancel_warranty_detail_t();
        cancelWarranty.contract_id = '111';
        RestResponseModel.cancel_warranty_response_t result = RestCancelExtendedWarranty.cancelWarranty(cancelWarranty);

        System.assert(result != null);
        System.assert(!result.is_success);
    }

    @isTest static void withParameters(){

        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelExtendedWarranty';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.cancel_warranty_detail_t cancelWarranty = new RestResponseModel.cancel_warranty_detail_t();
        cancelWarranty.contract_id = '111';
        cancelWarranty.cancel_date = System.today();
        RestResponseModel.cancel_warranty_response_t result = RestCancelExtendedWarranty.cancelWarranty(cancelWarranty);

        System.assert(result != null);
        System.assert(result.is_success);
    }

    @isTest static void withNotMatchContractId(){

        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelExtendedWarranty';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.cancel_warranty_detail_t cancelWarranty = new RestResponseModel.cancel_warranty_detail_t();
        cancelWarranty.contract_id = '333';
        cancelWarranty.cancel_date = System.today();
        RestResponseModel.cancel_warranty_response_t result = RestCancelExtendedWarranty.cancelWarranty(cancelWarranty);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
}