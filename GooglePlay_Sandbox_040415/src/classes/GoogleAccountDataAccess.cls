/**
 * @author: Levementum LLC
 * @date:   04/12/15
 * @description:    General DML Operations for Account (GoogleAccount) object
 */
public with sharing class GoogleAccountDataAccess implements SObjectDataAccess
{
    public static String objectStatus = 'OK';
    
    /**
     * @author: Levementum LLC
     * @date:   04/30/15
     * @description:    Retrieves a Google Account based on an object model
     */
    public static Account selectObject(ObjectModel model)
    {
        Account googleAccount = null;
        try
        {
            String email = ((GoogleAccountModel)model).personEmail;system.debug(email);
            if(email != null)
            {
                googleAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, LastName, FirstName, Salutation, Type, RecordTypeId, ParentId, 
                                     BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
                                     BillingLongitude, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
                                     ShippingLatitude, ShippingLongitude, Phone, Fax, AccountNumber, Website, Sic, Industry, AnnualRevenue, 
                                     NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, 
                                     LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
                                     PersonContactId, IsPersonAccount, PersonMailingStreet, PersonMailingCity, PersonMailingState, 
                                     PersonMailingPostalCode, PersonMailingCountry, PersonMailingLatitude, PersonMailingLongitude, 
                                     PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountry, 
                                     PersonOtherLatitude, PersonOtherLongitude, PersonMobilePhone, PersonHomePhone, PersonOtherPhone, 
                                     PersonAssistantPhone, PersonEmail, PersonTitle, PersonDepartment, PersonAssistantName, PersonLeadSource, 
                                     PersonBirthdate, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonEmailBouncedReason, 
                                     PersonEmailBouncedDate, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, Last_Name__c, First_Name__c, 
                                     Mobile__c, Other_Phone__c, Salutation__c, LegacySFContactID__c, PersonAcctIDExternalID__c, MIgration__c, 
                                     DupFlag__c, PersonAcctContactLookup__c, Customer_Country__c, CID_Exception_Granted__c, 
                                     CID_Exception_Granted_Date_Time__c, CID_Exception_on_This_Asset__c, CID_Exception_Granted_Asset__c, 
                                     CID_Exception_Granted_Asset_Description__c, CID_Exception_Granted_Asset_Name__c, Primary_Email__pc, 
                                     Notes__pc, Email__pc, OtherEmail__pc, Migration__pc
                            FROM Account
                            WHERE PersonEmail =: email.trim() limit 1];
            }
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Google Account not found: ' + model.id;
        }
        return googleAccount;
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/12/15
     * @description:    Retrieves a Google Account based on an account id
     */
    public static Account selectObject(Id accountId)
    {
        Account googleAccount = null;
        try
        {
            googleAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, LastName, FirstName, Salutation, Type, RecordTypeId, ParentId, 
                                     BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
                                     BillingLongitude, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
                                     ShippingLatitude, ShippingLongitude, Phone, Fax, AccountNumber, Website, Sic, Industry, AnnualRevenue, 
                                     NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, 
                                     LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
                                     PersonContactId, IsPersonAccount, PersonMailingStreet, PersonMailingCity, PersonMailingState, 
                                     PersonMailingPostalCode, PersonMailingCountry, PersonMailingLatitude, PersonMailingLongitude, 
                                     PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountry, 
                                     PersonOtherLatitude, PersonOtherLongitude, PersonMobilePhone, PersonHomePhone, PersonOtherPhone, 
                                     PersonAssistantPhone, PersonEmail, PersonTitle, PersonDepartment, PersonAssistantName, PersonLeadSource, 
                                     PersonBirthdate, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonEmailBouncedReason, 
                                     PersonEmailBouncedDate, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, Last_Name__c, First_Name__c, 
                                     Mobile__c, Other_Phone__c, Salutation__c, LegacySFContactID__c, PersonAcctIDExternalID__c, MIgration__c, 
                                     DupFlag__c, PersonAcctContactLookup__c, Customer_Country__c, CID_Exception_Granted__c, 
                                     CID_Exception_Granted_Date_Time__c, CID_Exception_on_This_Asset__c, CID_Exception_Granted_Asset__c, 
                                     CID_Exception_Granted_Asset_Description__c, CID_Exception_Granted_Asset_Name__c, Primary_Email__pc, 
                                     Notes__pc, Email__pc, OtherEmail__pc, Migration__pc
                            FROM Account
                            WHERE id =: accountId limit 1];
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Google Account not found: ' + accountId;
        }
        return googleAccount;
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Retrieves a Google Account based on an account name
     */
    public static Account selectObject(String accountName)
    {
        Account googleAccount = null;
        try
        {
            googleAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, LastName, FirstName, Salutation, Type, RecordTypeId, ParentId, 
                                     BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
                                     BillingLongitude, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
                                     ShippingLatitude, ShippingLongitude, Phone, Fax, AccountNumber, Website, Sic, Industry, AnnualRevenue, 
                                     NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, 
                                     LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
                                     PersonContactId, IsPersonAccount, PersonMailingStreet, PersonMailingCity, PersonMailingState, 
                                     PersonMailingPostalCode, PersonMailingCountry, PersonMailingLatitude, PersonMailingLongitude, 
                                     PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountry, 
                                     PersonOtherLatitude, PersonOtherLongitude, PersonMobilePhone, PersonHomePhone, PersonOtherPhone, 
                                     PersonAssistantPhone, PersonEmail, PersonTitle, PersonDepartment, PersonAssistantName, PersonLeadSource, 
                                     PersonBirthdate, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonEmailBouncedReason, 
                                     PersonEmailBouncedDate, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, Last_Name__c, First_Name__c, 
                                     Mobile__c, Other_Phone__c, Salutation__c, LegacySFContactID__c, PersonAcctIDExternalID__c, MIgration__c, 
                                     DupFlag__c, PersonAcctContactLookup__c, Customer_Country__c, CID_Exception_Granted__c, 
                                     CID_Exception_Granted_Date_Time__c, CID_Exception_on_This_Asset__c, CID_Exception_Granted_Asset__c, 
                                     CID_Exception_Granted_Asset_Description__c, CID_Exception_Granted_Asset_Name__c, Primary_Email__pc, 
                                     Notes__pc, Email__pc, OtherEmail__pc, Migration__pc
                            FROM Account
                            WHERE Name =: accountName.trim() limit 1];
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Google Account not found: ' + accountName;
        }
        return googleAccount;
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Updates a Google Account
     */
    public void updateObject(sObject objectToUpdate, ObjectModel updatingData)
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(objectToUpdate == null)
            {
                insert insertObject(updatingData);
            }
            else
            {
                upsert updateModel(objectToUpdate, updatingData);
            }
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            objectStatus = ex.getMessage();
            system.debug(ex.getMessage());
        }
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Deletes a Google Accout by Name
     */
    public void deleteObject(String objectName)
    {
        try
        {
            Account accountToDelete = [SELECT id
                                       FROM Account
                                       WHERE Name =: objectName limit 1];
            delete accountToDelete;
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Unable to delete Google Account. Please check your Google Account name and try again';
        }
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Deletes a Google Accout by Id
     */
    public void deleteObject(Id objectId)
    {
        try
        {
            Account accountToDelete = [SELECT id
                                       FROM Account
                                       WHERE id =: objectId];
            delete accountToDelete;
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Unable to delete Google Account. Please check your Google Account id and try again';
        }
    }
    
    /**
     * @author: Levementum LLC
     * @date:   05/01/15
     * @description:    Deletes a Google Account by specifying the model field(s) to be deleted
     */
    public void deleteObject(ObjectModel objectModel)
    {
        try
        {
            String email = null;
            if(objectModel != null)
            {
                email = ((GoogleAccountModel)objectModel).personEmail;
            }
            else
            {
                objectStatus = 'Unable to delete Google Account. Please check your Google Account model and try again';
                return;
            }
            
            Account accountToDelete = [SELECT id
                                       FROM Account
                                       WHERE personEmail =: email];
            delete accountToDelete;
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = 'Unable to delete Google Account. Please check your Google Account id and try again';
        }
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Inserts a new Google Account/Account
     */
    public sObject insertObject(ObjectModel objectModel)
    {
        Account accountToInsert = new Account();
        GoogleAccountModel acctModel = (GoogleAccountModel)objectModel; 
        try
        {
            if(accountToInsert != null && acctModel != null)
            {
                if(acctModel.Name != null){accountToInsert.Name = acctModel.Name;}
                if(acctModel.LastName != null){accountToInsert.LastName = acctModel.LastName;}
                if(acctModel.FirstName != null){accountToInsert.FirstName = acctModel.FirstName;}
                if(acctModel.Salutation != null){accountToInsert.Salutation = acctModel.Salutation;}
                if(acctModel.Type != null){accountToInsert.Type = acctModel.Type;}
                if(acctModel.RecordTypeId != null){accountToInsert.RecordTypeId = acctModel.RecordTypeId;}
                if(acctModel.ParentId != null){accountToInsert.ParentId = acctModel.ParentId;}
                if(acctModel.BillingStreet != null){accountToInsert.BillingStreet = acctModel.BillingStreet;}
                if(acctModel.BillingCity != null){accountToInsert.BillingCity = acctModel.BillingCity;}
                if(acctModel.BillingState != null){accountToInsert.BillingState = acctModel.BillingState;}
                if(acctModel.BillingPostalCode != null){accountToInsert.BillingPostalCode = acctModel.BillingPostalCode;}
                if(acctModel.BillingCountry != null){accountToInsert.BillingCountry = acctModel.BillingCountry;}
                if(acctModel.BillingLatitude != null){accountToInsert.BillingLatitude = acctModel.BillingLatitude;}
                if(acctModel.BillingLongitude != null){accountToInsert.BillingLongitude = acctModel.BillingLongitude;}
                if(acctModel.ShippingStreet != null){accountToInsert.ShippingStreet = acctModel.ShippingStreet;}
                if(acctModel.ShippingCity != null){accountToInsert.ShippingCity = acctModel.ShippingCity;}
                if(acctModel.ShippingState != null){accountToInsert.ShippingState = acctModel.ShippingState;}
                if(acctModel.ShippingPostalCode != null){accountToInsert.ShippingPostalCode = acctModel.ShippingPostalCode;}
                if(acctModel.ShippingCountry != null){accountToInsert.ShippingCountry = acctModel.ShippingCountry;}
                if(acctModel.ShippingLatitude != null){accountToInsert.ShippingLatitude = acctModel.ShippingLatitude;}
                if(acctModel.ShippingLongitude != null){accountToInsert.ShippingLongitude = acctModel.ShippingLongitude;}
                if(acctModel.Phone != null){accountToInsert.Phone = acctModel.Phone;}
                if(acctModel.Fax != null){accountToInsert.Fax = acctModel.Fax;}
                if(acctModel.AccountNumber != null){accountToInsert.AccountNumber = acctModel.AccountNumber;}
                if(acctModel.Website != null){accountToInsert.Website = acctModel.Website;}
                if(acctModel.Sic != null){accountToInsert.Sic = acctModel.Sic;}
                if(acctModel.Industry != null){accountToInsert.Industry = acctModel.Industry;}
                if(acctModel.AnnualRevenue != null){accountToInsert.AnnualRevenue = acctModel.AnnualRevenue;}
                if(acctModel.NumberOfEmployees != null){accountToInsert.NumberOfEmployees = acctModel.NumberOfEmployees;}
                if(acctModel.Ownership != null){accountToInsert.Ownership = acctModel.Ownership;}
                if(acctModel.TickerSymbol != null){accountToInsert.TickerSymbol = acctModel.TickerSymbol;}
                if(acctModel.Description != null){accountToInsert.Description = acctModel.Description;}
                if(acctModel.Rating != null){accountToInsert.Rating = acctModel.Rating;}
                if(acctModel.Site != null){accountToInsert.Site = acctModel.Site;}
                if(acctModel.OwnerId != null){accountToInsert.OwnerId = acctModel.OwnerId;}
                if(acctModel.CreatedDate != null){accountToInsert.CreatedDate = acctModel.CreatedDate;}
                if(acctModel.CreatedById != null){accountToInsert.CreatedById = acctModel.CreatedById;}
                if(acctModel.LastModifiedDate != null){accountToInsert.LastModifiedDate = acctModel.LastModifiedDate;}
                if(acctModel.LastModifiedById != null){accountToInsert.LastModifiedById = acctModel.LastModifiedById;}
                if(acctModel.PersonMailingStreet != null){accountToInsert.PersonMailingStreet = acctModel.PersonMailingStreet;}
                if(acctModel.PersonMailingCity != null){accountToInsert.PersonMailingCity = acctModel.PersonMailingCity;}
                if(acctModel.PersonMailingState != null){accountToInsert.PersonMailingState = acctModel.PersonMailingState;}
                if(acctModel.PersonMailingPostalCode != null){accountToInsert.PersonMailingPostalCode = acctModel.PersonMailingPostalCode;}
                if(acctModel.PersonMailingCountry != null){accountToInsert.PersonMailingCountry = acctModel.PersonMailingCountry;}
                if(acctModel.PersonMailingLatitude != null){accountToInsert.PersonMailingLatitude = acctModel.PersonMailingLatitude;}
                if(acctModel.PersonMailingLongitude != null){accountToInsert.PersonMailingLongitude = acctModel.PersonMailingLongitude;}
                if(acctModel.PersonOtherStreet != null){accountToInsert.PersonOtherStreet = acctModel.PersonOtherStreet;}
                if(acctModel.PersonOtherCity != null){accountToInsert.PersonOtherCity = acctModel.PersonOtherCity;}
                if(acctModel.PersonOtherState != null){accountToInsert.PersonOtherState = acctModel.PersonOtherState;}
                if(acctModel.PersonOtherPostalCode != null){accountToInsert.PersonOtherPostalCode = acctModel.PersonOtherPostalCode;}
                if(acctModel.PersonOtherCountry != null){accountToInsert.PersonOtherCountry = acctModel.PersonOtherCountry;}
                if(acctModel.PersonOtherLatitude != null){accountToInsert.PersonOtherLatitude = acctModel.PersonOtherLatitude;}
                if(acctModel.PersonOtherLongitude != null){accountToInsert.PersonOtherLongitude = acctModel.PersonOtherLongitude;}
                if(acctModel.PersonMobilePhone != null){accountToInsert.PersonMobilePhone = acctModel.PersonMobilePhone;}
                if(acctModel.PersonHomePhone != null){accountToInsert.PersonHomePhone = acctModel.PersonHomePhone;}
                if(acctModel.PersonOtherPhone != null){accountToInsert.PersonOtherPhone = acctModel.PersonOtherPhone;}
                if(acctModel.PersonAssistantPhone != null){accountToInsert.PersonAssistantPhone = acctModel.PersonAssistantPhone;}
                if(acctModel.PersonEmail != null){accountToInsert.PersonEmail = acctModel.PersonEmail;}
                if(acctModel.PersonTitle != null){accountToInsert.PersonTitle = acctModel.PersonTitle;}
                if(acctModel.PersonDepartment != null){accountToInsert.PersonDepartment = acctModel.PersonDepartment;}
                if(acctModel.PersonAssistantName != null){accountToInsert.PersonAssistantName = acctModel.PersonAssistantName;}
                if(acctModel.PersonLeadSource != null){accountToInsert.PersonLeadSource = acctModel.PersonLeadSource;}
                if(acctModel.PersonBirthdate != null){accountToInsert.PersonBirthdate = acctModel.PersonBirthdate;}
                if(acctModel.PersonEmailBouncedReason != null){accountToInsert.PersonEmailBouncedReason = acctModel.PersonEmailBouncedReason;}
                if(acctModel.PersonEmailBouncedDate != null){accountToInsert.PersonEmailBouncedDate = acctModel.PersonEmailBouncedDate;}
                if(acctModel.Jigsaw != null){accountToInsert.Jigsaw = acctModel.Jigsaw;}
                if(acctModel.AccountSource != null){accountToInsert.AccountSource = acctModel.AccountSource;}
                if(acctModel.SicDesc != null){accountToInsert.SicDesc = acctModel.SicDesc;}
                if(acctModel.Last_Name != null){accountToInsert.Last_Name__c = acctModel.Last_Name;}
                if(acctModel.First_Name != null){accountToInsert.First_Name__c = acctModel.First_Name;}
                if(acctModel.Mobile != null){accountToInsert.Mobile__c = acctModel.Mobile;}
                if(acctModel.Other_Phone != null){accountToInsert.Other_Phone__c = acctModel.Other_Phone;}
                if(acctModel.Salutation != null){accountToInsert.Salutation__c = acctModel.Salutation;}
                if(acctModel.LegacySFContactID != null){accountToInsert.LegacySFContactID__c = acctModel.LegacySFContactID;}
                if(acctModel.PersonAcctIDExternalID != null){accountToInsert.PersonAcctIDExternalID__c = acctModel.PersonAcctIDExternalID;}
                if(acctModel.MIgration != null){accountToInsert.MIgration__c = acctModel.MIgration;}
                if(acctModel.DupFlag != null){accountToInsert.DupFlag__c = acctModel.DupFlag;}
                if(acctModel.PersonAcctContactLookup != null){accountToInsert.PersonAcctContactLookup__c = acctModel.PersonAcctContactLookup;}
                if(acctModel.CID_Exception_Granted != null){accountToInsert.CID_Exception_Granted__c = acctModel.CID_Exception_Granted;}
                if(acctModel.CID_Exception_Granted_Date_Time != null){accountToInsert.CID_Exception_Granted_Date_Time__c = acctModel.CID_Exception_Granted_Date_Time;}
                if(acctModel.CID_Exception_on_This_Asset != null){accountToInsert.CID_Exception_on_This_Asset__c = acctModel.CID_Exception_on_This_Asset;}
                if(acctModel.CID_Exception_Granted_Asset != null){accountToInsert.CID_Exception_Granted_Asset__c = acctModel.CID_Exception_Granted_Asset;}
                if(acctModel.Primary_Email_pc != null){accountToInsert.Primary_Email__pc = acctModel.Primary_Email_pc;}
                if(acctModel.Notes_pc != null){accountToInsert.Notes__pc = acctModel.Notes_pc;}
                if(acctModel.Email_pc != null){accountToInsert.Email__pc = acctModel.Email_pc;}
                if(acctModel.OtherEmail_pc != null){accountToInsert.OtherEmail__pc = acctModel.OtherEmail_pc;}
                if(acctModel.Migration_pc != null){accountToInsert.Migration__pc = acctModel.Migration_pc;}
            }
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = ex.getMessage();
        }
        return accountToInsert;
    }
    
    /**
     * @author: Levementum LLC
     * @date:   04/24/15
     * @description:    Updates current Google Account/Account
     */
    public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
    {
        Account accountToUpdate = (Account)sObjectToUpdate;
        GoogleAccountModel acctModel = (GoogleAccountModel)updatingData;
        try
        {
            if(accountToUpdate != null && updatingData != null)
            {
                if(acctModel.Name != null){accountToUpdate.Name = acctModel.Name;}
                if(acctModel.LastName != null){accountToUpdate.LastName = acctModel.LastName;}
                if(acctModel.FirstName != null){accountToUpdate.FirstName = acctModel.FirstName;}
                if(acctModel.Salutation != null){accountToUpdate.Salutation = acctModel.Salutation;}
                if(acctModel.Type != null){accountToUpdate.Type = acctModel.Type;}
                if(acctModel.RecordTypeId != null){accountToUpdate.RecordTypeId = acctModel.RecordTypeId;}
                if(acctModel.ParentId != null){accountToUpdate.ParentId = acctModel.ParentId;}
                if(acctModel.BillingStreet != null){accountToUpdate.BillingStreet = acctModel.BillingStreet;}
                if(acctModel.BillingCity != null){accountToUpdate.BillingCity = acctModel.BillingCity;}
                if(acctModel.BillingState != null){accountToUpdate.BillingState = acctModel.BillingState;}
                if(acctModel.BillingPostalCode != null){accountToUpdate.BillingPostalCode = acctModel.BillingPostalCode;}
                if(acctModel.BillingCountry != null){accountToUpdate.BillingCountry = acctModel.BillingCountry;}
                if(acctModel.BillingLatitude != null){accountToUpdate.BillingLatitude = acctModel.BillingLatitude;}
                if(acctModel.BillingLongitude != null){accountToUpdate.BillingLongitude = acctModel.BillingLongitude;}
                if(acctModel.ShippingStreet != null){accountToUpdate.ShippingStreet = acctModel.ShippingStreet;}
                if(acctModel.ShippingCity != null){accountToUpdate.ShippingCity = acctModel.ShippingCity;}
                if(acctModel.ShippingState != null){accountToUpdate.ShippingState = acctModel.ShippingState;}
                if(acctModel.ShippingPostalCode != null){accountToUpdate.ShippingPostalCode = acctModel.ShippingPostalCode;}
                if(acctModel.ShippingCountry != null){accountToUpdate.ShippingCountry = acctModel.ShippingCountry;}
                if(acctModel.ShippingLatitude != null){accountToUpdate.ShippingLatitude = acctModel.ShippingLatitude;}
                if(acctModel.ShippingLongitude != null){accountToUpdate.ShippingLongitude = acctModel.ShippingLongitude;}
                if(acctModel.Phone != null){accountToUpdate.Phone = acctModel.Phone;}
                if(acctModel.Fax != null){accountToUpdate.Fax = acctModel.Fax;}
                if(acctModel.AccountNumber != null){accountToUpdate.AccountNumber = acctModel.AccountNumber;}
                if(acctModel.Website != null){accountToUpdate.Website = acctModel.Website;}
                if(acctModel.Sic != null){accountToUpdate.Sic = acctModel.Sic;}
                if(acctModel.Industry != null){accountToUpdate.Industry = acctModel.Industry;}
                if(acctModel.AnnualRevenue != null){accountToUpdate.AnnualRevenue = acctModel.AnnualRevenue;}
                if(acctModel.NumberOfEmployees != null){accountToUpdate.NumberOfEmployees = acctModel.NumberOfEmployees;}
                if(acctModel.Ownership != null){accountToUpdate.Ownership = acctModel.Ownership;}
                if(acctModel.TickerSymbol != null){accountToUpdate.TickerSymbol = acctModel.TickerSymbol;}
                if(acctModel.Description != null){accountToUpdate.Description = acctModel.Description;}
                if(acctModel.Rating != null){accountToUpdate.Rating = acctModel.Rating;}
                if(acctModel.Site != null){accountToUpdate.Site = acctModel.Site;}
                if(acctModel.OwnerId != null){acctModel.OwnerId = acctModel.OwnerId;}
                if(acctModel.CreatedDate != null){accountToUpdate.CreatedDate = acctModel.CreatedDate;}
                if(acctModel.CreatedById != null){accountToUpdate.CreatedById = acctModel.CreatedById;}
                if(acctModel.LastModifiedDate != null){accountToUpdate.LastModifiedDate = acctModel.LastModifiedDate;}
                if(acctModel.LastModifiedById != null){accountToUpdate.LastModifiedById = acctModel.LastModifiedById;}
                if(acctModel.PersonMailingStreet != null){accountToUpdate.PersonMailingStreet = acctModel.PersonMailingStreet;}
                if(acctModel.PersonMailingCity != null){accountToUpdate.PersonMailingCity = acctModel.PersonMailingCity;}
                if(acctModel.PersonMailingState != null){accountToUpdate.PersonMailingState = acctModel.PersonMailingState;}
                if(acctModel.PersonMailingPostalCode != null){accountToUpdate.PersonMailingPostalCode = acctModel.PersonMailingPostalCode;}
                if(acctModel.PersonMailingCountry != null){accountToUpdate.PersonMailingCountry = acctModel.PersonMailingCountry;}
                if(acctModel.PersonMailingLatitude != null){accountToUpdate.PersonMailingLatitude = acctModel.PersonMailingLatitude;}
                if(acctModel.PersonMailingLongitude != null){accountToUpdate.PersonMailingLongitude = acctModel.PersonMailingLongitude;}
                if(acctModel.PersonOtherStreet != null){accountToUpdate.PersonOtherStreet = acctModel.PersonOtherStreet;}
                if(acctModel.PersonOtherCity != null){accountToUpdate.PersonOtherCity = acctModel.PersonOtherCity;}
                if(acctModel.PersonOtherState != null){accountToUpdate.PersonOtherState = acctModel.PersonOtherState;}
                if(acctModel.PersonOtherPostalCode != null){accountToUpdate.PersonOtherPostalCode = acctModel.PersonOtherPostalCode;}
                if(acctModel.PersonOtherCountry != null){accountToUpdate.PersonOtherCountry = acctModel.PersonOtherCountry;}
                if(acctModel.PersonOtherLatitude != null){accountToUpdate.PersonOtherLatitude = acctModel.PersonOtherLatitude;}
                if(acctModel.PersonOtherLongitude != null){accountToUpdate.PersonOtherLongitude = acctModel.PersonOtherLongitude;}
                if(acctModel.PersonMobilePhone != null){accountToUpdate.PersonMobilePhone = acctModel.PersonMobilePhone;}
                if(acctModel.PersonHomePhone != null){accountToUpdate.PersonHomePhone = acctModel.PersonHomePhone;}
                if(acctModel.PersonOtherPhone != null){accountToUpdate.PersonOtherPhone = acctModel.PersonOtherPhone;}
                if(acctModel.PersonAssistantPhone != null){accountToUpdate.PersonAssistantPhone = acctModel.PersonAssistantPhone;}
                if(acctModel.PersonEmail != null){accountToUpdate.PersonEmail = acctModel.PersonEmail;}
                if(acctModel.PersonTitle != null){accountToUpdate.PersonTitle = acctModel.PersonTitle;}
                if(acctModel.PersonDepartment != null){accountToUpdate.PersonDepartment = acctModel.PersonDepartment;}
                if(acctModel.PersonAssistantName != null){accountToUpdate.PersonAssistantName = acctModel.PersonAssistantName;}
                if(acctModel.PersonLeadSource != null){accountToUpdate.PersonLeadSource = acctModel.PersonLeadSource;}
                if(acctModel.PersonBirthdate != null){accountToUpdate.PersonBirthdate = acctModel.PersonBirthdate;}
                if(acctModel.PersonEmailBouncedReason != null){accountToUpdate.PersonEmailBouncedReason = acctModel.PersonEmailBouncedReason;}
                if(acctModel.PersonEmailBouncedDate != null){accountToUpdate.PersonEmailBouncedDate = acctModel.PersonEmailBouncedDate;}
                if(acctModel.Jigsaw != null){accountToUpdate.Jigsaw = acctModel.Jigsaw;}
                if(acctModel.AccountSource != null){accountToUpdate.AccountSource = acctModel.AccountSource;}
                if(acctModel.SicDesc != null){accountToUpdate.SicDesc = acctModel.SicDesc;}
                if(acctModel.Last_Name != null){accountToUpdate.Last_Name__c = acctModel.Last_Name;}
                if(acctModel.First_Name != null){accountToUpdate.First_Name__c = acctModel.First_Name;}
                if(acctModel.Mobile != null){accountToUpdate.Mobile__c = acctModel.Mobile;}
                if(acctModel.Mobile != null){accountToUpdate.Other_Phone__c = acctModel.Other_Phone;}
                if(acctModel.Salutation != null){accountToUpdate.Salutation__c = acctModel.Salutation;}
                if(acctModel.LegacySFContactID != null){accountToUpdate.LegacySFContactID__c = acctModel.LegacySFContactID;}
                if(acctModel.PersonAcctIDExternalID != null){accountToUpdate.PersonAcctIDExternalID__c = acctModel.PersonAcctIDExternalID;}
                if(acctModel.MIgration != null){accountToUpdate.MIgration__c = acctModel.MIgration;}
                if(acctModel.DupFlag != null){accountToUpdate.DupFlag__c = acctModel.DupFlag;}
                if(acctModel.PersonAcctContactLookup != null){accountToUpdate.PersonAcctContactLookup__c = acctModel.PersonAcctContactLookup;}
                if(acctModel.CID_Exception_Granted != null){accountToUpdate.CID_Exception_Granted__c = acctModel.CID_Exception_Granted;}
                if(acctModel.CID_Exception_Granted_Date_Time != null){accountToUpdate.CID_Exception_Granted_Date_Time__c = acctModel.CID_Exception_Granted_Date_Time;}
                if(acctModel.CID_Exception_on_This_Asset != null){accountToUpdate.CID_Exception_on_This_Asset__c = acctModel.CID_Exception_on_This_Asset;}
                if(acctModel.CID_Exception_Granted_Asset != null){accountToUpdate.CID_Exception_Granted_Asset__c = acctModel.CID_Exception_Granted_Asset;}
                if(acctModel.Primary_Email_pc != null){accountToUpdate.Primary_Email__pc = acctModel.Primary_Email_pc;}
                if(acctModel.Notes_pc != null){accountToUpdate.Notes__pc = acctModel.Notes_pc;}
                if(acctModel.Email_pc != null){accountToUpdate.Email__pc = acctModel.Email_pc;}
                if(acctModel.OtherEmail_pc != null){accountToUpdate.OtherEmail__pc = acctModel.OtherEmail_pc;}
                if(acctModel.Migration_pc != null){accountToUpdate.Migration__pc = acctModel.Migration_pc;}
            }
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
            objectStatus = ex.getMessage(); 
        }
        return accountToUpdate;
    }
}