public with sharing class RefundLostStolen 
{
	private Opportunity opp;	
	public Refund__c refund {get;set;}
	public RefundLostStolen(ApexPages.StandardController con)
	{
		opp = (Opportunity)con.getRecord();
		refund = new Refund__c();
		refund.RefundReason__c='Lost/Stolen';
	}
	public PageReference createLostStolenRefund()
	{		
		refund.OrderNumber__c=opp.Id;
		refund.PercentShippingItemRefund__c=100;
		refund.PercentShippingTaxRefund__c=100;
		refund.RefundStatus__c='Pending';
		//refund.RefundReason__c='Lost/Stolen';
		//check to see which date is further out
		Date plusBusinessDays = null;
		Date plusOneDay = Date.today().addDays(1);
		if(opp.Ship_By_Date__c!=null)
			plusBusinessDays = BusinessDays.addBusinessDays(opp.Ship_By_Date__c,3);
		else
			plusBusinessDays = Date.today();
		refund.ExpectedRefundDate__c = plusBusinessDays > plusOneDay ? plusBusinessDays : plusOneDay;
		refund.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Refund').getRecordTypeId();
		try
		{
			insert refund;
		}
		catch(DmlException e)
		{
			if (Apexpages.currentPage() != null) 
			{
				ApexPages.addMessages(e);
			}
			return null;
		}
		return new PageReference('/'+opp.Id);
	}
}