public class RestServiceContext 
{
	public SObjectDataAccess sObjectDA {get; set;}
	
	public RestServiceContext(SObjectDataAccess sObjectDA)
	{
		this.sObjectDA = sObjectDA;
	}
	
	public sObject selectObject(ObjectModel model)
	{
		return this.sObjectDA.selectObject(model);
	}
	
	public void updateObject(sObject obj, ObjectModel objModel)
	{
		this.SObjectDA.updateObject(obj, objModel);
	}
	
	public sObject selectObject(Id objId)
	{
		return this.sObjectDA.selectObject(objId);
	}
	
	public sObject selectObject(String objName)
	{
		return this.sObjectDA.selectObject(objName);
	}
	
	public void deleteObject(Id objId)
	{
		this.sObjectDA.deleteObject(objId);
	}
	
	public void deleteObject(String objName)
	{
		this.sObjectDA.deleteObject(objName);
	}
	
	public void deleteObject(ObjectModel objModel)
	{
		this.sObjectDA.deleteObject(objModel);
	}
	
	public sObject insertObject(ObjectModel objectModel)
	{
		return this.sObjectDA.insertObject(objectModel);
	}
	
	public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
	{
		return this.sObjectDA.updateModel(sObjectToUpdate, updatingData);
	}
	
	public static Id getId(String param)
	{
		Id rawId = null;
		try
		{
			if(param != null)
			{
	        	if((param.length() == 15 || param.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', param)) 
	        	{
	        		try
	        		{
	            		rawId = param;
	        		}
	        		catch(Exception ex)
	        		{
	        			system.debug(ex.getMessage());
	        		}
	        	}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return rawId;
	}
}