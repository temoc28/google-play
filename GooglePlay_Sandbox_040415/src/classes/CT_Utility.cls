public with sharing class CT_Utility
{       
    @future (callout=true)
    public Static void Process(Set<Id> RMAIds)
    {
        try
        {
            //Check the size of Set of Contact Ids if it is greater than ZERO
            if(RMAIds.size() > 0)
            {               
                //Fetch all Contacts from Contact Id Set
                List<RMA__C> lRMAs = [Select r.rma_country__c, r.external_RMA_num__c, r.Warranty_Repair_Status__c, 
                r.Warranty_Provider__c, r.Warranty_Identifier__c, r.WarrantyExpirationDate__c, r.WarrantyCountries__c, 
                r.VendorAssetReceivedNotificationDate__c, r.VendorAssetReceivedDate__c, r.UniqueIDType__c, r.Type__c, 
                r.Triage_Code__c, r.SystemModstamp, r.Status__c, r.Shipping_Service_Level_Code__c, r.Shipment_Date__c, 
                r.ServiceModel__c, r.SerialNumber__c, r.SKU__c, r.SKUDescription__c, r.Return_Shipping_Tracking_Number__c, 
                r.Return_Product_Instruction_Code__c, r.Return_Condition_Notes__c, r.Return_Code_Reason__c, 
                r.ReturnConditionNotificationDate__c, r.Replacement_Outbound_UPS__c, r.RepairPartner__c, r.RecordTypeId, 
                r.RMA_Sub_Category__c, r.RMA_ID_External_ID__c, r.RMA_Country_Formula__c, r.RMA_Category__c, 
                r.PersonAcctEmailAddress__c, r.OwnerId, r.Order__c, r.Order_Type__c, r.Opportunity__c, 
                r.Operating_System_Code__c, r.Notes__c, r.Needs_Shipping_Label__c, r.Name, r.MTR_ID__c, 
                r.Line_Number__c, r.LastModifiedDate, r.LastModifiedById, r.LastDateSpareParts__c, r.LastDateDOA__c, 
                r.LastDateBuyersRemorse__c, r.IsDeleted, r.Incomplete__c, r.Id, r.IMEI__c, r.Google_Order_ID__c, 
                r.Google_Asset_Unique_ID__c, r.Google_Account__c, r.GoogleCustomer__c, r.GoogleAsset__c, r.GPN__c, 
                r.Final_Asset_Disposition_Status__c, r.Failure_Observed_Condition_Code__c, r.Failure_Modified_Code__c, 
                r.Failure_Free_Form_Notes__c, r.Failure_Action_Code__c, r.Device_Condition__c, r.Date_Received__c,
                r.DataMigrationKey__c, r.Customer_changed_from_DOA_to_Remorse__c, r.Customer_Name__c, 
                r.Customer_Induced_Damage__c, r.CreatedDateHold__c, r.CreatedDate, 
                r.CreatedById, r.Condition__c, r.Condition_Code__c, r.Case__c, r.CTDataMigration__c, r.CSSN__c, 
                r.Barcode_Type__c, r.Barcode_Subtype__c, r.Amount_Refunded__c, r.Amount_Charged__c, 
                r.Amount_Authorized__c, r.Account_Street__c, r.Account_State__c, r.Account_Postal_Code__c, r.Created_on_Google_Server__c, 
                r.Account_Country__c, r.Account_City__c, r.GoogleAsset__r.ProductID__r.RepairPartner__c, r.GoogleCustomer__r.Name, r.GoogleAsset__r.Carrier__c, r.GoogleCustomer__r.Phone 
                From RMA__c r WHERE id in : RMAIds];
                // r.CustomerInducedDamage__c, r.AssetType__c, 
                //Check if there are more than no. of RMAs
                if(lRMAs.size() == 0) return;
                
                //Iterate over all fetched Contacts
                for(RMA__C r : lRMAs)
                {             
                    //Instantiate WS Stub class
                    ProcessRMAServiceController.ProcessRMAService_pt prmc = new ProcessRMAServiceController.ProcessRMAService_pt();
                    prmc.timeout_x = 120000;                   
                    
                    //Format CreatedDate for Google WS
                    /*
                    offset should be changed to use this code to account for DST
                    TimeZone tz = UserInfo.getTimeZone();
					System.debug('Display name: ' + tz.getDisplayName());
					integer offset = (tz.getOffset(System.now())) / 3600000 );
                    
                    Datetime d = r.CreatedDate;
                    String hours = (d.hour() <= 9)? '0'+d.hour() : ''+d.hour();
                    String minutes = (d.minute() <= 9)? '0'+d.minute() : ''+d.minute();
                    String seconds = (d.second() <= 9)? '0'+d.second() : ''+d.second();
                    String createdDate = d.format('yyyy-MM-dd') + 'T' + hours + ':' + minutes + ':' + seconds + '-08:00';
                    */
                    String createdDate=r.CreatedDate.format('yyyy-MM-dd\'T\'HH:mm:ssZ','America/Los_Angeles');
                    System.debug(LoggingLevel.INFO, 'Created Date ==' + createdDate);

                    //Format Warrenty Expiration Date for Google WS
                    String warrantyExpirationDate = '';
                    if(r.WarrantyExpirationDate__c != null)
                    {
                        /*d = r.WarrantyExpirationDate__c;
                        hours = (d.hour() <= 9)? '0'+d.hour() : ''+d.hour();
                        minutes = (d.minute() <= 9)? '0'+d.minute() : ''+d.minute();
                        seconds = (d.second() <= 9)? '0'+d.second() : ''+d.second();
                        warrentyExpirationDate = d.format('yyyy-MM-dd') + 'T' + hours + ':' + minutes + ':' + seconds + '-08:00';                        
                        //warrentyExpirationDate = r.WarrantyExpirationDate__c.year()+ '-' + r.WarrantyExpirationDate__c.month() + '-' + r.WarrantyExpirationDate__c.day() + 'T' + '00:00:00-08:00';
                        */
                        Datetime dt = Datetime.newInstance(r.WarrantyExpirationDate__c,Time.newInstance(0,0,0,0));
                        warrantyExpirationDate = dt.format('yyyy-MM-dd\'T\'HH:mm:ssZ','America/Los_Angeles');
                    }    
                    System.debug(LoggingLevel.INFO, 'Warrenty Expiration Date ==' + warrantyExpirationDate);
                    
                    //Convert Line number from Number to String
                    String lineNo = (r.Line_Number__c != null) ? String.valueOf(r.Line_Number__c) : '0';
                    
                    String country = (r.Account_Country__c!=null) ? r.Account_Country__c : r.RMA_Country_Formula__c;
                    String trackingNumber = (r.Return_Shipping_Tracking_Number__c!=null) ? r.Return_Shipping_Tracking_Number__c : '1111111111';
                    //Repair Partner
                    String RepairPartner = (r.GoogleAsset__r.ProductID__r.RepairPartner__c != null) ? String.valueOf(r.GoogleAsset__r.ProductID__r.RepairPartner__c) : '';
                    
                    System.debug(LoggingLevel.INFO, 'Line No. ==' + lineNo);
                    
                    //Call Google WS
                    /*String response = prmc.process(r.Name, lineNo, RepairPartner, r.Google_Asset_Unique_ID__c,
                     r.IMEI__c, r.SKU__c, createdDate, '', '', '', '', r.Return_Shipping_Tracking_Number__c, r.GoogleAsset__r.Carrier__c, 
                     r.GoogleCustomer__r.Name, r.Account_Street__c, '', '', '', r.Account_City__c, r.Account_State__c, r.Account_Country__c, r.Account_Postal_Code__c, 
                     r.GoogleCustomer__r.Phone, 'RR', warrentyExpirationDate, r.Customer_Induced_Damage__c, 
                     r.RMA_Category__c, r.RMA_Sub_Category__c, r.Notes__c, 'Pending Return');*/
                    
                    String response = prmc.process(r.Name, lineNo, RepairPartner, r.Google_Asset_Unique_ID__c,
                     r.IMEI__c, r.SKU__c, createdDate, '', '', '', '', trackingNumber, r.GoogleAsset__r.Carrier__c, 
                     'NON-REPAIR RMA', 'NON-REPAIR RMA', '', '', '', 'NON-REPAIR RMA', 'XX', country, '12345', 
                     '1234567890', 'RR', warrantyExpirationDate, r.Customer_Induced_Damage__c, 
                     r.RMA_Category__c, r.RMA_Sub_Category__c, r.Notes__c, 'Pending Return');
                                
                    //Print the result
                    System.debug(LoggingLevel.INFO, 'Response ==' + response);
                    
                    r.Created_on_Google_Server__c = true;                    
                }
                System.debug('===lRMAs==='+lRMAs);
                update lRMAs;
            }     
        }
        catch(Exception e) { System.debug('Error : ' + e.getMessage()); }
    }    
    
    static testMethod void testThisClass()
    {
        Test.startTest(); 
        
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        
        Contact con = new Contact(LastName = 'TestContact', AccountId = acc.Id, MailingStreet = '3300 Fremont Blvd Ste 100', 
            MailingState = 'CA', MailingCity = 'Fremont', MailingPostalCode = '94358');
        insert con;
        
        acc.PersonAcctContactLookup__c = con.Id;
        update acc;
        
        Id rtId = Schema.SObjectType.RMA__c.getRecordTypeInfosByName().get('Chromebook WiFi').getRecordTypeId();
        System.assertNotEquals(rtId, null);
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
        insert p;
        
        Google_Asset__c ga = new Google_Asset__c (Google_Asset_ID_External_ID__c = '9421341099', Asset_Type__c = 'New',
            Active_Device__c = 'Active', AssetOwner__c = acc.Id, Carrier__c = 'Carrier1', IMEI__c = 'IMEI',ProductId__c=p.Id );
        insert ga;
        System.assertNotEquals(ga.Id, null);
        
        Order__c ord = new Order__c(Name = 'testorder');
        insert ord;
        System.assertNotEquals(ord.Id, null);
        
        RMA__c rma = new RMA__c(rma_country__c = 'US', Type__c = 'Galaxy Nexus - Remorse', Status__c = 'Pending Return', 
            Notes__c = 'test-notes', 
            GoogleCustomer__c = acc.Id, Order__c = ord.Id, GoogleAsset__c = ga.Id,
            RecordTypeId = rtId, Line_Number__c = 1, Return_Shipping_Tracking_Number__c = 'F2012', 
            Customer_Induced_Damage__c = 'N', RMA_Sub_Category__c = 'LCD Issue', Google_Account__c = con.Id);
        insert rma;
        System.assertNotEquals(rma.Id, null);    
        
        Set<Id> RMAIds = new Set<Id>();
        RMAIds.add(rma.Id);
        CT_Utility.Process(RMAIds);
        
        Test.stopTest(); 
    }
}