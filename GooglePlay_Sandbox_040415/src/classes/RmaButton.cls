public with sharing class RmaButton 
{
	RMA__c rma;	
	public Refund__c formRefund {get;set;}
	private Map<String,ItemTaxRefundValues__c> taxMap;
	public boolean isTaxValueFound {get;set;}
	public RmaButton(ApexPages.StandardController con)
	{
		rma = (RMA__c)con.getRecord();		
		rma = [Select r.Opportunity__c,r.Triage_Code__c, r.Status__c,r.Line_Number__c, r.GoogleOrderType__c, r.Id, r.Customer_Induced_Damage__c, r.GoogleOrderCountry__c, r.GoogleOrderStateProvince__c, r.Type__c, r.OwnerId, (Select Id From Refunds__r) From RMA__c r where r.Id=:rma.Id];
		formRefund = new Refund__c();
		isTaxValueFound=true;
	}
	public PageReference saveCustomRefund()
	{
		rma.Status__c='Refund Pending';
		SavePoint sp;
		try
		{
			sp = Database.setSavepoint();
			update rma;
			createUpdateRefund(rma,'saveCustomRefund');			
		}
		catch(Exception e)
		{
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'An Error Occured: '+e.getMessage()));
			return null;
		}
		return new PageReference('/'+rma.Id);
	}
	public PageReference doAction()
	{		
		if(meetsCriteria())
		{
			if(!Test.isRunningTest())
			{
				if(rma.OwnerId != UserInfo.getUserId())
				{
					rma.addError('You are not allowed to take this action unless you are the owner.  Please claim the record');
					isTaxValueFound=false;
					return null;
				}
			}
			String action = ApexPages.currentPage().getParameters().get('action');			
			if(action=='fullRefund')
			{
				//does not query the tax table
				rma.Status__c='Refund Pending';							
			}
			else if(action=='refundWithRestockingFee')
			{				
				taxMap = getTaxMap(rma);
				if(taxMap.size()==0)
				{
					rma.addError(' No associated tax value was found');
					isTaxValueFound=false;
					return null;
				}				
				rma.Status__c='Refund Pending';
			}
			else if(action=='customRefund')
			{	
				taxMap = getTaxMap(rma);
				if(taxMap.size()==0)
				{
					rma.addError(' No associated tax value was found');
					isTaxValueFound=false;
				}
				return null;
			}
			else if(action=='closeNoRefund')
			{				
				rma.Status__c='Closed';
			}
			SavePoint sp;
			try
			{
				sp = Database.setSavepoint();
				update rma;
				if(action!='closeNoRefund')
					createUpdateRefund(rma,action);
			}
			catch(Exception e)
			{
				Database.rollback(sp);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'An Error Occured: '+e.getMessage()));
				return null;
			}
		}
		else
		{			
			PageReference pr =Page.RmaDoesNotMeetCriteria;
			pr.getParameters().put('Id',rma.Id); 
			return pr; 
		}
		return new PageReference('/'+rma.Id);
	}
	public PageReference showError()
	{
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'The following conditions must be met to create a refund: Status = Received,Triage code = C or D, Customer Induced Damage = Y or N, Google Order Type = Standard Order, Type = Buyer\'s Remorse'));
		return null;
	}
	private void createUpdateRefund(RMA__c rma,String action)
	{
		List<Refund__c> updateList = new List<Refund__c>();
		List<Refund__c> insertList = new List<Refund__c>();
		Refund__c refund;
		ItemTaxRefundValues__c refundValue;
		if(rma.Refunds__r != null && rma.Refunds__r.size()>0)
		{
			for(Refund__c refund2 : rma.Refunds__r)
			{
				refund = new Refund__c(Id=refund2.Id);				
				updateList.add(refund);	
			}
		}	
		else
		{
			refund = new Refund__c();
			refund.ExpectedRefundDate__c = Date.today().addDays(1);
			insertList.add(refund);
		}
		refund.RefundStatus__c='Pending';
		refund.OrderNumber__c = rma.Opportunity__c;
		refund.LineNumber__c = String.valueOf(rma.Line_Number__c);
		refund.RMA__c = rma.Id;		
		refund.RefundReason__c = 'RMA';
		refund.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Line-Item Refund').getRecordTypeId();
		
		if(action=='fullRefund')
		{
			refund.PercentItemRefund__c=100;
			refund.PercentTaxRefund__c=100;
		}
		else if(action=='refundWithRestockingFee')
		{			
			String key=rma.GoogleOrderStateProvince__c+rma.GoogleOrderCountry__c;
			if(!taxMap.containsKey(key))
			{
				rma.addError(' No associated tax value was found');
				return;
			}
			refundValue = taxMap.get(key);
			refund.PercentItemRefund__c=refundValue.ItemB__c;
			if(refundValue.TaxRule__c.equalsIgnoreCase('none'))
				refund.PercentTaxRefund__c=0;
			else if(refundValue.TaxRule__c.equalsIgnoreCase('prorated'))
				refund.PercentTaxRefund__c=refund.PercentItemRefund__c;
			else if(refundValue.TaxRule__c.equalsIgnoreCase('full'))
				refund.PercentTaxRefund__c=100;
		}
		else if(action=='saveCustomRefund')
		{
			refund.PercentItemRefund__c=formRefund.PercentItemRefund__c;			
			String key=rma.GoogleOrderStateProvince__c+rma.GoogleOrderCountry__c;
			if(!taxMap.containsKey(key))
			{
				key=null+rma.GoogleOrderCountry__c;				
				if(!taxMap.containsKey(key))
				{
					//rma.addError(' No associated tax value was found');
					//return;
					key=null;
					taxMap.put(key,new ItemTaxRefundValues__c(TaxRule__c='prorated',ItemA__c=100,ItemB__c=100));
				}
			}
			refundValue = taxMap.get(key);
			if(refund.PercentItemRefund__c!=100)
			{
				if(refundValue.TaxRule__c.equalsIgnoreCase('none'))
					refund.PercentTaxRefund__c=0;
				else if(refundValue.TaxRule__c.equalsIgnoreCase('prorated'))
					refund.PercentTaxRefund__c=formRefund.PercentItemRefund__c;
				else if(refundValue.TaxRule__c.equalsIgnoreCase('full'))
					refund.PercentTaxRefund__c=100;
			}
			else
				refund.PercentTaxRefund__c=100;
		}
		else if(action=='closeNoRefund')
		{
			//should we check to see if there is a refund record to delete???
		}
		if(insertList.size()>0)
			insert insertList;
		if(updateList.size()>0)
			update updateList;
	}
	private Map<String,ItemTaxRefundValues__c> getTaxMap(RMA__c rma)
	{
		Map<String,ItemTaxRefundValues__c> keyMap = new Map<String, ItemTaxRefundValues__c>();
		for(List<ItemTaxRefundValues__c> taxRefundList : [SELECT Id,TaxRule__c,ItemA__c,ItemB__c,Country__c,State__c FROM ItemTaxRefundValues__c WHERE Country__c = :rma.GoogleOrderCountry__c AND (State__c = :rma.GoogleOrderStateProvince__c OR State__c=null)])
		{
			for(ItemTaxRefundValues__c taxRefund : taxRefundList)
			{
				if(!keyMap.containsKey(taxRefund.State__c+taxRefund.Country__c))
					keyMap.put(taxRefund.State__c+taxRefund.Country__c, taxRefund);				
			}
		}
		return keyMap;
	}
	private boolean meetsCriteria()
	{
		boolean criteria=false;		
		if(rma.Status__c=='Received' && (rma.Triage_Code__c=='C'||rma.Triage_Code__c=='D') && (rma.Customer_Induced_Damage__c=='Y'||rma.Customer_Induced_Damage__c=='N') && rma.GoogleOrderType__c=='Standard Order' && rma.Type__c=='Buyer\'s Remorse')
			criteria=true;
		return criteria;
	}
}