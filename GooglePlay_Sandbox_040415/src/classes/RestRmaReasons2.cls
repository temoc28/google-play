@RestResource(urlMapping='/RmaReasons2/*')
global class RestRmaReasons2 
{	
	static List<RestResponseModel2.device_rma_reasons_t> returnList = new List<RestResponseModel2.device_rma_reasons_t>();
	
	@HttpGet
	global static List<RestResponseModel2.device_rma_reasons_t> getRmaReasons()
	{
		Double startTime = System.currentTimeMillis();
		Map<String,String> paramMap = RestContext.request.params;
		String orderNumber = paramMap.get('orderNumber');
		String rmaId = paramMap.get('rmaId');		
		String assetId = paramMap.get('assetId');
		List<Opportunity> oppList;
		List<RMA__c> rmaList;
		List<Google_Asset__c> gaList;
		Set<Id> returnReasonCodeSet = new Set<Id>();
		Map<Id,Return_Reason_Code_Set__c> rrcsMap = new Map<Id,Return_Reason_Code_Set__c>();
		
		if(String.isNotEmpty(orderNumber))
		{
			oppList = new List<Opportunity>([SELECT Id, (Select Id,Name,ProductID__r.Document__r.Return_Reason_Code_Set__c From Google_Assets__r) FROM Opportunity WHERE Name=:orderNumber]);
			for(Opportunity opp : oppList)
			{
				for(Google_Asset__c ga : opp.Google_Assets__r)
				{
					if(String.isNotEmpty(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c))
						returnReasonCodeSet.add(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c);
				}
			}
		}
		if(String.isNotEmpty(rmaId))
		{
			rmaList = new List<RMA__c>([SELECT Id,GoogleAsset__r.Name,GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c,Exception_Type__c,Exception_Reason__c,Exception_notes__c,I_understand_exception_RMAS_are_reviewed__c FROM RMA__c WHERE Name=:rmaId]);
			for(RMA__c rma : rmaList)
			{
				if(String.isNotEmpty(rma.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c))
					returnReasonCodeSet.add(rma.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c);
			}
		}
		if(String.isNotEmpty(assetId))
		{
			gaList = new List<Google_Asset__c>([SELECT Id,Name,ProductID__r.Document__r.Return_Reason_Code_Set__c FROM Google_Asset__c WHERE Name=:assetId]);
			for(Google_Asset__c ga : gaList)
			{
				if(String.isNotEmpty(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c))
					returnReasonCodeSet.add(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c);
			}
		}
		if(!returnReasonCodeSet.isEmpty())
			rrcsMap = new Map<Id,Return_Reason_Code_Set__c>([Select Id,(Select Name,Category__c, CID__c, RMA_Types__c From Return_Reason_Codes__r) From Return_Reason_Code_Set__c WHERE Id in :returnReasonCodeSet]);
		
		Set<Id> gaIdSet = new Set<Id>();
		if(oppList!=null)
		{
			for(Opportunity opp : oppList)
			{
				for(Google_Asset__c ga : opp.Google_Assets__r)
				{
					if(!gaIdSet.contains(ga.Id))
					{
						gaIdSet.add(ga.Id);										
						addGoogleAsset(ga,rrcsMap);
					}
				}
			}
		}
		if(rmaList!=null)
		{
			for(RMA__c rma : rmaList)
			{
				if(!gaIdSet.contains(rma.GoogleAsset__c))
				{					
					gaIdSet.add(rma.GoogleAsset__c);						
					if(rrcsMap.containsKey(rma.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c))
					{
						RestResponseModel2.device_rma_reasons_t device_rma_reasons = new RestResponseModel2.device_rma_reasons_t();
						device_rma_reasons.unique_id=rma.GoogleAsset__r.Name;
						Return_Reason_Code_Set__c rrcs = rrcsMap.get(rma.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c);						
						Map<String,RestResponseModel2.rma_type_with_reasons_t> rMap = new Map<String,RestResponseModel2.rma_type_with_reasons_t>();						
						RestResponseModel2.rma_type_with_reasons_t rma_type_with_reasons;
						Map<String,RestResponseModel2.rma_reasons_t> categoryMap = new Map<String,RestResponseModel2.rma_reasons_t>();
						RestResponseModel2.rma_reasons_t rma_reasons;
						Map<String,Set<String>> categoryToSubMap = new Map<String,Set<String>>();
						Map<String,Set<String>> categoryToRMATypeMap = new Map<String,Set<String>>();
						for(Return_Reason_Code__c rrc : rrcs.Return_Reason_Codes__r)
						{
							for(String rmaType : rrc.RMA_Types__c.split(';'))
							{
								if(!rMap.containsKey(rmaType))
								{
									rma_type_with_reasons = new RestResponseModel2.rma_type_with_reasons_t();
									rma_type_with_reasons.rma_type=rmaType;									
									rMap.put(rmaType,rma_type_with_reasons);
									device_rma_reasons.rma_type_with_reasons.add(rma_type_with_reasons);
								}
								rma_type_with_reasons = rMap.get(rmaType);
								if(!categoryMap.containsKey(rrc.Category__c))
								{
									rma_reasons = new RestResponseModel2.rma_reasons_t();
									rma_reasons.rma_category=rrc.Category__c;
									categoryMap.put(rrc.Category__c,rma_reasons);
									//rma_type_with_reasons.rma_reason.add(rma_reasons);
								}
								rma_reasons=categoryMap.get(rrc.Category__c);
								if(!categoryToSubMap.containsKey(rrc.Category__c))
								{
									categoryToSubMap.put(rrc.Category__c,new Set<String>());
								}				
								if(!categoryToSubMap.get(rrc.Category__c).contains(rrc.Name))
								{
									RestResponseModel2.rma_return_reason_code_t rrcode = new RestResponseModel2.rma_return_reason_code_t();
									//rma_reasons.rma_sub_category.add(rrc.Name);
									rrcode.CID = rrc.CID__c; rrcode.Name = rrc.Name;
									rma_reasons.rma_sub_category_cid.add(rrcode);
									categoryToSubMap.get(rrc.Category__c).add(rrc.Name);
								}
								
								if(!categoryToRMATypeMap.containsKey(rrc.Category__c))
								{
									categoryToRMATypeMap.put(rrc.Category__c,new Set<String>());
								}
								if(!categoryToRMATypeMap.get(rrc.Category__c).contains(rmaType))
								{
									rma_type_with_reasons.rma_reason.add(rma_reasons);
									categoryToRMATypeMap.get(rrc.Category__c).add(rmaType);
								}
							}
						}
						returnList.add(device_rma_reasons);
					}
				}
			}
		}
		if(gaList!=null)
		{
			for(Google_Asset__c ga : gaList)
			{
				if(!gaIdSet.contains(ga.Id))
				{
					gaIdSet.add(ga.Id);
					addGoogleAsset(ga,rrcsMap);
				}
			}
		}
		Double endTime = System.currentTimeMillis();
		system.debug('RUNNING TIME IN MS: ' + (endTime - startTime));
		return returnList;
	}
	private static void addGoogleAsset(Google_Asset__c ga,Map<Id,Return_Reason_Code_Set__c> rrcsMap)
	{				
		if(rrcsMap.containsKey(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c))
		{
			RestResponseModel2.device_rma_reasons_t device_rma_reasons = new RestResponseModel2.device_rma_reasons_t();
			device_rma_reasons.unique_id=ga.Name;
			Return_Reason_Code_Set__c rrcs = rrcsMap.get(ga.ProductID__r.Document__r.Return_Reason_Code_Set__c);
			Map<String,RestResponseModel2.rma_type_with_reasons_t> rMap = new Map<String,RestResponseModel2.rma_type_with_reasons_t>();						
			RestResponseModel2.rma_type_with_reasons_t rma_type_with_reasons;
			Map<String,RestResponseModel2.rma_reasons_t> categoryMap = new Map<String,RestResponseModel2.rma_reasons_t>();
			RestResponseModel2.rma_reasons_t rma_reasons;
			Map<String,Set<String>> categoryToSubMap = new Map<String,Set<String>>();
			Map<String,Set<String>> categoryToRMATypeMap = new Map<String,Set<String>>();
			for(Return_Reason_Code__c rrc : rrcs.Return_Reason_Codes__r)
			{
				for(String rmaType : rrc.RMA_Types__c.split(';'))
				{
					if(!rMap.containsKey(rmaType))
					{
						rma_type_with_reasons=new RestResponseModel2.rma_type_with_reasons_t();
						rma_type_with_reasons.rma_type=rmaType;
						rMap.put(rmaType,rma_type_with_reasons);
						device_rma_reasons.rma_type_with_reasons.add(rma_type_with_reasons);
					}
					rma_type_with_reasons=rMap.get(rmaType);
					if(!categoryMap.containsKey(rrc.Category__c))
					{
						rma_reasons = new RestResponseModel2.rma_reasons_t();
						rma_reasons.rma_category=rrc.Category__c;
						categoryMap.put(rrc.Category__c,rma_reasons);					
						//rma_type_with_reasons.rma_reason.add(rma_reasons);
					}
					rma_reasons=categoryMap.get(rrc.Category__c);
					if(!categoryToSubMap.containsKey(rrc.Category__c))
					{
						categoryToSubMap.put(rrc.Category__c,new Set<String>());
					}				
					if(!categoryToSubMap.get(rrc.Category__c).contains(rrc.Name))
					{
						RestResponseModel2.rma_return_reason_code_t rrcode = new RestResponseModel2.rma_return_reason_code_t();
						rrcode.CID = rrc.CID__c; rrcode.Name = rrc.Name;
						rma_reasons.rma_sub_category_cid.add(rrcode);
						//rma_reasons.rma_sub_category.add(rrc.Name);
						categoryToSubMap.get(rrc.Category__c).add(rrc.Name);
					}
					//rma_type_with_reasons.rma_reason.add(rma_reasons);

					// BEN Jan,11 2015 Fixed rma_category and RMA_subcategory values are return blank when call to RmaReason API
					if(!categoryToRMATypeMap.containsKey(rrc.Category__c))
					{
						categoryToRMATypeMap.put(rrc.Category__c,new Set<String>());
					}
					if(!categoryToRMATypeMap.get(rrc.Category__c).contains(rmaType))
					{
						rma_type_with_reasons.rma_reason.add(rma_reasons);
						categoryToRMATypeMap.get(rrc.Category__c).add(rmaType);
					}
				}
			}
			returnList.add(device_rma_reasons);
		}		
	}
}