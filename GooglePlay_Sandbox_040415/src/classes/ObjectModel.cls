global abstract class ObjectModel 
{
	public Id Id {get; set;}
	public Boolean isDeleted {get; set;}
	public String CreatedById {get; set;}
	public DateTime CreatedDate {get; set;}
	public Id LastModifiedById {get; set;}
	public DateTime LastModifiedDate {get; set;}
	public DateTime SystemModstamp {get; set;}
}