@isTest
private with sharing class TestRestOrder {
	@isTest static void testUpdateObjectByName() {
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/Order/';
        rReq.httpMethod = 'POST';
        rReq.addParameter('orderName','testNameOp');

        RestContext.request = rReq;
        RestContext.response = rRes;

        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;

        Opportunity opp = new Opportunity();
        opp.Name='testNameOp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='US';
        opp.State_Province__c='TX';
        insert opp;

        OpportunityModel om = (OpportunityModel)RestOrder.getObject();
        System.assert(om != null);
        OpportunityModel omResult = (OpportunityModel)RestOrder.updateObject(om);
        System.assert(omResult != null);
	}
	@isTest static void testUpdateObjectById() {
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/Order/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;

        Opportunity opp = new Opportunity();
        opp.Name='testNameOp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='US';
        opp.State_Province__c='TX';
        insert opp;

        rReq.addParameter('id',opp.Id);

        OpportunityModel om = (OpportunityModel)RestOrder.getObject();
        System.assert(om != null);
        OpportunityModel omResult = (OpportunityModel)RestOrder.updateObject(om);
        System.assert(omResult != null);
	}
	@isTest static void testInsertByName() {
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/Order/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        rReq.addParameter('orderName','testNameOp');
        
        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;

        OpportunityModel om = (OpportunityModel)RestOrder.getObject();
        System.assert(om != null);
        OpportunityModel omResult = (OpportunityModel)RestOrder.updateObject(om);
        System.assert(omResult != null);
	}
}