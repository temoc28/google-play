/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with @isTest the
 * keyword in the method definition.   
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRmaButton 
{
    
    @isTest static void customRefundTest() 
    {    	
        RMA__c rma;
        Account a = new Account();
        a.Name='TestAccount';
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type=null;
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
        insert p;

        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductId__c=p.Id;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Received';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;   
        rma.OwnerId = UserInfo.getUserId(); 
        insert rma;  

        PageReference pr = Page.RmaButton;
        pr.getParameters().put('action','customRefund');
        Test.setCurrentPageReference(pr);
        RmaButton rmaB = new RmaButton(new ApexPages.StandardController(rma));
        rmaB.doAction();
        rmaB.formRefund.PercentItemRefund__c=25;
        rmaB.saveCustomRefund();        
    	
    	 //add tax value
        ItemTaxRefundValues__c itr = new ItemTaxRefundValues__c();
    	itr.Country__c='US';
    	itr.State__c='GA';
    	itr.ItemA__c=25;
    	itr.ItemB__c=50;
    	itr.TaxRule__c='Prorated';
    	insert itr;	        
    	
    	PageReference pr2 = Page.RmaButton;
        pr2.getParameters().put('action','customRefund');
        Test.setCurrentPageReference(pr2);
        RmaButton rmaB2 = new RmaButton(new ApexPages.StandardController(rma));
        rmaB2.doAction();
        rmaB2.formRefund.PercentItemRefund__c=25;
        rmaB2.saveCustomRefund();
    }
    @isTest static void fullRefundTest()
    {    	
        RMA__c rma;
        Account a = new Account();
        a.Name='TestAccount';
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type=null;
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
        insert p;

        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductId__c=p.Id;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Received';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;   
        rma.OwnerId = UserInfo.getUserId(); 
        insert rma;  

    	PageReference pr = Page.RmaButton;
    	pr.getParameters().put('action','fullRefund');
    	Test.setCurrentPageReference(pr);
        RmaButton rmaB = new RmaButton(new ApexPages.StandardController(rma));
        rmaB.doAction();
        
        //add tax value
        ItemTaxRefundValues__c itr = new ItemTaxRefundValues__c();
    	itr.Country__c='US';
    	itr.State__c='GA';
    	itr.ItemA__c=25;
    	itr.ItemB__c=50;
    	itr.TaxRule__c='Prorated';
    	insert itr;        
        
        rmaB.doAction();
    }
    @isTest static void refundWithRestockingFeeTest()
    {    	
        RMA__c rma;
        Account a = new Account();
        a.Name='TestAccount';
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type=null;
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
        insert p;

        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductId__c=p.Id;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Received';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;   
        rma.OwnerId = UserInfo.getUserId(); 
        insert rma;  

    	PageReference pr = Page.RmaButton;
    	pr.getParameters().put('action','refundWithRestockingFee');
    	Test.setCurrentPageReference(pr);
        RmaButton rmaB = new RmaButton(new ApexPages.StandardController(rma));
        rmaB.doAction();
        
         //add tax value
        ItemTaxRefundValues__c itr = new ItemTaxRefundValues__c();
    	itr.Country__c='US';
    	itr.State__c='GA';
    	itr.ItemA__c=25;
    	itr.ItemB__c=50;
    	itr.TaxRule__c='Prorated';
    	insert itr;        
        
        rmaB.doAction();
    }
    @isTest static void closeNoRefundTest()
    {    	
        RMA__c rma;
        Account a = new Account();
        a.Name='TestAccount';
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type=null;
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Chromecast Test',Product_Family__c=pf.Id,RMARecordType__c='Chromecast');
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='SSSS',IsActive=true,Document__c=d.Id,ServiceModel__c='Remorse + DOA + Warranty');
        insert p;

        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductId__c=p.Id;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Received';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;   
        rma.OwnerId = UserInfo.getUserId(); 
        insert rma;  

    	PageReference pr = Page.RmaButton;
    	pr.getParameters().put('action','closeNoRefund');
    	Test.setCurrentPageReference(pr);
        RmaButton rmaB = new RmaButton(new ApexPages.StandardController(rma));
        rmaB.doAction();
    }
}