global class GlassDataToPlayWS 
{
	webservice static String createAll(AccountObject ao,OpportunityObject oppObj,AssetObject assetObj,RMAObject rmaObj)
	{
		String returnMessage='Success!';
		System.savePoint sp = Database.setSavepoint();
		ao = returnAccountObject(ao);		
		if(ao.hasError==null )
		{
			oppObj.account=ao.accountId;
			oppObj = createOpportunity(oppObj);
			System.debug(oppObj.hasError);
			if(oppObj.hasError==null)
			{
				assetObj.orderId=oppObj.opportunityId;
				assetObj.assetOwner=ao.accountId;
				assetObj = createAsset(assetObj);
				if(assetObj.hasError==null)
				{
					rmaObj.googleOrderId=oppObj.opportunityId;
					rmaObj.googleCustomer=ao.accountId;
					rmaObj.googleAsset=assetObj.assetId;
					rmaObj = createRma(rmaObj);
					if(rmaObj.hasError==null)
					{
						//success
					}
					else
						returnMessage=rmaObj.errorMessage;
				}
				else
					returnMessage=assetObj.errorMessage;
			}
			else
				returnMessage=oppObj.errorMessage;
		}
		else 
			returnMessage=ao.errorMessage;
		if(returnMessage!='Success!')
			Database.rollback(sp);
		
		return returnMessage;
	}
	webservice static AccountObject returnAccountObject(AccountObject ao)
	{
		List<Account> accountList = new List<Account>([SELECT Id FROM Account WHERE PersonEmail=:ao.email AND RecordType.DeveloperName='GoogleCustomer' LIMIT 1]);
		if(accountList.size()==1)
			ao.accountId =accountList.get(0).Id;
		else
		{
			Account a = new Account();
			a.FirstName=ao.firstName;
			a.LastName=ao.lastName;
			a.PersonEmail=ao.email;
			a.PersonMailingCity=ao.mailingCity;
			a.PersonMailingCountry=ao.mailingCountry;			
			a.PersonMailingPostalCode=ao.mailingPostalCode;
			a.PersonMailingState=ao.mailingState;
			a.PersonMailingStreet=ao.mailingStreet;
			a.Phone=ao.phone;
			a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
			try
			{
				insert a;
				ao.accountId = a.Id;
			}
			catch(Exception e)
			{
				ao.hasError=true;
				ao.errorMessage=e.getMessage();
			}			
		}
		return ao;
	}
	webservice static OpportunityObject createOpportunity(OpportunityObject oppObj)
	{
		List<Opportunity> oppList = new List<Opportunity>([SELECT Id FROM Opportunity WHERE OrderIdExternalId__c=:oppObj.name]);
		if(oppList.size()==1)
			oppObj.opportunityId=oppList.get(0).Id;
		else
		{
			Opportunity opp = new Opportunity();
			opp.Name = oppObj.name;
			opp.CloseDate=oppObj.closeDate;
			opp.AccountId=oppObj.account;
			opp.StageName=oppObj.stageName;
			opp.ShipmentDate__c=oppObj.shipmentDate;
			opp.Type='Glass Order';		
			try
			{
				insert opp;
				oppObj.opportunityId=opp.Id;
			}
			catch(Exception e)
			{
				oppObj.hasError=true;
				oppObj.errorMessage=e.getMessage();
			}
		}
		return oppObj;
	}	
	webservice static AssetObject createAsset(AssetObject assetObj)
	{
		List<Google_Asset__c> assetList = new List<Google_Asset__c>([SELECT Id FROM Google_Asset__c WHERE Name=:assetObj.serialNumber LIMIT 1]);
		if(assetList.size()==1)
			assetObj.assetId =assetList.get(0).Id;
		else
		{
			Google_Asset__c ga = new Google_Asset__c();
			ga.Name=assetObj.name;
			ga.SKU__c=assetObj.sku;
			ga.Order_Opportunity__c=assetObj.orderId;
			ga.Serial__c=assetObj.serialNumber;		
			ga.AssetOwner__c=assetObj.assetOwner;
			ga.Line_Number__c=0;			
			//not writeable
			//ga.Sale_Country__c=assetObj.saleCountry;
			//ga.RMA_Exceptions__c=assetObj.rmaExceptions;
			try
			{
				ga.ProductID__c=[Select Id from Product2 where SKU__c=:assetObj.sku].Id;
				insert ga;
				assetObj.assetId=ga.Id;
			}
			catch(Exception e)
			{
				assetObj.hasError=true;
				assetObj.errorMessage=e.getMessage();
			}	
		}	
		return assetObj;
	}
	webservice static RmaObject createRma(RmaObject rmaObj)
	{
		RMA__c rma = new RMA__c();
		rma.GoogleCustomer__c=rmaObj.googleCustomer;
		rma.GlassRmaNumber__c=rmaObj.name;
		//rma.Google_Account__c=rmaObj.googleAccount;
		//field is not writeable in play
		//rma.Customer_Name__c=rmaObj.customerName;
		rma.Opportunity__c=rmaObj.googleOrderId;
		rma.GoogleAsset__c=rmaObj.googleAsset;
		rma.Type__c=rmaObj.typeOfOrder;
		rma.GlassRmaId__c=rmaObj.glassRmaId;
		rma.Status__c=rmaObj.status;
		rma.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='RMA__c' AND DeveloperName='Google_Glass'].Id;
		try
		{
			upsert rma RMA__c.GlassRmaId__c;
		}
		catch(Exception e)
		{
			rmaObj.hasError=true;
			rmaObj.errorMessage=e.getMessage();
		}
		return rmaObj;
	}	
	webservice static List<RmaObject> changeRmaStatus(List<RmaObject> rmaObjList)
	{
		List<Rma__c> rmaList = new List<Rma__c>();
		for(RmaObject rma : rmaObjList)
		{
			rmaList.add(new Rma__c(GlassRmaId__c=rma.rmaId,Status__c=rma.status));
		}
		List<Database.Upsertresult> srList = Database.upsert(rmaList,RMA__c.GlassRmaId__c,false);
		integer i=0;
		for(Database.UpsertResult sr : srList)
		{
			if(!sr.isSuccess())
			{	
				rmaObjList.get(i).hasError=true;
				rmaObjList.get(i).errorMessage=sr.getErrors()[0].getMessage();				
			}
			i++;
		}
		return rmaObjList;
	}
	global class AccountObject
	{
		webservice String errorMessage {get;set;}
        webservice boolean hasError {get;set;}
		webservice String accountId {get;set;}
		webservice String name {get;set;}
		webservice String firstName {get;set;}
		webservice String lastName {get;set;}
		webservice String email {get;set;}
		webservice String mailingStreet {get;set;}
		webservice String mailingState {get;set;}
		webservice String mailingPostalCode {get;set;}
		webservice String mailingCountry {get;set;}
		webservice String mailingCity {get;set;}		
		webservice String phone {get;set;}		
	}
	global class OpportunityObject
	{
		webservice String errorMessage {get;set;}
        webservice boolean hasError {get;set;}
		webservice String opportunityId {get;set;}
		webservice String name {get;set;}		
		webservice String typeOfOrder {get;set;}
		webservice String account {get;set;}
		webservice Date closeDate {get;set;}
		webservice String stageName {get;set;}
		webservice String country {get;set;}
		webservice Date shipmentDate {get;set;}
	}	
	global class RmaObject 
	{
		webservice String errorMessage {get;set;}
        webservice boolean hasError {get;set;}
		webservice String rmaId {get;set;}
		webservice String name {get;set;}
		webservice String googleCustomer {get;set;}
		webservice String googleAccount {get;set;}
		webservice String customerName {get;set;}
		webservice String googleOrderId {get;set;}
		webservice String googleAsset {get;set;}
		webservice String typeOfOrder {get;set;}
		webservice String glassRmaId {get;set;}
		webservice String status {get;set;}	
	}
	global class AssetObject 
	{
		webservice String errorMessage {get;set;}
        webservice boolean hasError {get;set;}
        webservice String assetId {get;set;}
		webservice String name {get;set;}
		webservice String assetOwner {get;set;}
		webservice String sku {get;set;}
		webservice String orderId {get;set;}
		webservice String serialNumber {get;set;}		
		webservice String serviceModel {get;set;}
		webservice String rmaExceptions {get;set;}
		webservice String saleCountry {get;set;}
	}
}