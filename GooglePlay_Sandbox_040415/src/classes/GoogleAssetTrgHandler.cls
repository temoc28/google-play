public without sharing class GoogleAssetTrgHandler
{
	public static void onBeforeInsert(Google_Asset__c[] trgNew)
	{
		Map<String, Google_Asset__c> googleAssetMap = new Map<String, Google_Asset__c>();
		Set<String> skuSet = new Set<String>();
		List<Google_Asset__c> warrantyList = new List<Google_Asset__c>();
		Set<Id> advancedReplacementSet = new Set<Id>();
		Map<String,String> productDocumentMap = new Map<String,String>();
		Set<String> documentSet = new Set<String>();
		Set<String> productIDSet = new Set<String>();
		Set<String> saleCountrySet = new Set<String>();
		for (Google_Asset__c ga : trgNew)
		{
			if (ga.Name != null)
			{
				if (googleAssetMap.containsKey(ga.Name))
					ga.Name.addError('Another new Google Asset has the same name.');
				else
					googleAssetMap.put(ga.Name, ga);
			}
			ga.Google_Asset_ID_External_ID__c = ga.Name;
		}
		for (Google_Asset__c ga : [SELECT Name FROM Google_Asset__c WHERE Name IN :googleAssetMap.KeySet()])
		{
			Google_Asset__c newGoogleAsset = googleAssetMap.get(ga.Name);
			newGoogleAsset.Name.addError('A Google Asset with this name address already exists.');
		}
		//put the SKUUpsert value in sku if sku is null
		for(Google_Asset__c ga : trgNew)
		{
			if(ga.SKU__c == null && ga.SKUUpsert__c != null)
				ga.SKU__c = ga.SKUUpsert__c;

			if(ga.SKU__c!=null)
				skuSet.add(ga.SKU__c);
		}
		if(!skuSet.isEmpty())
		{
			Map<String, Product2> pMap = new Map<String,Product2>();
			Map<String, Map<String,Country_Variant__c>> countryVariantMap = new Map<String,Map<String,Country_Variant__c>>();
			Map<String, Country__c> countryMap = new Map<String,Country__c>();
			//Map<String, CountryRules__c> countryRulesMap = CountryRules__c.getAll();
			for(Product2 p:[Select Id,SKU__c,RemorsePeriod__c,DOAPeriod__c,MinimumStandardWarranty__c,MinimumReplacementWarranty__c,ExpectedSalesDate__c from Product2 where SKU__c in :skuSet])
				pMap.put(p.SKU__c, p);

			System.debug('pMap:' + pMap);
			for(Google_Asset__c ga : trgNew)
			{
				System.debug('sku' + ga.SKU__c);

				if(pMap.containsKey(ga.SKU__c))
				{
					ga.ProductID__c=pMap.get(ga.SKU__c).Id;
					productIDSet.add(ga.ProductID__c);
					//if it's an advanced replacement let the WarrantyHandler handle the transfer of the warranty
					if(String.isNotEmpty(ga.Sale_Country__c))
					{
						warrantyList.add(ga);
						//handleEntitlement(pMap,countryRulesMap,ga);
						if(ga.Order_Type__c=='Advanced Replacement')
							advancedReplacementSet.add(ga.Order_Opportunity__c);
					}
				}
				if(ga.Sale_Country__c != null){
					saleCountrySet.add(ga.Sale_Country__c);
				}
			}

			for(Product2 tempProduct:[Select Id,Document__c from Product2 where Id in :productIDSet]){
				documentSet.add(String.valueOf(tempProduct.Document__c));
				productDocumentMap.put(String.valueOf(tempProduct.Id), String.valueOf(tempProduct.Document__c));
			}

			for(Country_Variant__c each:[Select Id,Name,Document__c,Remorse_Period__c,DOA_Period__c,Warranty_Period__c,Minimum_Replacement_Warranty_Period__c,Expected_Sale_Period__c from Country_Variant__c where Document__c in :documentSet]){
				if(countryVariantMap.containsKey(String.valueOf(each.Document__c))){
					countryVariantMap.get(String.valueOf(each.Document__c)).put(String.valueOf(each.Name),each);
				}else{
					Map<String,Country_Variant__c> tempCountryVariant = new Map<String,Country_Variant__c>();
					tempCountryVariant.put(String.valueOf(each.Name), each);
					countryVariantMap.put(String.valueOf(each.Document__c), tempCountryVariant);
				}
			}

			for(Country__c eachCountry:[Select Id,Name,Remorse_Period__c,DOA_Period__c,Warranty_Period__c,Minimum_Replacement_Warranty_Period__c,Expected_Sale_Period__c from Country__c where Name in :saleCountrySet]){
				countryMap.put(String.valueOf(eachCountry.Name), eachCountry);
			}

			if(!warrantyList.isEmpty()){
				System.debug('onBeforeInsert');
				System.debug('advancedReplacementSet:'+advancedReplacementSet);
				handleEntitlement(pMap,countryVariantMap,warrantyList,advancedReplacementSet,countryMap,productDocumentMap);
			}

		}
	}

	public static void onBeforeUpdate(Google_Asset__c[] trgNew,Map<Id,Google_Asset__c> oldMap)
	{
		Map<String, Google_Asset__c> googleAssetMap = new Map<String, Google_Asset__c>();
		Set<String> skuSet = new Set<String>();
		Google_Asset__c oldGa;
		List<Google_Asset__c> warrantyList = new List<Google_Asset__c>();
		Set<Id> advancedReplacementSet = new Set<Id>();
		Map<String,String> productDocumentMap = new Map<String,String>();
		Set<String> documentSet = new Set<String>();
		Set<String> productIDSet = new Set<String>();
		Set<String> saleCountrySet = new Set<String>();
		for(Google_Asset__c ga : trgNew)
		{
			if(ga.Name!=null && ga.Name != oldMap.get(ga.Id).Name)
			{
				ga.Google_Asset_ID_External_ID__c = ga.Name;
				if (googleAssetMap.containsKey(ga.Name))
					ga.Name.addError('Another new Google Asset has the same name.');
				else
					googleAssetMap.put(ga.Name, ga);
			}
		}
		if(!googleAssetMap.isEmpty())
		{
			for (Google_Asset__c ga : [SELECT Name FROM Google_Asset__c WHERE Name IN :googleAssetMap.KeySet()])
			{
				Google_Asset__c newGoogleAsset = googleAssetMap.get(ga.Name);
				newGoogleAsset.Name.addError('A Google Asset with this name address already exists.');
			}
		}
		for(Google_Asset__c ga:trgNew)
		{
			oldGa = oldMap.get(ga.Id);
			System.debug('GA:'+ga);
			System.debug('OLD_GA:'+oldGa);
			if(ga.SKU__c == null && ga.SKUUpsert__c != null)
			{
				ga.SKU__c = ga.SKUUpsert__c;
			}
			//only update the ProductID__c field if the sku changed
			if (
					(ga.SKU__c != null && ((ga.SKU__c != null && ga.SKU__c != oldGa.SKU__c ) ||
					(ga.ProductID__c == null && ga.SKU__c != null) ||
					(ga.ProductID__c!=oldGa.ProductID__c) ||
					(ga.Order_Opportunity__c!=oldGa.Order_Opportunity__c) ||
					(ga.Retail_Sale_Date__c!=oldGa.Retail_Sale_Date__c) ||
					 ga.Order_Type__c=='Advanced Replacement'))
				)
				skuSet.add(ga.SKU__c.trim());
		}

		if(!skuSet.isEmpty())
		{
			Map<String, Product2> pMap = new Map<String,Product2>();
			Map<String, Map<String,Country_Variant__c>> countryVariantMap = new Map<String,Map<String,Country_Variant__c>>();
			Map<String, Country__c> countryMap = new Map<String,Country__c>();
			//Map<String, CountryRules__c> countryRulesMap = CountryRules__c.getAll();

			for(Product2 p:[Select Id,SKU__c,RemorsePeriod__c,DOAPeriod__c,MinimumStandardWarranty__c,MinimumReplacementWarranty__c,ExpectedSalesDate__c from Product2 where SKU__c in :skuSet])
				pMap.put(p.SKU__c.trim(), p);

			for(Google_Asset__c ga : trgNew)
			{
				oldGa = oldMap.get(ga.Id);
				if (
					(ga.SKU__c != null && ga.SKU__c != oldGa.SKU__c ) ||
					(ga.ProductID__c == null && ga.SKU__c != null) ||
					(ga.ProductID__c!=oldGa.ProductID__c) ||
					(ga.Order_Opportunity__c!=oldGa.Order_Opportunity__c) ||
					(ga.Retail_Sale_Date__c!=oldGa.Retail_Sale_Date__c) ||
					 ga.Order_Type__c=='Advanced Replacement')
				{
					ga.ProductID__c=null;
				}
				if(pMap.containsKey(ga.SKU__c))
				{
					ga.ProductID__c=pMap.get(ga.SKU__c).Id;
					productIDSet.add(ga.ProductID__c);
					//if it's an advanced replacement let the WarrantyHandler handle the transfer of the warranty
					//we only care that the  Sale_Country__c and ProductID__c is not blank and it's not Advanced Replacement
					//if not blank, check to see if entitlements have been applied, if so, check to see if the product ID, Google Order ID or retail sale date
					//has changed
					if(String.isNotEmpty(ga.Sale_Country__c))
					{
						warrantyList.add(ga);
						//handleEntitlement(pMap,countryRulesMap,ga);
						if(ga.Order_Type__c=='Advanced Replacement')
							advancedReplacementSet.add(ga.Order_Opportunity__c);
					}
				}
				if(ga.Sale_Country__c != null){
					saleCountrySet.add(ga.Sale_Country__c);
				}
			}

			for(Product2 tempProduct:[Select Id,Document__c from Product2 where Id in :productIDSet]){
				documentSet.add(String.valueOf(tempProduct.Document__c));
				productDocumentMap.put(String.valueOf(tempProduct.Id), String.valueOf(tempProduct.Document__c));
			}

			for(Country_Variant__c each:[Select Id,Name,Document__c,Remorse_Period__c,DOA_Period__c,Warranty_Period__c,Minimum_Replacement_Warranty_Period__c,Expected_Sale_Period__c from Country_Variant__c where Document__c in :documentSet]){
				if(countryVariantMap.containsKey(String.valueOf(each.Document__c))){
					countryVariantMap.get(String.valueOf(each.Document__c)).put(String.valueOf(each.Name),each);
				}else{
					Map<String,Country_Variant__c> tempCountryVariant = new Map<String,Country_Variant__c>();
					tempCountryVariant.put(String.valueOf(each.Name), each);
					countryVariantMap.put(String.valueOf(each.Document__c), tempCountryVariant);
				}
			}

			for(Country__c eachCountry:[Select Id,Name,Remorse_Period__c,DOA_Period__c,Warranty_Period__c,Minimum_Replacement_Warranty_Period__c,Expected_Sale_Period__c from Country__c where Name in :saleCountrySet]){
				countryMap.put(String.valueOf(eachCountry.Name), eachCountry);
			}

			if(!warrantyList.isEmpty()){
				System.debug('onBeforeUpdate');
				System.debug('advancedReplacementSet:'+advancedReplacementSet);
				handleEntitlement(pMap,countryVariantMap,warrantyList,advancedReplacementSet,countryMap,productDocumentMap);
			}
		}
	}

	public static void onAfterInsert(List<Google_Asset__c> trgNew)
	{
		Set<Id> gaIdSet = new Set<Id>();
		for(Google_Asset__c ga : trgNew)
		{
			if(ga.ProductFamily__c=='Glass' && String.isNotEmpty(ga.Order_Opportunity__c) && String.isNotEmpty(ga.AssetOwner__c)/*&& String.isEmpty(ga.GlassErrorMessage__c)&& !ga.GlassSent__c */)
				gaIdSet.add(ga.Id);
		}
		if(!gaIdSet.isEmpty())
			PlayToGlass.sendDataFuture(gaIdSet);
	}

	public static void onAfterUpdate(List<Google_Asset__c> trgNew,Map<Id,Google_Asset__c> oldMap)
	{
		Set<Id> gaIdSet = new Set<Id>();
		for(Google_Asset__c ga : trgNew)
		{
			if(ga.ProductFamily__c=='Glass' && String.isNotEmpty(ga.Order_Opportunity__c) && String.isNotEmpty(ga.AssetOwner__c) && String.isEmpty(ga.GlassErrorMessage__c)&& (!ga.GlassSent2__c || (ga.Order_Opportunity__c!=oldMap.get(ga.Id).Order_Opportunity__c)))
				gaIdSet.add(ga.Id);
		}
		if(!gaIdSet.isEmpty())
		{
			integer sendSize=gaIdSet.size();
			Map<String,GlassIntegrationBatchSize__c> gibMap = GlassIntegrationBatchSize__c.getAll();
			integer sizeLimit=Integer.valueOf(gibMap.get('sendDataFuture').BatchSize__c);
			Set<Id> sendIdSet = new Set<Id>();
			integer currentCount=0;
			integer totalCount=0;
			for(Id gaId : gaIdSet)
			{
				sendIdSet.add(gaId);
				currentCount++;
				totalCount++;
				if(sendIdSet.size()==sizeLimit || totalCount==sendSize)
				{
					PlayToGlass.sendDataFuture(sendIdSet);
					sendIdSet.clear();
					currentCount=0;
				}
			}
		}
	}
	private static void handleEntitlement(Map<String, Product2> pMap,Map<String, Map<String,Country_Variant__c>> countryVariantMap,List<Google_Asset__c> gaList,Set<Id> advancedReplacementSet,Map<String, Country__c> countryMap,Map<String,String> productDocumentMap)
	{
		Product2 p;
		CountryRules__c cr;
		Country_Variant__c countryVariant;
		Country__c country;
		Map<Id,List<RMA__c>> replacementToRmaMap = new Map<Id,List<RMA__c>>();
		List<Id> extendedWarrantyId = new List<Id>();
		Date dToday = Date.today();
		System.debug('pMap:'+pMap);
		System.debug('countryVariantMap:'+countryVariantMap);
		System.debug('gaList:'+gaList);
		System.debug('advancedReplacementSet:'+advancedReplacementSet);
		System.debug('countryMap:'+countryMap);
		System.debug('productDocumentMap:'+productDocumentMap);
		if(!advancedReplacementSet.isEmpty())
		{
			//fields below represent the asset that is new
			List<RMA__c> rmaResults = [SELECT Id,Type__c,GoogleAsset__c,GoogleAsset__r.ProductID__c,GoogleAsset__r.Sale_Country__c,GoogleAsset__r.DOA_Period__c,GoogleAsset__r.Remorse_Period__c,GoogleAsset__r.Warranty_Period__c,GoogleAsset__r.Minimum_Replacement_Warranty_Period__c,GoogleAsset__r.Warranty_Expiration_Date__c,GoogleAsset__r.Expected_Sale_Days__c,GoogleAsset__r.Extended_Warranty__c,GoogleAsset__r.Active_Extended_Warranty__c,Opportunity__c,ReplacementOrder__c FROM RMA__c WHERE ReplacementOrder__c IN :advancedReplacementSet AND GoogleAsset__c != null];
			System.debug('rmaResults:'+rmaResults);
			for(RMA__c rma : rmaResults)
			{
				if(rma.GoogleAsset__r.Extended_Warranty__c != null){
					extendedWarrantyId.add(rma.GoogleAsset__r.Extended_Warranty__c);
				}
				if(!replacementToRmaMap.containsKey(rma.ReplacementOrder__c)){
					replacementToRmaMap.put(rma.ReplacementOrder__c,new List<RMA__c>());
				}
				replacementToRmaMap.get(rma.ReplacementOrder__c).add(rma);
			}
		}
		System.debug('replacementToRmaMap:'+replacementToRmaMap);

		for(Google_Asset__c ga : gaList)
		{
			if(String.isNotEmpty(ga.Sale_Country__c)){
				if(countryVariantMap.containsKey(String.valueOf(productDocumentMap.get(String.valueOf(ga.ProductID__c))))){
					Map<String,Country_Variant__c> tempCountryVariantMap = new Map<String,Country_Variant__c>();
					tempCountryVariantMap = countryVariantMap.get(String.valueOf(productDocumentMap.get(String.valueOf(ga.ProductID__c))));
					if(tempCountryVariantMap.containsKey(String.valueOf(ga.Sale_Country__c))){
						// get from country variant
						countryVariant = tempCountryVariantMap.get(String.valueOf(ga.Sale_Country__c));
						ga.DOA_Period__c=countryVariant.DOA_Period__c;
						ga.Warranty_Period__c=countryVariant.Warranty_Period__c;
						ga.Remorse_Period__c=countryVariant.Remorse_Period__c;
						ga.Minimum_Replacement_Warranty_Period__c=countryVariant.Minimum_Replacement_Warranty_Period__c;
						ga.Expected_Sale_Days__c=countryVariant.Expected_Sale_Period__c;
					}else if(countryMap.containsKey(String.valueOf(ga.Sale_Country__c))){
						// get from country
						country = countryMap.get(String.valueOf(ga.Sale_Country__c));
						ga.DOA_Period__c=country.DOA_Period__c;
						ga.Warranty_Period__c=country.Warranty_Period__c;
						ga.Remorse_Period__c=country.Remorse_Period__c;
						ga.Minimum_Replacement_Warranty_Period__c=country.Minimum_Replacement_Warranty_Period__c;
						ga.Expected_Sale_Days__c=country.Expected_Sale_Period__c;
					}
				}else{
					if(countryMap.containsKey(String.valueOf(ga.Sale_Country__c))){
						// get from country
						country = countryMap.get(String.valueOf(ga.Sale_Country__c));
						ga.DOA_Period__c=country.DOA_Period__c;
						ga.Warranty_Period__c=country.Warranty_Period__c;
						ga.Remorse_Period__c=country.Remorse_Period__c;
						ga.Minimum_Replacement_Warranty_Period__c=country.Minimum_Replacement_Warranty_Period__c;
						ga.Expected_Sale_Days__c=country.Expected_Sale_Period__c;
					}
				}
			}
			System.debug('::ga::'+ga);

			if(ga.Order_Type__c=='Advanced Replacement')
			{
				//attempt to transfer the warranty if the RMA__c.Type__c == Warranty Regular
				if(replacementToRmaMap.containsKey(ga.Order_Opportunity__c))
				{
					//If more than 1 result, then use the entitlement logic(note: this should never happen, but should be included to handle this error case)
					//If 1 result (as expected), transfer the longer of the warranty.
					List<RMA__c> rmaList = replacementToRmaMap.get(ga.Order_Opportunity__c);
					if(rmaList.size()==1)
					{
						RMA__c rma = rmaList.get(0);
						integer originalRemaining=0;
						System.debug('Active_Extended_Warranty__c::'+rma.GoogleAsset__r.Active_Extended_Warranty__c);
						if(rma.GoogleAsset__r.Active_Extended_Warranty__c){
							ga.Extended_Warranty__c = rma.GoogleAsset__r.Extended_Warranty__c;
							List<Extended_Warranty__c> extendedWarrantyList = new List<Extended_Warranty__c>();
							Extended_Warranty__c tempExtendedWarranty = new Extended_Warranty__c();
							extendedWarrantyList = [SELECT Id,Current_Active_Asset__c,Google_Order__c FROM Extended_Warranty__c WHERE Id=:rma.GoogleAsset__r.Extended_Warranty__c LIMIT 1];
							System.debug('extendedWarrantyList:'+extendedWarrantyList);
							if(extendedWarrantyList.size() == 1){
								tempExtendedWarranty = extendedWarrantyList.get(0);
								tempExtendedWarranty.Current_Active_Asset__c = ga.Id;
								System.debug('tempExtendedWarranty:'+tempExtendedWarranty);
								update tempExtendedWarranty;
							}
						}

						if(rma.Type__c == 'Warranty Regular' || rma.Type__c == 'Extended Warranty'){
							if(rma.GoogleAsset__r.Warranty_Expiration_Date__c!=null){
								originalRemaining = dToday.daysBetween(rma.GoogleAsset__r.Warranty_Expiration_Date__c);
							}
							ga.Warranty_Period__c=ga.Minimum_Replacement_Warranty_Period__c>originalRemaining?ga.Minimum_Replacement_Warranty_Period__c:originalRemaining;
							ga.DOA_Period__c=-50;
							ga.Remorse_Period__c=-50;
						}
					}
				}
			}

		}
	}
}