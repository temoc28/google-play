public with sharing class RMADetailController {
	public Rma__c rmaToEdit {get; set;}
	public ApexPages.StandardController controller {get; set;}
	@TestVisible
	private Map<String,Map<String,List<String>>> rmaCategoryMap {get;set;}
	public List<SelectOption> optionsType {get; set;}
	public List<SelectOption> optionsCat {get; set;}
	public List<SelectOption> optionsSubCat {get; set;}
	public Integer counter {get; set;}
	public RMADetailController(ApexPages.StandardController std) {
		try
		{
			controller = std;
			rmaToEdit = (Rma__c)std.getRecord();
			rmaCategoryMap = new Map<String,Map<String,List<String>>>();
			counter = 0;
			if(rmaToEdit.Id != null)
			{
				String rmaId = rmaToEdit.Id;
				SObjectType soType = Schema.getGlobalDescribe().get('RMA__c');
   				Map<String,Schema.SObjectField> currentFields = soType.getDescribe().fields.getMap();
   				String queryRMA = 'SELECT ';
   				for(String currentField : currentFields.keySet())
   				{
   					queryRMA += currentField + ', ';
   				}
   				queryRMA += 'GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c FROM RMA__c WHERE Id =: rmaId Limit 1';
   				rmaToEdit = Database.query(queryRMA);
				getOptionMap();
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}

	public void getOptionMap()
	{
		try
		{
			Return_Reason_Code_Set__c reasonCode = ([Select Name,Id
													From Return_Reason_Code_Set__c
													where Id =: rmaToEdit.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c limit 1]);
			if(reasonCode != null)
			{
				List<Return_Reason_Code__c> rCodes = [Select Name, Category__c, CID__c, RMA_Types__c, Return_Reason_Code_Set__c from Return_Reason_Code__c where Return_Reason_Code_Set__c =: reasonCode.Id];
				for(Return_Reason_Code__c rrc : rCodes)
				{
					if(rrc.RMA_Types__c != null && rrc.RMA_Types__c.length() > 0)
					{
						String[] typesSplitter = rrc.RMA_Types__c.split(';');
						if(typesSplitter.size() <= 0)
						{
							typesSplitter = new String[1];
							typesSplitter[0] = rrc.RMA_Types__c;
						}
						for(String rmaType : typesSplitter)
						{
							if(!rmaCategoryMap.containsKey(rmaType))
							{
								Map<String,List<String>> mapcat = new Map<String,List<String>>();
								List<String> nameToadd = new List<String>();
								if(rrc.Name != null)
								{
									nameToadd.add(rrc.Name);
								}
								if(rrc.Category__c != null)
								{
									mapcat.put(rrc.Category__c, nameToadd);
								}
								rmaCategoryMap.put(rmaType, mapcat);
							}
							else
							{
								Map<String,List<String>> mapcat = rmaCategoryMap.get(rmaType);
								if(mapcat.containsKey(rrc.Category__c))
								{
									List<String> nameToadd = mapcat.get(rrc.Category__c);
									if(rrc.Name != null && rrc.Name.length() >0)
									{
										nameToadd.add(rrc.Name);
									}
									mapcat.put(rrc.Category__c, nameToadd);
									rmaCategoryMap.put(rmaType, mapcat);
								}
								else
								{
									List<String> nameToadd = new List<String>();
									if(rrc.Name != null && rrc.Name.length() >0)
									{
										nameToadd.add(rrc.Name);
									}
									if(rrc.Category__c != null && rrc.Category__c.length() > 0)
									{
										mapcat.put(rrc.Category__c, nameToadd);
										rmaCategoryMap.put(rmaType, mapcat);
									}
								}
							}

						}
					}
				}
			}
			getAllOptions();
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}

	public void getAllOptions()
	{
		try
		{
			optionsType = getTypes();
			optionsCat = getCategories();
			optionsSubCat = getSubCategories();
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}
	public List<SelectOption> getTypes(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0)
			{
				for(String s : rmaCategoryMap.keySet())
				{
					options.add(new SelectOption(s,s));
				}
			}
			if(counter > 3)
			{
				saveRecord();
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
		counter ++;
		return options;
	}

	public List<SelectOption> getCategories(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0 && rmaCategoryMap.containsKey(rmaToEdit.Type__c) && rmaCategoryMap.get(rmaToEdit.Type__c).size() >0)
			{
				for(String s : rmaCategoryMap.get(rmaToEdit.Type__c).keySet())
				{
					options.add(new SelectOption(s,s));
				}
			}
			if(counter > 3)
			{
				saveRecord();
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
		counter++;
		return options;
	}
	public List<SelectOption> getSubCategories(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0 && rmaCategoryMap.containsKey(rmaToEdit.Type__c) && rmaCategoryMap.get(rmaToEdit.Type__c).size() > 0 &&  rmaCategoryMap.get(rmaToEdit.Type__c).containsKey(rmaToEdit.RMA_Category__c) &&  rmaCategoryMap.get(rmaToEdit.Type__c).get(rmaToEdit.RMA_Category__c).size() > 0)
			{
				for(String s : rmaCategoryMap.get(rmaToEdit.Type__c).get(rmaToEdit.RMA_Category__c))
				{
					options.add(new SelectOption(s,s));
				}
				system.debug('Sub options ' + options);
			}
			if(counter > 3)
			{
				saveRecord();
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
		counter ++;
		return options;
	}
	public PageReference saveRecord()
	{
		try
		{
			if(rmaToEdit.Type__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Type: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Category: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Sub_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Sub-Category: Validation Error: Value is required.'));
				return null;
			}
			upsert rmaToEdit;
			return new PageReference('/'+ rmaToEdit.Id);
		}
		catch(System.DMLException e) {
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, e.getDMLMessage(0)));
			return null;
		}
	}
}