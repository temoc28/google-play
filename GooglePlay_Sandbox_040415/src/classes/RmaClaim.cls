public with sharing class RmaClaim 
{
	public List<RMA__c> rmaList {get;set;}
	private RMA__c rma;
	private final Id currentUserId;
	private boolean showPage;
	
	public RmaClaim(ApexPages.StandardController con)
	{
		showPage=false;		
		currentUserId = UserInfo.getUserId();
		rma = (RMA__c)con.getRecord();
		rmaList = new List<RMA__c>([SELECT Id, Owner.Type,OwnerId,Name FROM RMA__c where Id=:rma.Id]);
		//refundList.add(refund);
		checkOwnership(rmaList);		
	}
	public RmaClaim(ApexPages.StandardSetController setCon)
	{
		showPage=false;
		currentUserId = UserInfo.getUserId();
		rmaList = new List<RMA__c>([SELECT Id, Owner.Type,OwnerId,Name FROM RMA__c where Id in :setCon.getRecords()]);
		checkOwnership(rmaList);		
	}
	private void checkOwnership(List<RMA__c> rmaList)
	{
		for(RMA__c rma : rmaList)
		{
			if(rma.Owner.Type=='User' && rma.OwnerId!=currentUserId)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Are you sure you want to take ownership of this RMA from another Support Rep?'));
				showPage=true;
				break;
			}
		}
	}
	public PageReference checkClaim()
	{
		if(showPage)
			return null;
		else
			return claim();
	}
	public PageReference claim()
	{		
		for(RMA__c rma : rmaList)
			rma.OwnerId = currentUserId;
					
		update rmaList;	
		
		if(rmaList.size()==1)
			return new PageReference('/'+rmaList.get(0).Id);
		else
			return new PageReference('/a0B');
	}
}