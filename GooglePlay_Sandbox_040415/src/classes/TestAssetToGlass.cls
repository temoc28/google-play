/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAssetToGlass 
{
    static testMethod void myUnitTest() 
    {
        Google_Asset__c ga;
        Account a = new Account();
        a.Name='TestAccount';
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product2 p2 = new Product2(Sale_Country__c = 'UK',Name='TestProduct',ProductCode='xxxBBBccc',Family='Glass',SKU__c='xxxBBBccc',IsActive=true);
        insert p2;
        
        ga = new Google_Asset__c();
        ga.Name='46654';
        ga.SKU__c='xxxBBBccc';
        ga.Order_Opportunity__c=opp.Id;
        ga.ProductId__c = p2.Id;
        ga.Line_Number__c=1;
        insert ga;

        AssetToGlass atg = new AssetToGlass(new ApexPages.StandardController(ga));
        atg.sendToGlass();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TestMockImpl());
        PlayToGlass.sendDataFuture(new Set<Id>{ga.Id});
        Test.stopTest();
    }
}