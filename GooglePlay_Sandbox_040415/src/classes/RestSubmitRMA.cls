/*
		Description: Gets theGoogle asset based on the unique Id that was sent in. if it doesn't exist one then it creates one.
		Gets Account related if one doesn't exist it creates a new one.
		It creates a new RMA__c using the details of the request body sent in.
		sends back the details of the shipping center depending on the information sent in.
	*/
@RestResource(urlMapping='/SubmitRMA/*')
global class RestSubmitRMA
{
	public static String GOOGLE_CUSTOMER ='GoogleCustomer';
	@HttpPost
	global static RestResponseModel.submit_rma_response_t doSubmitRMAs(RestResponseModel.rma_initiation_details_t requestDetails)
	{
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();

		List<Opportunity> orderList = new List<Opportunity>();
		List<Google_Asset__c> assetList = new List<Google_Asset__c>();
		List<Extended_Warranty__c> extendList = new List<Extended_Warranty__c>();
		List<Account> accountList = new List<Account>();

		Opportunity orderObj;
		Google_Asset__c assetObj;
		Extended_Warranty__c extendObj;
		Account accountObj;

		Boolean queryOpp=true;
		Id oppId;

		Savepoint sp = Database.setSavepoint();

		if(requestDetails.rma_type != null && requestDetails.rma_type.trim().equalsIgnoreCase('Warranty Refund'))
		{			
			assetList = [SELECT Id,RecordTypeId,RecordType.Name,Order_Opportunity__c,Line_Number__c,Service_Model__c,ProductID__r.Document__r.RMARecordType__c,Extended_Warranty__c 
						 FROM Google_Asset__c 
						 WHERE Name=:requestDetails.unique_id];

			if(assetList.size()!=1)
			{
				submit_rma_response.failure_reason='Device not found';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}
			else
			{
				assetObj = assetList.get(0);
				oppId=assetObj.Order_Opportunity__c;
				if(assetObj.RecordTypeId == RestSubmitRMA.getRecordTypeAssetIdByName('Google Play Asset') && oppId==null)
				{
					submit_rma_response.failure_reason='This is a Google Play Asset but there is not a related order.';
					submit_rma_response.is_success=false;
					return submit_rma_response;
				}
			}
			accountList = [SELECT Id,FirstName,LastName,PersonMailingStreet,PersonMailingCity,PersonMailingState,PersonMailingCountry,PersonMailingPostalCode FROM Account WHERE PersonEmail=:requestDetails.rma_requester_email];

			if(accountList.size()!=1)
			{
				submit_rma_response.failure_reason='Customer not found';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}else accountObj = accountList.get(0);
	
			submit_rma_response.rma_success_details = new RestResponseModel.rma_success_details_t();
			submit_rma_response.rma_success_details.shipping_label_needed = false;
		}

		//EXTENDED WARRANTY LOGIC
		else if(requestDetails.rma_type != null && requestDetails.rma_type.trim().equalsIgnoreCase('Extended Warranty')){
			if(requestDetails.extended_warranty_claim_id==null){
				submit_rma_response.failure_reason = 'No Extended Warranty Claim Id.';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}else if(requestDetails.sku==null){
				submit_rma_response.failure_reason = 'No SKU';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}else{
				List<RMA__c> checkingExistRMAList = new List<RMA__c>();
				checkingExistRMAList = [SELECT Name,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,Type__c,RMA_Action__c,GoogleAsset__c,SKU__c,Extended_Warranty_Contract_ID_Backup__c,Extended_Warranty_Claim_ID__c FROM RMA__c WHERE Extended_Warranty_Claim_ID__c=:requestDetails.extended_warranty_claim_id AND Type__c='Extended Warranty'];
				if(checkingExistRMAList.size()!=0){
					submit_rma_response.failure_reason='There is already an open Extended Warranty RMA for the asset with the same claim ID : ' + requestDetails.extended_warranty_claim_id;
					submit_rma_response.is_success=false;

					submit_rma_response.rma_success_details = new RmaDetailsBuilder()
					.setCV(checkingExistRMAList.get(0).RMA_Country_Workflow__c,checkingExistRMAList.get(0).GoogleAsset__r.ProductID__r.Document__c)
					.setRMA(checkingExistRMAList.get(0))
					.stampParameter()
					.getRMASuccessDetail();

					submit_rma_response.rma_success_details.sku = checkingExistRMAList.get(0).SKU__c;
					submit_rma_response.rma_success_details.rma_id=checkingExistRMAList.get(0).Name;
					//submit_rma_response.rma_success_details.extended_warranty_claim_id = checkingExistRMAList.get(0).Extended_Warranty_Claim_ID__c;
					submit_rma_response.rma_success_details.extended_warranty_contract_id = checkingExistRMAList.get(0).Extended_Warranty_Contract_ID_Backup__c;

					return submit_rma_response;
				}
			}

			try{
				//Query account information from rma_requestor_email, if account does not exist create a new person account
				accountList = [SELECT Id,FirstName,LastName,PersonMailingStreet,PersonMailingCity,PersonMailingState,PersonMailingCountry,PersonMailingPostalCode FROM Account WHERE PersonEmail=:requestDetails.rma_requester_email LIMIT 1];
				if(accountList.size() == 0){//create new account
					accountObj = new Account();
					accountObj.FirstName =  requestDetails.rma_requester_first_name;
					accountObj.LastName =  requestDetails.rma_requester_last_name;
					accountObj.PersonEmail =  requestDetails.rma_requester_email;
					accountObj.PersonMailingStreet=requestDetails.rma_requester_address.address_street;
					accountObj.PersonMailingCity=requestDetails.rma_requester_address.address_city;
					accountObj.PersonMailingState=requestDetails.rma_requester_address.address_state_province;
					accountObj.PersonMailingCountry=requestDetails.rma_requester_address.address_country;
					accountObj.PersonMailingPostalCode=requestDetails.rma_requester_address.address_postal_code;

					accountObj.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName=:RestSubmitRMA.GOOGLE_CUSTOMER].Id;

					insert accountObj;
				}
				else
					accountObj = accountList.get(0);

				orderList = [SELECT Id, Country__c FROM Opportunity WHERE Name=:requestDetails.google_order_id LIMIT 1];
				if(orderList.size() == 0){//create new order
					orderObj = new Opportunity();
					orderObj.Name = requestDetails.google_order_id;
					orderObj.StageName = RestCreateExtendedWarranty.CLOSED_WON;
					orderObj.CloseDate = System.Today();
					orderObj.Country__c = requestDetails.rma_requester_address.address_country;


					try{
						insert orderObj;
					}
					catch(DMLException dmle){
						submit_rma_response.failure_reason=dmle.getMessage() + dmle.getStackTraceString();
		                submit_rma_response.is_success=false;
		                Database.rollBack(sp);
		                return submit_rma_response;
					}

				}
				else
				{
					orderObj = orderList.get(0);
					if(orderObj.Country__c == null)
					{
						orderObj.Country__c = requestDetails.rma_requester_address.address_country;
						update orderObj;
					}
				}

				assetList = [SELECT Id,RecordTypeId,RecordType.Name,Order_Opportunity__c,Line_Number__c,Service_Model__c,ProductID__r.Document__r.RMARecordType__c,Sale_Country__c,Extended_Warranty__c  FROM Google_Asset__c WHERE Name=:requestDetails.unique_id LIMIT 1];
				if(assetList.size() == 0){//create new assert

					List<Product2> productList = new List<Product2>();
					productList = [SELECT Id, Sale_Country__c FROM Product2 WHERE SKU__c = :requestDetails.sku];
					if(productList.size() ==0){
						submit_rma_response.failure_reason='Cannot Find Any Products From SKU';
						submit_rma_response.is_success=false;
						Database.rollBack(sp);
						return submit_rma_response;
					}else{
						assetObj = new Google_Asset__c();
						assetObj.Name = requestDetails.unique_id;
						assetObj.SKU__c = requestDetails.sku;
						assetObj.ProductID__c = productList.get(0).Id;
						assetObj.RecordTypeId = RestSubmitRMA.getRecordTypeAssetIdByName('Google Play Asset');
						assetObj.Order_Opportunity__c = orderObj.Id;

						//If new asset cannot be created, roll back and return error
						try{
							insert assetObj;
						}
						catch(DMLException dmle){
							submit_rma_response.failure_reason=dmle.getMessage() + dmle.getStackTraceString();
							submit_rma_response.is_success=false;
							Database.rollBack(sp);
							return submit_rma_response;
						}

						oppId=assetObj.Order_Opportunity__c;
					}
				}
				else
				{
					assetObj = assetList.get(0);
					oppId = assetObj.Order_Opportunity__c;
				}

			}catch(Exception e){
				submit_rma_response.failure_reason=e.getMessage();
				submit_rma_response.is_success=false;
				Database.rollBack(sp);
				return submit_rma_response;
			}
		}
		//Non extended warranty logic
		else{
			assetList = [SELECT Id,RecordTypeId,RecordType.Name,Order_Opportunity__c,Line_Number__c,Service_Model__c,ProductID__r.Document__r.RMARecordType__c,Extended_Warranty__c FROM Google_Asset__c WHERE Name=:requestDetails.unique_id];

			if(assetList.size()!=1)
			{
				submit_rma_response.failure_reason='Device not found';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}
			else
			{
				assetObj = assetList.get(0);
				oppId=assetObj.Order_Opportunity__c;
				if(assetObj.RecordTypeId == RestSubmitRMA.getRecordTypeAssetIdByName('Google Play Asset') && oppId==null)
				{
					submit_rma_response.failure_reason='This is a Google Play Asset but there is not a related order.';
					submit_rma_response.is_success=false;
					return submit_rma_response;
				}
			}
			accountList = [SELECT Id,FirstName,LastName,PersonMailingStreet,PersonMailingCity,PersonMailingState,PersonMailingCountry,PersonMailingPostalCode FROM Account WHERE PersonEmail=:requestDetails.rma_requester_email];

			if(accountList.size()!=1)
			{
				submit_rma_response.failure_reason='Customer not found';
				submit_rma_response.is_success=false;
				return submit_rma_response;
			}else accountObj = accountList.get(0);
		}
		/*
        removed in V1 - per Jordan
        if(assetObj.RecordType.Name!='Google Play Asset')
        {
            List<Opportunity> oppList = new List<Opportunity>([SELECT Id FROM Opportunity WHERE Name=:requestDetails.google_order_id]);
            if(oppList.size()!=1)
            {
                submit_rma_response.failure_reason='Order not found';
                submit_rma_response.is_success=false;
                return submit_rma_response;
            }
            else
                oppId=oppList.get(0).Id;
        }*/
		String ownerId;

		// cmunoz@levementum.com. 04/28/15. Check if enough licenses are available iff a user is inactive and requires reactivation
		List<User> uList = new List<User>([SELECT Id, IsActive FROM User WHERE LDAP__c=:requestDetails.rma_creator]);

		if(uList.size()==1)
		{
			if(!uList.get(0).IsActive)
			{
				List<UserLicense> salesforceLicense = [SELECT LicenseDefinitionKey, TotalLicenses, UsedLicenses FROM UserLicense WHERE LicenseDefinitionKey =: 'SFDC'];

				if(salesforceLicense.size() == 1)
				{
					if(salesforceLicense.get(0).usedLicenses < salesforceLicense.get(0).totalLicenses)
					{
						try
						{
							RestSubmitRMA.updateUser(uList.get(0).Id);
						}
						catch(Exception ex)
						{
							submit_rma_response.failure_reason='User not active: '+ requestDetails.rma_creator;
							submit_rma_response.is_success=false;
							return submit_rma_response;
						}
					}
					else
					{
						submit_rma_response.failure_reason='Not enough Salesforce licenses. Cannot activate user: '+ requestDetails.rma_creator;
						submit_rma_response.is_success=false;
						return submit_rma_response;
					}
				}
			}
			ownerId=uList.get(0).Id;
		}
		else
		{
			submit_rma_response.failure_reason='No user found for '+ requestDetails.rma_creator;
			submit_rma_response.is_success=false;
			return submit_rma_response;
		}
		// end cmunoz@levementum.com. 04/28/15

		/*if(ownerId==null)
        {
            submit_rma_response.failure_reason='No user found for '+requestDetails.rma_creator;
            submit_rma_response.is_success=false;
            return submit_rma_response;
        }*/
		try{
			//validation against line number to update

			if(assetObj.RecordTypeId != RestSubmitRMA.getRecordTypeAssetIdByName('Retail Asset') && assetObj.Line_Number__c==null && (requestDetails.rma_type=='Buyer\'s Remorse'||requestDetails.rma_type=='Warranty Refund'))
			{
				assetObj.Line_Number__c=0;
				update assetObj;
			}

			List<RecordType> rtList = null;
			Product2 pd;
			Document__c doc;
			List<Product2> gaProductList = [SELECT Id, Document__c FROM Product2 WHERE Id =: assetObj.ProductID__c];
			if(gaProductList.size() > 0){
				pd=gaProductList.get(0);
				List<Document__c> gaDocumentList = [SELECT Id, RMARecordType__c FROM Document__c WHERE Id =: pd.Document__c];
				if(gaDocumentList.size() > 0){
					doc=gaDocumentList.get(0);
					rtList = [SELECT Id,IsActive,Name,SobjectType FROM RecordType where SobjectType='RMA__c' AND IsActive = true AND Name =: doc.RMARecordType__c];
				}
			}

			RMA__c rma = new RMA__c();
			rma.GoogleCustomer__c=accountObj.Id;
			rma.Opportunity__c=oppId;
			rma.GoogleAsset__c=assetObj.Id;
			rma.Type__c=requestDetails.rma_type;
			rma.RMA_Category__c=requestDetails.rma_category;
			rma.RMA_Sub_Category__c=requestDetails.rma_sub_category;
			rma.Notes__c=requestDetails.rma_notes;
			rma.Extended_Warranty_Claim_ID__c = requestDetails.extended_warranty_claim_id;
			rma.gCases_ID__c=requestDetails.case_id;
			rma.OwnerId=ownerId;
			rma.RMA_Action__c=requestDetails.rma_action;
			rma.RMA_Country_Workflow__c=requestDetails.rma_requester_address.address_country;
			rma.ServiceModel__c=assetObj.Service_Model__c;
			if(rtList != null && rtList.size() > 0){
				rma.RecordTypeId=rtList[0].Id;
			}

			insert rma;

			rma = [SELECT Name,GoogleAsset__r.ProductID__r.Document__c,Extended_Warranty_Claim_ID__c,Extended_Warranty_Contract_ID_Backup__c,RMA_Country_Workflow__c,Type__c,RMA_Action__c,GoogleAsset__c FROM RMA__c WHERE Id=:rma.Id];

			RmaDetailsBuilder rdb = new RmaDetailsBuilder();

			submit_rma_response.rma_success_details = rdb
					.setCV(rma.RMA_Country_Workflow__c,rma.GoogleAsset__r.ProductID__r.Document__c)
					.setRMA(rma)
					.stampParameter()
					.getRMASuccessDetail();


			if(rdb.getNoCVForExtendCase()){
				//send warning
				submit_rma_response.warning_reason = 'Device not found';
				submit_rma_response.rma_success_details.sku = requestDetails.sku;
			}

			submit_rma_response.rma_success_details.rma_id=rma.Name;
			//submit_rma_response.rma_success_details.extended_warranty_claim_id = rma.Extended_Warranty_Claim_ID__c;
			submit_rma_response.rma_success_details.extended_warranty_contract_id = rma.Extended_Warranty_Contract_ID_Backup__c;

			accountObj.FirstName=requestDetails.rma_requester_first_name;
			accountObj.LastName=requestDetails.rma_requester_last_name;
			accountObj.PersonMailingStreet=requestDetails.rma_requester_address.address_street;
			accountObj.PersonMailingCity=requestDetails.rma_requester_address.address_city;
			accountObj.PersonMailingState=requestDetails.rma_requester_address.address_state_province;
			accountObj.PersonMailingCountry=requestDetails.rma_requester_address.address_country;
			accountObj.PersonMailingPostalCode=requestDetails.rma_requester_address.address_postal_code;

			upsert accountObj;

			submit_rma_response.is_success=true;
		}
		catch(Exception e)
		{
			submit_rma_response.failure_reason=e.getMessage() + ': Line ' + e.getLineNumber();
			submit_rma_response.is_success=false;
			submit_rma_response.rma_success_details=null;
			Database.rollBack(sp);
		}

		return submit_rma_response;
	}

	public static Id getRecordTypeAssetIdByName(String name){
		return (Schema.SObjectType.Google_Asset__c.getRecordTypeInfosByName().get(name)!=null)?Schema.SObjectType.Google_Asset__c.getRecordTypeInfosByName().get(name).getRecordTypeId():null;
	}

	@future
	public static void updateUser(Id userId)
	{
		update new User(Id = userId,IsActive = true);
	}
}