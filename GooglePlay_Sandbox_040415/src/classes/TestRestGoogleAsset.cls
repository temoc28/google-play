@isTest
private class TestRestGoogleAsset {

	@testSetup static void setUp() {
        Google_Asset__c googleAsset = new Google_Asset__c();
        googleAsset.Name = 'TESTINSERT';
        insert googleAsset;
	}	

	@isTest static void testRetrieve() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAsset/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        // TEST
        Test.startTest();

		GoogleAssetModel retrieveModel = RestGoogleAsset.retrieveGoogleAsset();

		Test.stopTest();
		
		// CHECK
		System.assert(retrieveModel != null);
	}
	
	//@isTest static void testRetrieveByName() {
	//	// SETUP
	//	RestRequest rReq = new RestRequest();
	//	RestResponse rRes = new RestResponse();
	//	rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAsset/';
 //       rReq.httpMethod = 'POST';
 //       rReq.addParameter('assetName', 'TESTINSERT');

 //       RestContext.request = rReq;
 //       RestContext.response = rRes;

 //       // TEST
 //       Test.startTest();

	//	GoogleAssetModel retrieveModel = RestGoogleAsset.retrieveGoogleAsset();

	//	Test.stopTest();
		
	//	// CHECK
	//	System.assert(retrieveModel != null);
	//	System.assertEquals(retrieveModel.Name, 'TESTINSERT');

	//}

	@isTest static void testDeleteObject() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAsset/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

        Google_Asset__c[] googleAssets = [SELECT Id FROM Google_Asset__c];
        System.assert(googleAssets.size() > 0);

        // TEST
        String status = RestGoogleAsset.deleteObject();

        // CHECK
        System.assertEquals('Delete succeeded', status);
	}

	@isTest static void testUpdate() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAsset/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

		GoogleAssetModel retrieveModel = RestGoogleAsset.retrieveGoogleAsset();
		String oldName = retrieveModel.Name;

		retrieveModel.Name = 'UPDATENAME';
		retrieveModel.AlternateSerialNumber1 = 'ga.AlternateSerialNumber1__c';
		retrieveModel.AlternateSerialNumber2 = 'ga.AlternateSerialNumber2__c';
		retrieveModel.AlternateSerialNumber3 = 'ga.AlternateSerialNumber3__c';
		retrieveModel.AlternateSerialNumber4 = 'ga.AlternateSerialNumber4__c';
		retrieveModel.Purchaser_Email = 'ga.Purchaser_Email__c';
		retrieveModel.Serial = 'ga.Serial__c';
		retrieveModel.Sale_Country = 'ga.Sale_Country__c'; 
		retrieveModel.VIP = true;
		retrieveModel.Tracking_URL = 'ga.Tracking_URL__c';
		retrieveModel.Carrier = 'ga.Carrier__c';
		retrieveModel.Tracking_Link = 'ga.Tracking_Link__c';
		retrieveModel.RMA_Exceptions = 'ga.RMA_Exceptions__c';
		retrieveModel.Service_Model = 'ga.Service_Model__c'; 
		retrieveModel.Line_Number = 100;
		retrieveModel.SKU_Description = 'ga.SKU_Description__c';
		retrieveModel.SKU = 'ga.SKU_Description__c';
		retrieveModel.IMEI = 'ga.SKU_Description__c';
		retrieveModel.ProductSKUDescription = 'ga.ProductSKUDescription__c';
		retrieveModel.ProductSKU = 'ga.ProductSKU__c';
		retrieveModel.RemoteDisable = 'ga.RemoteDisable__c'; 
		retrieveModel.RepairPartner = 'ga.RepairPartner__c'; 

		// TEST
		GoogleAssetModel updatedGoogleAssetModel = RestGoogleAsset.updateGoogleAsset(retrieveModel);

		// CHECK
		System.assertNotEquals(oldName, updatedGoogleAssetModel.Name);

	}

	@isTest static void testUpdateByName() {
		// SETUP
		RestRequest rReq = new RestRequest();
		RestResponse rRes = new RestResponse();
		rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/GoogleAsset/';
        rReq.httpMethod = 'POST';

        RestContext.request = rReq;
        RestContext.response = rRes;

		GoogleAssetModel retrieveModel = RestGoogleAsset.retrieveGoogleAsset();
		retrieveModel.Purchaser_Email = 'CHANGE_SOMETHING';

		// TEST
		GoogleAssetModel updatedGoogleAssetModel = RestGoogleAsset.updateGoogleAsset(retrieveModel);

		// CHECK
		System.assert(updatedGoogleAssetModel != null);

	}
	
}