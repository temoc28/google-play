public class soapPlayHelper {
    public class AllowFieldTruncationHeader_element {
        public Boolean allowFieldTruncation;
        private String[] allowFieldTruncation_type_info = new String[]{'allowFieldTruncation','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'allowFieldTruncation'};
    }
    public class refundedGaResponse_element {
        public soapPlayHelper.refundedGa[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class CallOptions_element {
        public String client;
        private String[] client_type_info = new String[]{'client','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'client'};
    }
    public class OpportunityObject {
        public String account;
        public Date closeDate;
        public String country;
        public String errorMessage;
        public Boolean hasError;
        public String name;
        public String opportunityId;
        public String playOpportunityId;
        public Date shipmentDate;
        public String stageName;
        public String typeOfOrder;
        private String[] account_type_info = new String[]{'account','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] closeDate_type_info = new String[]{'closeDate','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] country_type_info = new String[]{'country','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] opportunityId_type_info = new String[]{'opportunityId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] playOpportunityId_type_info = new String[]{'playOpportunityId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] shipmentDate_type_info = new String[]{'shipmentDate','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] stageName_type_info = new String[]{'stageName','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] typeOfOrder_type_info = new String[]{'typeOfOrder','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'account','closeDate','country','errorMessage','hasError','name','opportunityId','playOpportunityId','shipmentDate','stageName','typeOfOrder'};
    }
    public class AccountObject {
        public String accountId;
        public String email;
        public String errorMessage;
        public String firstName;
        public Boolean hasError;
        public String lastName;
        public String mailingCity;
        public String mailingCountry;
        public String mailingPostalCode;
        public String mailingState;
        public String mailingStreet;
        public String name;
        public String phone;
        private String[] accountId_type_info = new String[]{'accountId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] email_type_info = new String[]{'email','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] firstName_type_info = new String[]{'firstName','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] lastName_type_info = new String[]{'lastName','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] mailingCity_type_info = new String[]{'mailingCity','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] mailingCountry_type_info = new String[]{'mailingCountry','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] mailingPostalCode_type_info = new String[]{'mailingPostalCode','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] mailingState_type_info = new String[]{'mailingState','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] mailingStreet_type_info = new String[]{'mailingStreet','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] phone_type_info = new String[]{'phone','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'accountId','email','errorMessage','firstName','hasError','lastName','mailingCity','mailingCountry','mailingPostalCode','mailingState','mailingStreet','name','phone'};
    }
    public class SessionHeader_element {
        public String sessionId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId'};
    }
    public class changeRmaStatus_element {
        public soapPlayHelper.RmaObject[] rmaObjList;
        private String[] rmaObjList_type_info = new String[]{'rmaObjList','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'rmaObjList'};
    }
    public class LogInfo {
        public String category;
        public String level;
        private String[] category_type_info = new String[]{'category','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] level_type_info = new String[]{'level','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'category','level'};
    }
    public class refundedGa {
        public String errorMessage;
        public Boolean hasError;
        public String playGaId;
        public String serialNumber;
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] playGaId_type_info = new String[]{'playGaId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] serialNumber_type_info = new String[]{'serialNumber','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'errorMessage','hasError','playGaId','serialNumber'};
    }
    public class DebuggingHeader_element {
        public soapPlayHelper.LogInfo[] categories;
        public String debugLevel;
        private String[] categories_type_info = new String[]{'categories','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','false'};
        private String[] debugLevel_type_info = new String[]{'debugLevel','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'categories','debugLevel'};
    }
    public class BundleObject {
        public String accountId;
        public soapPlayHelper.AccountObject ao;
        public String errorMessage;
        public Boolean hasError;
        public soapPlayHelper.OpportunityLineItemObject oliObj;
        public soapPlayHelper.OpportunityObject oppObj;
        public String opportunityId;
        public String opportunityLineItemId;
        public String playAssetId;
        private String[] accountId_type_info = new String[]{'accountId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] ao_type_info = new String[]{'ao','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] oliObj_type_info = new String[]{'oliObj','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] oppObj_type_info = new String[]{'oppObj','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] opportunityId_type_info = new String[]{'opportunityId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] opportunityLineItemId_type_info = new String[]{'opportunityLineItemId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] playAssetId_type_info = new String[]{'playAssetId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'accountId','ao','errorMessage','hasError','oliObj','oppObj','opportunityId','opportunityLineItemId','playAssetId'};
    }
    public class createAllList_element {
        public soapPlayHelper.BundleObject[] boList;
        private String[] boList_type_info = new String[]{'boList','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'boList'};
    }
    public class changeRmaStatusResponse_element {
        public soapPlayHelper.RmaObject[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class refundedGa_element {
        public soapPlayHelper.refundedGa[] gaList;
        private String[] gaList_type_info = new String[]{'gaList','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'gaList'};
    }
    public class createAllListResponse_element {
        public soapPlayHelper.BundleObject[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class DebuggingInfo_element {
        public String debugLog;
        private String[] debugLog_type_info = new String[]{'debugLog','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'debugLog'};
    }
    public class OpportunityLineItemObject {
        public String errorMessage;
        public Boolean hasError;
        public String opportunityId;
        public String opportunityLineItemId;
        public String playOpportunityLineItemId;
        public String serialNumber;
        public String sku;
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] opportunityId_type_info = new String[]{'opportunityId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] opportunityLineItemId_type_info = new String[]{'opportunityLineItemId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] playOpportunityLineItemId_type_info = new String[]{'playOpportunityLineItemId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] serialNumber_type_info = new String[]{'serialNumber','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] sku_type_info = new String[]{'sku','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'errorMessage','hasError','opportunityId','opportunityLineItemId','playOpportunityLineItemId','serialNumber','sku'};
    }
    public class RmaObject {
        public String errorMessage;
        public Boolean hasError;
        public String rmaId;
        public String status;
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] hasError_type_info = new String[]{'hasError','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] rmaId_type_info = new String[]{'rmaId','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] status_type_info = new String[]{'status','http://soap.sforce.com/schemas/class/PlayDataToGlassWS',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS','true','false'};
        private String[] field_order_type_info = new String[]{'errorMessage','hasError','rmaId','status'};
    }
    public class PlayDataToGlassWS {
        public String endpoint_x = UserInfo.getOrganizationId()=='00DU0000000KYL9MAO'?'https://na14.salesforce.com/services/Soap/class/PlayDataToGlassWS':'https://cs10.salesforce.com/services/Soap/class/PlayDataToGlassWS';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public soapPlayHelper.DebuggingInfo_element DebuggingInfo;
        public soapPlayHelper.SessionHeader_element SessionHeader;
        public soapPlayHelper.AllowFieldTruncationHeader_element AllowFieldTruncationHeader;
        public soapPlayHelper.CallOptions_element CallOptions;
        public soapPlayHelper.DebuggingHeader_element DebuggingHeader;
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/schemas/class/PlayDataToGlassWS';
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/schemas/class/PlayDataToGlassWS';
        private String AllowFieldTruncationHeader_hns = 'AllowFieldTruncationHeader=http://soap.sforce.com/schemas/class/PlayDataToGlassWS';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/schemas/class/PlayDataToGlassWS';
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/schemas/class/PlayDataToGlassWS';
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/schemas/class/PlayDataToGlassWS', 'soapPlayHelper'};
        public soapPlayHelper.refundedGa[] refundedGa(soapPlayHelper.refundedGa[] gaList) {
            soapPlayHelper.refundedGa_element request_x = new soapPlayHelper.refundedGa_element();
            request_x.gaList = gaList;
            soapPlayHelper.refundedGaResponse_element response_x;
            Map<String, soapPlayHelper.refundedGaResponse_element> response_map_x = new Map<String, soapPlayHelper.refundedGaResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'refundedGa',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'refundedGaResponse',
              'soapPlayHelper.refundedGaResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public soapPlayHelper.BundleObject[] createAllList(soapPlayHelper.BundleObject[] boList) {
            soapPlayHelper.createAllList_element request_x = new soapPlayHelper.createAllList_element();
            request_x.boList = boList;
            soapPlayHelper.createAllListResponse_element response_x;
            Map<String, soapPlayHelper.createAllListResponse_element> response_map_x = new Map<String, soapPlayHelper.createAllListResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'createAllList',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'createAllListResponse',
              'soapPlayHelper.createAllListResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public soapPlayHelper.RmaObject[] changeRmaStatus(soapPlayHelper.RmaObject[] rmaObjList) {
            soapPlayHelper.changeRmaStatus_element request_x = new soapPlayHelper.changeRmaStatus_element();
            request_x.rmaObjList = rmaObjList;
            soapPlayHelper.changeRmaStatusResponse_element response_x;
            Map<String, soapPlayHelper.changeRmaStatusResponse_element> response_map_x = new Map<String, soapPlayHelper.changeRmaStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'changeRmaStatus',
              'http://soap.sforce.com/schemas/class/PlayDataToGlassWS',
              'changeRmaStatusResponse',
              'soapPlayHelper.changeRmaStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
    }

     public static String login()
    {
    //public String endpoint_x = UserInfo.getOrganizationId()=='00DU0000000KYL9MAO'?'https://na14.salesforce.com/services/Soap/class/PlayDataToGlassWS':'https://cs10.salesforce.com/services/Soap/class/PlayDataToGlassWS';
      //CHANGE THESE VARIABLES
    List<GlassIntegration__c> giList = GlassIntegration__c.getALL().values();    
    final String LOGIN_DOMAIN = giList.get(0).domain__c;//options: www, test, prerellogin.pre
    final String USERNAME  = giList.get(0).Name;
    final String PASSWORD  = giList.get(0).Glass__c;
     
    //----------------------------------------------------------------------
    // Login via SOAP/XML web service api to establish session
    //----------------------------------------------------------------------
    HttpRequest request = new HttpRequest();
    request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/u/22.0');
    request.setMethod('POST');
    request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
    request.setHeader('SOAPAction', '""');
    //not escaping username and password because we're setting those variables above
    //in other words, this line "trusts" the lines above
    //if username and password were sourced elsewhere, they'd need to be escaped below
    request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + USERNAME + '</username><password>' + PASSWORD + '</password></login></Body></Envelope>');
    Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
      .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
      .getChildElement('loginResponse','urn:partner.soap.sforce.com')
      .getChildElement('result','urn:partner.soap.sforce.com');
     
    //----------------------------------------------------------------------
    // Grab session id and server url (ie the session)
    //----------------------------------------------------------------------
    //final String SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
    final String SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
      return SESSION_ID;
    } 
}