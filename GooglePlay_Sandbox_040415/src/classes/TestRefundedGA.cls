/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRefundedGA 
{
    @isTest static void myUnitTest() 
    {
        
        set<Id> gaIdSet = new Set<Id>();
        GlassIntegration__c gi = new GlassIntegration__c(domain__c='test',name='test@test.com',glass__c='asdf');
        insert gi;    
        
        Account a = new Account();
        a.Name='TestAccount';        
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
        
        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c(
            Name = 'CHromecaseTest');
        insert codeSet;

        Product_Family__c prodFam = new Product_Family__c(Name = 'Chromecast Test');
        insert prodFam;

        Document__c doc = new Document__c(
            Name = 'Chromecast Test'
            ,Product_Family__c = prodFam.Id
            ,Return_Reason_Code_Set__c = codeSet.Id);
        insert doc;

        Product2 prod = new Product2(Name = 'Chromecast US Test'
            ,SKU__c = '86002596-01-Test'
            ,Sale_Country__c = 'US'
            ,Unique_ID_Type__c = 'Serial Number'
            ,RMARecordType__c = 'Chromecast'
            ,Document__c = doc.Id);
        insert prod;

        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';        
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductID__c = prod.Id;
        insert gAsset;
        gaidSet.add(gAsset.Id);

        Test.startTest();
        MainWebServiceMock mockClass = new MainWebServiceMock();
        mockClass.calloutName = 'refundedGA';
        Test.setMock(HttpCalloutMock.class,new MainRESTWebServiceMock());
        Test.setMock(WebServiceMock.class, mockClass);
        PlayToGlass.refundedGa(gaIdSet);
        Test.stopTest();
    }
}