/*//This class is triggered on an Case operation and updates the Account on the Case to the Account on the Associated Email
//or the Email and Account of the Case to the Account and Order of the Email
public class UpdateLinksOnCaseTrigger {
    public static void UpdateValues(List<Case> TNew){
        List<Id> EmailIdList = new List<Id>(); //List of all Emails associated with Cases
        Map<Id, Case> EmailIdCaseMap = new Map<Id, Case>(); //Map of email ids to Cases
        
        List<Id> OrderIdList = new List<Id>(); //List of all Orderss associated with Cases
        Map<Id, Case> OrderIdCaseMap = new Map<Id, Case>(); //Map of Order ids to Cases
        
        //Populate a list of the Emails and Orders associated to the Cases and Map them
        for(Case C : TNew){
            if(C.ContactId != NULL){
                EmailIdList.Add(C.ContactId);
                EmailIdCaseMap.put(C.ContactId,C);
            }
            if(C.Order__c != NULL){
                OrderIdList.Add(C.Order__c);
                OrderIdCaseMap.put(C.Order__c,C);
            }
        }
        
        //Update the account of the Case to the account of the Email and the Account of the Order
        for(Order__c O : [SELECT Id, Google_Account__c FROM Order__c WHERE Id IN: OrderIdList AND Google_Account__c != NULL]){
            if(OrderIdCaseMap.containsKey(O.Id)){
                if(OrderIdCaseMap.get(O.Id).ContactId == NULL){
                    OrderIdCaseMap.get(O.Id).ContactId = O.Google_Account__c;
                }
            }
        }
        return;
    }
    
    @isTest
    static void myUnitTest() {
        Contact c = new Contact(LastName = 'test');
        insert c;
        
        //insert new Case(ContactId = c.Id);
        
        Order__c O = new Order__c(Google_Account__c = c.Id);
        insert O;
        
        //insert new Case(Order__c = O.Id);
    }
}

*/
//This class is triggered on an Case operation and updates the Account on the Case to the Account on the Associated Email
//or the Email and Account of the Case to the Account and Order of the Email
public class UpdateLinksOnCaseTrigger {
    
    public static void UpdateValues(List<Case> tNew){
        
        // Get Record Type Id
        Id RecId = [
            SELECT r.Id, r.Name, r.DeveloperName, r.IsPersonType 
            FROM RecordType r 
            WHERE sObjectType = 'Account' AND IsPersonType=True AND DeveloperName='GoogleCustomer'
        ].Id;

        // Build Query to check if Account with matching email exists
        String filters = '';
        for(Case c : tNew) {
            if(c.SuppliedEmail != null) {
                if(filters == '') {
                    filters = ' WHERE ';
                } else {
                    filters = filters + ' OR ';
                }
                filters = filters + 'PersonEmail=\'' + c.SuppliedEmail + '\'';
            }
        }
        String queryString = '';
        if(filters != '') {
            queryString = 'SELECT Id, PersonEmail FROM Account' + filters;           
        }
        
        // Query Accounts
        List<Account> AccountList = new List<Account>();       
        if(queryString != '') {
            try {
                AccountList = Database.query(queryString);
            } catch (Exception e) {
                System.Debug(LoggingLevel.ERROR, e);
            }
        }
        //System.Debug(LoggingLevel.Debug, 'Account List: ' + AccountList);
        
        // Map Accounts found
        Map<String,Account> AccountMap = new Map<String,Account>();
        for(Account a : AccountList) {
            AccountMap.put(a.PersonEmail, a);       
        }

        // Update case and/or create new Account
        List<Account> toUpsert = new List<Account>();
        for(Case c : tNew) {
            if(c.AccountId == null && c.SuppliedEmail != null) {
                if(AccountMap.get(c.SuppliedEmail) != null) {
                    c.AccountId = AccountMap.get(c.SuppliedEmail).Id;
                } else {
                    //Create Account
                    Account newAcct = new Account(RecordTypeId=RecId, LastName=c.Name__c, PersonEmail=c.SuppliedEmail, Phone=c.Phone__c);
                    if(c.Name__c==null && c.SuppliedName!=null) {
                        newAcct.LastName = c.SuppliedName;
                    } else if(c.Name__c==null && c.SuppliedName==null){
                        newAcct.LastName = 'Unknown';
                    }
                    if(newAcct.Phone==null) {
                        newAcct.Phone = c.SuppliedPhone;
                    }
                    toUpsert.add(newAcct);  
                    AccountMap.put(c.SuppliedEmail, newAcct);                                  
                }
            }
        }
        System.Debug(toUpsert);
        try {
            // Upsert Accounts
            Database.upsert(toUpsert);
        } catch (System.DmlException e) {
            for (Integer i = 0; i < e.getNumDml(); i++) {          
                System.Debug(e.getDmlMessage(i)); 
                
            }
        }

        // Update the case with the IDs of the newly created accounts
        for(Case c : tNew) {
            if(AccountMap.get(c.SuppliedEmail) != null) {
                c.AccountId = AccountMap.get(c.SuppliedEmail).Id;
            }    
        } 
    } 
    
    @isTest
    static void myUnitTest() {       
        Id RecId = [
            SELECT r.Id, r.Name, r.DeveloperName, r.IsPersonType 
            FROM RecordType r 
            WHERE sObjectType = 'Account' AND IsPersonType=True AND DeveloperName='GoogleCustomer'
        ].Id;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', 
            LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser33@testorg.com');

        System.runAs(u) {
            Account a = new Account(RecordTypeId = RecId, LastName='Test',  PersonEmail='test@newtest.com', Phone='555-555-5555', PersonMailingCountry='US', PersonMailingState='GA');
            insert a;      
    
            Case ca = new Case(SuppliedEmail='test@newtest.com', SuppliedName='TestCase', 
                SuppliedPhone='555-555-5555', Serial_Number__c='55555555', Description='Testing', Subject='Test');
            insert ca;
            
            List<Case> cList = new List<Case>();
            cList.add(new Case(SuppliedEmail='test1@testorg.com', SuppliedName='TestCase1'));
            cList.add(new Case(SuppliedEmail='test2@testorg.com', SuppliedName='TestCase2'));
            cList.add(new Case(SuppliedEmail='test3@testorg.com', SuppliedName='TestCase3'));
            insert cList;
        }
    }  
}