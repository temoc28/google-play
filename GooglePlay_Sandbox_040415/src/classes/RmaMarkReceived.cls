public with sharing class RmaMarkReceived 
{
	RMA__c rma;
	public boolean isPendingReturn{get;set;}
	public RmaMarkReceived(ApexPages.StandardController con)
	{
		rma = (RMA__c)con.getRecord();
		isPendingReturn = false;
		if(rma.Status__c=='Pending Return')
			isPendingReturn = true;
		if(!isPendingReturn)
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'You can only mark an RMA received, if it is Pending Return.'));
	}
	public PageReference markReceived()
	{
		rma.MarkedReceivedBy__c=UserInfo.getUserId();
		rma.Status__c='Received';
		//write the value to RMA Received Reason but we are showing the other field on the VF page
		rma.RMAReceivedReason__c=rma.MarkRMAReceivedReason__c;	
		rma.ManuallyReceivedBy__c=UserInfo.getUserId();	
		update rma;
		
		return new PageReference('/'+rma.Id);
	}
}