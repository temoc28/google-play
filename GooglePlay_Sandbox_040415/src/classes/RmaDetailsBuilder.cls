public class RmaDetailsBuilder {
	private RMA__c rma;
	private RestResponseModel.rma_success_details_t successDetailObj;
	private Country_Variant__c cv;
	private Map<String,Country_Variant__c> cvMap;
	private Map<String,List<Shipping_Label_Method_Rule__c>> rcToRuleMap;
	private Set<String> rcIdSet;
	private Map<String,String> notCVMapping;
	private Map<String,String> countryMap;
	private Map<String,Country__c> cMap;
	private Boolean noCVForExtendCase = false;

	public RmaDetailsBuilder(){
		cvMap = new Map<String,Country_Variant__c>();
		rcToRuleMap = new Map<String,List<Shipping_Label_Method_Rule__c>>();
		rcIdSet = new Set<String>();
		notCVMapping = new Map<String,String>();
		countryMap = new Map<String,String>();
		cMap = new Map<String,Country__c>();
	}
	public RmaDetailsBuilder setRMA(RMA__c rma){
		this.rma = rma;
		return this;
	}

	public RmaDetailsBuilder setCV(String rmaCountryWorkflowString , String docIdString){
		return setCV(new Set<String>{rmaCountryWorkflowString},new Set<String>{docIdString});
	}

	public RmaDetailsBuilder setCV(Set<String> rmaCountryWorkflowSet , Set<String> docIdSet){

		for(Country_Variant__c cv : [SELECT Id,Name,Requires_Shipping_Label__c,Buyer_s_Remorse_Article_ID__c,DOA_Article_ID__c,DOA_Replacement_Doc_ID__c,
		                             DOA_Replacement_GPN__c,DOA_Replacement_Google_Part__r.Name,Warranty_Article_ID__c,Warranty_Replacement_Doc_ID__c,Warranty_Replacement_GPN__c,
		                             Warranty_Refund_Article_ID__c,Warranty_Replacement_Google_Part__r.Name,
		                             AR_Method__c,Custom_Return_Center__r.Address_Line_1__c,Custom_Return_Center__c,Custom_Return_Center__r.Address_Line_2__c,
		                             Custom_Return_Center__r.Address_Line_3__c,Custom_Return_Center__r.City__c,Custom_Return_Center__r.State__c,
		                             Custom_Return_Center__r.Postal_Code__c,Custom_Return_Center__r.Country__c,Custom_Return_Center__r.Shipping_Label_Service_Code__c,
		                             Custom_Return_Center__r.Name,Document__c,Country__c,Country__r.Return_Center__c,Country__r.Return_Center__r.Address_Line_1__c,
		                             Country__r.Return_Center__r.Address_Line_2__c,Country__r.Return_Center__r.Address_Line_3__c,Country__r.Return_Center__r.City__c,
		                             Country__r.Return_Center__r.State__c,Country__r.Return_Center__r.Country__c,Country__r.Return_Center__r.Postal_Code__c,Return_Center_ID__c,
		                             Return_Center_Name__c
		                             FROM Country_Variant__c
		                             WHERE Name in :rmaCountryWorkflowSet AND Document__c=:docIdSet])
		{
			cvMap.put(cv.Name+cv.Document__c,cv);
			rcIdSet.add(cv.Custom_Return_Center__c);
		}

		for(String tempRMACountryWorkflow:rmaCountryWorkflowSet){
			for(String tempDocId : docIdSet){
				if(!cvMap.containsKey(tempRMACountryWorkflow+tempDocId)){
					notCVMapping.put(tempRMACountryWorkflow+tempDocId,tempRMACountryWorkflow);
					if(!countryMap.containsKey(tempRMACountryWorkflow)){
						countryMap.put(tempRMACountryWorkflow,tempDocId);
					}
				}
			}
		}
		System.debug('countryMap'+countryMap);
		if(countryMap.size() > 0){
			for(Country__c countryReturn : [SELECT Id,Name,Buyer_s_Remorse_Article_ID__c,
			                                Warranty_Refund_Article_ID__c,AR_Method__c,Return_Center__c,Return_Center__r.Address_Line_1__c,Return_Center__r.Address_Line_2__c,
			                                Return_Center__r.Address_Line_3__c,Return_Center__r.City__c,Return_Center__r.State__c,Return_Center__r.Country__c,
			                                Return_Center__r.Postal_Code__c
			                                FROM Country__c
			                                WHERE Name in :countryMap.keyset()])
			{
				System.debug('countryReturn'+countryReturn);
				cMap.put(countryReturn.Name+String.valueOf(countryMap.get(countryReturn.Name)),countryReturn);
			}
		}
		return setRules();
	}

	public RmaDetailsBuilder setRules(){
		for(Shipping_Label_Method_Rule__c sRule : [SELECT Id,Return_Center__c,Shipping_Label_Method__c, RMA_Method__c, Action__c FROM Shipping_Label_Method_Rule__c WHERE Return_Center__c IN :rcIdSet])
		{
			if(!rcToRuleMap.containsKey(sRule.Return_Center__c))
				rcToRuleMap.put(sRule.Return_Center__c,new List<Shipping_Label_Method_Rule__c>());
			rcToRuleMap.get(sRule.Return_Center__c).add(sRule);
		}
		return this;
	}

	public RmaDetailsBuilder stampParameter(){
		successDetailObj = new RestResponseModel.rma_success_details_t();
		successDetailObj.shipping_destination_address = new RestResponseModel.address_t();
		if(cvMap.containsKey(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c))
		{
			Country_Variant__c cv = cvMap.get(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c);
			if(cv.Custom_Return_Center__c != null){
				successDetailObj.shipping_destination_address.address_street=cv.Custom_Return_Center__r.Address_Line_1__c;
				successDetailObj.shipping_destination_address.address_street_2=cv.Custom_Return_Center__r.Address_Line_2__c;
				successDetailObj.shipping_destination_address.address_street_3=cv.Custom_Return_Center__r.Address_Line_3__c;
				successDetailObj.shipping_destination_address.address_city=cv.Custom_Return_Center__r.City__c;
				successDetailObj.shipping_destination_address.address_state_province=cv.Custom_Return_Center__r.State__c;
				successDetailObj.shipping_destination_address.address_country=cv.Custom_Return_Center__r.Country__c;
				successDetailObj.shipping_destination_address.address_postal_code=cv.Custom_Return_Center__r.Postal_Code__c;
				successDetailObj.shipping_center_id=cv.Custom_Return_Center__r.Shipping_Label_Service_Code__c;
				successDetailObj.shipping_center_name=cv.Custom_Return_Center__r.Name;

				if(cv.Requires_Shipping_Label__c && rcToRuleMap.containsKey(cv.Custom_Return_Center__c))
				{
					for(Shipping_Label_Method_Rule__c slRule : rcToRuleMap.get(cv.Custom_Return_Center__c))
					{
						if(rma.RMA_Action__c == 'ADVANCED_EXCHANGE')
						{
							if(slRule.RMA_Method__c==cv.AR_Method__c && slRule.Action__c==rma.RMA_Action__c)
								successDetailObj.shipping_method=slRule.Shipping_Label_Method__c;
						}
						else
						{
							if(slRule.Action__c==rma.RMA_Action__c)
								successDetailObj.shipping_method=slRule.Shipping_Label_Method__c;
						}
					}
				}
			}else{
				successDetailObj.shipping_destination_address.address_street=cv.Country__r.Return_Center__r.Address_Line_1__c;
				successDetailObj.shipping_destination_address.address_street_2=cv.Country__r.Return_Center__r.Address_Line_2__c;
				successDetailObj.shipping_destination_address.address_street_3=cv.Country__r.Return_Center__r.Address_Line_3__c;
				successDetailObj.shipping_destination_address.address_city=cv.Country__r.Return_Center__r.City__c;
				successDetailObj.shipping_destination_address.address_state_province=cv.Country__r.Return_Center__r.State__c;
				successDetailObj.shipping_destination_address.address_country=cv.Country__r.Return_Center__r.Country__c;
				successDetailObj.shipping_destination_address.address_postal_code=cv.Country__r.Return_Center__r.Postal_Code__c;
				successDetailObj.shipping_center_id=cv.Return_Center_ID__c;
				successDetailObj.shipping_center_name=cv.Return_Center_Name__c;

				if(cv.Requires_Shipping_Label__c && rcToRuleMap.containsKey(cv.Country__r.Return_Center__c))
				{
					for(Shipping_Label_Method_Rule__c slRule : rcToRuleMap.get(cv.Country__r.Return_Center__c))
					{
						if(rma.RMA_Action__c == 'ADVANCED_EXCHANGE')
						{
							if(slRule.RMA_Method__c==cv.AR_Method__c && slRule.Action__c==rma.RMA_Action__c)
								successDetailObj.shipping_method=slRule.Shipping_Label_Method__c;
						}
						else
						{
							if(slRule.Action__c==rma.RMA_Action__c)
								successDetailObj.shipping_method=slRule.Shipping_Label_Method__c;
						}
					}
				}
			}
			successDetailObj.shipping_label_needed=cv.Requires_Shipping_Label__c;
			successDetailObj.need_rma_label='true';
			if(rma.Type__c=='Buyer\'s Remorse'){
				successDetailObj.canned_response=cv.Buyer_s_Remorse_Article_ID__c;
			}
			else if(rma.Type__c=='Warranty DOA'){
				successDetailObj.canned_response=cv.DOA_Article_ID__c;
				successDetailObj.exchange_gpn=cv.DOA_Replacement_GPN__c;
				successDetailObj.exchange_doc_id = cv.DOA_Replacement_Doc_ID__c;
				successDetailObj.exchange_sku_description=cv.DOA_Replacement_Google_Part__r.Name;
			}
			else if(rma.Type__c=='Warranty Regular'){
				successDetailObj.canned_response=cv.Warranty_Article_ID__c;
				successDetailObj.exchange_gpn=cv.Warranty_Replacement_GPN__c;
				successDetailObj.exchange_doc_id = cv.Warranty_Replacement_Doc_ID__c;
				successDetailObj.exchange_sku_description=cv.Warranty_Replacement_Google_Part__r.Name;
			}
			else if(rma.Type__c=='Warranty Refund'){
				successDetailObj.canned_response=cv.Warranty_Refund_Article_ID__c;
				successDetailObj.need_rma_label='false';
			}
			successDetailObj.ar_method=cv.AR_Method__c;
		}else if(cMap.containsKey(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c)){
			if(rma.Type__c != 'Extended Warranty'){
				Country__c tempCountry = cMap.get(rma.RMA_Country_Workflow__c+rma.GoogleAsset__r.ProductID__r.Document__c);

				successDetailObj.shipping_destination_address.address_street=tempCountry.Return_Center__r.Address_Line_1__c;
				successDetailObj.shipping_destination_address.address_street_2=tempCountry.Return_Center__r.Address_Line_2__c;
				successDetailObj.shipping_destination_address.address_street_3=tempCountry.Return_Center__r.Address_Line_3__c;
				successDetailObj.shipping_destination_address.address_city=tempCountry.Return_Center__r.City__c;
				successDetailObj.shipping_destination_address.address_state_province=tempCountry.Return_Center__r.State__c;
				successDetailObj.shipping_destination_address.address_country=tempCountry.Return_Center__r.Country__c;
				successDetailObj.shipping_destination_address.address_postal_code=tempCountry.Return_Center__r.Postal_Code__c;

				successDetailObj.need_rma_label='true';
				if(rma.Type__c=='Buyer\'s Remorse'){
					successDetailObj.canned_response=tempCountry.Buyer_s_Remorse_Article_ID__c;
				}
				else if(rma.Type__c=='Warranty Refund'){
					successDetailObj.canned_response=tempCountry.Warranty_Refund_Article_ID__c;
					successDetailObj.need_rma_label='false';
				}
				successDetailObj.ar_method=tempCountry.AR_Method__c;
			}else{
				noCVForExtendCase = true;
			}
		}
		return this;
	}
	public Boolean getNoCVForExtendCase(){
		return noCVForExtendCase;
	}
	public RestResponseModel.rma_success_details_t getRMASuccessDetail(){
		return successDetailObj;
	}

}