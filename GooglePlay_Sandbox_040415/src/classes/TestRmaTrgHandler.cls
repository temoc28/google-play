/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRmaTrgHandler 
{
    static testMethod void myUnitTest() 
    {
        RMA__c rma;
        Account a;
        Opportunity opp;
        Google_Asset__c gAsset;
        ItemTaxRefundValues__c itr = new ItemTaxRefundValues__c();
        itr.Country__c='US';
        itr.State__c='GA';
        itr.ItemA__c=25;
        itr.ItemB__c=50;
        itr.TaxRule__c='Prorated';
        insert itr;
        
        a = new Account();
        a.Name='TestAccount';
        insert a;
        
        opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='US';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Test',Product_Family__c=pf.Id);
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='xxxBBBccc',IsActive=true,Document__c=d.Id);
        insert p;

        gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.ProductId__c=p.Id;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='A';
        rma.Type__c='Buyer\'s Remorse';     
        insert rma;     

        test.startTest();
        RMA__c rma3 = new RMA__c(Id=rma.Id);              
        rma3.Status__c='Received'; 
        rma3.Type__c='Warranty DOA';
        update rma3;
        test.stopTest();
        
        // Fault Exception
        RMA__c rma2 = new RMA__c();
    	rma2.Opportunity__c = opp.Id;
    	rma2.GoogleAsset__c=gAsset.Id;
    	rma2.Status__c='Pending Return';
    	rma2.Triage_Code__c='A';
    	rma2.Type__c='Buyer\'s Remorse';  
    	try
    	{  	
    	   insert rma2;
    	}catch(exception e){
    		
    	}

        Google_Asset__c gAsset2 = new Google_Asset__c();
        gAsset2.Name='466546';
        gAsset2.SKU__c='xxxBBBcccDD';
        gAsset2.Order_Opportunity__c=opp.Id;
        gAsset2.Line_Number__c=1;
        gAsset2.ProductId__c=p.Id;
        insert gAsset2;

        RMA__c rma4 = new RMA__c();
        rma4.Opportunity__c = opp.Id;
        rma4.GoogleAsset__c=gAsset2.Id;           
        rma4.Status__c='Received';
        rma4.Triage_Code__c='A';
        rma4.Type__c='Warranty Refund';
        insert rma4;
    }
}