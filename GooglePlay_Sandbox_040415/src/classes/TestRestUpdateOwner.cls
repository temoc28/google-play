// jescamilla@levementum.com 12/2/14 Description: Test coverage.
@isTest
private class TestRestUpdateOwner {
	
	@isTest static void testDoUpdateOwner() {
		

		TestRestUtilities testRestUtils = new TestRestUtilities();

		Test.startTest();
		RestResponseModel.update_owner_details_t ownerDetails = new RestResponseModel.update_owner_details_t();
		RestResponseModel.update_owner_details_response_t response = RestUpdateOwner.doUpdateOwner(ownerDetails); //call with nulls.

		testRestUtils.createAccount();
		testRestUtils.createAccount2();
		testRestUtils.createGoogleAsset();
		testRestUtils.createGoogleAsset2();

		RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
        rReq.httpMethod = 'POST';
        
        RestContext.request = rReq;
        RestContext.response = rRes;

        ownerDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateOwner to fire the account retrieval
        ownerDetails.new_owner_first_name = 'John';
        ownerDetails.new_owner_last_name = 'Wick';
        ownerDetails.new_owner_email = 'johnwick@mailinator.com';
        ownerDetails.new_requester_address = new RestResponseModel.address_t();
        ownerDetails.new_requester_address.address_state_province = 'TX';
        ownerDetails.new_requester_address.address_country = 'US';
        ownerDetails.new_requester_address.address_postal_code = '79938';

        response = RestUpdateOwner.doUpdateOwner(ownerDetails);//Call with no matching Account

        //testRestUtils.createAccount3('johnwick@mailinator.com');
        //response = RestUpdateOwner.doUpdateOwner(ownerDetails); //Call with matching accounts matchinv

        
	}
	
}