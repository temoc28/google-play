@isTest
private class TestRestWarrantyTransfer
{
	@isTest static void noParameters(){
		RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();

        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
	}
    @isTest static void noParametersButOrder(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
    @isTest static void noParametersButOrderAndRMA(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        warrantyTransfer.rma_no = '11111';
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
    @isTest static void noParametersButOrderAndCountry(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        warrantyTransfer.country = '11111';
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
    @isTest static void noParametersButOrderAndCountryAndRMA(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        warrantyTransfer.country = '11111';
        warrantyTransfer.rma_no = '11111';
        
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
    @isTest static void noParametersButOrderExceptSKU(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        warrantyTransfer.country = '11111';
        warrantyTransfer.rma_no = '11111';
        warrantyTransfer.asset_id = '11111';
        
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
    
    @isTest static void cannotFindRMA(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        warrantyTransfer.order_id = '111';
        warrantyTransfer.country = '11111';
        warrantyTransfer.rma_no = '11111';
        warrantyTransfer.asset_id = '11111';
        warrantyTransfer.sku = '11111';
        
        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);

        System.assert(result != null);
        System.assert(!result.is_success);
    }
	@isTest static void updateWarrantyTransfer(){
    	RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        
        String expectedRMANo;
        String expectedOrderId = '2222';
        String expectedCountry = '3333';
        String expectedAssetId = '11111';
        String expectedSku = '11111';

        warrantyTransfer.order_id = expectedOrderId;
		warrantyTransfer.country = expectedCountry;
        warrantyTransfer.asset_id = expectedAssetId;
        warrantyTransfer.sku = expectedSku;
		
        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Test',Product_Family__c=pf.Id);
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='xxxBBBccc',IsActive=true,Document__c=d.Id);
        insert p;
        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.ProductId__c=p.Id;
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.Retail_Country__c = 'Set by SKU';
        gAsset.ProductId__c=p.Id;
        insert gAsset;

        RMA__c rma = new RMA__c();
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;
        rma.Notes__c='Test';
        insert rma;

        List<RMA__c> resultRMA = new List<RMA__c>();
        resultRMA = [SELECT Id,Name,ReplacementOrder__c FROM RMA__c LIMIT 1];
        expectedRMANo = resultRMA.get(0).Name;
        system.debug(expectedRMANo);
        warrantyTransfer.rma_no = expectedRMANo;

        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);
        System.assert(result != null);
        System.assert(result.is_success);

		resultRMA = [SELECT Id,ReplacementOrder__c FROM RMA__c WHERE Name = :expectedRMANo LIMIT 1];
		
		List<Opportunity> resultOpportunity = new List<Opportunity>();
		resultOpportunity = [SELECT Id,Name,Country__c,Type FROM Opportunity WHERE Id in (SELECT ReplacementOrder__c FROM RMA__c WHERE Name=:expectedRMANo) LIMIT 1];

		System.assert(resultRMA.size() == 1);
		System.assert(resultOpportunity.size() == 1);

		System.assertEquals(resultOpportunity.get(0).Id,resultRMA.get(0).ReplacementOrder__c);
		System.assertEquals(expectedOrderId,resultOpportunity.get(0).Name);
		System.assertEquals(expectedCountry,resultOpportunity.get(0).Country__c);
		System.assertEquals(RestWarrantyTransfer.ADVANCED_REPLACEMENT,resultOpportunity.get(0).Type);
    }
    @isTest static void updateWarrantyTransferAndExistOrder(){
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/WarrantyTransfer';
        rReq.httpMethod = 'POST';
        RestContext.request = rReq;
        RestContext.response = rRes;

        RestResponseModel.warranty_transfer_detail_t warrantyTransfer = new RestResponseModel.warranty_transfer_detail_t();
        
        String expectedRMANo;
        String expectedOrderId = '2222';
        String expectedCountry = '3333';
        String expectedAssetId = '11111';
        String expectedSku = '11111';

        warrantyTransfer.order_id = expectedOrderId;
        warrantyTransfer.country = expectedCountry;
        warrantyTransfer.asset_id = expectedAssetId;
        warrantyTransfer.sku = expectedSku;
        
        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Test',Product_Family__c=pf.Id);
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Sale_Country__c = 'UK', Name='TEst',ProductCode='Test',SKU__c='xxxBBBccc',IsActive=true,Document__c=d.Id);
        insert p;
        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name=expectedAssetId;
        gAsset.ProductId__c=p.Id;
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        gAsset.Retail_Country__c = 'Set by SKU';
        gAsset.ProductId__c=p.Id;
        insert gAsset;

        RMA__c rma = new RMA__c();
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.ReplacementOrder__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;
        rma.Notes__c='Test';
        insert rma;

        List<RMA__c> resultRMA = new List<RMA__c>();
        resultRMA = [SELECT Id,Name,ReplacementOrder__c FROM RMA__c LIMIT 1];
        expectedRMANo = resultRMA.get(0).Name;
        system.debug(expectedRMANo);
        warrantyTransfer.rma_no = expectedRMANo;

        RestResponseModel.warranty_transfer_response_t result = RestWarrantyTransfer.updateWarrantyTransfer(warrantyTransfer);
        System.assert(result != null);
        System.assert(result.is_success);

        resultRMA = [SELECT Id,ReplacementOrder__c FROM RMA__c WHERE Name = :expectedRMANo LIMIT 1];
        
        List<Opportunity> resultOpportunity = new List<Opportunity>();
        resultOpportunity = [SELECT Id,Name,Country__c,Type FROM Opportunity WHERE Id in (SELECT ReplacementOrder__c FROM RMA__c WHERE Name=:expectedRMANo) LIMIT 1];

        System.assert(resultRMA.size() == 1);
        System.assert(resultOpportunity.size() == 1);

        System.assertEquals(resultOpportunity.get(0).Id,resultRMA.get(0).ReplacementOrder__c);
        System.assertEquals(expectedOrderId,resultOpportunity.get(0).Name);
        System.assertEquals(expectedCountry,resultOpportunity.get(0).Country__c);
        System.assertEquals(RestWarrantyTransfer.ADVANCED_REPLACEMENT,resultOpportunity.get(0).Type);
    }
}