/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/14/15
 * @email:	cmunoz@levementum.com
 * @description:	Test class for RestCountryCode at /CountryCode
 */
@isTest
public class TestRestCountryCode 
{
    public static testmethod void testCountryCodes() 
    {
    	TestRestUtilities tr = new TestRestUtilities();
    	Test.startTest();
    		tr.createCountry('US');
    		tr.createCountry('UK');
    		tr.createCOuntry('JP');
    	
    		RestRequest rReq = new RestRequest();
        	RestResponse rRes = new RestResponse();
        	
	        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CountryCode';
	        rReq.httpMethod = 'GET';
	        RestContext.request = rReq;
	        RestContext.response = rRes;
	        RestResponseModel.country_model_t countries = RestCountryCode.retrieveCountryCodes();
	        system.assertNotEquals(countries, null);
	        system.assertNotEquals(countries.country_codes.size(), 0);
        Test.stopTest();
    }
}