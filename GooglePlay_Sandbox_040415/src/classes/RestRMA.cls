/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/20/15
 * @email:	cmunoz@levementum.com
 * @description:	This REST service exposes the Order (Opportunity) object from Salesforce to be used
 * during integration testing at Google. Will use @HttpPut to update and post to insert
 */
@RestResource(urlMapping='/RMA/*')
global class RestRMA
{
	public static SObjectDataAccess dataAccess;
	
	@HttpPost
	global static ObjectModel insertObject(RMAModel objModel)
	{
		RMAModel response = null;
		try
		{
			dataAccess = new RMADataAccess();
			RMA__c objToUpdate;
			RMA__c updatedObj;
			
			if(objModel.Id != null)
			{
				objToUpdate = (RMA__c)dataAccess.selectObject(objModel.Id);
			}
			else
			{
				objToUpdate = (RMA__c)dataAccess.selectObject(objModel.Name);
			}			
			dataAccess.updateObject(objToUpdate, objModel);
			
			// REFRESH FORMULA FIELDS
			if(objModel.Id != null)
			{
				updatedObj = (RMA__c)dataAccess.selectObject(objModel.Id);
			}
			else
			{
				updatedObj = (RMA__c)dataAccess.selectObject(objModel.Name); 
			}
			
    		response = (RMAModel)updateModel(updatedObj);
    		response.status = RMADataAccess.objectStatus;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return response;
	}
	
	@HttpGet
	global static ObjectModel getObject()
	{
    	RMAModel response = null;
    	try
    	{
    		dataAccess = new RMADataAccess();
    		Map<String, String> paramMap = RestContext.request.params;
			String objName = paramMap.get('rmaName');
			
			if(String.isNotEmpty(objName))
			{
				RMA__c currentObj = (RMA__c)dataAccess.selectObject(objName);			
				response = (RMAModel)updateModel(currentObj);
			}
    	}		
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	if(response != null)
    	{
    		response.status = RMADataAccess.objectStatus;
    	}
        return response;
    }
	
	@HttpDelete
	global static void deleteObject()
	{
		try
		{
			dataAccess = new RMADataAccess();
    		Map<String, String> paramMap = RestContext.request.params;
			String objName = paramMap.get('rmaName');	
			dataAccess.deleteObject(objName);
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public static ObjectModel updateModel(sObject updatedsObject)
    {
    	RMAModel objModel = new RMAModel();
    	RMA__c objSObject = (RMA__c)updatedsObject;
    	try
    	{
    		if(objSObject != null)
    		{
	    		//objModel.
    		}
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	return objModel;
    }
}