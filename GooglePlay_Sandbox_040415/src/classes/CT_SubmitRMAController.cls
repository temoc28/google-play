/*
* @auther : CloudSherpas Inc. 
* @date : 02/07/2013
* @description : Apex class for marking RMA for Ready for Submit
*/
public class CT_SubmitRMAController
{
    //Variable Declaration
    public RMA__c rmaObj;
    public Id rmaId;
    String sfdcBaseURL;
    
    //Constructor
    public CT_SubmitRMAController(ApexPages.StandardController stdController) {
    
        rmaId = ApexPages.currentPage().getParameters().get('id');
        sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();    
        
        rmaObj = (rmaid == null) ? new RMA__c() : [SELECT id, Name, Ready_to_Submit__c FROM RMA__c WHERE id =: rmaid LIMIT 1];
    }  
    
    public PageReference autoRun() {   
        try {
            rmaObj.Ready_to_Submit__c = true;
            update rmaObj;            
        }catch(Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error : ' + e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }          
        return RedirectToRMA();
    }    
       
    public PageReference RedirectToRMA() {
        PageReference Page = new PageReference(sfdcBaseURL+'/'+rmaId);
        Page.setRedirect(true);
        return Page;
    }        
    
    static testMethod void testSubmitRMAController()
    {
        Test.startTest();         
            
          Account acc = new Account(Name = 'TestAccount');
        insert acc;
        
        Contact con = new Contact(LastName = 'TestContact', AccountId = acc.Id, MailingStreet = '3300 Fremont Blvd Ste 100', 
            MailingState = 'CA', MailingCity = 'Fremont', MailingPostalCode = '94358');
        insert con;
        
        acc.PersonAcctContactLookup__c = con.Id;
        update acc;

        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c(
            Name = 'CHromecaseTest');
        insert codeSet;

        Product_Family__c prodFam = new Product_Family__c(Name = 'Chromecast Test');
        insert prodFam;

        Document__c doc = new Document__c(
            Name = 'Chromecast Test'
            ,Product_Family__c = prodFam.Id
            ,Return_Reason_Code_Set__c = codeSet.Id);
        insert doc;

        Product2 prod = new Product2(Name = 'Chromecast US Test'
            ,SKU__c = '86002596-01-Test'
            ,Sale_Country__c = 'US'
            ,Unique_ID_Type__c = 'Serial Number'
            ,RMARecordType__c = 'Chromecast'
            ,Document__c = doc.Id);
        insert prod;

        Id rtId = Schema.SObjectType.RMA__c.getRecordTypeInfosByName().get('Chromebook WiFi').getRecordTypeId();
        System.assertNotEquals(rtId, null);
        
        Google_Asset__c ga = new Google_Asset__c (Google_Asset_ID_External_ID__c = '9421341099', Asset_Type__c = 'New',
            Active_Device__c = 'Active', AssetOwner__c = acc.Id, Carrier__c = 'Carrier1', IMEI__c = 'IMEI',ProductID__c = prod.Id );
        insert ga;
        System.assertNotEquals(ga.Id, null);
        
        Order__c ord = new Order__c(Name = 'testorder');
        insert ord;
        System.assertNotEquals(ord.Id, null);
        
        RMA__c rma = new RMA__c(rma_country__c = 'US', Type__c = 'Galaxy Nexus - Remorse', Status__c = 'Pending Return', 
            Notes__c = 'test-notes', 
            GoogleCustomer__c = acc.Id, Order__c = ord.Id, GoogleAsset__c = ga.Id,
            RecordTypeId = rtId, Line_Number__c = 1, Return_Shipping_Tracking_Number__c = 'F2012', 
            Customer_Induced_Damage__c = 'N', RMA_Sub_Category__c = 'LCD Issue', Google_Account__c = con.Id,
            Ready_to_Submit__c = true);
        insert rma;
        System.assertNotEquals(rma.Id, null);     
        
        ApexPages.StandardController sc = new ApexPages.standardController(rma);            
        CT_SubmitRMAController myPageCon = new CT_SubmitRMAController(sc);
        PageReference pref = myPageCon.autoRun(); 
        myPageCon.RedirectToRMA(); 
        
        Test.stopTest();
    }  
}