/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/14/15
 * @email:	cmunoz@levementum.com
 * @description:	General DML Operations for Country custom object
 */desfd
public with sharing class CountryDataAccess 
{
	public static String objectStatus = 'OK';
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/14/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Retrieves all Countries
	 */
	public static List<Country__c> selectCountries()
	{
		system.debug('Total Number of SOQL Queries allowed in this apex code context: ' +  Limits.getLimitQueries());
    	system.debug('Total Number of records that can be queried  in this apex code context: ' +  Limits.getLimitDmlRows());
		system.debug('Total Number of DML statements allowed in this apex code context: ' +  Limits.getLimitDmlStatements() );
		
		List<Country__c> countries = new List<Country__c>();
		try
		{
			countries = [SELECT AR_Method__c, Advanced_Replacement_New_Article_ID__c, Advanced_Replacement_Refurb_Article_ID__c, 
						        Buyer_s_Remorse_Article_ID__c, Name, Country_ID_External_ID__c, CreatedById, CreatedDate, 
						        Currency__c, DOA_Period__c, Default_Service_Model__c, IsDeleted, Exchange_New_Article_ID__c, 
						        Exchange_Refurb_Article_ID__c, Expected_Sale_Period__c, LastModifiedById, LastModifiedDate, 
						        Minimum_Replacement_Warranty_Period__c, OwnerId, Id, Remorse_Period__c, Replace_No_Return_Article_ID__c, 
						        Replacement_Model__c, Return_Center__c, Supported_Languages__c, SystemModstamp, Warranty_Period__c, 
						        Warranty_Refund_Article_ID__c 
						 FROM Country__c];
			system.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    		system.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    		system.debug('3. Number of script statements used so far : ' +  Limits.getDmlStatements());
    		system.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    		system.debug('5.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}
		return countries;
	}
}