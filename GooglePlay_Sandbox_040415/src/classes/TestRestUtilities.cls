public without sharing class TestRestUtilities {

    public Map<String,Id> idsMap = new Map<String,Id>();
    public Map<String,String> namesMap = new Map<String,string>();

    public void createAccount(){
        Account acct = new Account();
        acct.PersonEmail = 'test@levtest.com';
        acct.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'GoogleCustomer'].id;
        acct.FirstName = 'test';
        acct.LastName = 'levtest';
        acct.PersonMailingCountry = 'US';
        acct.PersonMailingState = 'AZ';
        insert acct;
        idsMap.put('Account1',acct.Id);
    }


    public void createAccount2(){
        Account acct2 = new Account();
        acct2.Name = 'TestLevBiz';
        acct2.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'BusinessAccount'].id;
        insert acct2;
        idsMap.put('Account2',acct2.Id);
    }

    public void createAccount3(String email){
        Account acct = new Account();
        acct.PersonEmail = email;
        acct.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'GoogleCustomer'].id;
        acct.FirstName = 'custom';
        acct.LastName = 'Email Account';
        acct.PersonMailingCountry = 'US';
        acct.PersonMailingState = 'AZ';
        insert acct;
        idsMap.put('Account3',acct.Id);
    }

    public void createCodeSet(){
        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c();
        codeSet.Name = 'CHromecaseTest';
        insert codeSet;
        idsMap.put('Return_Reason_Code_Set__c1',codeSet.Id);
    }

    public void createReasonCode(){
         Return_Reason_Code__c code = new Return_Reason_Code__c();
        code.Name = 'ChromecaseTest';
        code.RMA_Types__c = 'Buyer\'s Remorse';
        code.Return_Reason_Code_Set__c = idsMap.get('Return_Reason_Code_Set__c1');
        insert code;
        idsMap.put('Return_Reason_Code__c1',code.Id);
    }

    public void createProductFamily(){
         Product_Family__c prodFam = new Product_Family__c();
        prodFam.Name = 'Chromecast Test';
        insert prodFam;
        idsMap.put('Product_Family__c1',prodFam.Id);
    }

    public void createDocument(){
        Document__c doc = new Document__c();
        doc.Name = 'Chromecast Test';
        doc.Product_Family__c = idsMap.get('Product_Family__c1');
        doc.Return_Reason_Code_Set__c = idsMap.get('Return_Reason_Code_Set__c1');
        insert doc;
        idsMap.put('Document__c1',doc.Id);
    }

    public void createCountryVariant(){
        Country_Variant__c cv = new Country_Variant__c();
        cv.Document__c = idsMap.get('Document__c1');
        cv.Name = 'US';
        insert cv;
        idsMap.put('Country_Variant__c1',cv.Id);
    }

    public void createProduct1(){
        Product2 prod = new Product2();
        prod.Name = 'Chromecast US Test';
        prod.SKU__c = '86002596-01-Test';
        prod.Sale_Country__c = 'US';
        prod.Unique_ID_Type__c = 'Serial Number';
        prod.RMARecordType__c = 'Chromecast';
        prod.Document__c = idsMap.get('Document__c1');
        insert prod;
        idsMap.put('Product2',prod.Id);
    }

    public void createOpportunity(){
        Opportunity oppt = new Opportunity();
        oppt.AccountId = idsMap.get('Account1');
        oppt.OrderIDExternalID__c = '32434902134BNMBd';
        oppt.TransactionDate__c = dateTime.now();
        oppt.Country__c = 'US';
        oppt.State_Province__c = 'AZ';
        oppt.Name = 'testOppt';
        oppt.Type = 'Standard Order';
        oppt.CloseDate = Date.today().addDays(30);
        oppt.StageName = 'Prospecting';
        insert oppt;
        idsMap.put('Opportunity1',oppt.Id);        
        namesMap.put('Opportunity1', oppt.Name);
    }

    public void createOpportunity2(){
        Opportunity oppt2 = new Opportunity();
        oppt2.AccountId = idsMap.get('Account2');
        oppt2.OrderIDExternalID__c = '32434902134BNMBd2';
        oppt2.TransactionDate__c = dateTime.now();
        oppt2.Country__c = 'US';
        oppt2.State_Province__c = 'AZ';
        oppt2.Name = 'testOppt2';
        oppt2.Type = 'Standard Order';
        oppt2.CloseDate = Date.today().addDays(30);
        oppt2.StageName = 'Prospecting';
        insert oppt2;
        idsMap.put('Opportunity2',oppt2.Id);
    }

    public void createGoogleAsset(){
        Google_Asset__c ga = new Google_Asset__c();
        ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga.AssetOwner__c = idsMap.get('Account1');
        ga.SKU__c = '86002596-01-Test';
        ga.Name = 'TestGA';
        ga.Order_Opportunity__c = idsMap.get('Opportunity1');
        ga.Line_Number__c = 1;
        ga.ProductID__c = idsMap.get('Opportunity1');
        insert ga;
        idsMap.put('Google_Asset__c1',ga.Id);
    }

    public void createGoogleAsset2(){
        Google_Asset__c ga2 = new Google_Asset__c();
        ga2.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga2.AssetOwner__c = idsMap.get('Account2');
        ga2.SKU__c = '86002596-01-Test2';
        ga2.Name = 'TestGA2';
        ga2.Order_Opportunity__c = idsMap.get('Opportunity2');
        ga2.Line_Number__c = 1;
        ga2.ProductID__c = idsMap.get('Product2');
        insert ga2;
        idsMap.put('Google_Asset__c2',ga2.Id);
    }

    public void createRMA1()
	{
		try
		{
			RMA__c rma = new RMA__c();
			rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
			rma.Notes__c = 'test notes';
			rma.gCases_ID__c = '5-7327000004804';
			rma.GoogleCustomer__c = idsMap.get('Account1');
			rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
			insert rma;

			rma = [SELECT name from RMA__c WHERE Id = :rma.Id]; //reload the RMA to get the Name
			idsMap.put('RMA1',rma.Id);
			namesMap.put('RMA1_NAME',rma.Name);
        }
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
    }

    public void createLevUser()
    {
        User u = null;
        try
        {
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
          u = new User(Alias = 'stdlev', Email='standarduser@levementum.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@levementum.com',LDAP__c='standarduser@levementum.com');
          insert u;
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
        }
        idsMap.put('levUser1',u.Id);
    }

	public Refund__c createRefund()
	{
		List<RecordType> rTypes = (List<RecordType>)getRecordType('Refund__c');		
		Refund__c ref = null;
		if(rTypes.size() > 0)
		{
	        ref = new Refund__c();
	        ref.RecordTypeId = rTypes.get(0).Id;
	        insert ref;
	        idsMap.put('Refund1', ref.Id);        
	        namesMap.put('Refund1', ref.Name);
		}
		return ref;
    }
    
    public List<sObject> getRecordType(String sObjectName)
    {
    	List<sObject> recordTypes = new List<sObject>();
    	try
    	{
    		String soql = 'SELECT Id, Name, DeveloperName '
        					+ 'FROM RecordType '
        					+ 'WHERE SObjectType = \'' + sObjectName + '\' '
        					+ 'AND IsActive = TRUE';
        	recordTypes = Database.query(soql);
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    	return recordTypes;
    }
}