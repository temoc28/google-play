@isTest
private class TestCancelExtendedWarranty {
    
    static Extended_Warranty__c ext;

    static testMethod void myUnitTest() {
        Account a = new Account();
        a.Name = 'TestAccount';
        insert a;

        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Closed Won';
        opp.AccountId = a.Id;
        opp.Type = 'Advanced Replacement';
        insert opp;

        ext = new Extended_Warranty__c(Google_Order__c = opp.Id, Line_Number__c = 2);
        insert ext;

        CancelExtendedWarranty cancelExtended = new CancelExtendedWarranty(new ApexPages.StandardController(ext));
        cancelExtended.cancelWarranty();

        Refund__c refu = [SELECT Id, LineNumber__c, OrderNumber__c, PercentItemRefund__c, PercentTaxRefund__c FROM Refund__c WHERE ExtendedWarranty__c = :ext.Id LIMIT 1];
        System.assertNotEquals(null, refu);
        System.assertEquals(ext.Line_Number__c + '', refu.LineNumber__c);
        System.assertEquals(ext.Google_Order__c, refu.OrderNumber__c);
        System.assertEquals(100, refu.PercentItemRefund__c);
        System.assertEquals(100, refu.PercentTaxRefund__c);
    }
   
    
}