trigger RMA_Object_Trigger on RMA__c (after insert, after update, before insert, before update)
{
    if(trigger.isBefore)
    {
        if(trigger.isInsert || trigger.isUpdate)
        {
            // REQ #2: need to ensure only 1 active RMA per asset
            Set<Id> assets = new Set<Id>();
            // get related Google Account contacts (on insert you cannot use the relation, the address is null)
            for(RMA__c rma : trigger.new)
            {
                // get a list of Assets being used
                if(rma.GoogleAsset__c != null)
                    assets.add(rma.GoogleAsset__c);
                else if((rma.Type__c != null && !rma.Type__c.equalsIgnoreCase('Extended Warranty')) || rma.Type__c == null)
                    rma.addError('Sorry, each RMA record MUST have a related asset.');
            }
            map<Id, Id> assetMap = new map<Id, Id>();    
            // get a list of RMAs that are trying to use the same google asset
            for(RMA__c rma:[Select Id, Status__c, GoogleAsset__c from RMA__c where GoogleAsset__c in :assets])
            {
                if(rma.Status__c != 'Closed')
                    assetMap.put(rma.GoogleAsset__c, rma.Id);
            }    
            // append the newly related google account address to the Google Account Address field
            for(RMA__c rma : trigger.new)
            {
                Boolean IsInsertAndExistingRmaNotClosedWithSameAsset=(trigger.isInsert && assetMap.get(rma.GoogleAsset__c) != null);
                Boolean IsUpdateAndAssetHasChangedAndExistingRmaNotClosedWithSameAsset=(trigger.isUpdate && (rma.GoogleAsset__c != trigger.oldMap.get(rma.Id).GoogleAsset__c) && assetMap.get(rma.GoogleAsset__c) != null );      
                // if the asset is already being used by another open RMA, issue an error
                if(((rma.Type__c != null && !rma.Type__c.equalsIgnoreCase('Extended Warranty')) || rma.Type__c == null) && ( IsInsertAndExistingRmaNotClosedWithSameAsset || IsUpdateAndAssetHasChangedAndExistingRmaNotClosedWithSameAsset ))          
                    rma.addError('Sorry, you can\'t add this asset. There is already an open RMA for this asset.  <a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + assetMap.get(rma.GoogleAsset__c) + '\'>' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + + assetMap.get(rma.GoogleAsset__c) + '</a>');
            }
        }
        if(trigger.isInsert)
            UpdateLinksOnRMATrigger.UpdateValues(trigger.new); //Update the Account on the GAE to the GAE Email's Account
        if(trigger.isUpdate)
            RmaTrgHandler.checkCreateRefund(trigger.new, trigger.newMap, trigger.oldMap);
    
  }
 
  else if(trigger.isAfter)
  {
      List<Account> RMACIDExceptionsToUpdate = new List<Account>();
  Set<ID> accountsToPossiblyUncheck = new Set<ID>();
  Set<ID> nonExceptionRMAs = new Set<ID>();     //the bizappshelper class prevents the insert trigger from calling the update
     //trigger as a result of the insert calling update    
     if(trigger.isInsert||trigger.isAfter){
         for(RMA__c r : trigger.new){
              if(r.RMA_Sub_Category__c == 'CID Exception'){
                  if(trigger.isInsert){
                      //check parent box to true
                      RMACIDExceptionsToUpdate.add(new Account(ID = r.GoogleCustomer__c,CID_Exception_Granted__c = true));
                  }else{
                      if(r.RMA_Sub_Category__c != trigger.oldMap.get(r.id).RMA_Sub_Category__c){
                          RMACIDExceptionsToUpdate.add(new Account(ID = r.GoogleCustomer__c,CID_Exception_Granted__c = true));
                      }
                  }  
               }
               else if(trigger.isUpdate && trigger.oldMap.get(r.id).RMA_Sub_Category__c=='CID Exception'){
                        //need to possibly uncheck the box on account
                    accountsToPossiblyUncheck.add(r.GoogleCustomer__c);
                    nonExceptionRMAs.add(r.id);
               }
         }
         for(Account a : [Select ID, (Select ID from RMA__r where RMA_Sub_Category__c ='CID Exception' AND ID NOT IN : nonExceptionRMAs limit 1) from Account where ID IN: accountsToPossiblyUncheck]){
             if(a.RMA__r.size()==0){
                RMACIDExceptionsToUpdate.add(new Account(ID = a.Id,CID_Exception_Granted__c = false));
                System.debug('Anydatatype_msg' + RMACIDExceptionsToUpdate);
             } 
         }
         if(RMACIDExceptionsToUpdate.size()>0){
             update RMACIDExceptionsToUpdate;
         }
     }
     if(trigger.isInsert)
     {      
        //BizAppsHelper.setProcessed();
        //CopyRMAIdentifier.UpdateValues(trigger.new); //Copy the RMA ID to the external ID
        List<RMA__c> warrantyRMAList = new List<RMA__c>();
        
        //CID Exception Field Update
        Map<Id,RMA__c> newTrigger = trigger.newMap;
        if(newTrigger.size()>0){
          List<String> rmaAccountIdList = new List<String>();
          List<Account> rmaUpdateAccountList = new List<Account>();
          for(RMA__c tempRMA:newTrigger.values()){
              rmaAccountIdList.add(String.valueOf(tempRMA.GoogleCustomer__c));
          }
          Map<Id,Account> possibleAccountMap = new Map<Id,Account>([SELECT Id,CID_Exception_Granted__c,CID_Exception_Granted_Asset__c,CID_Exception_Granted_Date_Time__c,
                                                          CID_Exception_on_This_Asset__c FROM Account WHERE Id IN:rmaAccountIdList]);
          for(RMA__c tempRMA:newTrigger.values()){
            Datetime exceptionDateTime = null;
            if(possibleAccountMap.get(tempRMA.GoogleCustomer__c)!=null && possibleAccountMap.get(tempRMA.GoogleCustomer__c).get('CID_Exception_Granted_Date_Time__c') != null){
              exceptionDateTime = Datetime.valueOf(possibleAccountMap.get(tempRMA.GoogleCustomer__c).get('CID_Exception_Granted_Date_Time__c'));
            }
            if(tempRMA.RMA_Category__c=='1x Exception' && (exceptionDateTime==null || exceptionDateTime.date()<tempRMA.CreatedDate.date())){
              Account tempUpdateAccount = new Account(Id=tempRMA.GoogleCustomer__c);
              tempUpdateAccount.CID_Exception_Granted__c=true;
              tempUpdateAccount.CID_Exception_Granted_Asset__c=tempRMA.GoogleAsset__c;
              tempUpdateAccount.CID_Exception_Granted_Date_Time__c=datetime.now();
              tempUpdateAccount.CID_Exception_on_This_Asset__c=tempRMA.RMA_Sub_Category__c;
              rmaUpdateAccountList.add(tempUpdateAccount);
            }
          }
          if(rmaUpdateAccountList.size()>0){
            update rmaUpdateAccountList;
          }
          System.debug('Trigger Insert rmaUpdateAccountList :: '+rmaUpdateAccountList);
        }
     }
     else if(trigger.isUpdate && (!BizAppsHelper.hasProcessed()||Test.isRunningTest()))
     {
        Set<Id> SRMAIds = new Set<Id>();
        Set<Id> toGlassIdSet = new Set<Id>();
        Id totalOrderRtId;
        List<Refund__c> refundList = new List<Refund__c>();
        List<RMA__c> rmaList = new List<RMA__c>();
        Set<Id> ordersToCheck = new Set<Id>();
        Map<Id,Refund__c> refundMap = new Map<Id,Refund__c>();
        List<RMA__c> warrantyRMAList = new List<RMA__c>();
        

        //CID Exception Field Update
        Map<Id,RMA__c> updateTrigger = trigger.newMap;
        if(updateTrigger.size()>0){
          List<String> rmaAccountIdList = new List<String>();
          List<Account> rmaUpdateAccountList = new List<Account>();
          for(RMA__c tempRMA:updateTrigger.values()){
              rmaAccountIdList.add(String.valueOf(tempRMA.GoogleCustomer__c));

              //check to see if there are already total order refunds for the ones we should try and create a Total Order Refund for
              //if entry criteria changes, change in the other loop too
              if((tempRMA.Status__c=='Received' && trigger.oldMap.get(tempRMA.Id).Status__c!=tempRMA.Status__c) || (tempRMA.ReplacementOrder__c!=trigger.oldMap.get(tempRMA.Id).ReplacementOrder__c && tempRMA.Status__c=='Received') 
              && (tempRMA.Type__c=='Warranty DOA' || tempRMA.Type__c=='Warranty Regular') && tempRMA.Google_Order_Type_Display__c!='Glass Order')
              {
                  if(String.isNotEmpty(tempRMA.ReplacementOrder__c))
                  {
                      ordersToCheck.add(tempRMA.ReplacementOrder__c);
                      warrantyRMAList.add(tempRMA);
                  }
              }    
              if(tempRMA.SendToBizApps__c==true && tempRMA.Created_on_Google_Server__c==false)   
                  SRMAIds.add(tempRMA.Id);  
              
              if(String.isNotEmpty(tempRMA.GlassRmaId__c) && (tempRMA.Status__c=='Received'||tempRMA.Status__c=='Closed') && trigger.oldMap.get(tempRMA.Id).Status__c!=tempRMA.Status__c)
                  toGlassIdSet.add(tempRMA.Id);
          }
          Map<Id,Account> possibleAccountMap = new Map<Id,Account>([SELECT Id,CID_Exception_Granted__c,CID_Exception_Granted_Asset__c,CID_Exception_Granted_Date_Time__c,
                                                          CID_Exception_on_This_Asset__c FROM Account WHERE Id IN:rmaAccountIdList]);
          for(RMA__c tempRMA:updateTrigger.values()){
            Datetime exceptionDateTime = null;
            if(possibleAccountMap.get(tempRMA.GoogleCustomer__c)!=null && possibleAccountMap.get(tempRMA.GoogleCustomer__c).get('CID_Exception_Granted_Date_Time__c') != null){
              exceptionDateTime = Datetime.valueOf(possibleAccountMap.get(tempRMA.GoogleCustomer__c).get('CID_Exception_Granted_Date_Time__c'));
            }
            if(tempRMA.RMA_Category__c=='1x Exception' && (exceptionDateTime==null || exceptionDateTime.date()<tempRMA.CreatedDate.date())){
              Account tempUpdateAccount = new Account(Id=tempRMA.GoogleCustomer__c);
              tempUpdateAccount.CID_Exception_Granted__c=true;
              tempUpdateAccount.CID_Exception_Granted_Asset__c=tempRMA.GoogleAsset__c;
              tempUpdateAccount.CID_Exception_Granted_Date_Time__c=datetime.now();
              tempUpdateAccount.CID_Exception_on_This_Asset__c=tempRMA.RMA_Sub_Category__c;
              rmaUpdateAccountList.add(tempUpdateAccount);
            }
          }
          if(rmaUpdateAccountList.size()>0){
            update rmaUpdateAccountList;
          }
          System.debug('Trigger Update rmaUpdateAccountList :: '+rmaUpdateAccountList);
        }


        /*for(RMA__c rma : trigger.new)
        {
            //check to see if there are already total order refunds for the ones we should try and create a Total Order Refund for
            //if entry criteria changes, change in the other loop too
            if((rma.Status__c=='Received' && trigger.oldMap.get(rma.Id).Status__c!=rma.Status__c) || (rma.ReplacementOrder__c!=trigger.oldMap.get(rma.Id).ReplacementOrder__c && rma.Status__c=='Received') 
            && (rma.Type__c=='Warranty DOA' || rma.Type__c=='Warranty Regular') && rma.Google_Order_Type_Display__c!='Glass Order')
            {
                if(String.isNotEmpty(rma.ReplacementOrder__c))
                {
                    ordersToCheck.add(rma.ReplacementOrder__c);
                    warrantyRMAList.add(rma);
                }
            }    
            if(rma.SendToBizApps__c==true && rma.Created_on_Google_Server__c==false)   
                SRMAIds.add(rma.Id);  
            
            if(String.isNotEmpty(rma.GlassRmaId__c) && (rma.Status__c=='Received'||rma.Status__c=='Closed') && trigger.oldMap.get(rma.Id).Status__c!=rma.Status__c)
                toGlassIdSet.add(rma.Id);
        }*/
        if(!ordersToCheck.isEmpty())
        {
            totalOrderRtId=Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Refund').getRecordTypeId();
            for(Refund__c refund : [SELECT Id,OrderNumber__c FROM Refund__c WHERE RecordTypeId=:totalOrderRtId AND OrderNumber__c in :ordersToCheck])
            {
                if(!refundMap.containsKey(refund.OrderNumber__c))
                    refundMap.put(refund.OrderNumber__c,refund);
            }
        }
        for(RMA__c rma : warrantyRmaList)
        {
            //if(mChromeBookRTs.ContainsKey(rma.RecordTypeId) && rma.Created_on_Google_Server__c == false)
            
            if((rma.Status__c=='Received' && trigger.oldMap.get(rma.Id).Status__c!=rma.Status__c) || (rma.ReplacementOrder__c!=trigger.oldMap.get(rma.Id).ReplacementOrder__c) 
            && (rma.Type__c=='Warranty DOA' || rma.Type__c=='Warranty Regular'))
            {
                if(String.isEmpty(rma.ReplacementOrder__c))
                    continue;
                
                if(!refundMap.containsKey(rma.ReplacementOrder__c))
                {
                    //create total order refund against the replacement order if a total order refund does not exist
                    //otherwise, just mark the RMA closed           
                    refundList.add(new Refund__c(RMA__c=rma.Id,OrderNumber__c=rma.ReplacementOrder__c,
                    PercentShippingItemRefund__c=100,PercentShippingTaxRefund__c=100,
                    RefundStatus__c='Pending',ExpectedRefundDate__c=BusinessDays.addBusinessDays(Date.today(),1),
                    RecordTypeId=totalOrderRtId,RefundReason__c='Cancel Auth'));
                
                    rmaList.add(new RMA__c(Id=rma.Id,Status__c='Cancel Auth Pending'));
                }
                else
                    rmaList.add(new RMA__c(Id=rma.Id,Status__c='Closed'));
            }
        }  
        
        if(!refundList.isEmpty())
            insert refundList;
        if(!rmaList.isEmpty())
            update rmaList;        
        if(!SRMAIds.isEmpty() && BizAppsHelper.processedRmaIdSet.isEmpty())
        {
            CT_Utility.Process(SRMAIds);
            BizAppsHelper.processedRmaIdSet.addAll(SRMAIds);
        }
        else if(!SRMAIds.isEmpty())
        {
            //make sure we aren't resending
            Set<Id> toSend = new Set<Id>();
            for(Id rmaId : SRMAIds)
            {
                if(!BizAppsHelper.processedRmaIdSet.contains(rmaId))
                    toSend.add(rmaId);
            }
            if(!toSend.isEmpty())
                CT_Utility.Process(toSend);
        }
        //if(!SRMAIds.isEmpty() && !BizAppsHelper.hasProcessed())
        //  CT_Utility.Process(SRMAIds);
        //BizAppsHelper.setProcessed();
        //makes a future callout to Glass to mark the rma received        
        List<GlassIntegration__c> piList = GlassIntegration__c.getALL().values();       
        if(!toGlassIdSet.isEmpty() && UserInfo.getUserName()!= 'playglassintegration@levementum.com.gs')
            PlayToGlass.changeRmaStatus(toGlassIdSet);
    }  
  }
}