trigger Opportunity_Object_Trigger on Opportunity (before insert,after insert,after update) 
{
	if(trigger.isBefore)
	{
		if(trigger.isInsert)
			OpportunityTrgHandler.onBeforeInsert(trigger.new);					
	}	
	else if(trigger.isAfter)
	{
		if(trigger.isInsert)
			OpportunityTrgHandler.onAfterInsert(trigger.new);			
		else if(trigger.isUpdate)
			OpportunityTrgHandler.onAfterUpdate(trigger.new,trigger.oldMap);
	}	
}