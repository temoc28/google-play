trigger Account_Object_Trigger on Account (after delete, after insert, after update, after undelete, before delete, before insert, before update) {
    
    Map<String, Account> accountMap = new Map<String, Account>();
    
    if(trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            CopyAccountIdentifier.UpdateValues(trigger.new); //Update the External Id with the Account Name (Id) 
        } 
        if(!Trigger.isDelete && !Trigger.isUndelete) {
            for (Account a : System.Trigger.new) {
                if ((a.PersonEmail != null) && (System.Trigger.isInsert ||
                        (a.PersonEmail != System.Trigger.oldMap.get(a.Id).PersonEmail))) {
                    if (accountMap.containsKey(a.PersonEmail)) {
                        a.PersonEmail.addError('Another new Account has the same email address.');
                    } else {
                        accountMap.put(a.PersonEmail, a);
                    }
                }
            }
            for (Account a : [SELECT PersonEmail FROM Account WHERE PersonEmail IN :accountMap.KeySet()]) {
                Account newAccount = accountMap.get(a.PersonEmail);
                newAccount.PersonEmail.addError('An Account with this email address already exists.');
            }
        }
    } else if(trigger.isAfter){
        if(trigger.isInsert || trigger.isUpdate){
            CT_AccountServices.CreatePersonAccountContactLookup(trigger.new);
        }
        
    }
    

    

}