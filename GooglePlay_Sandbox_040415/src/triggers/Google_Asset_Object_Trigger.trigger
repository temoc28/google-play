trigger Google_Asset_Object_Trigger on Google_Asset__c (before insert,before update,after insert,after update) 
{        
    if(trigger.isBefore)
    {
        if(trigger.isInsert)
        	GoogleAssetTrgHandler.onBeforeInsert(trigger.new);
        else if(trigger.isUpdate)
        	GoogleAssetTrgHandler.onBeforeUpdate(trigger.new,trigger.oldMap);       
    }    
    else if(trigger.isAfter)
    {
    	if(trigger.isInsert)
    		GoogleAssetTrgHandler.onAfterInsert(trigger.new);
    	else if(trigger.isUpdate)
    		GoogleAssetTrgHandler.onAfterUpdate(trigger.new,trigger.oldMap);
    }
}