trigger EmailMessage_Object_Trigger on EmailMessage (before insert) 
{
	if(trigger.isBefore)
	{
		if(trigger.isInsert)
			EmailMessageTriggerHandler.onBeforeInsert(trigger.new);
	}
}