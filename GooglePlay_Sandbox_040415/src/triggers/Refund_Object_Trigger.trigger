trigger Refund_Object_Trigger on Refund__c (before update, before insert,after update,after insert) 
{
	if(trigger.isBefore)
	{
		if(trigger.isInsert)
			RefundTrgHandler.onBeforeInsert(trigger.new);
		if(trigger.isUpdate)
			RefundTrgHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
	}
	else if(trigger.isAfter)
	{
		if(trigger.isInsert)
			RefundTrgHandler.onAfterInsert(trigger.new);
		if(trigger.isUpdate)
			RefundTrgHandler.onAfterUpdate(trigger.new, trigger.oldMap);
	}
}