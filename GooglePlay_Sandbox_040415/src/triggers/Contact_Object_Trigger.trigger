trigger Contact_Object_Trigger on Contact (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	if(Trigger.isBefore){
		if(Trigger.isInsert){
			SetAccountOnContact.UpdateValues(Trigger.new); //Set an Account for the Contact if it does not have one already
		}
	}
}