trigger Case_Object_Trigger on Case (before insert, before update)
{
	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			CaseTriggerHandler.updateStatusTime(Trigger.new);
			CaseTriggerHandler.ensureRandom(Trigger.new);
		}
		if(Trigger.isUpdate)
		{
			CaseTriggerHandler.recalculateBusinessHoursAges(Trigger.new, Trigger.oldMap);
			CaseTriggerHandler.ensureRandom(Trigger.new);
		}
	}
}