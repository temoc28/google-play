<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_User_External_ID</fullName>
        <field>Username_External_ID__c</field>
        <formula>Username</formula>
        <name>Update User External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update External ID</fullName>
        <actions>
            <name>Update_User_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update External ID</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>de-activate inactive users</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.LastLoginDate</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
