<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Chrome_OOW_RMA_is_shipped</fullName>
        <description>Chrome OOW RMA is shipped</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Chrome_OOW_RMA_is_shipped</template>
    </alerts>
    <alerts>
        <fullName>Chrome_OOW_RMA_needs_authroization</fullName>
        <description>Chrome OOW RMA needs authroization</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Chrome_OOW_RMA_needs_auth</template>
    </alerts>
    <alerts>
        <fullName>DOA_21_Day_Notify_Customer_to_Return_Device</fullName>
        <description>DOA: 21 Day Notify Customer to Return Device</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/X21_Day_Notify_Customer_to_Return_Device</template>
    </alerts>
    <alerts>
        <fullName>DOA_28_Day_Notify_Customer_to_Return_Device</fullName>
        <description>DOA: 28 Day Notify Customer to Return Device</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/X28_Day_Notify_Customer_to_Return_Device</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Owner_to_Refund_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RMA_Needs_Refund</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to Refund Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Received</fullName>
        <description>Changes RMA status to received</description>
        <field>Status__c</field>
        <literalValue>Received</literalValue>
        <name>Change Status to Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Play_RMA_Audit</fullName>
        <field>RMA_Audit__c</field>
        <literalValue>1</literalValue>
        <name>Check RMA Audit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checks_the_RMA_Needs_Review</fullName>
        <field>RMA_Needs_Review__c</field>
        <literalValue>1</literalValue>
        <name>Checks the RMA Needs Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insert_Needs_Shipping_Label_Date</fullName>
        <field>Needs_Shipping_Label__c</field>
        <formula>TODAY()</formula>
        <name>Insert Needs Shipping Label Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_RMA_data_received_date</fullName>
        <description>Set date for when RMA triage data was uploaded into Salesforce</description>
        <field>VendorAssetReceivedNotificationDate__c</field>
        <formula>Today()</formula>
        <name>Mark RMA data received date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_RMA_Category</fullName>
        <field>Previous_RMA_Category__c</field>
        <formula>PRIORVALUE(  RMA_Sub_Category__c  )</formula>
        <name>Previous Sub RMA Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Extended_Warranty_Contract_ID_Backup</fullName>
        <field>Extended_Warranty_Contract_ID_Backup__c</field>
        <formula>Extended_Warranty_Contract_ID__c</formula>
        <name>Set Extended Warranty Contract ID Backup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_In_Transit_Date</fullName>
        <field>RMA_In_Transit_Date__c</field>
        <formula>Today() - 1</formula>
        <name>Set In Transit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RMA_Closed_Date_as_Today</fullName>
        <field>RMA_Closed_Date__c</field>
        <formula>Today()</formula>
        <name>Set RMA Closed Date as Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RMA_Country</fullName>
        <description>Sets RMA country to that of Asset Sale Country</description>
        <field>RMA_Country_Workflow__c</field>
        <formula>GoogleAsset__r.Sale_Country__c</formula>
        <name>Set RMA Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RMA_Delivered_date</fullName>
        <field>RMA_Delivered_Date__c</field>
        <formula>Today() - 1</formula>
        <name>Set RMA Delivered date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RMA_Manufacturer</fullName>
        <field>Manufacturer__c</field>
        <formula>text(GoogleAsset__r.ProductID__r.Manufacturer__c)</formula>
        <name>Set RMA Manufacturer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Induced_Damage_to_N</fullName>
        <description>Update Customer Induced Damage to N</description>
        <field>Customer_Induced_Damage__c</field>
        <literalValue>N</literalValue>
        <name>Update Customer Induced Damage to N</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Remorse_RMA_AR_Review</fullName>
        <description>Field update that sets the Owner to the Remorse RMA AR Review queue when the workflow evaluates the Opportunity_Order_Type__c = Advanced Replacement</description>
        <field>OwnerId</field>
        <lookupValue>Remorse_RMA_AR_Review</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Remorse RMA AR Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Remorse_RMA_Review</fullName>
        <description>Field update that sets the Owner to Remorse Refund Review Queue when the workflow rule evaluates the RMA Triage_Code__c = C or D.</description>
        <field>OwnerId</field>
        <lookupValue>Remorse_RMA_Review</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Remorse RMA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_RMA_Category</fullName>
        <field>Previous_RMA_Category__c</field>
        <formula>IF(
ISPICKVAL(PRIORVALUE( RMA_Category__c ),&quot;Other&quot;),&quot;Other&quot;,
IF(
ISPICKVAL(PRIORVALUE(RMA_Category__c),&quot;Buyer&apos;s Remorse&quot;),&quot;Buyer&apos;s Remorse&quot;,
IF(
ISPICKVAL(PRIORVALUE(RMA_Category__c),&quot;Warranty DOA&quot;),&quot;Warranty DOA&quot;,
IF(
ISPICKVAL(PRIORVALUE(RMA_Category__c),&quot;Warranty Repair&quot;),&quot;Warranty Repair&quot;,
IF(
ISPICKVAL(PRIORVALUE(RMA_Category__c),&quot;OOW Repair&quot;),&quot;OOW Repair&quot;,
IF(
ISPICKVAL(PRIORVALUE(RMA_Category__c),&quot;Warranty Regular&quot;),&quot;Warranty Regular&quot;,
&quot;&quot;))))))</formula>
        <name>Update Previous RMA Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RMA_Change</fullName>
        <field>RMA_Category_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update RMA Sub Category Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RMA_Ready_field</fullName>
        <description>Update&apos;s RMA ready field with today&apos;s date</description>
        <field>Ready_to_send__c</field>
        <formula>Today()</formula>
        <name>Update RMA Ready field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RMA_category_date</fullName>
        <field>RMA_Category_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update RMA category date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Received</fullName>
        <field>Status__c</field>
        <literalValue>Received</literalValue>
        <name>Update Status to Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Triage_Code_to_A</fullName>
        <description>Field update that sets the default value of the Triage_Code__c = A when the Triage Code is blank and the RMA Status is Received</description>
        <field>Triage_Code__c</field>
        <literalValue>A</literalValue>
        <name>Update Triage Code to A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add address for Buyers Remorse</fullName>
        <active>false</active>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Buyer&apos;s Remorse</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.rma_country__c</field>
            <operation>notEqual</operation>
            <value>US,CA</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automatically Set Warranty Refund to Received</fullName>
        <actions>
            <name>Update_Status_to_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Warranty Refund</value>
        </criteriaItems>
        <description>Automatically Set Warranty Refund to Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome DOA 28 Day Notify</fullName>
        <actions>
            <name>X28_Day_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chromebook 3G/4G,Chromebook WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Return</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Warranty DOA</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>LAST 28 DAYS</value>
        </criteriaItems>
        <description>If an RMA is older than 28 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome DOA Notify</fullName>
        <actions>
            <name>DOA_21_Day_Notify_Customer_to_Return_Device</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chromebook 3G/4G,Chromebook WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Return</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Warranty DOA</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>LAST 21 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Chrome</value>
        </criteriaItems>
        <description>If an RMA is older than 21 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome OOW RMA needs authorization</fullName>
        <actions>
            <name>Chrome_OOW_RMA_needs_authroization</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chromebook 3G/4G,Chromebook WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Return</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>OOW Repair</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Chrome</value>
        </criteriaItems>
        <description>If an RMA is pending and OOW, Ninja should authorize return</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome Repair No returned device</fullName>
        <actions>
            <name>Chrome_Pending_Return_older_than_2_weeks</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chromebook 3G/4G,Chromebook WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Return</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>LAST 14 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Chrome</value>
        </criteriaItems>
        <description>If the RMA is pending return and the RMA is older than 2 weeks, the Ninja gets a reminder to ping the customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome Send Closing CR</fullName>
        <actions>
            <name>Send_closing_CR</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chromebook 3G/4G,Chromebook WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>OOW Repair,Warranty Repair,Warranty Regular,Warranty DOA,Buyer&apos;s Remorse</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Chrome</value>
        </criteriaItems>
        <description>When a device with a Chrome RMA is shipped, create a task to remind the Ninja to let customer know send closing CR.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insert Needs Shipping Label Date</fullName>
        <actions>
            <name>Insert_Needs_Shipping_Label_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>RMA__c.RMA_Country_Formula__c</field>
            <operation>equals</operation>
            <value>UK</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Buyer&apos;s Remorse</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Incomplete__c</field>
            <operation>equals</operation>
            <value>FALSE</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Country_Formula__c</field>
            <operation>equals</operation>
            <value>GB</value>
        </criteriaItems>
        <description>If Tech Data country, Type is changed to Buyer&apos;s Remorse, and Incomplete = False, set needs shipping label date to today.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insert Ready to be Sent date</fullName>
        <actions>
            <name>Update_RMA_Ready_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an RMA is finally complete, inserts today&apos;s date such that the RMA will be included in reports.</description>
        <formula>or(and(Incomplete__c = &apos;FALSE&apos;, ischanged(Incomplete__c)), and(Incomplete__c = &apos;FALSE&apos;, ischanged(RMA_Country_Workflow__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Play Needs Review</fullName>
        <actions>
            <name>Checks_the_RMA_Needs_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RMA_Sub_Category__c</field>
            <operation>contains</operation>
            <value>Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Auditor_Notes__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Phone RMAs,Phone/Tablet 3G/4G,Tablet RMAs,Tablet WiFi</value>
        </criteriaItems>
        <description>Checks the RMA Needs Review Checkbox fields when
1) RMA Sub Category contains Other
2) There are no Audit Notes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Play Previous Sub Category</fullName>
        <actions>
            <name>Previous_RMA_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_RMA_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Phone RMAs,Phone/Tablet 3G/4G,Tablet RMAs,Tablet WiFi</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.RMA_Sub_Category__c</field>
            <operation>notContain</operation>
            <value>Other</value>
        </criteriaItems>
        <description>Records the Previous Sub Category when a RMA Category is changed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Play RMA Audit</fullName>
        <actions>
            <name>Check_Play_RMA_Audit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Previous_RMA_Category__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Checks the box on this field when previous sub category = open</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Previous RMA category Update</fullName>
        <actions>
            <name>Update_Previous_RMA_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_RMA_category_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Displays the previous RMA category for audit</description>
        <formula>ISCHANGED( RMA_Category__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RMA Delivered</fullName>
        <actions>
            <name>Set_RMA_Delivered_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>Delivered</value>
        </criteriaItems>
        <description>When we receive notification from our carrier that a device has been delivered, sets the date for tracking purposes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA In Transit</fullName>
        <actions>
            <name>Set_In_Transit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>In Transit</value>
        </criteriaItems>
        <description>When we receive notification from our carrier that a device has been picked up, sets the date for tracking purposes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA Received</fullName>
        <actions>
            <name>Mark_RMA_data_received_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <description>When we receive notification from our repair vendor that a device has been received, sets the date for tracking purposes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA Received and Open</fullName>
        <actions>
            <name>Change_Status_to_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>Delivered</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Upserted_Status__c</field>
            <operation>equals</operation>
            <value>In Transit</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Return</value>
        </criteriaItems>
        <description>If RMA is received and is still open in our system, sets status of RMA to &quot;Received.&quot; This way, closed RMAs are not reopened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA Status Received CID Blank Update CID</fullName>
        <actions>
            <name>Update_Customer_Induced_Damage_to_N</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule that evaluates when the RMA Status = Received and checks to see if r the CID = blank. If so, CID to N</description>
        <formula>AND( ISPICKVAL(Status__c, &quot;Received&quot;),( ISPICKVAL(Customer_Induced_Damage__c, &quot;&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA Status Received Triage Code Blank Update Triage Code</fullName>
        <actions>
            <name>Update_Triage_Code_to_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule that evaluates when the RMA Status = Received and checks to see if the Triage Code = blank. If so, Triage Code to A</description>
        <formula>AND( ISPICKVAL(Status__c, &quot;Received&quot;),( ISPICKVAL(Triage_Code__c , &quot;&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA Triage Code C or D Set Owner to Remorse RMA Review Queue</fullName>
        <actions>
            <name>Update_Owner_to_Remorse_RMA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Triage_Code__c</field>
            <operation>equals</operation>
            <value>D,C</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.GoogleOrderType__c</field>
            <operation>notEqual</operation>
            <value>Advanced Replacement</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Buyer&apos;s Remorse,Warranty Refund</value>
        </criteriaItems>
        <description>Workflow rule that evaluates the RMA Triage Code - if equal to C or D, call a field update that sets the Owner to the Remorse RMA Review Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMA is AR Owner set to Remorse RMA AR Review Queue</fullName>
        <actions>
            <name>Update_Owner_to_Remorse_RMA_AR_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.GoogleOrderType__c</field>
            <operation>equals</operation>
            <value>Advanced Replacement</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>RMA__c.Type__c</field>
            <operation>equals</operation>
            <value>Buyer&apos;s Remorse,Warranty Refund</value>
        </criteriaItems>
        <description>Workflow rule that evaluates the GoogleOrderType__c formula field - if it is equal to Advanced Replacement Order, then set the Owner to the RMA AR Review Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Created By Agent</fullName>
        <active>false</active>
        <criteriaItems>
            <field>RMA__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set RMA Closed Date</fullName>
        <actions>
            <name>Set_RMA_Closed_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set RMA Country</fullName>
        <actions>
            <name>Set_RMA_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set RMA Country field</description>
        <formula>and(or(isnew(), ischanged(GoogleAsset__c)), CreatedBy.Profile.Name != &apos;Robot API&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set RMA Manufacturer</fullName>
        <actions>
            <name>Set_RMA_Manufacturer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set RMA Manufacturer field</description>
        <formula>or(isnew(), ischanged(GoogleAsset__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set RMA Owner to Active</fullName>
        <active>false</active>
        <criteriaItems>
            <field>RMA__c.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Extended Warranty Contract ID</fullName>
        <actions>
            <name>Set_Extended_Warranty_Contract_ID_Backup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RMA__c.Extended_Warranty_Contract_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Chrome_Pending_Return_older_than_2_weeks</fullName>
        <assignedToType>owner</assignedToType>
        <description>Chrome Pending Return older than 2 weeks - 

This RMA is pending return and the RMA is older than 2 weeks Please contact the customer.

http://support.google.com/chromeninjas/answer/2947237?hl=en&amp;ref_topic=2944671</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Chrome Pending Return older than 2 weeks</subject>
    </tasks>
    <tasks>
        <fullName>DOA_21_Day_Notify_Customer_to_Return_Device</fullName>
        <assignedToType>owner</assignedToType>
        <description>DOA: 21 Day Notify Customer to Return Device

Please Notify Customer 

http://support.google.com/chromeninjas/answer/2947275?hl=en&amp;ref_topic=2944671</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>DOA: 21 Day Notify Customer to Return Device</subject>
    </tasks>
    <tasks>
        <fullName>Send_closing_CR</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please send a closing CR to this customer.

http://support.google.com/chromeninjas/answer/2947287?hl=en&amp;ref_topic=2944671</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send closing CR</subject>
    </tasks>
    <tasks>
        <fullName>X28_Day_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <description>DOA: 28 Day Reminder

Please charge customer for this device since the DOA device has not been returned.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>28 Day Reminder - Please Charge Customer for non-returned DOA</subject>
    </tasks>
</Workflow>
