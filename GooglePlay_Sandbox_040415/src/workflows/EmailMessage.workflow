<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Advance_Case_Status_to_WaitingOnCustomer</fullName>
        <field>Status</field>
        <literalValue>Waiting on Customer Reply</literalValue>
        <name>Advance Case Status to WaitingOnCustomer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Notes_Update</fullName>
        <description>Updates the case notes</description>
        <field>Notes__c</field>
        <formula>&quot;This case has been reopened by the customer based upon a customer email&quot;</formula>
        <name>Case Notes Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Notes_Case_too_old_to_reopen</fullName>
        <field>Notes__c</field>
        <formula>&quot;Case too old to reopen&quot;</formula>
        <name>Notes : Case too old to reopen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReOpens_Case</fullName>
        <description>Sets case to in progress Google support</description>
        <field>Status</field>
        <literalValue>In Progress - Google Support</literalValue>
        <name>ReOpens Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Notes</fullName>
        <field>Notes__c</field>
        <name>Update Case Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_REsponse</fullName>
        <description>Checks field if customer is waiting on a response and check customer replied to true</description>
        <field>Customer_Replied__c</field>
        <literalValue>1</literalValue>
        <name>Update Customer REsponse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Waiting_on_Customer_In_Progress</fullName>
        <description>Advances the case status to In progress - Google Support</description>
        <field>Status</field>
        <literalValue>In Progress - Google Support</literalValue>
        <name>Waiting on Customer - In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case is too old to reopen</fullName>
        <actions>
            <name>Notes_Case_too_old_to_reopen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ClosedDate</field>
            <operation>lessThan</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <description>Users shouldn’t be able to re-open their cases after 7 days - Restrict edits to Supervisors/Ops profiles
Criteria:
a) Before 7 days: if customer replies, case should re-open in CSR queue
b) After 7 days: if customer replies, case should remain closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Re-open case if customer emails</fullName>
        <actions>
            <name>Case_Notes_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ReOpens_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ClosedDate</field>
            <operation>equals</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Duplicate</value>
        </criteriaItems>
        <description>Sets case to In Process if a customer emails after a case is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waiting on Customer - CSR emails</fullName>
        <actions>
            <name>Advance_Case_Status_to_WaitingOnCustomer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>,In Progress - Google Support,Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Updates the status to waiting on customer reply after a CSR responds for the first time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waiting on Customer - Email Replies</fullName>
        <actions>
            <name>Update_Customer_REsponse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Waiting_on_Customer_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Customer Action,Waiting on Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.FromName</field>
            <operation>notContain</operation>
            <value>support</value>
        </criteriaItems>
        <description>When a customer replies - update case status to In Progress Google Support</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
