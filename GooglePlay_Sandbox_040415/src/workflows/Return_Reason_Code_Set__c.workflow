<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Reason_Code_Set_External_ID</fullName>
        <field>Return_Reason_Code_Set_External_ID__c</field>
        <formula>Name</formula>
        <name>Update Reason Code Set External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Reason Code Set External ID</fullName>
        <actions>
            <name>Update_Reason_Code_Set_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Return_Reason_Code_Set__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
