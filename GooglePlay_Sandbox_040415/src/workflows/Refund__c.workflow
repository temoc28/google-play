<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Refund_type_to_Total_Order</fullName>
        <field>RecordTypeId</field>
        <lookupValue>TotalOrderRefund</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Refund type to Total Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Pending</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Change status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Line_item_Error_Code_if_line_s_0</fullName>
        <description>This field up will set the Failure Error to No line item specified if the Line item number equals 0.</description>
        <field>FailureError__c</field>
        <formula>&quot;No line item specified&quot;</formula>
        <name>Line-item Error Code if line # =&apos;s 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Line_item_Refund_Status_if_line_s_0</fullName>
        <description>This field update will update the Refund Status to Error if the Line Item equals several on a Line-Item Refund.</description>
        <field>RefundStatus__c</field>
        <literalValue>Error</literalValue>
        <name>Line-item Refund Status if line # =&apos;s 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_as_manual_retry</fullName>
        <description>Mark order as manual retry to prevent retry/error loop.</description>
        <field>ManualRetry__c</field>
        <literalValue>1</literalValue>
        <name>Mark as manual retry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_to_Pending</fullName>
        <description>Change refund status back to pending to retry order.</description>
        <field>RefundStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Reset to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Manual_Retry_to_True</fullName>
        <field>ManualRetry__c</field>
        <literalValue>1</literalValue>
        <name>Set Manual Retry to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Refund_Status_to_Fail</fullName>
        <description>Set&apos;s status to fail</description>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set Refund Status to Fail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Refund_Status_to_Failed_Closed</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set Refund Status to Failed - Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Refund_Status_to_Failure</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set Refund Status to Failure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Fail</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set Status to Fail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Failure</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set Status to Failure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Tax_Refund_to_0</fullName>
        <field>TaxRefundAmount__c</field>
        <formula>0</formula>
        <name>Set Tax Refund to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Failure_Closed</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Set to Failure Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sets_to_Pending</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Sets to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_Refund_Date_to_TODAY_1</fullName>
        <description>Field update that sets the Expected Refund Date to TODAY()+1 when the RecordType.Name = Specific Amount Refund.</description>
        <field>ExpectedRefundDate__c</field>
        <formula>TODAY()+1</formula>
        <name>Update Expected Refund Date to TODAY()+1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Failure_Reason</fullName>
        <description>Update error with &quot;Cannot refund AR orders&quot;</description>
        <field>FailureError__c</field>
        <formula>&quot;Can&apos;t refund AR orders&quot;</formula>
        <name>Update Failure Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Item_Refund_to_0</fullName>
        <field>ItemRefundAmount__c</field>
        <formula>0</formula>
        <name>Update Item Refund to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Manual_Refund_Review_Esc</fullName>
        <description>Field update that sets the Refund Owner to the Manual Refund Review Escalation</description>
        <field>OwnerId</field>
        <lookupValue>Manual_Refund_Review_Escalation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Manual Refund Review Esc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Refund_Owner_to_Manual_Refund_Que</fullName>
        <description>Field update that sets the Refund Owner to the Manual Refund Review Queue if the conditions of the workflow rule are met</description>
        <field>OwnerId</field>
        <lookupValue>Manual_Refund_Review</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Refund Owner to Manual Refund Que</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Refund_Status_to_Failure_Closed</fullName>
        <description>Field update that is called when the Failure Error on the Refund object = Already Refunded and the Refund Status not equal to Failure - Closed set the RefundStatus__c to Failure -Closed</description>
        <field>RefundStatus__c</field>
        <literalValue>Failure - Closed</literalValue>
        <name>Update Refund Status to Failure - Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Refund_Status_to_Pending</fullName>
        <description>Field Update that sets the RefundStatus__c = Pending when the RefundStatus__c was Failure - Closed and the Failure_Error__c = &quot;Order Not Charged&quot;.</description>
        <field>RefundStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Refund Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Success</fullName>
        <field>RefundStatus__c</field>
        <literalValue>Success</literalValue>
        <name>Update Status to Success</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Success_Date</fullName>
        <field>Successful_Refund_Date__c</field>
        <formula>today()</formula>
        <name>Update Success Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto retry failed charges</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Total Order Charge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.ManualRetry__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If the status of a charge is Error and Manual Retry = False, set manual retry to True and change status to Pending after 5 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_status_to_Pending</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Manual_Retry_to_True</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Auto-Fail AMOUNT_TOO_LARGE for Total Order Refunds</fullName>
        <actions>
            <name>Set_Status_to_Failure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Total Order Refund</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>AMOUNT_TOO_LARGE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.ManualRetry__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If a Total Order Refund fails with &quot;AMOUNT_TOO_LARGE&quot; it&apos;s because the order has already been completely refunded. Auto-Fail these.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto-Fail Advanced Replacement Full Order Refunds</fullName>
        <actions>
            <name>Set_Status_to_Fail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Failure_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Refund__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Total Order Refund</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>Advanced Replacement</value>
        </criteriaItems>
        <description>If type = Total Order Refund &amp; order type = advanced replacement, automatically fail.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Auto-retry AMOUNT_TOO_LARGE errors once</fullName>
        <actions>
            <name>Mark_as_manual_retry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_to_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>AMOUNT_TOO_LARGE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.ManualRetry__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If a refund is in an error state with reason AMOUNT_TOO_LARGE, and the order hasn&apos;t been manually retried yet, set to pending and mark manual retry as true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Aready Cancelled%2C Set to Failure Close</fullName>
        <actions>
            <name>Set_Refund_Status_to_Failure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>ORDER_ALREADY_CANCELLED</value>
        </criteriaItems>
        <description>If the status of an refund is &quot;Error&quot; and the Error Code is &quot;ORDER_ALREADY_CANCELLED&quot;, then change status to &quot;Failure - Closed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Chargeback%2C Set to Failure Close</fullName>
        <actions>
            <name>Set_Refund_Status_to_Fail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>ORDER_HAS_CHARGEBACK</value>
        </criteriaItems>
        <description>If the status of an refund is &quot;Error&quot; and the Error Code is &quot;ORDER_HAS_CHARGEBACK&quot;, then change status to &quot;Failure - Closed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Not Charged%2C Set to Failure Close</fullName>
        <actions>
            <name>Set_Refund_Status_to_Failed_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>ORDER_NOT_CHARGED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>Advanced Replacement</value>
        </criteriaItems>
        <description>If the status of an refund is &quot;Error&quot; and the Error Code is &quot;ORDER_NOT_CHARGED&quot; and the order type is &quot;Advanced Replacement&quot;, then change status to &quot;Failure - Closed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Refund is 0%2C Set to Failure Close</fullName>
        <actions>
            <name>Set_to_Failure_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.FailureError__c</field>
            <operation>equals</operation>
            <value>REFUND_AMOUNT_ZERO</value>
        </criteriaItems>
        <description>If the status of an refund is &quot;Error&quot; and the Error Code is &quot;REFUND_AMOUNT_ZERO&quot;, then change status to &quot;Failure - Closed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Status is Blank%2C set to Pending</fullName>
        <actions>
            <name>Sets_to_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set&apos;s status to pending if blank</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Line-item Refund with no Line Number</fullName>
        <actions>
            <name>Line_item_Error_Code_if_line_s_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Line_item_Refund_Status_if_line_s_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Line-Item Refund</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.LineNumber__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>This workflow will update the Refund status to Error and the Failure Error to No line item specified if the line item number field equals 0.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nova Refunds should be total_order_refunds</fullName>
        <actions>
            <name>Change_Refund_type_to_Total_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a Line-Item Refund is created against an RMA for a Nova SKU, then change the refund record type to Total Order Refund</description>
        <formula>Type__c = &apos;Line-Item Refund&apos; &amp;&amp;  ( RMA__r.SKU__c = &apos;00639NAGPENO&apos; || RMA__r.SKU__c = &apos;00638NAGPENO&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Refund Failure Error Already Refunded Set Status to Failure - Closed</fullName>
        <actions>
            <name>Update_Refund_Status_to_Failure_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow rule that evaluates the Failure Error on the Refund object - if it equals Already Refunded, call field update &quot;Update Refund Status to Failure - Closed&quot;</description>
        <formula>AND (  FailureError__c = &quot;Already Refunded&quot;, NOT(ISPICKVAL(RefundStatus__c, &quot;Failure - Closed&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refund RT is Specific Amount Refunded set Expected Date to TODAY%2B1</fullName>
        <actions>
            <name>Update_Expected_Refund_Date_to_TODAY_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule that evaluates the Refund Record Type - if the record type = Specific Amount Refund, then calls a field update to set the Expected Refund Date to TODAY()+1</description>
        <formula>RecordType.Name = &quot;Specific Amount Refund&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Refund Status Error changed to Pending Based on Failure Error</fullName>
        <actions>
            <name>Update_Refund_Status_to_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Refund Status = Error and the Failure Error = Order Not Charged, the workflow rule calls a field update to set the Refund Status = Pending</description>
        <formula>AND(ISPICKVAL( RefundStatus__c , &quot;Error&quot;), FailureError__c = &quot;Order Not Charged&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refund Status Error set Refund Owner to Manual Refund Queue</fullName>
        <actions>
            <name>Update_Refund_Owner_to_Manual_Refund_Que</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Refund Status = Error and the Failure Error &lt;&gt; Order Not Charged, the workflow rule calls a field update to set the Refund Owner to Manual Refund Queue</description>
        <formula>AND( ISPICKVAL( RefundStatus__c,  &quot;Error&quot;), FailureError__c &lt;&gt; &quot;Order Not Charged&quot;,  ManualRetry__c &lt;&gt; True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refund Status is Error and Manual Retry is True set Owner to Manual Refund Review Escalation Queue</fullName>
        <actions>
            <name>Update_Owner_to_Manual_Refund_Review_Esc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.ManualRetry__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow rule that check if the Refund Status = Error and the Manual Retry checkbox is checked, then calls a field update to change the Refund Owner to the Manual Refund Review Escalation queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refund Status is Pending and Expected Due Date is Past Due Move to Manual Refund Review Queue</fullName>
        <actions>
            <name>Update_Refund_Owner_to_Manual_Refund_Que</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.ExpectedRefundDate__c</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Refund__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>Manual Refund Review Escalation</value>
        </criteriaItems>
        <description>Workflow rule that determines if the Refund Status = Pending and the Expected Due Date is past due, calls a field update to set the Owner to the Manual Refund Review Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Successful Refund Date</fullName>
        <actions>
            <name>Update_Success_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Refund__c.RefundStatus__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <description>When the status of a refund is set to Success, fill in the Successful Refund Date with Today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Shipping Refunds for Countries with Free Shipping</fullName>
        <actions>
            <name>Set_Tax_Refund_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Item_Refund_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Success</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If country has free shipping, auto succeed shipping refunds and mark as 0 refund.</description>
        <formula>and( Type__c = &apos;Shipping Refund&apos;,  or(ISPICKVAL(OrderNumber__r.Country__c, &apos;JP&apos;), ISPICKVAL(OrderNumber__r.Country__c, &apos;IN&apos;), ISPICKVAL(OrderNumber__r.Country__c, &apos;KR&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
