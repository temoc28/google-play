<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculate_DOA_Period</fullName>
        <field>DOA_Period__c</field>
        <formula>case( Sale_Country__c, &apos;US&apos;,  ProductID__r.DOA_Period_US__c , &apos;UK&apos;, ProductID__r.DOA_Period_UK__c , &apos;CA&apos;, ProductID__r.DOA_Period_CA__c , &apos;AU&apos;, ProductID__r.DOA_Period_AU__c , &apos;FR&apos;, ProductID__r.DOA_Period_FR__c , &apos;DE&apos;, ProductID__r.DOA_Period_DE__c , &apos;ES&apos;, ProductID__r.DOA_Period_ES__c , &apos;IT&apos;, ProductID__r.DOA_Period_IT__c , &apos;JP&apos;, ProductID__r.DOA_Period_JP__c , &apos;KR&apos;, ProductID__r.DOA_Period_KR__c , &apos;IN&apos;, ProductID__r.DOA_Period_IN__c , &apos;HK&apos;, ProductID__r.DOA_Period_HK__c , Null)</formula>
        <name>Calculate DOA Period</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calculate_Remorse_Period</fullName>
        <field>Remorse_Period__c</field>
        <formula>case( Sale_Country__c, &apos;US&apos;,  ProductID__r.Remorse_Period_US__c , &apos;UK&apos;, ProductID__r.Remorse_Period_UK__c , &apos;CA&apos;, ProductID__r.Remorse_Period_CA__c , &apos;AU&apos;, ProductID__r.Remorse_Period_AU__c , &apos;FR&apos;, ProductID__r.Remorse_Period_FR__c , &apos;DE&apos;, ProductID__r.Remorse_Period_DE__c , &apos;ES&apos;, ProductID__r.Remorse_Period_ES__c , &apos;IT&apos;, ProductID__r.Remorse_Period_IT__c , &apos;JP&apos;, ProductID__r.Remorse_Period_JP__c , &apos;KR&apos;, ProductID__r.Remorse_Period_KR__c , &apos;IN&apos;, ProductID__r.Remorse_Period_IN__c , &apos;HK&apos;, ProductID__r.Remorse_Period_HK__c , Null)</formula>
        <name>Calculate Remorse Period</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calculate_Warranty_Period</fullName>
        <description>Calculates Warranty Period</description>
        <field>Warranty_Period__c</field>
        <formula>case( Sale_Country__c, &apos;US&apos;, ProductID__r.Warranty_Period_US__c , &apos;UK&apos;, ProductID__r.Warranty_Period_UK__c , &apos;CA&apos;, ProductID__r.Warranty_Period_CA__c , &apos;AU&apos;, ProductID__r.Warranty_Period_AU__c , &apos;FR&apos;, ProductID__r.Warranty_Period_FR__c , &apos;DE&apos;, ProductID__r.Warranty_Period_DE__c , &apos;ES&apos;, ProductID__r.Warranty_Period_ES__c , &apos;IT&apos;, ProductID__r.Warranty_Period_IT__c , &apos;JP&apos;, ProductID__r.Warranty_Period_JP__c , &apos;KR&apos;, ProductID__r.Warranty_Period_KR__c , &apos;IN&apos;, ProductID__r.Warranty_Period_IN__c , &apos;HK&apos;, ProductID__r.Warranty_Period_HK__c , Null)</formula>
        <name>Calculate Warranty Period</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_SKU_to_US_N7_8GB</fullName>
        <description>Changes SKU field to US N7 8GB</description>
        <field>SKU__c</field>
        <formula>&apos;90OK0MW1100190U&apos;</formula>
        <name>Change SKU to US N7 8GB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_record_type_to_Google_Play_Asset</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PlayAsset</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type to Google Play Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_record_type_to_Retail_Asset</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Retail_Asset</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type to Retail Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_record_type_to_Retail_Asset_2</fullName>
        <description>duplicate of workflow rule #1</description>
        <field>RecordTypeId</field>
        <lookupValue>Retail_Asset</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type to Retail Asset #2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SKU_FREE_Bumper_White</fullName>
        <field>SKU__c</field>
        <formula>&quot;CCH-190.ACUSWH-FREE&quot;</formula>
        <name>SKU FREE Bumper White</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Retail_Country</fullName>
        <field>Retail_Country__c</field>
        <literalValue>Set by SKU</literalValue>
        <name>Set Retail Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SKU_to_Upserted_SKU</fullName>
        <field>SKU__c</field>
        <formula>if(right(SKUUpsert__c, 2) = &apos;-R&apos;, SKUUpsert__c + &apos;A&apos;, SKUUpsert__c)</formula>
        <name>Set SKU to Upserted SKU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SKU_to_Black_Bumper_FREE</fullName>
        <field>SKU__c</field>
        <formula>&quot;CCH-190.ACUSBK-FREE&quot;</formula>
        <name>Update SKU to Black Bumper FREE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allow Upserted Refurbs to Set SKU</fullName>
        <actions>
            <name>Set_SKU_to_Upserted_SKU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If an Upserted SKU ends in -R, set the SKU to the value of the upserted SKU.</description>
        <formula>or(right(SKUUpsert__c, 2) = &apos;-R&apos;, right(SKUUpsert__c, 3) = &apos;-RA&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto set Play Store assets</fullName>
        <actions>
            <name>Change_record_type_to_Google_Play_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If an asset is associated with a product with Retail Channel = &quot;Play Only&quot;, change the record type of the asset to &quot;Google Play Asset&quot;</description>
        <formula>ispickval(ProductID__r.Retail_Channel__c, &apos;Play Only&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automatically set retail country for certain SKUs</fullName>
        <actions>
            <name>Set_Retail_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>and(not(ispickval(ProductID__r.Sale_Country__c, &apos;&apos;)), not(ispickval(ProductID__r.Sale_Country__c, &apos;n/a&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Calculate Entitlement Periods</fullName>
        <actions>
            <name>Calculate_DOA_Period</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calculate_Remorse_Period</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calculate_Warranty_Period</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Google_Asset__c.Sale_Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Order_ID_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Retail_Sale_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Amazon or Best Buy change to Retail Asset</fullName>
        <actions>
            <name>Change_record_type_to_Retail_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17 OR 18 OR 19 OR 20 OR 21 OR 22 OR 23 OR 24 OR 25</booleanFilter>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Amazon.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Best Buy Purchasing LLC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Walmart.com USA LLC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>SK Networks Co. Ltd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Staples Inc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Synnex Corporation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Amazon EU SARL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>DSG Retail Ltd.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data Service GMBH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data France</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Synnex Australia Pty Ltd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data Limited</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data Espana</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data Italia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Best Buy Canada Ltd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>ASBISC ENTERPRISES PLC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Groupon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>TROP COMERCIO EXTERIOR LTDA.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Amazon.com.ca Inc.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data Espana,S.L.U</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>TD Tech Data AB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Amazon.com,LLC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>SYNNEX Infotec Corporation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Black Bumper SKU%2C change to FREE Black Bumper SKU</fullName>
        <actions>
            <name>Update_SKU_to_Black_Bumper_FREE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Google_Asset__c.SKU__c</field>
            <operation>equals</operation>
            <value>CCH-190.ACUSBK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Sale_Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>If Black Bumper SKU, change to FREE Black Bumper SKU</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>If Retailer Name is change to Retail Asset Part 2</fullName>
        <actions>
            <name>Change_record_type_to_Retail_Asset_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Tech Data BVBA/SPRL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>EPIX/Studio3 Partners LLC</value>
        </criteriaItems>
        <description>Ran out of lines for workflow rules part 1, adding a duplicate to account for more rules</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If White Bumper SKU%2C change to FREE White Bumper SKU</fullName>
        <actions>
            <name>SKU_FREE_Bumper_White</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Google_Asset__c.SKU__c</field>
            <operation>equals</operation>
            <value>CCH-190.ACUSWH</value>
        </criteriaItems>
        <description>If White Bumper SKU, change to FREE White Bumper SKU</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Play Store to Google Play</fullName>
        <actions>
            <name>Change_record_type_to_Google_Play_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Play Store</value>
        </criteriaItems>
        <criteriaItems>
            <field>Google_Asset__c.Original_Retail_Store_Name__c</field>
            <operation>equals</operation>
            <value>Play</value>
        </criteriaItems>
        <description>Changing Retail Channel &quot;Play Store&quot; to &quot;Google Play&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Re-SKU CA%2FFR%2FDE N7 8GB</fullName>
        <actions>
            <name>Change_SKU_to_US_N7_8GB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a CA/FR/DE N7 8GB is sold to a US customer, re-SKU as US.</description>
        <formula>and( or(SKU__c = &apos;90OK0MW1100200U&apos;, SKU__c =&apos;90OK0MW1100250U&apos; , SKU__c =&apos;90OK0MW1100270U&apos; ),   ispickval(Order_Opportunity__r.Country__c , &apos;US&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
