<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_External_ID_Field</fullName>
        <field>Return_Reason_Code_External_ID__c</field>
        <formula>Return_Reason_Code_Set__r.Name +  Name</formula>
        <name>Update Reason Code External ID Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Reason Code External ID</fullName>
        <actions>
            <name>Update_External_ID_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Return_Reason_Code__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
