<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Shipping_Method_External_ID</fullName>
        <field>Shipping_Label_Method_Rule_External_ID__c</field>
        <formula>Return_Center__r.Shipping_Label_Service_Code__c +  text(Action__c) + text( RMA_Method__c )</formula>
        <name>Update Shipping Method External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Shipping Label Method Rule External ID</fullName>
        <actions>
            <name>Update_Shipping_Method_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Shipping_Label_Method_Rule__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
