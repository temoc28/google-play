<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_Close_Case_Email</fullName>
        <description>Auto Close Case Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Case_Auto_Close</template>
    </alerts>
    <alerts>
        <fullName>Closed_due_to_inactivity</fullName>
        <description>Closed due to inactivity</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Case_Closed_due_to_inactivity</template>
    </alerts>
    <alerts>
        <fullName>Emails_customer_to_reopen_Case</fullName>
        <description>Emails customer to reopen Case</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Cannot_Re_open_case</template>
    </alerts>
    <alerts>
        <fullName>Send_EN_Phone_Survey_WEB_BASED_to_Account_email</fullName>
        <description>Send EN Phone Survey - WEB-BASED to Account email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Support_CSAT_Survey_Web_Form</template>
    </alerts>
    <alerts>
        <fullName>Send_EN_Survey_WEB_BASED</fullName>
        <description>Send EN Survey - WEB-BASED</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Support_CSAT_Survey_Web_Form</template>
    </alerts>
    <alerts>
        <fullName>Send_Warm_Welcome_WEB_BASED</fullName>
        <description>Send Warm Welcome- WEB-BASED</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Warm_Welcome_CSAT_New</template>
    </alerts>
    <alerts>
        <fullName>Send_survey</fullName>
        <description>Send survey</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>chrome-ninja-csr@google.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Chromebook_Ninjas/Support_CSAT_Survey_Web_Form</template>
    </alerts>
    <fieldUpdates>
        <fullName>Advance_Status_to_Assigned</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Advance Status to Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assigned_Status</fullName>
        <description>If a Case owner changes, update the status to assigned</description>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Assigned Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Reason_Drive_Offer</fullName>
        <description>Update the case reason for drive offer to User Education</description>
        <field>Reason</field>
        <literalValue>User Education</literalValue>
        <name>Drive Offer Case Reason Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Reason_Transfer_to_Play</fullName>
        <field>Reason</field>
        <literalValue>Transferred to Play</literalValue>
        <name>Case Reason Transfer to Play</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_hardware_vendor_to_CR_48</fullName>
        <field>Hardware_Vendor__c</field>
        <literalValue>CR-48</literalValue>
        <name>Change hardware vendor to CR-48</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_hardware_vendor_to_Samsung</fullName>
        <field>Hardware_Vendor__c</field>
        <literalValue>Samsung</literalValue>
        <name>Change hardware vendor to Samsung</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_owner_Test_Case</fullName>
        <field>OwnerId</field>
        <lookupValue>levadmin@googleplay.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change owner Test Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Send_Survey</fullName>
        <field>Send_Customer_Feedback_Survey__c</field>
        <literalValue>1</literalValue>
        <name>Check Send Survey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Send_Survey_Box</fullName>
        <description>Checks the send a survey box</description>
        <field>Send_Customer_Feedback_Survey__c</field>
        <literalValue>1</literalValue>
        <name>Check Send Survey Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_the_PyICS_link_updated_field</fullName>
        <field>PyICS_Link_Added__c</field>
        <literalValue>1</literalValue>
        <name>Check the PyICS link added field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Component_transfer_to_Play</fullName>
        <field>Component__c</field>
        <literalValue>Transfer to Play</literalValue>
        <name>Component transfer to Play</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Drive_Offer</fullName>
        <field>Subject</field>
        <formula>&quot;Drive Offer&quot;</formula>
        <name>Drive Offer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Drive_Offer_Update_Status</fullName>
        <description>Updates the Case Status to Waiting on Customer if Case reason = drive offer</description>
        <field>Status</field>
        <literalValue>Waiting on Customer Reply</literalValue>
        <name>Drive Offer Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Case_Reason_No_Reponse</fullName>
        <field>Reason</field>
        <literalValue>No Response</literalValue>
        <name>Make Case Reason No Reponse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manufacturer_is_Acer</fullName>
        <description>Update hardware vendor to Acer</description>
        <field>Hardware_Vendor__c</field>
        <literalValue>Acer</literalValue>
        <name>Hardware Vendor is Acer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Case_Duplicate</fullName>
        <field>Reason</field>
        <literalValue>Duplicate</literalValue>
        <name>Mark Case Duplicate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PyICS_Date_Stamp</fullName>
        <description>Stamps the Date when a PyICS link is created</description>
        <field>PyICS_Link_added_date__c</field>
        <formula>NOW()</formula>
        <name>PyICS Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Subcomponent_Play_transfer</fullName>
        <field>Subcomponent__c</field>
        <literalValue>Transfer to Play</literalValue>
        <name>Subcomponent Play transfer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp_Customer_Approval_Date</fullName>
        <description>Timestamps the date customer approved payment for RMA</description>
        <field>Customer_Payment_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Timestamp Customer Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Call_Status_to_Schedule</fullName>
        <field>Call_Status__c</field>
        <literalValue>Scheduled</literalValue>
        <name>Update Call Status to Schedule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Assigned</fullName>
        <description>When a case becomes owned by the role Chrome Agent, Update the status to assigned</description>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Update Case Status to Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Checkbox_Closed_Permanently</fullName>
        <description>Case is Closed more than a week than check the box Case is Closed more than a week</description>
        <field>Case_is_Closed_more_than_a_week__c</field>
        <literalValue>1</literalValue>
        <name>Update Checkbox Closed Permanently</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Chrome_Case_Type</fullName>
        <field>Chrome_Case_Type__c</field>
        <literalValue>Drive</literalValue>
        <name>Update Chrome Case Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Component_to_Warm_elcome</fullName>
        <description>Updates Component to Warm Welcome</description>
        <field>Component__c</field>
        <literalValue>Warm Welcome</literalValue>
        <name>Update Component to Warm elcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Component_to_webpage_display_issu</fullName>
        <description>Updates Drive offers component to webpage display issues</description>
        <field>Component__c</field>
        <literalValue>Webpage display issues</literalValue>
        <name>Update Component to webpage display issu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Notes</fullName>
        <field>Notes__c</field>
        <formula>&quot;Enterprise Case&quot;</formula>
        <name>Update Notes Enterprise Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Notes_Auto_Closed</fullName>
        <field>Notes__c</field>
        <formula>&quot;This case was auto closed because it was opened via email&quot;</formula>
        <name>Update Notes Auto Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Notes_Closed_Inactivity</fullName>
        <description>Workflow should: 
c) update case notes

Closed due to inactivity</description>
        <field>Notes__c</field>
        <formula>&quot;Closed due to inactivity&quot;</formula>
        <name>Update Notes Closed Inactivity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Notes_field_to_Drive</fullName>
        <field>Notes__c</field>
        <formula>&quot;Drive Offer&quot;</formula>
        <name>Update Notes field to Drive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_on_CSr_Response</fullName>
        <description>Updates the Case Status Field when a CSR emails</description>
        <field>Status</field>
        <literalValue>Waiting on Customer Reply</literalValue>
        <name>Update Status on CSR Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subcomonent_to_ECHO_Offer</fullName>
        <description>Updates the Subcomponent to ECHO/Offers for all Drive Offer issues</description>
        <field>Subcomponent__c</field>
        <literalValue>ECHO/Offers</literalValue>
        <name>Update Subcomonent to ECHO/Offer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subcomponent_Warm_Welcome</fullName>
        <description>Updates Subcomponent</description>
        <field>Subcomponent__c</field>
        <literalValue>Get to Know Your Chromebook</literalValue>
        <name>Update Subcomponent Warm Welcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Sent</fullName>
        <description>Updates the &quot;Survey Sent&quot; Case field to indicate that the Customer Feedback Survey has been sent.</description>
        <field>Survey_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update Survey Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_WW_Chrome_Case_Type</fullName>
        <description>Updates chrome case type to warm welcome if subject contains</description>
        <field>Chrome_Case_Type__c</field>
        <literalValue>Warm Welcome</literalValue>
        <name>Update WW Chrome Case Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto Close Email Cases</fullName>
        <actions>
            <name>Auto_Close_Case_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mark_Case_Duplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Notes_Auto_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Age Old send email</fullName>
        <actions>
            <name>Emails_customer_to_reopen_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Notes__c</field>
            <operation>equals</operation>
            <value>Case too old to reopen</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check Case Too Old</fullName>
        <actions>
            <name>Update_Checkbox_Closed_Permanently</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ClosedDate</field>
            <operation>lessThan</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check Send Survey</fullName>
        <actions>
            <name>Check_Send_Survey_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Feedback_Survey__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chrome DOA Order Reminder - Replacement</fullName>
        <actions>
            <name>DOA_remind_customer_to_order_a_device</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PyICS_Link__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Google RMA</value>
        </criteriaItems>
        <description>When PyICS created date is two days old, reminds ninja via to remind customer to order device</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Payment Approval</fullName>
        <actions>
            <name>Timestamp_Customer_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Customer_Payment_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Customer Payment Approval date when customer approves RMA payment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Drive Offer No Response</fullName>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Case_Reason_No_Reponse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Drive Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Modified_Date__c</field>
            <operation>lessThan</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <description>For Drive Offer Cases that are waiting in customer reply for more than 7 days, mark the case closed and Case Reason to no response..</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise Case</fullName>
        <actions>
            <name>Mark_Case_Duplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager Case Assignment Status update to Assigned</fullName>
        <actions>
            <name>Advance_Status_to_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Case is Assigned out of Queue to an individual CSR the Status is advanced to &apos;Ássigned&apos;.</description>
        <formula>AND(
         RecordTypeId = &quot;012U0000000ZK0s&quot;,
         ISPICKVAL(Status,&quot;New&quot;),
         OwnerId  &lt;&gt; &quot;00GU0000001JIp0&quot;
         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PyICS Checkbox</fullName>
        <actions>
            <name>Check_the_PyICS_link_updated_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.PyICS_Link__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Checks the box for PyICS link added</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PyICS Date Stamp</fullName>
        <actions>
            <name>PyICS_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.PyICS_Link_Added__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>dds a date when PyICS is updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Phone Web-Based CSAT Survey %28EN%29</fullName>
        <actions>
            <name>Send_EN_Phone_Survey_WEB_BASED_to_Account_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Feedback_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Component__c</field>
            <operation>notContain</operation>
            <value>Welcome</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notContain</operation>
            <value>enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>No Response,Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Phone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Survey_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends the web-based Customer Feedback Survey (English) when a Case is Closed and the send customer feedback field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Warm Welcome CSAT Survey %28EN%29</fullName>
        <actions>
            <name>Send_Warm_Welcome_WEB_BASED</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Feedback_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Component__c</field>
            <operation>contains</operation>
            <value>Welcome</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Legacy_Case_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Call_Status__c</field>
            <operation>notEqual</operation>
            <value>Cancelled,No answer</value>
        </criteriaItems>
        <description>Sends the WW Feedback Survey (English) when a Case is Closed and the send customer feedback field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Web-Based CSAT Survey %28EN%29</fullName>
        <actions>
            <name>Send_EN_Survey_WEB_BASED</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Feedback_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Component__c</field>
            <operation>notContain</operation>
            <value>Welcome</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notContain</operation>
            <value>enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>No Response,Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Survey_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends the web-based Customer Feedback Survey (English) when a Case is Closed and the send customer feedback field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test Cases - Out Of queue</fullName>
        <actions>
            <name>Change_owner_Test_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mark_Case_Duplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Test_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Changes owner to Chrombebooks Admin if Test is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test Check Update Call Status Needed</fullName>
        <actions>
            <name>Update_Call_Status_to_Schedule</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.APID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule updates the Call Status to Scheduled upon creation of a warm welcome Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Timestamp PyICS Creation Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.PyICS_Link_Added__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Transfer to Play</fullName>
        <actions>
            <name>Case_Reason_Transfer_to_Play</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Component_transfer_to_Play</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Subcomponent_Play_transfer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>Transfer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Status Drive Offer</fullName>
        <actions>
            <name>Case_Reason_Drive_Offer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Drive_Offer_Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Drive Offer</value>
        </criteriaItems>
        <description>This rule updates drive offer cases to waiting on customer reply after creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Drive Offer Chrome Type</fullName>
        <actions>
            <name>Update_Chrome_Case_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Component_to_webpage_display_issu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subcomonent_to_ECHO_Offer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Drive Offer</value>
        </criteriaItems>
        <description>Updates the Chrome Case Type to Drive Offer if the Case reason is drive offer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Manufacturer - Acer</fullName>
        <actions>
            <name>Manufacturer_is_Acer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Model_Name__c</field>
            <operation>contains</operation>
            <value>Acer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Manufacturer - CR-48</fullName>
        <actions>
            <name>Change_hardware_vendor_to_CR_48</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Model_Name__c</field>
            <operation>contains</operation>
            <value>Cr-48</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Manufacturer - Samsung</fullName>
        <actions>
            <name>Change_hardware_vendor_to_Samsung</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Model_Name__c</field>
            <operation>contains</operation>
            <value>Samsung</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status for Drive Offer</fullName>
        <actions>
            <name>Update_Notes_field_to_Drive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>contains</operation>
            <value>Drive Offer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Warm Welcome Chrome Type</fullName>
        <actions>
            <name>Update_WW_Chrome_Case_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Welcome Call Requested</value>
        </criteriaItems>
        <description>Updates the Chrome Case Type to Warm Welcome if the Case subject contains Welcome Call Requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waiting on Customer more than 5</fullName>
        <actions>
            <name>Closed_due_to_inactivity</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Case_Reason_No_Reponse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Notes_Closed_Inactivity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Modified_Date__c</field>
            <operation>lessThan</operation>
            <value>LAST 5 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Customer Action,Waiting on Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Drive Offer</value>
        </criteriaItems>
        <description>Criteria: status = “waiting on customer” for 5 days

Workflow should: 
a) send closing email
b) mark case as closed
c) update case notes

Update case notes:
Closed due to inactivity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Call Update Component-Sub</fullName>
        <actions>
            <name>Update_Component_to_Warm_elcome</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subcomponent_Warm_Welcome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ChromeOS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Welcome Call Requested</value>
        </criteriaItems>
        <description>If the subject contains welcome call, update component and subcomponent</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Reopened</fullName>
        <assignedToType>owner</assignedToType>
        <description>Case has been Reopened</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Reopened</subject>
    </tasks>
    <tasks>
        <fullName>DOA_remind_customer_to_order_a_device</fullName>
        <assignedToType>owner</assignedToType>
        <description>The PyICS link was added 2 days ago. Please remind customer to order device.

http://support.google.com/chromeninjas/answer/2947274?hl=en&amp;ref_topic=2944671</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.PyICS_Link_added_date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>DOA remind customer to order a device</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_on_this_case</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a follow up task on a case that is created for the owner when an email is sent as a reply</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow Up Required</subject>
    </tasks>
    <tasks>
        <fullName>X3_Day_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Follow Up Task after a case has not been modified for 3 days</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>3 Day Follow Up</subject>
    </tasks>
</Workflow>
