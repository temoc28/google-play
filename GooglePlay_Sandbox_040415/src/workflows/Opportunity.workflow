<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Country_to_GB</fullName>
        <field>Country__c</field>
        <literalValue>GB</literalValue>
        <name>Change Country to GB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Convert UK to GB</fullName>
        <actions>
            <name>Change_Country_to_GB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>UK</value>
        </criteriaItems>
        <description>Convert country code UK to GB</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
