trigger Product2_Object_Trigger on Product2 (after insert, after update) 
{
	if(trigger.isAfter)
	{
		if(trigger.isInsert)
			Product2TrgHandler.onAfterInsert(trigger.new);
		else if(trigger.isUpdate)
			Product2TrgHandler.onAfterUpdate(trigger.new, trigger.oldMap);
	}
}