trigger CT_RMAAfterInsertUpdate on RMA__c (after update)
{
    Set<Id> SRMAIds = new Set<Id>();    
    Map<Id, RecordType> mChromeBookRTs = new Map<Id, RecordType>([SELECT Id, Name FROM RecordType WHERE sObjectType = 'RMA__c' AND isActive = true AND Name LIKE 'Chrome%']);
    
    for(RMA__c rma : trigger.new)
    {   
        if(mChromeBookRTs.ContainsKey(rma.RecordTypeId) && rma.Created_on_Google_Server__c == false)   
            SRMAIds.add(rma.Id);
    }  
    if(SRMAIds.size() > 0)
    	CT_Utility.Process(SRMAIds); 
}