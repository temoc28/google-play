/*
Trigger to ensure random case secret and Rhino secret (numeric) are assigned to every case.
Written by Jon Gauldingv
*/

trigger SetCaseSecret on Case (before update, before insert){
    for (Case c : Trigger.new){
        if (c.Case_Secret__c == null){
          long random_secret = Crypto.getRandomLong();
          random_secret = random_secret/10;
          if (random_secret < 0) {
              random_secret = -random_secret;
          }
          c.Case_Secret__c = random_secret;
        }
    }
}