trigger User_Object_Trigger on User (after insert) 
{
	if(trigger.isAfter)
	{
		if(trigger.isInsert)
			UserTrgHandler.onAfterInsert(trigger.new);
	}
}