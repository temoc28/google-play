trigger Case_Object_Trigger on Case (before insert, before update) 
{    
    if(Trigger.isBefore)
    {
        if(trigger.isInsert)
        {
            UpdateLinksOnCaseTrigger.UpdateValues(trigger.new); //Update the Account on the Case to the Case Email's Account or the Email/Account on the Order
            CaseTriggerHandler.onBeforeInsert(trigger.new);
        }
        else if(trigger.isUpdate)
        	CaseTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
    }
}