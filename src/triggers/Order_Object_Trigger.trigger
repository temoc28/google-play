trigger Order_Object_Trigger on Order__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if(Trigger.isBefore){
		if(Trigger.isInsert){
			CopyOrderIdentifier.UpdateValues(Trigger.new); //Update the External Id with the Google Asset Name (Id)
		}
		if(Trigger.isUpdate){
			CopyOrderIdentifier.UpdateValues(Trigger.new); //Update the External Id with the Google Asset Name (Id)
		}
	}
}