<apex:page showHeader="true" sidebar="true" standardController="RMA__c" title="RMA Edit" extensions="RMAEditController">
    <apex:form id="editForm">
        <apex:sectionHeader title="RMA Edit" subtitle="{!rmaToEdit.Name}"/>
        <apex:outputPanel id="msg"><apex:pageMessages /></apex:outputPanel>
        <apex:pageBlock title="RMA Edit" mode="edit" id="mainBLock">
            <apex:pageBlockButtons >
                <apex:commandButton action="{!SaveRecord}" value="Save" reRender="editForm"/>
                <apex:commandButton action="{!saveNew}" value="Save & New" reRender="editForm"/>
                <apex:commandButton action="{!Cancel}" value="Cancel" reRender="editForm"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection columns="2" title="Information">
                <apex:outputField value="{!rmaToEdit.RecordTypeId}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Service Model" for="service"/>
                    <apex:actionRegion >
                        <apex:outputPanel >
                            <apex:inputField id="service" value="{!rmaToEdit.ServiceModel__c}">
                                <apex:actionSupport event="onchange" action="{!changeModel}" rerender="typ, cat, subCat, msg"/>
                            </apex:inputField>
                        </apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!rmaToEdit.Replacement_Cart_ID__c}"/>
                <apex:inputField value="{!rmaToEdit.Status__c}"/>
                <apex:inputField value="{!rmaToEdit.ReplacementOrder__c}"/>
                <apex:inputField value="{!rmaToEdit.Return_Shipping_Tracking_Number__c}"/>
                <apex:inputField value="{!rmaToEdit.Name}"/>
                <br/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Type"  for="typ"/>
                    <apex:actionRegion >
                        <apex:outputPanel id="typ" layout="block"  styleClass="requiredInput">
                            <apex:selectList value="{!rmaToEdit.Type__c}" required="true" size="1">
                                <apex:outputPanel layout="block" styleClass="requiredBlock">
                                </apex:outputPanel>
                                <apex:selectOptions value="{!optionsType}"/>
                                <apex:actionSupport event="onchange" action="{!getAllOptions}" rerender="typ, cat, subCat, msg"/>
                            </apex:selectList>
                        </apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                <br/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="RMA Category"  for="cat"/>
                    <apex:actionRegion >
                        <apex:outputPanel id="cat" layout="block"  styleClass="requiredInput">
                            <apex:selectList value="{!rmaToEdit.RMA_Category__c}" required="true" size="1">
                                <apex:outputPanel layout="block" styleClass="requiredBlock">
                                </apex:outputPanel>
                                <apex:selectOptions value="{!optionsCat}"/>
                                <apex:actionSupport event="onchange" action="{!getAllOptions}" rerender="typ, cat, subCat, msg"/>
                            </apex:selectList>
                        </apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                <br/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="RMA Sub-Category"  for="subCat"/>
                    <apex:actionRegion >
                        <apex:outputPanel id="subCat" layout="block"  styleClass="requiredInput">
                            <apex:selectList value="{!rmaToEdit.RMA_Sub_Category__c}" required="true" size="1">
                                <apex:outputPanel layout="block" styleClass="requiredBlock">
                                </apex:outputPanel>
                                <apex:selectOptions value="{!optionsSubCat}"/>
                                <apex:actionSupport event="onchange" action="{!getAllOptions}" rerender="typ, cat, subCat, msg"/>
                            </apex:selectList>
                        </apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                <br/>
                <apex:inputField value="{!rmaToEdit.Notes__c}" required="true" style="height: 34px; width:263px;"/>
                <br/>
                <apex:inputField value="{!rmaToEdit.gCases_ID__c}" required="true"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="Additional Information (Required only for certain issue types)">
                <apex:inputField value="{!rmaToEdit.Router_Brand__c}"/>
                <apex:inputField value="{!rmaToEdit.suggested_category__c}"/>
                <apex:inputField value="{!rmaToEdit.Router_Model__c}"/>
                <apex:inputField value="{!rmaToEdit.why_other__c}" style="height: 34px; width:263px;"/>
                <apex:inputField value="{!rmaToEdit.TV_Brand__c}"/>
                <apex:inputField value="{!rmaToEdit.acknowledge_review__c}"/>
                <apex:inputField value="{!rmaToEdit.TV_Model__c}"/>
                <apex:inputField value="{!rmaToEdit.ensured_N7__c}" />
                <apex:inputField value="{!rmaToEdit.AVR_Brand_Model__c}" />
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="Exceptions (Required only for exception RMAs)">
                <apex:inputField value="{!rmaToEdit.Exception_Type__c}"/>
                <apex:inputField value="{!rmaToEdit.Exception_Notes__c}" style="height: 34px; width:263px;"/>
                <apex:inputField value="{!rmaToEdit.Exception_Reason__c}"/>
                <apex:inputField value="{!rmaToEdit.I_understand_Exception_RMAs_are_reviewed__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="Google Account">
                <apex:inputField value="{!rmaToEdit.GoogleCustomer__c}" required="true"/>
                <apex:inputField value="{!rmaToEdit.Opportunity__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="Google Asset Information">
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="Google Asset" for="GA"/>
                <apex:actionRegion >
                    <apex:outputPanel >
                        <apex:inputField id="GA" value="{!rmaToEdit.GoogleAsset__c}" required="true">
                            <apex:actionSupport id="checkAsset" event="onchange"  action="{!checkAsset}" rerender="typ, cat, subCat, msg"/>
                        </apex:inputField>
                    </apex:outputPanel>
                </apex:actionRegion>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="Repair Vendor Triage Information">
                <apex:inputField value="{!rmaToEdit.Return_Condition_Notes__c}" style="height: 48px; width:263px;"/>
                <apex:inputField value="{!rmaToEdit.Date_Received__c}"/>
                <br/>
                <apex:inputField value="{!rmaToEdit.external_RMA_num__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" title="System Information">
                <apex:inputField value="{!rmaToEdit.RMA_Country_Workflow__c}"/>
                <apex:inputField value="{!rmaToEdit.RMA_Audit__c}"/>
                <apex:inputField value="{!rmaToEdit.Ready_to_send__c}"/>
                <apex:inputField value="{!rmaToEdit.Upserted_Status__c}"/>
                <apex:inputField value="{!rmaToEdit.RMA_In_Transit_Date__c}"/>
                <apex:inputField value="{!rmaToEdit.RMA_Delivered_Date__c}"/>
                <apex:inputField value="{!rmaToEdit.RMA_Auditor_Notes__c}" style="height: 48px; width:263px;"/>
                <apex:inputField value="{!rmaToEdit.Extended_Warranty_Contract_ID_Backup__c}"/>
                <apex:outputField value="{!rmaToEdit.OwnerId}"/>
                <br/>
                <apex:inputField value="{!rmaToEdit.Extended_Warranty_Claim_ID__c}" />
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>