// Copyright 2012 Google Inc. All Rights Reserved.

/**
 * Class providing tests for the feedbackExt custom Visualforce controller.
 *
 * @author jongaulding@google.com (Jon Gaulding)
 */
@isTest
public class feedbackExtTest {
  private static String CASE_COMMENTS = 'This is something of import.';
  
  private static String CASE_SUBJECT = 'Some Case Subject';
  
  private static String validCaseId;
  private static String VALID_CASE_SECRET = '1234321234321';
  
  private static String INVALID_CASE_ID = 'thisisbogus';
  private static String INVALID_CASE_SECRET = 'nowaydude';
  
  private static String caseNumber;
  
  static void setUp() {
    Case testCase = new Case(
        Subject=CASE_SUBJECT,
        Case_Secret__c=Long.valueOf(VALID_CASE_SECRET));
    insert testCase;
    
    validCaseId = testCase.Id;
    
    // retrieve test case with additional fields
    testCase = [SELECT Id, CaseNumber, Subject FROM Case WHERE Id=:validCaseId LIMIT 1];
    
    caseNumber = testCase.CaseNumber;
  }
  
  /**
   * Tests the controller under normal circumstances with no language specified.
   */
  static testMethod void testWithAllValidDefaultLanguage() {
    testWithAllValid(null /* languageSpecified */);
  }
  
  /**
   * Tests the controller under normal circumstances with non-default language specified.
   */
  static testMethod void testWithAllValidNonDefaultLanguage() {
    testWithAllValid('pt_BR' /* languageSpecified */);
  }

  /**
   * Helper method for testing the controller under normal circumstances.
   */
  private static void testWithAllValid(String languageSpecified) {
    setUp();
    
    PageReference feedbackPage = Page.CrOS;
    Test.setCurrentPage(feedbackPage);
    
    ApexPages.currentPage().getParameters().put('cid', validCaseId);
    ApexPages.currentPage().getParameters().put('cs', VALID_CASE_SECRET);
    if (languageSpecified != null) {
      ApexPages.currentPage().getParameters().put('lang', languageSpecified);
    }
    
    // insert previous feedback, to be overwritten
    insert new Customer_Feedback__c(Case__c=validCaseId);
    
    Test.startTest();
    
    feedbackExt feedbackExtUnderTest =
        new feedbackExt(new ApexPages.StandardController(new Customer_Feedback__c()));
    
    // verify data from controller visible in page
    System.assertNotEquals(null, feedbackExtUnderTest.c);
    System.assertEquals(CASE_SUBJECT, feedbackExtUnderTest.c.Subject);
    System.assertEquals(caseNumber, feedbackExtUnderTest.c.CaseNumber);
    
    // set form fields
    feedbackExtUnderTest.customerFeedback.Customer_Effort__c = 5;
    feedbackExtUnderTest.customerFeedback.Case_Comments__c = CASE_COMMENTS;
    
    // submit feedback
    String afterSubmissionPageUrl = feedbackExtUnderTest.save().getUrl();
    
    Test.stopTest();
    
    // verify behavior after submission
    
    String afterSubmitUrl = languageSpecified == null ?
        'http://www.google.com/support/enterprise/bin/answer.py?answer=138860&hl=en' :
        'http://www.google.com/support/enterprise/bin/answer.py?answer=138860&hl=' +
            feedbackExt.THANK_YOU_PAGES_MAP.get(languageSpecified);
    
    System.assertEquals(afterSubmitUrl, afterSubmissionPageUrl);
    
    Customer_Feedback__c[] caseFeedback = [SELECT Customer_Effort__c, Case_Comments__c
                                           FROM Customer_Feedback__c
                                           WHERE Case__c=:validCaseId];
    
    System.assertEquals(1, caseFeedback.size());
    
    Customer_Feedback__c latestFeedback = caseFeedback.get(0);
    
    System.assertEquals(5, latestFeedback.Customer_Effort__c);
    System.assertEquals(CASE_COMMENTS, latestFeedback.Case_Comments__c);
  }

  /**
   * Tests the controller for a case with no case secret set on the case itself.
   */
  static testMethod void testWithAllValidExceptNoSecretSetOnCase() {
    setUp();
    
    // unset case secret
    Case testCase = [SELECT Id, Case_Secret__c FROM Case WHERE Id=:validCaseId LIMIT 1];
    testCase.Case_Secret__c = null;
    update testCase;
    
    PageReference feedbackPage = Page.CrOS;
    Test.setCurrentPage(feedbackPage);
    
    ApexPages.currentPage().getParameters().put('cid', validCaseId);
    ApexPages.currentPage().getParameters().put('cs', '');
    
    Test.startTest();
    
    feedbackExt feedbackExtUnderTest =
        new feedbackExt(new ApexPages.StandardController(new Customer_Feedback__c()));
    
    // verify data from controller not visible in page
    System.assertEquals(null, feedbackExtUnderTest.c);  
    
    // attempt to submit feedback
    String afterSubmissionPageUrl = feedbackExtUnderTest.save().getUrl();
    
    Test.stopTest();
    
    // verify behavior after submission
    
    System.assertEquals(true, afterSubmissionPageUrl.startsWith('/apex/feedbackproblem'));
    
    verifyNoChange(validCaseId);
  }
  
  /**
   * Tests the controller when invalid case secret is given.
   */
  static testMethod void testWithInvalidSecretGiven() {
    String invalidSecret = String.valueOf(Long.valueOf(VALID_CASE_SECRET) + 1);
    
    testWithInvalidOrNoSecretGiven(invalidSecret /* secretGiven */);
  }
  
  /**
   * Tests the controller when blank case secret is given.
   */
  static testMethod void testWithBlankSecretGiven() {
        testWithInvalidOrNoSecretGiven('' /* secretGiven */);
  }
  
  /**
   * Tests the controller when no case secret is given.
   */
  static testMethod void testWithNoCaseSecretGiven() {
    testWithInvalidOrNoSecretGiven(null /* secretGiven */);
  }
  
  /**
   * Helper method for testing the controller with no valid case secret given.
   */
  private static void testWithInvalidOrNoSecretGiven(String secretGiven) {
    setUp();
    
    PageReference feedbackPage = Page.CrOS;
    Test.setCurrentPage(feedbackPage);
    
    ApexPages.currentPage().getParameters().put('cid', validCaseId);
    if (secretGiven != null) {
      ApexPages.currentPage().getParameters().put('cs', secretGiven);
    }
    
    Test.startTest();
    
    feedbackExt feedbackExtUnderTest =
        new feedbackExt(new ApexPages.StandardController(new Customer_Feedback__c()));
    
    // verify data from controller not visible in page
    System.assertEquals(null, feedbackExtUnderTest.c);  
    
    // attempt to submit feedback
    String afterSubmissionPageUrl = feedbackExtUnderTest.save().getUrl();
    
    Test.stopTest();
    
    // verify behavior after submission
    
    System.assertEquals(true, afterSubmissionPageUrl.startsWith('/apex/feedbackproblem'));
    
    verifyNoChange(validCaseId);
  }
  
  /**
   * Tests the controller when no case ID is given.
   */
  static testMethod void testWithNoCaseIdGiven() {
    setUp();
    
    PageReference feedbackPage = Page.CrOS;
    Test.setCurrentPage(feedbackPage);
    
    ApexPages.currentPage().getParameters().put('cs', VALID_CASE_SECRET);
    
    Test.startTest();
    
    feedbackExt feedbackExtUnderTest =
        new feedbackExt(new ApexPages.StandardController(new Customer_Feedback__c()));
    
    // verify data from controller not visible in page
    System.assertEquals(null, feedbackExtUnderTest.c);  
    
    // attempt to submit feedback
    String afterSubmissionPageUrl = feedbackExtUnderTest.save().getUrl();
    
    Test.stopTest();
    
    // verify behavior after submission
    
    System.assertEquals(true, afterSubmissionPageUrl.startsWith('/apex/feedbackproblem'));
    
    verifyNoChange(validCaseId);
  }
  
  /**
   * Tests the controller when no parameters are given.
   */
  static testMethod void testWithNoParams() {
    setUp();
    
    PageReference feedbackPage = Page.CrOS;
    Test.setCurrentPage(feedbackPage);
    
    Test.startTest();
    
    feedbackExt feedbackExtUnderTest =
        new feedbackExt(new ApexPages.StandardController(new Customer_Feedback__c()));
    
    // verify data from controller not visible in page
    System.assertEquals(null, feedbackExtUnderTest.c);  
    
    // attempt to submit feedback
    String afterSubmissionPageUrl = feedbackExtUnderTest.save().getUrl();
    
    Test.stopTest();
    
    // verify behavior after submission
    
    System.assertEquals(true, afterSubmissionPageUrl.startsWith('/apex/feedbackproblem'));
    
    verifyNoChange(validCaseId);
  }
  
  /**
   * Verifies no feedback has been inserted for the case
   */
  private static void verifyNoChange(String caseId) {
    Case[] cases = [SELECT Id FROM Case WHERE Id=:caseId];
    Customer_Feedback__c[] caseFeedback =
        [SELECT Id FROM Customer_Feedback__c WHERE Case__c=:caseId];
    
    System.assertEquals(1, cases.size());
    System.assertEquals(0, caseFeedback.size());
  }
}