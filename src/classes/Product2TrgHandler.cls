public without sharing class Product2TrgHandler 
{
	public static void onAfterInsert(List<Product2> trgNew)
	{		
		List<Country_Variant__c> cvList = new List<Country_Variant__c>();
		Set<String> existingCvSet = new Set<String>();
		Set<String> countryCodeSet = new Set<String>();
		Map<String,Country__c> countryMap = new Map<String,Country__c>();
		Set<Id> docIdSet = new Set<Id>();
		for(Product2 p2 : trgNew)
		{
			if(String.isNotEmpty(p2.WarrantyCountries__c) && String.isNotEmpty(p2.Document__c))
			{
				docIdSet.add(p2.Document__c);
				for(String countryCode : p2.WarrantyCountries__c.split(';'))
					countryCodeSet.add(countryCode);
			}
		}
		for(Country__c c : [SELECT Id,Name FROM Country__c WHERE Name IN :countryCodeSet])		
			countryMap.put(c.Name,c);
		
		for(Country_Variant__c cv : [SELECT Name,Document__c FROM Country_Variant__c WHERE Name in :countryCodeSet AND Document__c in :docIdSet])
			existingCvSet.add(cv.Name+cv.Document__c);
		
		for(Product2 p2 : trgNew)
		{
			if(String.isNotEmpty(p2.WarrantyCountries__c))
			{
				for(String countryCode : p2.WarrantyCountries__c.split(';'))
				{
					if(countryMap.containsKey(countryCode) && String.isNotEmpty(p2.Document__c) && !existingCvSet.contains(countryCode+p2.Document__c))
						cvList.add(new Country_Variant__c(Name=countryCode,Country__c=countryMap.get(countryCode).Id,Document__c=p2.Document__c,Original_Google_Part__c=p2.Id,DOA_Replacement_Google_Part__c=p2.Id,Warranty_Replacement_Google_Part__c=p2.Id));
				}
			}
		}
		
		if(!cvList.isEmpty())
			insert cvList;
	}
	public static void onAfterUpdate(List<Product2> trgNew, Map<Id,Product2> oldMap)
	{
		Set<String> countryCodeSet = new Set<String>();		
		Product2 oldProduct;
		List<Product2> changedList = new List<Product2>();		
		Set<Id> docIdSet = new Set<Id>();
		
		for(Product2 p2 : trgNew)
		{
			oldProduct = oldMap.get(p2.Id);
			if(String.isNotEmpty(p2.WarrantyCountries__c) && String.isNotEmpty(p2.Document__c) && p2.WarrantyCountries__c!=oldProduct.WarrantyCountries__c)
			{
				changedList.add(p2);
				for(String countryCode : p2.WarrantyCountries__c.split(';'))
					countryCodeSet.add(countryCode);
				docIdSet.add(p2.Document__c);
			}
		}
		if(!changedList.isEmpty())
		{			
			List<Country_Variant__c> cvList = new List<Country_Variant__c>();
			Map<String,Country__c> countryMap = new Map<String,Country__c>();
			Set<String> existingCvSet = new Set<String>();
			
			for(Country__c c : [SELECT Id,Name FROM Country__c WHERE Name IN :countryCodeSet])		
				countryMap.put(c.Name,c);
			for(Country_Variant__c cv : [SELECT Name,Document__c FROM Country_Variant__c WHERE Name in :countryCodeSet AND Document__c in :docIdSet])
				existingCvSet.add(cv.Name+cv.Document__c);
		
			for(Product2 p2 : changedList)
			{
				if(String.isNotEmpty(p2.WarrantyCountries__c))
				{
					for(String countryCode : p2.WarrantyCountries__c.split(';'))
					{
						if(countryMap.containsKey(countryCode) && String.isNotEmpty(p2.Document__c) && !existingCvSet.contains(countryCode+p2.Document__c))
							cvList.add(new Country_Variant__c(Name=countryCode,Country__c=countryMap.get(countryCode).Id,Document__c=p2.Document__c,Original_Google_Part__c=p2.Id,DOA_Replacement_Google_Part__c=p2.Id,Warranty_Replacement_Google_Part__c=p2.Id));
					}
				}
			}
			if(!cvList.isEmpty())
				insert cvList;
		}
	}
}