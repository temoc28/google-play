public without sharing class OpportunityTrgHandler 
{
	public static void onAfterInsert(Opportunity[] trgNew)
	{
		Map<Id,Id> assetOwnerMap = new Map<Id,Id>();		
		for(Opportunity o : trgNew)
		{
			//create a map of google assets and the opp owner
			if(o.GoogleAsset__c != null)
				assetOwnerMap.put(o.GoogleAsset__c,o.AccountId);			
		}
	
		//update the OwnerAsset
		Google_Asset__c[] gaUpdate = new Google_Asset__c[0];
		for(Google_Asset__c ga : [Select Id, AssetOwner__c from Google_Asset__c where Id in :assetOwnerMap.keySet()])
		{
			if(assetOwnerMap.containsKey(ga.Id))
			{
				ga.AssetOwner__c = assetOwnerMap.get(ga.Id);
				gaUpdate.add(ga);
			}
		}
		update gaUpdate;		
	}
	public static void onBeforeInsert(Opportunity[] trgNew)
	{
		for(Opportunity opp : trgNew)
			opp.OrderIDExternalID__c = opp.Name;		
	}
	public static void onAfterUpdate(Opportunity[] trgNew, Map<Id,Opportunity> oldMap)
	{
		List<Opportunity> replacementOrderList = new List<Opportunity>();
		for(Opportunity opp : trgNew)
		{
			if(opp.Type!=oldMap.get(opp.Id).Type || opp.Country__c!=oldMap.get(opp.Id).Country__c)
				replacementOrderList.add(opp);
		}
		if(!replacementOrderList.isEmpty())
		{
			//WarrantyHandler.handleWarranty(replacementOrderList);
			Map<Id,Opportunity> oppGaMap = new Map<Id,Opportunity>([Select Id, Country__c, Type, (Select ProductID__c,Sale_Country__c,Warranty_Expiration_Date__c From Google_Assets__r), (Select Id, GoogleAsset__c, GoogleAsset__r.Warranty_Expiration_Date__c From RMA1__r WHERE GoogleAsset__c!=null AND Type__c='Warranty Regular') From Opportunity WHERE Id in :replacementOrderList]);
			Map<Id,Map<Id,Google_Asset__c>> oppToProductMap = new Map<Id,Map<Id,Google_Asset__c>>();
			Map<Id,List<RMA__c>> replacementToRmaMap = new Map<Id,List<RMA__c>>();
			Set<Id> productIdSet = new Set<Id>();
			for(Opportunity opp : oppGaMap.values())
			{
				if(!oppToProductMap.containsKey(opp.Id))
					oppToProductMap.put(opp.Id,new Map<Id,Google_Asset__c>());
				for(Google_Asset__c ga : opp.Google_Assets__r)
				{
					productIdSet.add(ga.ProductID__c);
					Map<Id,Google_Asset__c> gaMap = oppToProductMap.get(opp.Id);
					if(!gaMap.containsKey(ga.ProductID__c))
						gaMap.put(ga.ProductID__c,ga);						
				}
			}
				
			Map<Id,Product2> productMap = new Map<Id,Product2>([SELECT Id,RemorsePeriod__c,DOAPeriod__c,MinimumStandardWarranty__c,MinimumReplacementWarranty__c,ExpectedSalesDate__c FROM Product2 WHERE Id in :productIdSet]);
			
			List<Google_Asset__c> gaList = new List<Google_Asset__c>();
			Google_Asset__c ga;
			Map<String, CountryRules__c> countryRulesMap = CountryRules__c.getAll();
			Date dToday = Date.today();
			Opportunity opp;
			Product2 p;
			CountryRules__c cr;
			for(Opportunity opp2 : replacementOrderList)
			{
				if(oppGaMap.containsKey(opp2.Id))
				{
					opp = oppGaMap.get(opp2.Id);
					for(Google_Asset__c gaTemp : opp.Google_Assets__r)
					{
						ga = new Google_Asset__c(Id=gaTemp.Id,Warranty_Period__c=0);						
						if(productMap.containsKey(gaTemp.ProductID__c))
						{
							p = productMap.get(gaTemp.ProductID__c);							
							ga.DOA_Period__c=String.isEmpty(String.valueOf(p.DOAPeriod__c))?0:p.DOAPeriod__c;
							ga.Remorse_Period__c=String.isEmpty(String.valueOf(p.RemorsePeriod__c))?0:p.RemorsePeriod__c;
							ga.Warranty_Period__c=String.isEmpty(String.valueOf(p.MinimumStandardWarranty__c))?0:p.MinimumStandardWarranty__c;
							ga.Minimum_Replacement_Warranty_Period__c=String.isEmpty(String.valueOf(p.MinimumReplacementWarranty__c))?0:p.MinimumReplacementWarranty__c;
							ga.Expected_Sale_Days__c=String.isEmpty(String.valueOf(p.ExpectedSalesDate__c))?0:p.ExpectedSalesDate__c;
						}
						else
						{
							ga.DOA_Period__c=0;
							ga.Remorse_Period__c=0;
							ga.Warranty_Period__c=0;
							ga.Minimum_Replacement_Warranty_Period__c=0;
							ga.Expected_Sale_Days__c=0;
						}
						if(countryRulesMap.containsKey(gaTemp.Sale_Country__c))
						{
							cr = countryRulesMap.get(gaTemp.Sale_Country__c);
							ga.DOA_Period__c=ga.DOA_Period__c>cr.DOAPeriod__c?ga.DOA_Period__c:cr.DOAPeriod__c;
							ga.Warranty_Period__c=ga.Warranty_Period__c>cr.MinimumStandardWarranty__c?ga.Warranty_Period__c:cr.MinimumStandardWarranty__c;
							ga.Remorse_Period__c=ga.Remorse_Period__c>cr.RemorsePeriod__c?ga.Remorse_Period__c:cr.RemorsePeriod__c;							
							ga.Minimum_Replacement_Warranty_Period__c=ga.Minimum_Replacement_Warranty_Period__c>cr.MinimumReplacementWarranty__c?ga.Minimum_Replacement_Warranty_Period__C:cr.MinimumReplacementWarranty__c;
							ga.Expected_Sale_Days__c=ga.Expected_Sale_Days__c>cr.ExpectedSalesDate__c?ga.Expected_Sale_Days__c:cr.ExpectedSalesDate__c;
						}
						if(opp.Type=='Advanced Replacement')
						{							
							if(opp.RMA1__r!=null && opp.RMA1__r.size()==1)
							{
								ga.DOA_Period__c=-50;
								ga.Remorse_Period__c=-50;
								RMA__c rma = opp.RMA1__r.get(0);
								integer originalRemaining = dToday.daysBetween(rma.GoogleAsset__r.Warranty_Expiration_Date__c);
								ga.Warranty_Period__c=ga.Minimum_Replacement_Warranty_Period__c>originalRemaining?ga.Minimum_Replacement_Warranty_Period__c:originalRemaining;
								//ga.Warranty_Period__c=ga.Warranty_Period__c>originalRemaining?ga.Warranty_Period__c:originalRemaining;
							}
						}
						gaList.add(ga);
					}
				}
			}
			if(!gaList.isEmpty())
				update gaList;
		}
	}
}