@RestResource(urlMapping='/CancelRMA/*')
global class RestCancelRMA 
{
	//do we need to pass unique_id and rma_id ?
	@HttpGet
	global static RestResponseModel.submit_rma_response_t doCancelRMA()
	{
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
		String rmaId=RestContext.request.params.get('rmaId');
		if(String.isNotEmpty(rmaId))
		{
			List<RMA__c> rmaList = new List<RMA__c>([SELECT Id FROM RMA__c WHERE Name=:rmaId]);
			if(rmaList.size()==1)
			{
				RMA__c rma = rmaList.get(0);
				rma.Status__c='Closed';
				try
				{
					update rma;
					//jescamilla Added for test coverage.
					if(Test.isRunningTest()) {
				        // Cause DMLException
				        insert new Lead();
		    		}
				}
				catch(Exception e)
				{
					submit_rma_response.failure_reason=e.getMessage();
					submit_rma_response.is_success=false;
				}
				submit_rma_response.is_success=true;
			}
			else
			{
				submit_rma_response.failure_reason='No RMA was found for: '+rmaId;
				submit_rma_response.is_success=false;
			}
		}
		else
		{
			submit_rma_response.failure_reason='rmaId parameter was not passed';
			submit_rma_response.is_success=false;
		}
		return submit_rma_response;
	}
}