public without sharing class ArOrder 
{
    public Opportunity opp{get;set;}
    public boolean isAr{get;set;}
    public Refund__c refund{get;set;}
    
    public ArOrder(ApexPages.StandardController con)
    {
        //opp = (Opportunity)con.getRecord();
        opp = [SELECT Id,Type,Ship_By_Date__c,Name FROM Opportunity WHERE Id = :con.getId()];
        isAr=false;
        refund = new Refund__c();
        if(opp.Type=='Advanced Replacement')
        	isAr=true;
        if(!isAr)
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'This is not an AR order.'));
    }
    public PageReference cancelArOrder()
    {        
        Refund__c r = new Refund__c();
        r.OrderNumber__c=opp.Id;
        r.PercentShippingItemRefund__c=100;
        r.PercentShippingTaxRefund__c=100;
        r.RefundStatus__c='Pending';
        r.RefundReason__c='Cancel Auth';
        //check to see which date is further out
        Date plusBusinessDays = null;
        Date plusOneDay = Date.today().addDays(1);
        if(opp.Ship_By_Date__c!=null)
            plusBusinessDays = BusinessDays.addBusinessDays(opp.Ship_By_Date__c,3);
        else
            plusBusinessDays = Date.today();
        r.ExpectedRefundDate__c = plusBusinessDays > plusOneDay ? plusBusinessDays : plusOneDay;
        r.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Refund').getRecordTypeId();
        try
        {
            insert r;
            //opp.
        }
        catch(DmlException e)
        {
            ApexPages.addMessages(e);
            return null;
        }       
    	return new PageReference('/'+opp.Id);
    }
    public PageReference newCharge()
    {
    	if(isAr)
    	{
    		//Charge__c c = new Charge__c();
    		refund = new Refund__c();
    		refund.OrderNumber__c = opp.Id;
    		refund.RefundStatus__c='Pending';    		
    		//c.ChargeStatus__c = 'Pending';
    		refund.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Charge').getRecordTypeId();
    		//c.PercentItemCharge__c=100;
			//c.PercentTaxCharge__c=100;
			refund.PercentItemRefund__c=100;
			refund.PercentTaxRefund__c=100;
			refund.RefundReason__c='Charge AR Order';
			refund.ExpectedRefundDate__c=Date.today().addDays(1);
    		try
	        {
	            insert refund;	            
	        }
	        catch(DmlException e)
	        {
	            ApexPages.addMessages(e);
	            return null;
	        }   
    		return new PageReference('/'+opp.Id);
    	}
    	else
    		return null;
    }
}