@RestResource(urlMapping='/DeviceFamily/*')
global class RestGetDeviceFamily 
{
	@HttpGet
	global static RestResponseModel.device_types_t getDeviceFamily()
	{
		RestResponseModel.device_types_t device_types = new RestResponseModel.device_types_t();
		
		return device_types;
	}
}