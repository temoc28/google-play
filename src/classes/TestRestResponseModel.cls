@isTest
private class TestRestResponseModel {
	
	@isTest static void testClassInst() {
		RestResponseModel restRespMod = new RestResponseModel();

		RestResponseModel.customer_records_t customer_records = new RestResponseModel.customer_records_t('johnwick@levementum.com');
		RestResponseModel.device_details_t device_details = new RestResponseModel.device_details_t();
		System.assertNotEquals(device_details.components, null);
		System.assertNotEquals(device_details.substitute_gpns, null);
				
		RestResponseModel.rma_details_t rma_details = new RestResponseModel.rma_details_t();
		
		RestResponseModel.device_warranty_details_t device_warranty_details = new RestResponseModel.device_warranty_details_t();
		RestResponseModel.device_purchase_details_t device_purchase_details = new RestResponseModel.device_purchase_details_t();
		RestResponseModel.device_rma_reasons_t device_rma_reasons = new RestResponseModel.device_rma_reasons_t();

		RestResponseModel.rma_type_with_reasons_t rma_type_with_reasons = new RestResponseModel.rma_type_with_reasons_t();
		System.assertNotEquals(rma_type_with_reasons.rma_reason, null);

		RestResponseModel.rma_initiation_details_t rma_initiation_details = new RestResponseModel.rma_initiation_details_t();
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
		RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
		RestResponseModel.rma_success_details_t rma_success_details = new RestResponseModel.rma_success_details_t();
		RestResponseModel.update_owner_details_t update_owner_details = new RestResponseModel.update_owner_details_t();
		RestResponseModel.update_owner_details_response_t update_owner_details_response = new RestResponseModel.update_owner_details_response_t();
		RestResponseModel.rma_events_t rma_events = new RestResponseModel.rma_events_t();
		RestResponseModel.owner_details_t owner_details = new RestResponseModel.owner_details_t();
		RestResponseModel.customer_details_t customer_details = new RestResponseModel.customer_details_t();
		RestResponseModel.address_t address = new RestResponseModel.address_t();
		RestResponseModel.device_types_t device_types = new RestResponseModel.device_types_t();
				
		RestResponseModel.warranty_model_t warranty_model = new RestResponseModel.warranty_model_t();
		warranty_model.warranty_model = 'Remorse Only';
		warranty_model.rma_action_list = new List<String>();		
	}
}