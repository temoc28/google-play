@isTest
public class TestRestRmaDetails {
	
    static testmethod void testGetRmaDetails0(){
        Map<String,Id> idsMap = createTestData(true);
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.Type__c = 'Warranty Regular';
        rma.Opportunity__c = idsMap.get('Opportunity2'); // Levementum LLC. 04/12/15. Added to pass validation test
        insert rma;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', rma.Name);
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/RmaDetails';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
         List<RestResponseModel.rma_details_t> testList = RestRmaDetails.getRmaDetails();
    }
    
    static testmethod void testGetRmaDetails1(){
        Map<String,Id> idsMap = createTestData(true);
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.Type__c = 'Warranty Regular';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.Opportunity__c = idsMap.get('Opportunity2'); // Levementum LLC. 04/12/15. Added to pass validation test
        insert rma;
        
        rma = [select id,name from RMA__c where id = :rma.Id];


        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', 'TestGA');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/RmaDetails';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
         List<RestResponseModel.rma_details_t> testList = RestRmaDetails.getRmaDetails();
    }
    
    static testmethod void testGetRmaDetails2(){
        Map<String,Id> idsMap = createTestData(true);


        Refund__c ref1 = [select id from Refund__c where id = :idsMap.get('refund1')];
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.Type__c = 'Warranty Regular';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.Opportunity__c = idsMap.get('Opportunity1');
        rma.ReplacementOrder__c = idsMap.get('Opportunity2');

        insert rma;

        //Link test refund to RMA (Coverage Increase)
        ref1.RMA__c = rma.Id;
        update ref1;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('orderNumber', 'testOppt');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/RmaDetails';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
         List<RestResponseModel.rma_details_t> testList = RestRmaDetails.getRmaDetails();
    }
    
    static testmethod void testGetRmaDetails3(){
        Map<String,Id> idsMap = createTestData(true);


        Refund__c ref1 = [select id from Refund__c where id = :idsMap.get('refund1')];
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.Type__c = 'Warranty Regular';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.Opportunity__c = idsMap.get('Opportunity1');
        rma.ReplacementOrder__c = idsMap.get('Opportunity2');

        insert rma;

        //Link test refund to RMA (Coverage Increase)
        ref1.RMA__c = rma.Id;
        update ref1;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('orderNumber', 'testOppt');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        rReq.addParameter('email', 'test@levtest.com');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/RmaDetails';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
         List<RestResponseModel.rma_details_t> testList = RestRmaDetails.getRmaDetails();
    }
    
    static testMethod void testSetCV()
    {
    	try
    	{
    		Test.startTest();
    			RmaDetailsBuilder rBuilder = new RmaDetailsBuilder();
    			rBuilder.setCV('', '');
    			rBuilder.getNoCVForExtendCase();
    			rBuilder.setCV('23', '23');
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    static testMethod void testStampParameter()
    {
    	try
    	{
    		Test.startTest();
    			RmaDetailsBuilder rBuilder = new RmaDetailsBuilder();
    			Map<String,Id> idsMap = createTestData(false);
    			RMA__c rma = new RMA__c();
		        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		        rma.Notes__c = 'test notes';
		        rma.gCases_ID__c = '5-7327000004804';
		        rma.Type__c = 'Warranty DOA';
		        rma.GoogleCustomer__c = idsMap.get('Account1');
		        rma.GoogleAsset__c = idsMap.get('Google_Asset__c3');
		        rma.Opportunity__c = idsMap.get('Opportunity1');
		        rma.ReplacementOrder__c = idsMap.get('Opportunity2');
		        rma.RMA_Country_Workflow__c = 'US';
        		insert rma;
        		
        		Country_Variant__c cv = [SELECT Id,Name,Requires_Shipping_Label__c,Buyer_s_Remorse_Article_ID__c,DOA_Article_ID__c,DOA_Replacement_Doc_ID__c,
                                     			DOA_Replacement_GPN__c,DOA_Replacement_Google_Part__r.Name,Warranty_Article_ID__c,Warranty_Replacement_Doc_ID__c,Warranty_Replacement_GPN__c,
                                     			Warranty_Refund_Article_ID__c,Warranty_Replacement_Google_Part__r.Name,
                                     			AR_Method__c,Custom_Return_Center__r.Address_Line_1__c,Custom_Return_Center__c,Custom_Return_Center__r.Address_Line_2__c,
                                     			Custom_Return_Center__r.Address_Line_3__c,Custom_Return_Center__r.City__c,Custom_Return_Center__r.State__c,
                                     			Custom_Return_Center__r.Postal_Code__c,Custom_Return_Center__r.Country__c,Custom_Return_Center__r.Shipping_Label_Service_Code__c,
                                     			Custom_Return_Center__r.Name,Document__c,Country__c,Country__r.Return_Center__c,Country__r.Return_Center__r.Address_Line_1__c,
                                     			Country__r.Return_Center__r.Address_Line_2__c,Country__r.Return_Center__r.Address_Line_3__c,Country__r.Return_Center__r.City__c,
                                     			Country__r.Return_Center__r.State__c,Country__r.Return_Center__r.Country__c,Country__r.Return_Center__r.Postal_Code__c,Return_Center_ID__c,
                                     			Return_Center_Name__c
                                     	FROM Country_Variant__c 
                                     	WHERE id =: idsMap.get('Country_Variant__c1')];
    			rBuilder.rma = rma;
    			rBuilder.cvMap.put('US' + rma.GoogleAsset__r.ProductID__r.Document__c, cv);
    			rBuilder.stampParameter();
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    static testmethod void testWarrantyDOA()
    {
        Map<String,Id> idsMap = createTestData(false);
        Refund__c ref1 = [select id from Refund__c where id = :idsMap.get('refund1')];
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.Type__c = 'Warranty DOA';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c3');
        rma.Opportunity__c = idsMap.get('Opportunity1');
        rma.ReplacementOrder__c = idsMap.get('Opportunity2');

        insert rma;

        //Link test refund to RMA (Coverage Increase)
        ref1.RMA__c = rma.Id;
        update ref1;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', rma.Id);
        rReq.addParameter('email', 'test@levtest.com');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/RmaDetails';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.rma_details_t> testList = RestRmaDetails.getRmaDetails();
    }
    
    static testmethod void testWarrantyRefund()
    {
    		Test.startTest();
    			RmaDetailsBuilder rBuilder = new RmaDetailsBuilder();
    			Map<String,Id> idsMap = createTestData(false);
    			RMA__c rma = new RMA__c();
		        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		        rma.Notes__c = 'test notes';
		        rma.gCases_ID__c = '5-7327000004804';
		        rma.Type__c = 'Warranty Refund';
		        rma.GoogleCustomer__c = idsMap.get('Account1');
		        rma.GoogleAsset__c = idsMap.get('Google_Asset__c3');
		        rma.Opportunity__c = idsMap.get('Opportunity1');
		        rma.ReplacementOrder__c = idsMap.get('Opportunity2');
		        rma.RMA_Country_Workflow__c = 'US';
        		insert rma;
        		
        		Country_Variant__c cv = [SELECT Id,Name,Requires_Shipping_Label__c,Buyer_s_Remorse_Article_ID__c,DOA_Article_ID__c,DOA_Replacement_Doc_ID__c,
                                     			DOA_Replacement_GPN__c,DOA_Replacement_Google_Part__r.Name,Warranty_Article_ID__c,Warranty_Replacement_Doc_ID__c,Warranty_Replacement_GPN__c,
                                     			Warranty_Refund_Article_ID__c,Warranty_Replacement_Google_Part__r.Name,
                                     			AR_Method__c,Custom_Return_Center__r.Address_Line_1__c,Custom_Return_Center__c,Custom_Return_Center__r.Address_Line_2__c,
                                     			Custom_Return_Center__r.Address_Line_3__c,Custom_Return_Center__r.City__c,Custom_Return_Center__r.State__c,
                                     			Custom_Return_Center__r.Postal_Code__c,Custom_Return_Center__r.Country__c,Custom_Return_Center__r.Shipping_Label_Service_Code__c,
                                     			Custom_Return_Center__r.Name,Document__c,Country__c,Country__r.Return_Center__c,Country__r.Return_Center__r.Address_Line_1__c,
                                     			Country__r.Return_Center__r.Address_Line_2__c,Country__r.Return_Center__r.Address_Line_3__c,Country__r.Return_Center__r.City__c,
                                     			Country__r.Return_Center__r.State__c,Country__r.Return_Center__r.Country__c,Country__r.Return_Center__r.Postal_Code__c,Return_Center_ID__c,
                                     			Return_Center_Name__c
                                     	FROM Country_Variant__c 
                                     	WHERE id =: idsMap.get('Country_Variant__c1')];
    			rBuilder.rma = rma;
    			rBuilder.cvMap.put('US' + rma.GoogleAsset__r.ProductID__r.Document__c, cv);
    			rBuilder.stampParameter();
    		Test.stopTest();
    }
    
    static testmethod void testCountryMap()
    {
    		Test.startTest();
    			RmaDetailsBuilder rBuilder = new RmaDetailsBuilder();
    			Map<String,Id> idsMap = createTestData(false);
    			RMA__c rma = new RMA__c();
		        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		        rma.Notes__c = 'test notes';
		        rma.gCases_ID__c = '5-7327000004804';
		        rma.Type__c = 'Warranty Refund';
		        rma.GoogleCustomer__c = idsMap.get('Account1');
		        rma.GoogleAsset__c = idsMap.get('Google_Asset__c3');
		        rma.Opportunity__c = idsMap.get('Opportunity1');
		        rma.ReplacementOrder__c = idsMap.get('Opportunity2');
		        rma.RMA_Country_Workflow__c = 'US';
        		insert rma;
        		
        		Country__c c = [SELECT Id,Name,Buyer_s_Remorse_Article_ID__c,
                                            	Warranty_Refund_Article_ID__c,AR_Method__c,Return_Center__c,Return_Center__r.Address_Line_1__c,Return_Center__r.Address_Line_2__c,
                                            	Return_Center__r.Address_Line_3__c,Return_Center__r.City__c,Return_Center__r.State__c,Return_Center__r.Country__c,
                                            	Return_Center__r.Postal_Code__c
                                          FROM Country__c
                                          WHERE id =: idsMap.get('Country1')];
    			rBuilder.rma = rma;
    			rBuilder.cMap.put('US' + rma.GoogleAsset__r.ProductID__r.Document__c, c);
    			rBuilder.stampParameter();
    		Test.stopTest();
    }
    
    static Map<String,Id> createTestData(boolean isCustomReturnCenter){
        Map<String,Id> idsMap = new Map<String,Id>();
        
        Account acct = new Account();
        acct.PersonEmail = 'test@levtest.com';
        acct.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'GoogleCustomer'].id;
        acct.FirstName = 'test';
        acct.LastName = 'levtest';
        acct.PersonMailingCountry = 'US';
        acct.PersonMailingState = 'AZ';
        insert acct;
        idsMap.put('Account1',acct.Id);
        
        Account acct2 = new Account();
        acct2.Name = 'TestLevBiz';
        acct2.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'BusinessAccount'].id;
        insert acct2;
        idsMap.put('Account2',acct2.Id);
        
        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c();
        codeSet.Name = 'CHromecaseTest';
        insert codeSet;
        idsMap.put('Return_Reason_Code_Set__c1',codeSet.Id);
        
        Return_Reason_Code__c code = new Return_Reason_Code__c();
        code.Name = 'ChromecaseTest';
        code.RMA_Types__c = 'Buyer\'s Remorse';
        code.Return_Reason_Code_Set__c = codeSet.Id;
        insert code;
        idsMap.put('Return_Reason_Code__c1',code.Id);
        
        Product_Family__c prodFam = new Product_Family__c();
        prodFam.Name = 'Chromecast Test';
        insert prodFam;
        idsMap.put('Product_Family__c1',prodFam.Id);
        
        Document__c doc = new Document__c();
        doc.Name = 'Chromecast Test';
        doc.Product_Family__c = prodFam.Id;
        doc.Return_Reason_Code_Set__c = codeset.Id;
        insert doc;
        idsMap.put('Document__c1',doc.Id);
        
        Return_Center__c rCenter = new Return_Center__c();
        rCenter.Country__c = 'US';
        rCenter.Requires_Shipping_Label_Service__c = true;
        insert rCenter;
        
        Country__c ctry = new Country__c();
        ctry.Name = 'US';
        ctry.Return_Center__c = rCenter.Id;
        insert ctry;
        idsMap.put('Country1', ctry.Id);
        
        Country_Variant__c cv = new Country_Variant__c();
        cv.Document__c = doc.Id;
        cv.Name = 'US';
        cv.Country__c = ctry.Id;
        if(isCustomReturnCenter){cv.Custom_Return_Center__c = rCenter.Id;}
        insert cv;
        idsMap.put('Country_Variant__c1',cv.Id);
        
        Product2 prod = new Product2();
        prod.Name = 'Chromecast US Test';
        prod.SKU__c = '86002596-01-Test';
        prod.Sale_Country__c = 'US';
        prod.Unique_ID_Type__c = 'Serial Number';
        prod.RMARecordType__c = 'Chromecast';
        prod.Document__c = doc.Id;
        insert prod;
        idsMap.put('Product21',prod.Id);
        
        Opportunity oppt = new Opportunity();
        oppt.AccountId = acct.Id;
        oppt.OrderIDExternalID__c = '32434902134BNMBd';
        oppt.TransactionDate__c = dateTime.now();
        oppt.Country__c = 'US';
        oppt.State_Province__c = 'AZ';
        oppt.Name = 'testOppt';
        oppt.Type = 'Standard Order';
        oppt.CloseDate = Date.today().addDays(30);
        oppt.StageName = 'Prospecting';
        insert oppt;
        idsMap.put('Opportunity1',oppt.Id);
        
        Opportunity oppt2 = new Opportunity();
        oppt2.AccountId = acct2.Id;
        oppt2.OrderIDExternalID__c = '32434902134BNMBd2';
        oppt2.TransactionDate__c = dateTime.now();
        oppt2.Country__c = 'US';
        oppt2.State_Province__c = 'AZ';
        oppt2.Name = 'testOppt2';
        oppt2.Type = 'Standard Order';
        oppt2.CloseDate = Date.today().addDays(30);
        oppt2.StageName = 'Prospecting';
        insert oppt2;
        idsMap.put('Opportunity2',oppt2.Id);
        
        Google_Asset__c ga = new Google_Asset__c();
        ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga.AssetOwner__c = acct.Id;
        ga.SKU__c = '86002596-01-Test';
        ga.Name = 'TestGA';
        ga.Order_Opportunity__c = oppt.Id;
        ga.Line_Number__c = 1;
        ga.ProductID__c = prod.Id;
        insert ga;
        idsMap.put('Google_Asset__c1',ga.Id);
        
        Google_Asset__c ga2 = new Google_Asset__c();
        ga2.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga2.AssetOwner__c = acct2.Id;
        ga2.SKU__c = '86002596-01-Test2';
        ga2.Name = 'TestGA2';
        ga2.Order_Opportunity__c = oppt2.Id;
        ga2.Line_Number__c = 1;
        ga2.ProductID__c = prod.Id;
        insert ga2;
        idsMap.put('Google_Asset__c2',ga2.Id);
        
        Google_Asset__c ga3 = new Google_Asset__c();
        ga3.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg342';
        ga3.AssetOwner__c = acct.Id;
        ga3.SKU__c = '86002596-01-Test2';
        ga3.Name = 'TestGA3';
        ga3.Order_Opportunity__c = oppt.Id;
        ga3.Line_Number__c = 1;
        insert ga3;
        idsMap.put('Google_Asset__c3',ga3.Id);

        // Levementum LLC. 12/2/14 Description: Added Refund object for coverage increase.
        Refund__c r = new Refund__c();
        r.OrderNumber__c = idsMap.get('Opportunity1');
        r.PercentShippingItemRefund__c=100;
        r.PercentShippingTaxRefund__c=100;
        r.RefundStatus__c='Pending';
        r.RefundReason__c='Cancel Auth';
        r.ExpectedRefundDate__c = Date.today();
        r.RecordTypeId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Refund').getRecordTypeId();

        insert r;
        idsMap.put('refund1', r.Id);

        
        return idsMap;
    }
}