public with sharing class RmaTrgHandler 
{
	public static void checkCreateRefund(RMA__c[] trgNew, Map<Id,RMA__c> trgNewMap, Map<Id,RMA__c> trgOldMap)
	{		
		Set<String> countrySet = new Set<String>();
		Set<String> shippingCountrySet = new Set<String>();
		Set<String> shippingStateSet = new Set<String>();
		Set<String> stateSet = new Set<String>();
		Set<Id> orderIdSet = new Set<Id>();
		List<RMA__c> rmaList = new List<RMA__c>();
		List<RMA__c> shippingRmaList = new List<RMA__c>();
		List<Refund__c> insertList = new List<Refund__c>();
		Map<Id,RMA__c> rmaChangedOrderMap = new Map<Id,RMA__c>();
		Set<Id> totalRefundCheck = new Set<Id>();
		Map<Id,Id> orderToRefundMap = new Map<Id,Id>();
		
		for(RMA__c rma : trgNew)
		{
			if(!String.isEmpty(rma.Opportunity__c))
				totalRefundCheck.add(rma.Opportunity__c);
		}
		for(Refund__c refund : [SELECT Id,OrderNumber__c FROM Refund__c WHERE RecordType.DeveloperName='TotalOrderRefund' AND OrderNumber__c in :totalRefundCheck])
		{
			if(!orderToRefundMap.containsKey(refund.OrderNumber__c))
				orderToRefundMap.put(refund.OrderNumber__c,refund.Id);
		}
					
		for(RMA__c rma : trgNew)
		{			
			if((rma.Triage_Code__c==null||rma.Triage_Code__c=='') || (rma.Customer_Induced_Damage__c==null||rma.Customer_Induced_Damage__c=='') && rma.Status__c=='Received')
			{
				rma.Triage_Code__c='A';
				rma.Customer_Induced_Damage__c='N';
			}
			
			if((rma.GoogleOrderType__c!='Advanced Replacement') &&
			(rma.Status__c=='Received') &&
			(rma.Triage_Code__c=='A'||rma.Triage_Code__c=='B') &&
			(rma.Customer_Induced_Damage__c=='Y'||rma.Customer_Induced_Damage__c=='N') &&
			(rma.GoogleOrderType__c=='Standard Order') &&
			(rma.Type__c=='Buyer\'s Remorse' || rma.Type__c=='Warranty Refund'))
			{
				if(orderToRefundMap.containsKey(rma.Opportunity__c))
				{
					rma.Status__c='Closed';
					continue;
				}
				//check to see if the Order has changed, if so, lookup the Opportunity for the changed values
				if(rma.Opportunity__c!=trgOldMap.get(rma.Id).Opportunity__c)
				{
					//System.debug('COUNTRY:::::::::'+rma.GoogleOrderCountry__c);
					//System.debug('STATE:::::::::'+rma.GoogleOrderStateProvince__c);
					rmaChangedOrderMap.put(rma.Id,rma);
				}
				countrySet.add(rma.GoogleOrderCountry__c);
				stateSet.add(rma.GoogleOrderStateProvince__c);
				rmaList.add(rma);
				
			}
			if((rma.GoogleOrderType__c!='Advanced Replacement') &&
			(rma.Status__c=='Received') &&
			(rma.Triage_Code__c=='A'||rma.Triage_Code__c=='B'||rma.Triage_Code__c=='C'||rma.Triage_Code__c=='D') &&
			//(rma.RMA_Category__c=='Physical Defect')&&
			(rma.Type__c=='Buyer\'s Remorse'||rma.Type__c=='Warranty Refund'))
			{	
				if(orderToRefundMap.containsKey(rma.Opportunity__c))
				{
					rma.Status__c='Closed';
					continue;
				}			
				shippingRmaList.add(rma);
				shippingCountrySet.add(rma.GoogleOrderCountry__c);
				shippingStateSet.add(rma.GoogleOrderStateProvince__c);
				orderIdSet.add(rma.Opportunity__c);				
			}			
		}
		if(rmaList.size()>0)
			insertList.addAll(processLineItemRefund(rmaList,countrySet,stateSet,trgNewMap,rmaChangedOrderMap));
		if(shippingRmaList.size()>0)
			insertList.addAll(processShippingRefund(shippingRmaList,orderIdSet,rmaChangedOrderMap,shippingCountrySet,shippingStateSet));
		if(insertList.size()>0)
			insert insertList;				
	}
	private static List<Refund__c> processShippingRefund(List<RMA__c> rmaList,Set<Id> orderIdSet,Map<Id,RMA__c> rmaChangedOrderMap,Set<String> shippingCountrySet, Set<String> shippingStateSet)
	{
		Set<Id> shippingOrderIdSet = new Set<Id>();
		List<Refund__c> refundList = new List<Refund__c>();
		Id shippingRtId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Shipping Refund').getRecordTypeId();
		//Set<String> countrySet = new Set<String>();
		Map<String,ItemTaxRefundValues__c> keyMap = new Map<String, ItemTaxRefundValues__c>();
		//for(ItemTaxRefundValues__c item : [SELECT State__c,Country__c FROM ItemTaxRefundValues__c WHERE Country__c in :shippingCountrySet AND (State__c IN :shippingStateSet OR State__c=null) AND RefundShipping__c='Always'])
		//	countrySet.add(item.State__c+item.Country__c);
		String key='';
		for(List<ItemTaxRefundValues__c> itemList : [SELECT State__c,Country__c FROM ItemTaxRefundValues__c WHERE Country__c in :shippingCountrySet AND (State__c IN :shippingStateSet OR State__c=null) AND RefundShipping__c='Always'])
		{
			for(ItemTaxRefundValues__c item : itemList)
			{
				if(!keyMap.containsKey(item.State__c+item.Country__c))
					keyMap.put(item.State__c+item.Country__c, item);				
			}
		}
		for(Refund__c refund : [SELECT Id,OrderNumber__c FROM Refund__c WHERE OrderNumber__c=:orderIdSet AND RecordTypeId=:shippingRtId])
			shippingOrderIdSet.add(refund.OrderNumber__c);
		for(RMA__c rma : rmaList)
		{
			//only create a refund if there isn't one created and the category must be physical defect or the rule must be Always in the tax table
			if(shippingOrderIdSet.contains(rma.Opportunity__c))
				continue;		
			key = rma.GoogleOrderStateProvince__c+rma.GoogleOrderCountry__c;
			if(!keyMap.containsKey(key))
			{
				key=null+rma.GoogleOrderCountry__c;
				if(rma.RMA_Category__c!='Physical Defect' && !keyMap.containsKey(key))
					continue;
			}
			
			rma.Status__c='Refund Pending';
			Refund__c refund = new Refund__c();
			refund.OrderNumber__c=rmaChangedOrderMap.containsKey(rma.Id)==true?rmaChangedOrderMap.get(rma.Id).Opportunity__c:rma.Opportunity__c;
			refund.PercentShippingItemRefund__c=100;
			refund.PercentShippingTaxRefund__c=100;
			refund.RefundStatus__c='Pending';
			refund.ExpectedRefundDate__c=BusinessDays.addBusinessDays(Date.today(),3);
			refund.RecordTypeId=shippingRtId;
			refund.RefundReason__c='Shipping';
			refundList.add(refund);
			//add the order number to set to make sure records in the same batch don't trigger multiple shipping refunds
			shippingOrderIdSet.add(refund.OrderNumber__c);
		}
		return refundList;
	}
	private static List<Refund__c> processLineItemRefund(List<RMA__c> rmaList, Set<String> countrySet, Set<String> stateSet,Map<Id,RMA__c> trgNewMap,Map<Id,RMA__c> rmaChangedOrderMap)
	{
		Map<String,ItemTaxRefundValues__c> keyMap = new Map<String, ItemTaxRefundValues__c>();
		for(List<ItemTaxRefundValues__c> taxRefundList : [SELECT Id,TaxRule__c,ItemA__c,ItemB__c,Country__c,State__c FROM ItemTaxRefundValues__c WHERE Country__c IN :countrySet AND (State__c IN :stateSet OR State__c=null)])
		{
			for(ItemTaxRefundValues__c taxRefund : taxRefundList)
			{
				if(!keyMap.containsKey(taxRefund.State__c+taxRefund.Country__c))
					keyMap.put(taxRefund.State__c+taxRefund.Country__c, taxRefund);				
			}
		}		
		Id rtId = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Line-Item Refund').getRecordTypeId();
		List<Refund__c> refundList = new List<Refund__c>();
		String key='';
		ItemTaxRefundValues__c refundValue;
		//query for the rma list so we can determine if it already has a refund created
		rmaList = [Select r.Triage_Code__c, r.Opportunity__c, r.Line_Number__c, r.Id, r.GoogleOrderStateProvince__c, r.GoogleOrderCountry__c, r.Google_Asset_Line_Number__c, (Select Id From Refunds__r) From RMA__c r where r.Id in :rmaList];
		for(RMA__c rma : rmaList)
		{
			if(rma.Google_Asset_Line_Number__c==null)
			{
				trgNewMap.get(rma.Id).addError('There is no associated asset line number to the RMA');
				continue;
			}
			//if the refund has already been created, don't create a new one
			if(rma.Refunds__r!=null && rma.Refunds__r.size()!=0)	
				continue;
			key=trgNewMap.get(rma.Id).GoogleOrderStateProvince__c+trgNewMap.get(rma.Id).GoogleOrderCountry__c;
			if(!keyMap.containsKey(key))
			{
				key=null+trgNewMap.get(rma.Id).GoogleOrderCountry__c;				
				if(!keyMap.containsKey(key))
				{
					//trgNewMap.get(rma.Id).addError(' No associated tax value was found');
					//continue;
					key=null;
					keyMap.put(key,new ItemTaxRefundValues__c(TaxRule__c='prorated',ItemA__c=100,ItemB__c=100));
				}
			}
			trgNewMap.get(rma.Id).Status__c='Refund Pending';
			refundValue = keyMap.get(key);			
			Refund__c refund = new Refund__c();
			refund.OrderNumber__c = rmaChangedOrderMap.containsKey(rma.Id)==true?rmaChangedOrderMap.get(rma.Id).Opportunity__c:rma.Opportunity__c;
			refund.LineNumber__c = String.valueOf(rma.Google_Asset_Line_Number__c);				
			refund.PercentItemRefund__c=trgNewMap.get(rma.Id).Triage_Code__c=='A' ? refundValue.ItemA__c : refundValue.ItemB__c;
			refund.RefundReason__c='RMA';
			if(refund.PercentItemRefund__c!=100)
			{
				if(refundValue.TaxRule__c.equalsIgnoreCase('none'))
					refund.PercentTaxRefund__c=0;
				else if(refundValue.TaxRule__c.equalsIgnoreCase('prorated'))
					refund.PercentTaxRefund__c=trgNewMap.get(rma.Id).Triage_Code__c=='A' ? refundValue.ItemA__c : refundValue.ItemB__c;
				else if(refundValue.TaxRule__c.equalsIgnoreCase('full'))
					refund.PercentTaxRefund__c=100;
			}
			else
				refund.PercentTaxRefund__c=100;			
			refund.RMA__c = rma.Id;
			refund.RefundStatus__c = 'Pending';
			refund.ExpectedRefundDate__c = Date.today().addDays(1);
			refund.RecordTypeId = rtId;
			refundList.add(refund);
		}
		return refundList;
	}	
}