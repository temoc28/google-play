public without sharing class RefundTrgHandler 
{
	public static void onBeforeInsert(Refund__c[] trgNew)
	{
		//check that there aren't full refunds created for the order already, if so, block the creation
		Set<Id> oppIdSet = new Set<Id>();
		Set<Id> fullRefundIdSet = new Set<Id>();
		Map<Id,Refund__c> refundMap = new Map<Id,Refund__c>();
		Id totalOrderRt =Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Total Order Refund').getRecordTypeId();
		for(Refund__c refund : trgNew)
		{
			oppIdSet.add(refund.OrderNumber__c);
			if(refund.RecordTypeId==totalOrderRt && refund.RefundStatus__c != 'Failure - Closed')
			{
				if(!fullRefundIdSet.add(refund.OrderNumber__c))
					refund.addError('There are two total order refunds in this batch.');
			}
		}	
		for(Refund__c refund : [SELECT Id,OrderNumber__c FROM Refund__c WHERE RecordTypeId=:totalOrderRt AND OrderNumber__c in :oppIdSet AND RefundStatus__c != 'Failure - Closed'])
		{
			if(!refundMap.containsKey(refund.OrderNumber__c))
				refundMap.put(refund.OrderNumber__c,refund);
		}
		for(Refund__c refund : trgNew)
		{
			if(refundMap.containsKey(refund.OrderNumber__c))
				refund.addError('There is already a Total Order Refund created for this Order. Refund Id: '+refundMap.get(refund.OrderNumber__c).Id);
		}
	}
	public static void onBeforeUpdate(Refund__c[] trgNew,Map<Id,Refund__c> oldMap)
	{
		Set<Id> rmaIdSet = new Set<Id>();
		Refund__c oldRefund;
		for(Refund__c refund : trgNew)
		{
			oldRefund = oldMap.get(refund.Id);
			if(refund.RMA__c!=null && (refund.RefundStatus__c=='Success' || refund.RefundStatus__c=='Failure - Closed' && refund.RefundStatus__c!=oldRefund.RefundStatus__c))
				rmaIdSet.add(refund.RMA__c);
		}
		if(rmaIdSet.size()>0)
		{
			List<RMA__c> rmaList = new List<RMA__c>();			
			for(Id rmaId : rmaIdSet)
				rmaList.add(new RMA__c(Id=rmaId,Status__c='Closed'));
			
			if(rmaList.size()>0)
				update rmaList;
		}
	}
	public static void onAfterInsert(List<Refund__c> trgNew)
	{		
		Set<Id> orderIds = new Set<Id>();
		for(Refund__c ref : trgNew)
		{
		    orderIds.add(ref.OrderNumber__c);
		}
		
		// Get related RMAs as this relationship does not exist: ReplacementOrder__r.RMA__r
		List<RMA__c> rmas = [Select Opportunity__c, Name, ReplacementOrder__c, RCL_Verify_ID__c FROM RMA__c where Opportunity__c in : orderIds];		
		Map<Id, List<RMA__c>> relatedOrderRmas = new Map<Id, List<RMA__c>>();
		for(RMA__c rma : rmas)
		{
		    List<RMA__c> newRmas = new List<RMA__c>();
		    if(relatedOrderRmas.get(rma.Opportunity__c) == null)
		    {
		        newRmas.add(rma);
		    }
		    else
		    {
		         newRmas = relatedOrderRmas.get(rma.Opportunity__c);
		         newRmas.add(rma);
		    }
		    relatedOrderRmas.put(rma.Opportunity__c, newRmas);
		}
		
		Set<Id> oppIdSet = new Set<Id>();
		List<Refund__c> refList = new List<Refund__c>();
		
		Id totalOrderRefundId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TotalOrderRefund' AND sObjectType='Refund__c' LIMIT 1].Id;
		
		Set<Id> rmasToUpdate = new Set<Id>();
		
		for(Refund__c ref : trgNew)
		{
			if((ref.RefundReason__c=='Undeliverable'||ref.RefundReason__c=='Lost/Stolen')&&String.isNotEmpty(ref.OrderNumber__c))
			{
				oppIdSet.add(ref.OrderNumber__c);
				refList.add(ref);
			}
			
			List<RMA__c> refundOrderRmas = relatedOrderRmas.get(ref.OrderNumber__c);
			if(refundOrderRmas != null)
			{
				//if total order refund then allow cancel on RMA
				if(ref.RecordTypeId == totalOrderRefundId)
				{
					for(RMA__c rma : refundOrderRmas)
					{					
						if(rma.ReplacementOrder__c != null)
						{
							rmasToUpdate.add(rma.Id);
						}
						else if(String.isNotEmpty(ref.OrderNumber__r.Name) && String.isNotEmpty(rma.RCL_Verify_ID__c))
						{
							if(ref.OrderNumber__r.Name == rma.RCL_Verify_ID__c)
							{
								rmasToUpdate.add(rma.Id);
							}
						}
					}
				}
			}
		}
		
		//update rmas with checkbox
		List<RMA__c> rmaUpdateList = new List<RMA__c>();
		
		for(Id i : rmasToUpdate){
			RMA__c rma = new RMA__c(Id = i);
			rma.AllowCancel__c = true;
			rmaUpdateList.add(rma);
		}
		
		try{
			update rmaUpdateList;
		}
		catch(DMLException dmle)
		{
			for(Refund__c ref : trgNew)
			{
				ref.addError(dmle.getMessage());
			}
			System.debug(dmle);
		}
		
		if(!oppIdSet.isEmpty())
		{
			/*
			The undeliverable refunds are associated with an Opportunity, which in turn have children assets, some of which may be Glass devices. 
			There is no direct link between the refund and the asset for an Undeliverable refund, since it can apply to an order that had multiple assets.
			*/
			Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id,(SELECT Id FROM Google_Assets__r WHERE ProductFamily__c='Glass') FROM Opportunity WHERE Id in :oppIdSet]);
			Set<Id> glassIdSet = new Set<Id>();
			for(Refund__c ref : refList)
			{
				if(oppMap.containsKey(ref.OrderNumber__c))
				{
					for(Google_Asset__c ga : oppMap.get(ref.OrderNumber__c).Google_Assets__r)
						glassIdSet.add(ga.Id);
				}
			}
			if(!glassIdSet.isEmpty())
			{
				//send the update to glass
				PlayToGlass.refundedGa(glassIdSet);
			}
		}
	}
	public static void onAfterUpdate(List<Refund__c> trgNew,Map<Id,Refund__c> oldMap)
	{		
		Set<Id> oppIdSet = new Set<Id>();
		List<Refund__c> refList = new List<Refund__c>();
		Refund__c oldRef;
		
		Id totalOrderRefundId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TotalOrderRefund' AND sObjectType='Refund__c' LIMIT 1].Id;
		Set<Id> rmasToCheck = new Set<Id>();
		Set<Id> rmasToUncheck = new Set<Id>();
		
		for(Refund__c ref : trgNew)
		{
			oldRef = oldMap.get(ref.Id);
			if(((ref.RefundReason__c=='Undeliverable'||ref.RefundReason__c=='Lost/Stolen')&&(oldRef.RefundReason__c!='Undeliverable'||oldRef.RefundReason__c!='Lost/Stolen')) && String.isNotEmpty(ref.OrderNumber__c))
			{
				oppIdSet.add(ref.OrderNumber__c);
				refList.add(ref);
			}
			
			if(ref.RecordTypeId != oldRef.RecordTypeId && ref.RecordTypeId == totalOrderRefundId && ref.RMA__c != null){
				rmasToCheck.add(ref.RMA__c);
			}
			else if(ref.RecordTypeId != oldRef.RecordTypeId && oldref.RecordTypeId == totalOrderRefundId && ref.RMA__c != null){
				rmasToUnCheck.add(ref.RMA__c);
			}
		}
		
		
		//update rmas with checkbox
		List<RMA__c> rmaUpdateList = new List<RMA__c>();
				
		for(Id i : rmasToCheck){
			RMA__c rma = new RMA__c(Id = i);
			rma.AllowCancel__c = true;
			rmaUpdateList.add(rma);
		}
		
		for(Id i : rmasToUncheck){
			RMA__c rma = new RMA__c(Id = i);
			rma.AllowCancel__c = false;
			rmaUpdateList.add(rma);
		}
		
		try{
			update rmaUpdateList;
		}
		catch(DMLException dmle){
			System.debug(dmle);
		}
		
		if(!oppIdSet.isEmpty())
		{
			/*
			The undeliverable refunds are associated with an Opportunity, which in turn have children assets, some of which may be Glass devices. 
			There is no direct link between the refund and the asset for an Undeliverable refund, since it can apply to an order that had multiple assets.
			*/
			Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id,(SELECT Id FROM Google_Assets__r WHERE ProductFamily__c='Glass') FROM Opportunity WHERE Id in :oppIdSet]);
			Set<Id> glassIdSet = new Set<Id>();
			for(Refund__c ref : refList)
			{
				if(oppMap.containsKey(ref.OrderNumber__c))
				{
					for(Google_Asset__c ga : oppMap.get(ref.OrderNumber__c).Google_Assets__r)
						glassIdSet.add(ga.Id);
				}
			}
			if(!glassIdSet.isEmpty())
			{
				//send the update to glass
				PlayToGlass.refundedGa(glassIdSet);
			}
		}
	}
}