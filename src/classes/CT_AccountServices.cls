public with sharing class CT_AccountServices {

    //@author : CloudTrigger, Inc.
	//@date : 10/19/2012
	//@description : method 
	// This logic is to circumvent a cross-object formula limitation introduced by Person Accounts.  This populates a lookup
	// to the mirrored Contact record for a Person Account so that the fields are all accessible with a cross-object formula 
	//@paramaters : collection of Accounts 
	//@returns : nothing
	public static void CreatePersonAccountContactLookup(list<Account> theAccounts){
		Set<String> accIds = new Set<String>();
 
		for(Account a : theAccounts) {
			if(a.PersonAcctContactLookup__c == null && a.PersonContactId != null) {
				accIds.add(a.Id);
			}
		}
		list<Account> accs = [SELECT Id, PersonContactId, PersonAcctContactLookup__c FROM Account WHERE Id IN :accIds];
		for(Account a : accs) {
			a.PersonAcctContactLookup__c = a.PersonContactId;
		}
 
		if(!accs.isEmpty()) {
			update accs;
		}
	}
	@isTest(SeeAllData=true)
    static void myUnitTest() {
        Account A = new Account (Name = 'test');
        insert A;
    }
}