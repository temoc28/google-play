/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestProduct2Trg 
{
    static testMethod void myUnitTest() 
    {
        Product_Family__c pf = new Product_Family__c(Name='test');
        insert pf;
        Document__c d = new Document__c(Name='test',Product_Family__c=pf.Id);
        insert d;
        Country__c c = new Country__c(Name='US');
        insert c;
        Country__c c2 = new Country__c(Name='JP');
        insert c2;
        Product2 p2 = new Product2();
        p2.Name='Test';
        p2.WarrantyCountries__c='US;';
        p2.Document__c=d.Id;
        p2.IsActive=true;
        p2.SKU__c='abcxyz123';
        insert p2;
        
        p2.WarrantyCountries__c='JP;US;';
        update p2;
    }
}