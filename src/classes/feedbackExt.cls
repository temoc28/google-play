// Copyright 2012 Google Inc. All Rights Reserved.

/**
 * Custom controller class for Visualforce page used by customers to submit support case feedback.
 *
 * @author jongaulding@google.com (Jon Gaulding)
 */
public class feedbackExt {
  // Map for connecting the URL-encoded language to the Google help center thank you page in the
  // correct language.
  public static final Map<String,String> THANK_YOU_PAGES_MAP =
      new Map<String,String>{'en' => 'en',
                             'fr' => 'fr',
                             'nl_NL' => 'nl',
                             'de' => 'de',
                             'ja' => 'ja',
                             'it' => 'it',
                             'es' => 'es',
                             'pt_BR' => 'pt-BR',
                             'ru' => 'ru'};
  
  public static final Set<String> NOT_SAT_REASON_FIELDS = new Set<String>{
      'ns_unres__c', 'ns_sdiff__c', 'ns_squal__c', 'ns_istat__c', 'ns_resol__c', 'ns_other__c'};
  
  private final String DEFAULT_LANGUAGE_CODE = 'en';
  
  private boolean currUserIsAuthorized;
  private String languageCode;
  private Case associatedCase;
  private final Customer_Feedback__c customerFeedback;  
  
  public Case c {
    get {
      return associatedCase;
    }
    set;
  }
  
  public String messageStyle {get; set;}

  // Constructor
  public feedbackExt(ApexPages.StandardController stdController) {
    String providedCaseId = ApexPages.CurrentPage().getParameters().get('cid');
    String providedCaseSecret = ApexPages.CurrentPage().getParameters().get('cs');
    languageCode = ApexPages.CurrentPage().getParameters().get('lang');
    
    // set default empty strings if needed
    providedCaseId = providedCaseId == null ? '' : providedCaseId;
    providedCaseSecret = providedCaseSecret == null ? '' : providedCaseSecret;
    
    languageCode = THANK_YOU_PAGES_MAP.containsKey(languageCode) ?
        THANK_YOU_PAGES_MAP.get(languageCode) : DEFAULT_LANGUAGE_CODE;
    
    customerFeedback = (Customer_Feedback__c) stdController.getRecord();
    
    Case[] matchingCases = [SELECT Id, CaseNumber, Subject, Case_Secret_String__c
                            FROM Case
                            WHERE Id=:providedCaseId];
    
    currUserIsAuthorized =
        matchingCases.size() == 1 &&
        matchingCases.get(0).Case_Secret_String__c != null &&
        matchingCases.get(0).Case_Secret_String__c != '' &&
        providedCaseSecret.equals(matchingCases.get(0).Case_Secret_String__c);
    
    associatedCase = currUserIsAuthorized ? matchingCases.get(0) : null;
  }
  
  /* Inserts the feedback if the current user if authorized, or otherwise refers the user to the
   * feedback problem page.
   */
  public pageReference save() {
    if (!currUserIsAuthorized) {
      PageReference problemPage = Page.feedbackproblem;
      problemPage.getParameters().put('lang', languageCode);
      return problemPage;
    }
    
    try {
    	
      if(ApexPages.currentPage().getUrl().contains('ChromebookWelcome'))
      {
      	customerFeedback.Type__c = 'Warm Welcome';
      }
      else   
      {
       	customerFeedback.Type__c = 'ChromeSurvey';
      }	
      
      // Fetch prior feedback for deletion
      Customer_Feedback__c[] previousFeedback = [SELECT Id
                                                 FROM Customer_Feedback__c
                                                 WHERE Case__c=:associatedCase.id];
      		
      customerFeedback.Case__c = c.Id;
      
      if(ApexPages.currentPage().getUrl().contains('ChromebookWelcome'))
      {
      	customerFeedback.Type__c = 'Warm Welcome';
      }
      else
      if(ApexPages.currentPage().getUrl().contains('CrOS'))
      {
      	customerFeedback.Type__c = 'Chrome Survey';
      }
      
      insert customerFeedback;
      
      delete previousFeedback;
      
      String redirectUrl =
          'http://www.google.com/support/enterprise/bin/answer.py?answer=138860&hl='
              + languageCode;
      PageReference helpCenterThanksPage = new PageReference(redirectUrl);
      helpCenterThanksPage.setRedirect(true);
      return helpCenterThanksPage;
        
    } catch (DMLException e) {
      ApexPages.addMessages(e);
      messageStyle = 'background-color:#FDD;background-image:'
          + 'url(/resource/1281805104000/AssetPortalNo);border-color:#900;';
      return ApexPages.CurrentPage();
    }
  }
}