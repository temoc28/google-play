public with sharing class CaseTriggerHandler 
{
	public static void onBeforeUpdate(Case[] trgNewCase, Map<Id,Case> oldMap)
	{
		BusinessHours defaultHours = [select Id from BusinessHours where IsDefault=true];
		RecordType rt = [SELECT Id FROM RecordType where SobjectType='Case' and Name = 'ChromeOs'];
        for (Case updatedCase: trgNewCase) 
        {	
           	//only process ChromeOs record types
           	if(rt.Id!=updatedCase.RecordTypeId)
           		continue;
           	Case oldCase = oldMap.get(updatedCase.Id);
           	if (oldCase.Status!=updatedCase.Status && updatedCase.LastStatusChange__c!=null) 
           	{
                //OK, the status has changed
                if (!oldCase.IsClosed) 
                {
                    //We only update the buckets for open cases
                    //On the off-chance that the business hours on the case are null, use the default ones instead
                    Id hoursToUse = updatedCase.BusinessHoursId!=null?updatedCase.BusinessHoursId:defaultHours.Id;
                    //The diff method comes back in milliseconds, and we want minutes, so we divide by 60000.
                    Long timeSinceLastStatus = BusinessHours.diff(hoursToUse, updatedCase.LastStatusChange__c, System.now())/60000;
                    
                    if(updatedCase.Status=='Assigned')
                    	updatedCase.Time_Assigned__c=System.now();
                    if(updatedCase.Status=='Waiting on Customer Reply' && updatedCase.TimeUntilFirstResponse__c==0) 
                    	updatedCase.TimeUntilFirstResponse__c=BusinessHours.diff(hoursToUse, updatedCase.CreatedDate, System.now())/60000;
                    if(updatedCase.Status=='Assigned' && updatedCase.TimeUntilAssigned__c == 0) 
                    	updatedCase.TimeUntilAssigned__c=BusinessHours.diff(hoursToUse, updatedCase.CreatedDate, System.now())/60000;
                    
                    if(oldCase.Status=='Waiting on Third Party') 
                    	updatedCase.TimeSpentWaitingonThirdParty__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='Waiting on Customer Reply') 
                    	updatedCase.TimeSpentWaitingonCustomerReply__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='Waiting on Customer Action') 
                    	updatedCase.TimeSpentWaitingonCustomerAction__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='Solution Provided') 
	                    updatedCase.TimeSpentSolutionProvided__c+=timeSinceLastStatus;
	                else if(oldCase.Status=='New') 
                    	updatedCase.TimeSpentNew__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Verizon Support') 
                    	updatedCase.TimeSpentInProgressVerizonSupport__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Samsung Support') 
                    	updatedCase.TimeSpentInProgressSamsungSupport__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Quanta Support') 
                    	updatedCase.TimeSpentInProgressQuantaSupport__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Google Support') 
                    	updatedCase.TimeSpentInProgressGoogleSupport__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Google Other') 
                    	updatedCase.TimeSpentInProgressGoogleOther__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Google Engineering') 
                    	updatedCase.TimeSpentInProgressGoogleEngineer__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='In Progress - Acer Support') 
                    	updatedCase.TimeSpentInProgressAcerSupport__c+=timeSinceLastStatus;
                    else if(oldCase.Status=='Assigned') 
                    	updatedCase.TimeSpentAssigned__c+=timeSinceLastStatus; 
                }
                updatedCase.LastStatusChange__c = System.now();
            }
         }
	}
	public static void onBeforeInsert(Case[] trgNewCase)
	{
		 for (Case c : trgNewCase) 
		 {
            c.LastStatusChange__c = System.now();
            c.TimeUntilFirstResponse__c = 0;
            c.TimeUntilAssigned__c = 0;
            c.TimeSpentWaitingonThirdParty__c = 0;
            c.TimeSpentWaitingonCustomerReply__c = 0;
            c.TimeSpentWaitingonCustomerAction__c = 0;
            c.TimeSpentSolutionProvided__c = 0;
            c.TimeSpentNew__c = 0;
            c.TimeSpentInProgressVerizonSupport__c = 0;
            c.TimeSpentInProgressSamsungSupport__c = 0;
            c.TimeSpentInProgressQuantaSupport__c = 0;
            c.TimeSpentInProgressGoogleSupport__c = 0;
            c.TimeSpentInProgressGoogleOther__c = 0;
            c.TimeSpentInProgressGoogleEngineer__c = 0;
            c.TimeSpentInProgressAcerSupport__c = 0;
            c.TimeSpentAssigned__c = 0;
        }
	}
}