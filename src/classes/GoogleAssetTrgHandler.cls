public without sharing class GoogleAssetTrgHandler 
{
   public static void onBeforeInsert(Google_Asset__c[] trgNew)
   {
      Map<String, Google_Asset__c> googleAssetMap = new Map<String, Google_Asset__c>();
      Set<String> skuSet = new Set<String>();
      List<Google_Asset__c> warrantyList = new List<Google_Asset__c>();
      Set<Id> advancedReplacementSet = new Set<Id>();
      Set<String> saleCountrySet = new Set<String>();
      for (Google_Asset__c ga : trgNew) 
      {
         if (ga.Name != null) 
         {
            if (googleAssetMap.containsKey(ga.Name)) 
                ga.Name.addError('Another new Google Asset has the same name.');
            else 
                googleAssetMap.put(ga.Name, ga);                    
        }
        ga.Google_Asset_ID_External_ID__c = ga.Name;
     }
     for (Google_Asset__c ga : [SELECT Name FROM Google_Asset__c WHERE Name IN :googleAssetMap.KeySet()]) 
     {
        Google_Asset__c newGoogleAsset = googleAssetMap.get(ga.Name);
        newGoogleAsset.Name.addError('A Google Asset with this name address already exists.');
     }            
     //put the SKUUpsert value in sku if sku is null
     for(Google_Asset__c ga : trgNew)
     {
        if(ga.SKU__c == null && ga.SKUUpsert__c != null)
           ga.SKU__c = ga.SKUUpsert__c;
            
        if(ga.SKU__c!=null)
          skuSet.add(ga.SKU__c);
         
        if(String.isNotEmpty(ga.Sale_Country__c))
        	saleCountrySet.add(ga.Sale_Country__c);
     }
     if(!skuSet.isEmpty())
     {
        Map<String, Product2> pMap = new Map<String,Product2>();
        Map<String, Product2> productMapWithKeyId = new Map<String,Product2>();
        //Map<String, CountryRules__c> countryRulesMap = CountryRules__c.getAll();
        for(Product2 p:[Select Id,Document__c,SKU__c,RemorsePeriod__c,DOAPeriod__c,MinimumStandardWarranty__c,MinimumReplacementWarranty__c,ExpectedSalesDate__c,Expected_Sale_Period__c,Retail_Warranty__c,(Select Name,Country__c, Document__c, Remorse_Period__c, DOA_Period__c, Warranty_Period__c,Expected_Sale_Period__c,Minimum_Replacement_Warranty_Period__c From Country_Variants__r WHERE Name in :saleCountrySet) from Product2 where SKU__c in :skuSet]){
           pMap.put(p.SKU__c, p);
           productMapWithKeyId.put(p.Id, p);
        }
      
        for(Google_Asset__c ga : trgNew)
        {
           if(pMap.containsKey(ga.SKU__c))
           {
              ga.ProductID__c=pMap.get(ga.SKU__c).Id;
          	  //if it's an advanced replacement let the WarrantyHandler handle the transfer of the warranty
              if(String.isNotEmpty(ga.Sale_Country__c))
              {
                 warrantyList.add(ga);
                //handleEntitlement(pMap,countryRulesMap,ga);
                if(ga.Order_Type__c=='Advanced Replacement')
                  advancedReplacementSet.add(ga.Order_Opportunity__c);
              }
           }
       }
       if(!warrantyList.isEmpty())
          handleEntitlement(pMap,/*countryRulesMap,*/warrantyList,advancedReplacementSet,productMapWithKeyId);
     }    
  }
  
  public static void onBeforeUpdate(Google_Asset__c[] trgNew,Map<Id,Google_Asset__c> oldMap)
  {
      Map<String, Google_Asset__c> googleAssetMap = new Map<String, Google_Asset__c>();      
      Set<String> skuSet = new Set<String>();
      Google_Asset__c oldGa;
      List<Google_Asset__c> warrantyList = new List<Google_Asset__c>();
      Set<Id> advancedReplacementSet = new Set<Id>();
      Set<String> saleCountrySet = new Set<String>();
      for(Google_Asset__c ga : trgNew)
      {
         if(ga.Name!=null && ga.Name != oldMap.get(ga.Id).Name)
         {
            ga.Google_Asset_ID_External_ID__c = ga.Name;
            if (googleAssetMap.containsKey(ga.Name)) 
               ga.Name.addError('Another new Google Asset has the same name.');
            else 
               googleAssetMap.put(ga.Name, ga);
         }
      }
      if(!googleAssetMap.isEmpty())
      {
          for (Google_Asset__c ga : [SELECT Name FROM Google_Asset__c WHERE Name IN :googleAssetMap.KeySet()]) 
          {
             Google_Asset__c newGoogleAsset = googleAssetMap.get(ga.Name);
             newGoogleAsset.Name.addError('A Google Asset with this name address already exists.');
          }
      }
      for(Google_Asset__c ga:trgNew)
      {
         oldGa = oldMap.get(ga.Id);
         if(ga.SKU__c == null && ga.SKUUpsert__c != null) 
         {
            ga.SKU__c = ga.SKUUpsert__c;
            //gaList.add(ga);                    
         } 
         //only update the ProductID__c field if the sku changed
         if ((ga.SKU__c != null && ga.SKU__c!=oldGa.SKU__c ) || ga.ProductID__c!=oldGa.ProductID__c || ga.Order_Opportunity__c!=oldGa.Order_Opportunity__c || ga.Retail_Sale_Date__c!=oldGa.Retail_Sale_Date__c)
         {         
             skuSet.add(ga.SKU__c); 
         
         	if(String.isNotEmpty(ga.Sale_Country__c))
        		saleCountrySet.add(ga.Sale_Country__c);
         }
      }
      if(!skuSet.isEmpty())
      {
          Map<String, Product2> pMap = new Map<String,Product2>();
          Map<String, Product2> productMapWithKeyId = new Map<String,Product2>();
          //Map<String, CountryRules__c> countryRulesMap = CountryRules__c.getAll();
          
         for(Product2 p:[Select Id,Document__c,SKU__c,RemorsePeriod__c,DOAPeriod__c,MinimumStandardWarranty__c,MinimumReplacementWarranty__c,ExpectedSalesDate__c,Expected_Sale_Period__c,Retail_Warranty__c,(Select Name,Country__c, Document__c, Remorse_Period__c, DOA_Period__c, Warranty_Period__c,Minimum_Replacement_Warranty_Period__c,Expected_Sale_Period__c From Country_Variants__r WHERE Name in :saleCountrySet) from Product2 where SKU__c in :skuSet]){
           pMap.put(p.SKU__c, p);
           productMapWithKeyId.put(p.Id, p);
         }
      
         for(Google_Asset__c ga : trgNew)
         {
            ga.ProductID__c=null;
        	oldGa = oldMap.get(ga.Id);
        	if(pMap.containsKey(ga.SKU__c))
        	{
          		ga.ProductID__c=pMap.get(ga.SKU__c).Id;
          		//if it's an advanced replacement let the WarrantyHandler handle the transfer of the warranty
          		//we only care that the  Sale_Country__c and ProductID__c is not blank and it's not Advanced Replacement
          		//if not blank, check to see if entitlements have been applied, if so, check to see if the product ID, Google Order ID or retail sale date
          		//has changed
          		if(String.isNotEmpty(ga.Sale_Country__c))
          		{
            		warrantyList.add(ga);
            		//handleEntitlement(pMap,countryRulesMap,ga);
            		if(ga.Order_Type__c=='Advanced Replacement')
              			advancedReplacementSet.add(ga.Order_Opportunity__c);
          		}  
        	}
      }
      if(!warrantyList.isEmpty())
        handleEntitlement(pMap,/*countryRulesMap,*/ warrantyList,advancedReplacementSet,productMapWithKeyId);
    }        
  }
  
   public static void onAfterInsert(List<Google_Asset__c> trgNew)
   {
      Set<Id> gaIdSet = new Set<Id>();
	  for(Google_Asset__c ga : trgNew)
	  {	
			if(ga.ProductFamily__c=='Glass' && String.isNotEmpty(ga.Order_Opportunity__c) && String.isNotEmpty(ga.AssetOwner__c)/*&& String.isEmpty(ga.GlassErrorMessage__c)&& !ga.GlassSent__c */)
				gaIdSet.add(ga.Id);
		}
		if(!gaIdSet.isEmpty())
			PlayToGlass.sendDataFuture(gaIdSet);		
	}
	
    public static void onAfterUpdate(List<Google_Asset__c> trgNew,Map<Id,Google_Asset__c> oldMap)
	{
		Set<Id> gaIdSet = new Set<Id>();
		for(Google_Asset__c ga : trgNew)
		{
			if(ga.ProductFamily__c=='Glass' && String.isNotEmpty(ga.Order_Opportunity__c) && String.isNotEmpty(ga.AssetOwner__c) && String.isEmpty(ga.GlassErrorMessage__c)&& (!ga.GlassSent2__c || (ga.Order_Opportunity__c!=oldMap.get(ga.Id).Order_Opportunity__c)))
				gaIdSet.add(ga.Id);
		}
		if(!gaIdSet.isEmpty())
		{
			integer sendSize=gaIdSet.size();
			Map<String,GlassIntegrationBatchSize__c> gibMap = GlassIntegrationBatchSize__c.getAll();
			integer sizeLimit=Integer.valueOf(gibMap.get('sendDataFuture').BatchSize__c);
			Set<Id> sendIdSet = new Set<Id>();
			integer currentCount=0;
			integer totalCount=0;
			for(Id gaId : gaIdSet)
			{
				sendIdSet.add(gaId);
				currentCount++;
				totalCount++;			    
				if(sendIdSet.size()==sizeLimit || totalCount==sendSize)
				{
					PlayToGlass.sendDataFuture(sendIdSet);
					sendIdSet.clear();
					currentCount=0;
				}
			}			
		}
	}
  private static void handleEntitlement(Map<String, Product2> pMap,/*Map<String, CountryRules__c> countryRulesMap,*/ List<Google_Asset__c> gaList,Set<Id> advancedReplacementSet,Map<String, Product2> productMapWithKeyId)
  {
    Product2 p;
    CountryRules__c cr;
    Map<Id,List<RMA__c>> replacementToRmaMap = new Map<Id,List<RMA__c>>();
    Date dToday = Date.today();
    if(!advancedReplacementSet.isEmpty())
    {
      for(RMA__c rma : [SELECT Id,Type__c,GoogleAsset__c,GoogleAsset__r.ProductID__c,GoogleAsset__r.Sale_Country__c,
      //fields below represent the asset that is new
      GoogleAsset__r.DOA_Period__c,GoogleAsset__r.Remorse_Period__c,GoogleAsset__r.Warranty_Period__c,GoogleAsset__r.Minimum_Replacement_Warranty_Period__c,GoogleAsset__r.Warranty_Expiration_Date__c,
      GoogleAsset__r.Expected_Sale_Days__c,Opportunity__c,ReplacementOrder__c    
      FROM RMA__c WHERE ReplacementOrder__c in :advancedReplacementSet AND GoogleAsset__c!=null AND Type__c='Warranty Regular'])
      {
        if(!replacementToRmaMap.containsKey(rma.ReplacementOrder__c))
          replacementToRmaMap.put(rma.ReplacementOrder__c,new List<RMA__c>());
        
        replacementToRmaMap.get(rma.ReplacementOrder__c).add(rma);
      }
    }
    
    for(Google_Asset__c ga : gaList)
    {
      if(pMap.containsKey(ga.SKU__c) && String.isNotEmpty(ga.Sale_Country__c))
      {
        p = pMap.get(ga.SKU__c);
        ga.ProductID__c=p.Id;
        /*ga.DOA_Period__c=String.isEmpty(String.valueOf(p.DOAPeriod__c))?0:p.DOAPeriod__c;
	        ga.Remorse_Period__c=String.isEmpty(String.valueOf(p.RemorsePeriod__c))?0:p.RemorsePeriod__c;
	        ga.Warranty_Period__c=String.isEmpty(String.valueOf(p.MinimumStandardWarranty__c))?0:p.MinimumStandardWarranty__c;
	        ga.Minimum_Replacement_Warranty_Period__c=String.isEmpty(String.valueOf(p.MinimumReplacementWarranty__c))?0:p.MinimumReplacementWarranty__c;
	        ga.Expected_Sale_Days__c=String.isEmpty(String.valueOf(p.ExpectedSalesDate__c))?0:p.ExpectedSalesDate__c;*/

        // Change flow to get data from CV to product for support CR0001
        ga.Warranty_Period__c = p.Retail_Warranty__c;
        ga.Expected_Sale_Days__c = p.Expected_Sale_Period__c;

        for(Country_Variant__c cv : p.Country_Variants__r)
        {
	        if(ga.Sale_Country__c==cv.Name)
	        {	        
	        	ga.DOA_Period__c=cv.DOA_Period__c;
	        	ga.Remorse_Period__c=cv.Remorse_Period__c;
	        	//ga.Warranty_Period__c=cv.Warranty_Period__c;
	        	ga.Minimum_Replacement_Warranty_Period__c=cv.Minimum_Replacement_Warranty_Period__c;
	        	//ga.Expected_Sale_Days__c=cv.Expected_Sale_Period__c;
	        }
        }
      }

      // Add Else condition for support CR0001
      else{
        //Expected_Sale_Period__c,Retail_Warranty__c
        p = productMapWithKeyId.get(ga.ProductID__c);
        ga.Warranty_Period__c = p.Retail_Warranty__c;
        ga.Expected_Sale_Days__c = p.Expected_Sale_Period__c;
      }
      

      /*if(countryRulesMap.containsKey(ga.Sale_Country__c))
      {
        cr = countryRulesMap.get(ga.Sale_Country__c);
        ga.DOA_Period__c=ga.DOA_Period__c>cr.DOAPeriod__c?ga.DOA_Period__c:cr.DOAPeriod__c;
        ga.Warranty_Period__c=ga.Warranty_Period__c>cr.MinimumStandardWarranty__c?ga.Warranty_Period__c:cr.MinimumStandardWarranty__c;
        ga.Remorse_Period__c=ga.Remorse_Period__c>cr.RemorsePeriod__c?ga.Remorse_Period__c:cr.RemorsePeriod__c;      
        ga.Minimum_Replacement_Warranty_Period__c=ga.Minimum_Replacement_Warranty_Period__c>cr.MinimumReplacementWarranty__c?ga.Minimum_Replacement_Warranty_Period__C:cr.MinimumReplacementWarranty__c;
        ga.Expected_Sale_Days__c=ga.Expected_Sale_Days__c>cr.ExpectedSalesDate__c?ga.Expected_Sale_Days__c:cr.ExpectedSalesDate__c;
      }*/
      if(ga.Order_Type__c=='Advanced Replacement')
      {        
        //attempt to transfer the warranty if the RMA__c.Type__c == Warranty Regular
        if(replacementToRmaMap.containsKey(ga.Order_Opportunity__c))
        {
          //If more than 1 result, then use the entitlement logic(note: this should never happen, but should be included to handle this error case)
          //If 1 result (as expected), transfer the longer of the warranty.
          List<RMA__c> rmaList = replacementToRmaMap.get(ga.Order_Opportunity__c);
          if(rmaList.size()==1)
          {
            ga.DOA_Period__c=-50;
            ga.Remorse_Period__c=-50;
            RMA__c rma = rmaList.get(0);
            integer originalRemaining=0;
            if(rma.GoogleAsset__r.Warranty_Expiration_Date__c!=null)
            	originalRemaining = dToday.daysBetween(rma.GoogleAsset__r.Warranty_Expiration_Date__c);
            ga.Warranty_Period__c=ga.Minimum_Replacement_Warranty_Period__c>originalRemaining?ga.Minimum_Replacement_Warranty_Period__c:originalRemaining;
          }
        }
      }
    }
  }  
}