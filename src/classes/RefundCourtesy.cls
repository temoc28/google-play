public with sharing class RefundCourtesy 
{
	Opportunity opp;
	public Refund__c refund {get;set;}
	public RefundCourtesy(ApexPages.StandardController con)
	{
		opp=(Opportunity)con.getRecord();
		refund = new Refund__c();
	}
	public PageReference createCourtesyRefund()
	{
		Id sAmountRefundRt = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Specific Amount Refund').getRecordTypeId();
		/*List<Refund__c> refundList = new List<Refund__c>([SELECT Id FROM Refund__c WHERE OrderNumber__c=:opp.Id AND RecordTypeId=:sAmountRefundRt]);
		if(refundList.size()>0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'This order already has a specific amount refund'));
			return null;
		}*/		
		refund.OrderNumber__c=opp.Id;		
		refund.RefundStatus__c='Pending';
		refund.ExpectedRefundDate__c=Date.today().addDays(1);
		refund.RecordTypeId=sAmountRefundRt;
		try
		{
			insert refund;
		}
		catch(DMLException e)
		{
			ApexPages.addMessages(e);
			return null;
		}
		return new PageReference('/'+refund.Id);
	}
}