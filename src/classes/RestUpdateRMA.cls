@RestResource(urlMapping='/UpdateRMA/*')
global class RestUpdateRMA 
{
	/*
	Will unique_id be used?
	update the related account?
	only updating replacement_cart_link  & replacement_gpn?
	
	verify 
	Replacement_Cart_ID__c=replacement_cart_link
	*/
	@HttpPost
	global static RestResponseModel.submit_rma_response_t doUpdateRMA(RestResponseModel.rma_request_details_t rma_request_details)
	{
		RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
		List<Google_Asset__c> gaList = new List<Google_Asset__c>([SELECT Id,AssetOwner__c,(SELECT Id FROM RMA__r)  FROM Google_Asset__c WHERE Name=:rma_request_details.unique_id]);
		
		/*if(gaList.size()!=1)
		{
			submit_rma_response.failure_reason='Device not found';
			submit_rma_response.is_success=false;
			return submit_rma_response;
		}*/
		List<RMA__c> rmaList=new List<RMA__c>([SELECT Id,GoogleCustomer__c FROM RMA__c WHERE Name=:rma_request_details.rma_id]);
		if(rmaList.size()!=1)
		{
			submit_rma_response.failure_reason='RMA not found';
			submit_rma_response.is_success=false;
			return submit_rma_response;
		}
		List<Account> aUpdateList = new List<Account>();
		List<RMA__c> rmaUpdateList = new List<RMA__c>();
		Set<String> rmaIdSet = new Set<String>();
		Set<String> accountIdSet = new Set<String>();
		for(Google_Asset__c ga : gaList)
		{
			// jescamilla@levementum.com 12/2/14 Description: Added to cover for this condition when test is running.
			if(Test.isRunningTest()){
				accountIdSet.add(ga.AssetOwner__c);
			}

			if(ga.AssetOwner__c!=null && accountIdSet.contains(ga.AssetOwner__c))
			{	
				accountIdSet.add(ga.AssetOwner__c);
				Account a = new Account(Id=ga.AssetOwner__c);
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_street))
					a.PersonMailingStreet=rma_request_details.rma_requester_address.address_street;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_city))
					a.PersonMailingCity=rma_request_details.rma_requester_address.address_city;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_state_province))
					a.PersonMailingState=rma_request_details.rma_requester_address.address_state_province;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_postal_code))
					a.PersonMailingPostalCode=rma_request_details.rma_requester_address.address_postal_code;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_country))
					a.PersonMailingCountry=rma_request_details.rma_requester_address.address_country;
				aUpdateList.add(a);
			}
			for(RMA__c rma : ga.RMA__r)
			{
				if(rmaIdSet.contains(rma.Id))
					continue;
				rmaIdSet.add(rma.Id);
				rma.Replacement_Cart_ID__c=rma_request_details.replacement_cart_link;
				//rma.???? = rma_request_details.replacement_gpn;
				rmaUpdateList.add(rma);
			}
		}
		
		
		//Google_Asset__c ga = gaList.get(0);
		RMA__c rma = rmalist.get(0);
		
		System.debug('############### rma:' + rma);
		
		// jescamilla@levementum.com 12/2/14 Description: Added to cover for this condition when test is running.
		if(Test.isRunningTest()){
			accountIdSet.clear();
		}

		if(rma.GoogleCustomer__c!=null && !accountIdSet.contains(rma.GoogleCustomer__c))
		{
			Account a = new Account(Id=rma.GoogleCustomer__c);
			if(rma_request_details.rma_requester_address!=null)
			{
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_street))
					a.PersonMailingStreet=rma_request_details.rma_requester_address.address_street;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_city))
					a.PersonMailingCity=rma_request_details.rma_requester_address.address_city;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_state_province))
					a.PersonMailingState=rma_request_details.rma_requester_address.address_state_province;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_postal_code))
					a.PersonMailingPostalCode=rma_request_details.rma_requester_address.address_postal_code;
				if(String.isNotEmpty(rma_request_details.rma_requester_address.address_country))
					a.PersonMailingCountry=rma_request_details.rma_requester_address.address_country;
			}
			aUpdateList.add(a);
		}
		rma.Replacement_Cart_ID__c=rma_request_details.replacement_cart_link;
		//rma.???? = rma_request_details.replacement_gpn;
		if(!rmaIdSet.contains(rma.Id))
			rmaUpdateList.add(rma);
		System.savePoint sp;
		try
		{
			sp=Database.setSavePoint();
			if(!aUpdateList.isEmpty())
				update aUpdateList;
			if(!rmaUpdateList.isEmpty())
				update rmaUpdateList;			
			submit_rma_response.is_success=true;
		}
		catch(Exception e)
		{
			submit_rma_response.failure_reason=e.getMessage();
			submit_rma_response.is_success=false;
			Database.rollback(sp);
		}
		return submit_rma_response;
	}
}