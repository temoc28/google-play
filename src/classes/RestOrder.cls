/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/20/15
 * @email:	cmunoz@levementum.com
 * @description:	This REST service exposes the Order (Opportunity) object from Salesforce to be used
 * during integration testing at Google.
 */
@RestResource(urlMapping='/Order/*')
global class RestOrder
{
	public static SObjectDataAccess dataAccess;
	
	@HttpPost
	global static ObjectModel updateObject(OpportunityModel orderModel)
	{
		OpportunityModel response = null;
		try
		{
			dataAccess = new OpportunityDataAccess();
			Opportunity orderToUpdate;
			Opportunity updatedOrder;
			
			if(orderModel.Id != null)
			{
				orderToUpdate = (Opportunity)dataAccess.selectObject(orderModel.Id);
			}
			else
			{
				orderToUpdate = (Opportunity)dataAccess.selectObject(orderModel.Name);
			}			
			dataAccess.updateObject(orderToUpdate, orderModel);
			orderModel.Name = OpportunityDataAccess.opp != null ? OpportunityDataAccess.opp.Name : null;
			
			// REFRESH FORMULA FIELDS
			if(orderModel.Id != null)
			{
				updatedOrder = (Opportunity)dataAccess.selectObject(orderModel.Id);
			}
			else
			{
				updatedOrder = (Opportunity)dataAccess.selectObject(orderModel.Name); 
			}
			
    		response = (OpportunityModel)updateModel(updatedOrder);
    		response.status = OpportunityDataAccess.objectStatus;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return response;
	}
	
	@HttpGet
	global static ObjectModel getObject()
	{
    	OpportunityModel response = null;
    	try
    	{
    		dataAccess = new OpportunityDataAccess();
    		Map<String, String> paramMap = RestContext.request.params;
			String orderName = paramMap.get('orderName');
			
			if(String.isNotEmpty(orderName))
			{
				Opportunity currentOrder = (Opportunity)dataAccess.selectObject(orderName);			
				response = (OpportunityModel)updateModel(currentOrder);
			}
    	}		
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	if(response != null)
    	{
    		response.status = OpportunityDataAccess.objectStatus;
    	}
        return response;
    }
	
	@HttpDelete
	global static void deleteObject()
	{
		try
		{
			dataAccess = new OpportunityDataAccess();
    		Map<String, String> paramMap = RestContext.request.params;
			String orderName = paramMap.get('orderName');	
			dataAccess.deleteObject(orderName);
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public static ObjectModel updateModel(sObject updatedsObject)
    {
    	OpportunityModel orderModel = new OpportunityModel();
    	Opportunity orderSObject = (Opportunity)updatedsObject;
    	try
    	{
    		if(orderSObject != null && orderModel != null)
    		{
	    		orderModel.Id = orderSObject.Id; 
	    		orderModel.AHNumber = orderSObject.AHNumber__c;   
				orderModel.Amount = orderSObject.Amount;      
				orderModel.AttentionTo = orderSObject.AttentionTo__c;  
				orderModel.CampaignId = orderSObject.CampaignId;   
				orderModel.City = orderSObject.City__c;
				orderModel.CloseDate = orderSObject.CloseDate;
				orderModel.IsClosed = orderSObject.IsClosed;     
				orderModel.Country = orderSObject.Country__c;
				orderModel.Description = orderSObject.Description;
				orderModel.Tax_Holiday = orderSObject.Tax_Holiday__c;
				orderModel.Email = orderSObject.Email__c;
				orderModel.Fiscal = orderSObject.Fiscal;
				orderModel.FiscalQuarter = orderSObject.FiscalQuarter;
				orderModel.FiscalYear = orderSObject.FiscalYear;
				orderModel.ForecastCategoryName = orderSObject.ForecastCategoryName;
				orderModel.ForecastCategory = orderSObject.ForecastCategory;
				orderModel.GoogleAccountLegacy = orderSObject.GoogleAccountLegacy__c;
				orderModel.AccountId = orderSObject.AccountId;
				orderModel.GoogleAsset = orderSObject.GoogleAsset__c;  
				orderModel.Name = orderSObject.Name; 
				orderModel.HasOpportunityLineItem = orderSObject.HasOpportunityLineItem;
				orderModel.LastActivityDate = orderSObject.LastActivityDate;
				orderModel.LeadSource = orderSObject.LeadSource;
				orderModel.MoveOrder = orderSObject.MoveOrder__c;    
				orderModel.NextStep = orderSObject.NextStep;     
				orderModel.OrderIDExternalID = orderSObject.OrderIDExternalID__c;
				orderModel.OwnerId = orderSObject.OwnerId;      
				orderModel.RecordTypeId = orderSObject.RecordTypeId;
				orderModel.Type = orderSObject.Type; 
				orderModel.Telephone = orderSObject.Telephone__c;
				orderModel.Pricebook2Id = orderSObject.Pricebook2Id;
				orderModel.Probability = orderSObject.Probability;
				orderModel.SFDCLegacyOrderID = orderSObject.SFDCLegacyOrderID__c;
				orderModel.Ship_By_Date = orderSObject.Ship_By_Date__c; 
				orderModel.ShipServiceLevel = orderSObject.ShipServiceLevel__c;
				orderModel.ShipmentDate = orderSObject.ShipmentDate__c;
				orderModel.ShippingProblem = orderSObject.ShippingProblem__c;
				orderModel.StageName = orderSObject.StageName; 
				orderModel.State_Province = orderSObject.State_Province__c;
				orderModel.Address = orderSObject.Address__c;
				orderModel.TrackingNumber = orderSObject.TrackingNumber__c;
				//orderModel.TrackingNumberLink = orderSObject.TrackingNumberLink__c;
				orderModel.TransactionDate = orderSObject.TransactionDate__c;
				orderModel.Warehouse = orderSObject.Warehouse__c;
				orderModel.IsWon = orderSObject.IsWon;
				orderModel.PostalCode = orderSObject.PostalCode__c;
				orderModel.temp_future_date_catcher = orderSObject.temp_future_date_catcher__c;
    		}
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	return orderModel;
    }
}