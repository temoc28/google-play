global class BatchCaseEmailCount implements Database.Batchable<sObject>
{	
	global Database.queryLocator start(Database.BatchableContext ctx)
	{        
        return Database.getQueryLocator([Select Id, (Select Incoming, FromName From EmailMessages) From Case WHERE RecordType.Name='ChromeOs' and Id in (SELECT ParentId FROM EmailMessage)]);
    }

    global void execute(Database.BatchableContext ctx, List<Sobject> scope)
    {
    	Case c;    	
    	for(Sobject so : scope)
    	{
    		c = (Case)so;
    		c.NumberofEmailsFromCustomer__c=0;
    		c.NumberofEmailsFromSupportTeam__c=0;
    		for(EmailMessage message : c.EmailMessages)
    		{    		
    			//if(!message.FromName.containsIgnoreCase('support') && message.Incoming)
    			if(message.Incoming)
    			{    				
    				c.NumberofEmailsFromCustomer__c+=1;
    			}
    			//else if(message.FromName.containsIgnoreCase('support') && !message.Incoming)
    			else if(!message.Incoming)
    			{
    				//if it's not incoming and the name contains support, increment    				
    				c.NumberofEmailsFromSupportTeam__c+=1;    				
    			}
    		}
    	}
    	update scope;
    }
    global void finish(Database.BatchableContext ctx)
    {
    
    }   
}