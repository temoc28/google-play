@isTest
public class TestRMAEditController 
{		
	@isTest static void TestRMAEditController()
	{
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createProductFamily();
		utils.createCodeSet();
		utils.createDocument();
		utils.createProduct1();
		utils.createOpportunity();
		Google_Asset__c ga = new Google_Asset__c();
		ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
		ga.AssetOwner__c = utils.idsMap.get('Account1');
		ga.SKU__c = '86002596-01-Test';
		ga.Name = 'TestGA';
		ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
		ga.Line_Number__c = 1;
		ga.ProductID__c = utils.idsMap.get('Product2');
		insert ga;
		RMA__c rma = new RMA__c();
		rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		rma.Notes__c = 'test notes';
		rma.Type__c = 'test';
		rma.RMA_Type__c = 'test';
		rma.RMA_Category__c = 'testcategory';
		rma.RMA_Sub_Category__c = 'ChromecaseTest';
		rma.gCases_ID__c = '5-7327000004804';
		rma.GoogleCustomer__c = utils.idsMap.get('Account1');
		rma.GoogleAsset__c = ga.Id;
		insert rma;
		Return_Reason_Code__c code = new Return_Reason_Code__c();
		code.Name = 'ChromecaseTest';
		code.Category__c = 'testcategory';
		code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
		code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code;
		Return_Reason_Code__c code2 = new Return_Reason_Code__c();
		code2.Name = 'testing';
		code2.Category__c = 'thisIs2';
		code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
		code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code2;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			controller.rmaToEdit.Status__c = 'Closed';
			controller.changeModel();
			controller.rmaToEdit.Type__c = 'test';
			controller.rmaToEdit.RMA_Category__c  = 'testcategory';
			controller.rmaToEdit.RMA_Sub_Category__c  = 'ChromecaseTest';
			controller.saveRecord();
			controller.saveNew();
			controller.checkAsset();
		Test.stopTest();

		System.assertEquals(controller.rmaToEdit.Type__c == 'test', true);
		System.assertEquals(controller.rmaToEdit.Notes__c == 'test notes', true);
		System.assertEquals(controller.rmaToEdit.GoogleAsset__c == ga.Id, true);
		System.assertEquals(controller.optionsType.size() > 0, true);
		System.assertEquals(controller.optionsCat.size() > 0, true);
		System.assertEquals(controller.optionsSubCat.size() > 0, true);
		Rma__c rmaToAssert = ([SELECT Status__c FROM Rma__c WHERE Id =: rma.Id limit 1]);
		System.assertEquals(rmaToAssert.Status__c, controller.rmaToEdit.Status__c);
	}
	@isTest static void TestRMATypeNone()
	{
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createProductFamily();
		utils.createCodeSet();
		utils.createDocument();
		utils.createProduct1();
		utils.createOpportunity();
		Google_Asset__c ga = new Google_Asset__c();
		ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
		ga.AssetOwner__c = utils.idsMap.get('Account1');
		ga.SKU__c = '86002596-01-Test';
		ga.Name = 'TestGA';
		ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
		ga.Line_Number__c = 1;
		ga.ProductID__c = utils.idsMap.get('Product2');
		insert ga;
		RMA__c rma = new RMA__c();
		rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		rma.Notes__c = 'test notes';
		rma.Type__c = 'test';
		rma.RMA_Type__c = 'test';
		rma.RMA_Category__c = 'testcategory';
		rma.RMA_Sub_Category__c = 'ChromecaseTest';
		rma.gCases_ID__c = '5-7327000004804';
		rma.GoogleCustomer__c = utils.idsMap.get('Account1');
		rma.GoogleAsset__c = ga.Id;
		insert rma;
		Return_Reason_Code__c code = new Return_Reason_Code__c();
		code.Name = 'ChromecaseTest';
		code.Category__c = 'testcategory';
		code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
		code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code;
		Return_Reason_Code__c code2 = new Return_Reason_Code__c();
		code2.Name = 'testing';
		code2.Category__c = 'thisIs2';
		code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
		code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code2;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			controller.rmaToEdit.Status__c = 'Closed';
			controller.changeModel();
			controller.rmaToEdit.Type__c = '--None--';
			controller.saveRecord();
			controller.saveNew();
		Test.stopTest();

		System.assertEquals(controller.rmaToEdit.Type__c == '--None--', true);
		System.assertEquals(controller.rmaToEdit.Notes__c == 'test notes', true);
		System.assertEquals(controller.rmaToEdit.GoogleAsset__c == ga.Id, true);
		System.assertEquals(controller.optionsType.size() > 0, true);
		System.assertEquals(controller.optionsCat.size() > 0, true);
		System.assertEquals(controller.optionsSubCat.size() > 0, true);
		Rma__c rmaToAssert = ([SELECT Status__c FROM Rma__c WHERE Id =: rma.Id limit 1]);
	}
	@isTest static void TestRMACatNone()
	{
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createProductFamily();
		utils.createCodeSet();
		utils.createDocument();
		utils.createProduct1();
		utils.createOpportunity();
		Google_Asset__c ga = new Google_Asset__c();
		ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
		ga.AssetOwner__c = utils.idsMap.get('Account1');
		ga.SKU__c = '86002596-01-Test';
		ga.Name = 'TestGA';
		ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
		ga.Line_Number__c = 1;
		ga.ProductID__c = utils.idsMap.get('Product2');
		insert ga;
		RMA__c rma = new RMA__c();
		rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		rma.Notes__c = 'test notes';
		rma.Type__c = 'test';
		rma.RMA_Type__c = 'test';
		rma.RMA_Category__c = 'testcategory';
		rma.RMA_Sub_Category__c = 'ChromecaseTest';
		rma.gCases_ID__c = '5-7327000004804';
		rma.GoogleCustomer__c = utils.idsMap.get('Account1');
		rma.GoogleAsset__c = ga.Id;
		insert rma;
		Return_Reason_Code__c code = new Return_Reason_Code__c();
		code.Name = 'ChromecaseTest';
		code.Category__c = 'testcategory';
		code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
		code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code;
		Return_Reason_Code__c code2 = new Return_Reason_Code__c();
		code2.Name = 'testing';
		code2.Category__c = 'thisIs2';
		code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
		code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code2;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			controller.rmaToEdit.Type__c = 'test';
			controller.rmaToEdit.RMA_Category__c  = '--None--';
			controller.saveRecord();
			controller.saveNew();
		Test.stopTest();

		System.assertEquals(controller.rmaToEdit.Type__c == 'test', true);
		System.assertEquals(controller.rmaToEdit.Notes__c == 'test notes', true);
		System.assertEquals(controller.rmaToEdit.GoogleAsset__c == ga.Id, true);
		System.assertEquals(controller.optionsType.size() > 0, true);
		System.assertEquals(controller.optionsCat.size() > 0, true);
		System.assertEquals(controller.optionsSubCat.size() > 0, true);
		Rma__c rmaToAssert = ([SELECT Status__c FROM Rma__c WHERE Id =: rma.Id limit 1]);
		System.assertEquals(rmaToAssert.Status__c, controller.rmaToEdit.Status__c);
	}
	@isTest static void TestRMAsubCat()
	{
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createProductFamily();
		utils.createCodeSet();
		utils.createDocument();
		utils.createProduct1();
		utils.createOpportunity();
		Google_Asset__c ga = new Google_Asset__c();
		ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
		ga.AssetOwner__c = utils.idsMap.get('Account1');
		ga.SKU__c = '86002596-01-Test';
		ga.Name = 'TestGA';
		ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
		ga.Line_Number__c = 1;
		ga.ProductID__c = utils.idsMap.get('Product2');
		insert ga;
		RMA__c rma = new RMA__c();
		rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		rma.Notes__c = 'test notes';
		rma.Type__c = 'test';
		rma.RMA_Type__c = 'test';
		rma.RMA_Category__c = 'testcategory';
		rma.RMA_Sub_Category__c = 'ChromecaseTest';
		rma.gCases_ID__c = '5-7327000004804';
		rma.GoogleCustomer__c = utils.idsMap.get('Account1');
		rma.GoogleAsset__c = ga.Id;
		insert rma;
		Return_Reason_Code__c code = new Return_Reason_Code__c();
		code.Name = 'ChromecaseTest';
		code.Category__c = 'testcategory';
		code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
		code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code;
		Return_Reason_Code__c code2 = new Return_Reason_Code__c();
		code2.Name = 'testing';
		code2.Category__c = 'thisIs2';
		code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
		code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code2;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			controller.rmaToEdit.Status__c = 'Closed';
			controller.rmaToEdit.Type__c = 'test';
			controller.rmaToEdit.RMA_Category__c  = 'testcategory';
			controller.rmaToEdit.RMA_Sub_Category__c  = '--None--';
			controller.saveRecord();
			controller.saveNew();
			controller.checkAsset();
		Test.stopTest();

		System.assertEquals(controller.rmaToEdit.Type__c == 'test', true);
		System.assertEquals(controller.rmaToEdit.Notes__c == 'test notes', true);
		System.assertEquals(controller.rmaToEdit.GoogleAsset__c == ga.Id, true);
		System.assertEquals(controller.optionsType.size() > 0, true);
		System.assertEquals(controller.optionsCat.size() > 0, true);
		System.assertEquals(controller.optionsSubCat.size() > 0, true);
		Rma__c rmaToAssert = ([SELECT Status__c FROM Rma__c WHERE Id =: rma.Id limit 1]);
	}
	@isTest static void TestRMAFilters()
	{
		TestRestUtilities utils = new TestRestUtilities();
		utils.createAccount();
		utils.createProductFamily();
		utils.createCodeSet();
		utils.createDocument();
		utils.createProduct1();
		utils.createOpportunity();
		Google_Asset__c ga = new Google_Asset__c();
		ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
		ga.AssetOwner__c = utils.idsMap.get('Account1');
		ga.SKU__c = '86002596-01-Test';
		ga.Name = 'TestGA';
		ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
		ga.Line_Number__c = 1;
		ga.ProductID__c = utils.idsMap.get('Product2');
		insert ga;
		RMA__c rma = new RMA__c();
		rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
		rma.Notes__c = 'test notes';
		rma.Type__c = 'test';
		rma.RMA_Type__c = 'test';
		rma.RMA_Category__c = 'testcategory';
		rma.RMA_Sub_Category__c = 'ChromecaseTest';
		rma.gCases_ID__c = '5-7327000004804';
		rma.GoogleCustomer__c = utils.idsMap.get('Account1');
		rma.GoogleAsset__c = ga.Id;
		insert rma;
		Return_Reason_Code__c code = new Return_Reason_Code__c();
		code.Name = 'ChromecaseTest';
		code.Category__c = 'testcategory';
		code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
		code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code;
		Return_Reason_Code__c code2 = new Return_Reason_Code__c();
		code2.Name = 'testing';
		code2.Category__c = 'thisIs2';
		code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
		code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
		insert code2;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			//test filtering:
			Map<String, String> filterMap = new Map<String, String>();
			controller.rmaToEdit.ServiceModel__c = 'Remorse Only';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 1);
			controller.rmaToEdit.ServiceModel__c = 'Remorse + DOA + Warranty';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 3);
			controller.rmaToEdit.ServiceModel__c = 'Remorse + DOA + Warranty + Repair';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 4);
			controller.rmaToEdit.ServiceModel__c = 'Remorse + DOA';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 2);
			controller.rmaToEdit.ServiceModel__c = 'Remorse + Warranty Refund';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 2);
			controller.rmaToEdit.ServiceModel__c = 'Warranty Only';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 1);
			controller.rmaToEdit.ServiceModel__c = 'Warranty + Repair';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 2);
			controller.rmaToEdit.ServiceModel__c = 'Repair Only';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 1);
			controller.rmaToEdit.ServiceModel__c = 'DOA + Warranty';
			filterMap = controller.filter();
			System.assertEquals(filterMap.size(), 2);
		Test.stopTest();
	}
	@isTest static void TestNewRMA()
	{
		RMA__c rma = new RMA__c();
		Test.startTest();
			PageReference setPage = Page.RMAEdit;
			Test.setCurrentPage(setPage);
			ApexPages.currentPage().getParameters().put('recordType', 'testType');
			ApexPages.StandardController std = new ApexPages.StandardController(rma);
			RMAEditController controller = new RMAEditController(std);
			controller.checkAsset();
		Test.stopTest();
		System.assertEquals(rma, controller.rmaToEdit);
	}
	
	@isTest static void TestGooglerUserOnlyRecordType()
	{
		try
		{
			Test.startTest();
				TestRestUtilities utils = new TestRestUtilities();
				utils.createAccount();
				utils.createProductFamily();
				utils.createCodeSet();
				utils.createDocument();
				utils.createProduct1();
				utils.createOpportunity();
				Google_Asset__c ga = new Google_Asset__c();
				ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
				ga.AssetOwner__c = utils.idsMap.get('Account1');
				ga.SKU__c = '86002596-01-Test';
				ga.Name = 'TestGA';
				ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
				ga.Line_Number__c = 1;
				ga.ProductID__c = utils.idsMap.get('Product2');
				insert ga;
				RMA__c rma = new RMA__c();
				rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
				rma.Notes__c = 'test notes';
				rma.Type__c = 'test';
				rma.RMA_Type__c = 'test';
				rma.RMA_Category__c = 'testcategory';
				rma.RMA_Sub_Category__c = 'ChromecaseTest';
				rma.gCases_ID__c = '5-7327000004804';
				rma.GoogleCustomer__c = utils.idsMap.get('Account1');
				rma.GoogleAsset__c = ga.Id;
				insert rma;
				Return_Reason_Code__c code = new Return_Reason_Code__c();
				code.Name = 'ChromecaseTest';
				code.Category__c = 'testcategory';
				code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
				code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
				insert code;
				Return_Reason_Code__c code2 = new Return_Reason_Code__c();
				code2.Name = 'testing';
				code2.Category__c = 'thisIs2';
				code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
				code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
				insert code2;
				PageReference setPage = Page.RMAEdit;
				Test.setCurrentPage(setPage);
				ApexPages.currentPage().getParameters().put('recordType', 'Googler User Only');
				ApexPages.StandardController std = new ApexPages.StandardController(rma);
				RMAEditController controller = new RMAEditController(std);
			Test.stopTest();
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
	}
	
	@isTest static void TestNovaRecordType()
	{
		try
		{
			Test.startTest();
				TestRestUtilities utils = new TestRestUtilities();
				utils.createAccount();
				utils.createProductFamily();
				utils.createCodeSet();
				utils.createDocument();
				utils.createProduct1();
				utils.createOpportunity();
				Google_Asset__c ga = new Google_Asset__c();
				ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
				ga.AssetOwner__c = utils.idsMap.get('Account1');
				ga.SKU__c = '86002596-01-Test';
				ga.Name = 'TestGA';
				ga.Order_Opportunity__c = utils.idsMap.get('Opportunity1');
				ga.Line_Number__c = 1;
				ga.ProductID__c = utils.idsMap.get('Product2');
				insert ga;
				RMA__c rma = new RMA__c();
				rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
				rma.Notes__c = 'test notes';
				rma.Type__c = 'test';
				rma.RMA_Type__c = 'test';
				rma.RMA_Category__c = 'testcategory';
				rma.RMA_Sub_Category__c = 'ChromecaseTest';
				rma.gCases_ID__c = '5-7327000004804';
				rma.GoogleCustomer__c = utils.idsMap.get('Account1');
				rma.GoogleAsset__c = ga.Id;
				insert rma;
				Return_Reason_Code__c code = new Return_Reason_Code__c();
				code.Name = 'ChromecaseTest';
				code.Category__c = 'testcategory';
				code.RMA_Types__c = 'Buyer\'s Remorse; test; test1; test2; test';
				code.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
				insert code;
				Return_Reason_Code__c code2 = new Return_Reason_Code__c();
				code2.Name = 'testing';
				code2.Category__c = 'thisIs2';
				code2.RMA_Types__c = '1;2;3;4;5;6;6;6;6;7;test';
				code2.Return_Reason_Code_Set__c = utils.idsMap.get('Return_Reason_Code_Set__c1');
				insert code2;
				PageReference setPage = Page.RMAEdit;
				Test.setCurrentPage(setPage);
				ApexPages.currentPage().getParameters().put('recordType', 'Nova Phone/Tablet');
				ApexPages.StandardController std = new ApexPages.StandardController(rma);
				RMAEditController controller = new RMAEditController(std);
			Test.stopTest();
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
	}
}