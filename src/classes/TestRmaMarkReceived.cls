/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRmaMarkReceived 
{
    static RMA__c rma;
    static testMethod void myUnitTest() 
    {
        RmaMarkReceived mRec = new RmaMarkReceived(new ApexPages.StandardController(rma));
        mRec.markReceived();
    }
    static
    {    	
    	Account a = new Account();
    	a.Name='TestAccount';
    	insert a;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name='TestOpp';
    	opp.CloseDate=Date.today();
    	opp.StageName='Closed Won';
    	opp.AccountId=a.Id;
    	insert opp;
    	
    	Google_Asset__c gAsset = new Google_Asset__c();
    	gAsset.Name='46654';
    	gAsset.SKU__c='xxxBBBccc';
    	gAsset.Order_Opportunity__c=opp.Id;
    	insert gAsset;
    	
    	Google_Asset__c gAsset2 = new Google_Asset__c();
    	gAsset2.Name='466541';
    	gAsset2.SKU__c='xxxBBBccc1';
    	gAsset2.Order_Opportunity__c=opp.Id;
    	insert gAsset2;
    	
    	rma = new RMA__c();
    	rma.Opportunity__c = opp.Id;
    	rma.GoogleAsset__c=gAsset.Id;
    	insert rma;
    }
}