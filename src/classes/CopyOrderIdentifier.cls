public class CopyOrderIdentifier {
	
	public static void UpdateValues(List<Order__c> TNew){
		for(Order__c O : TNew){
			O.Order_ID_External_ID__c = O.Name;
		}
	}
	
	@isTest
    static void myUnitTest() {
        Order__c O = new Order__c (Name = 'test');
        insert O;
    }
}