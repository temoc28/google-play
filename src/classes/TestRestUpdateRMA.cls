// jescamilla@levementum.com 12/2/14 Description: Test Coverage for RestUpdateRMA.cls.
@isTest
private class TestRestUpdateRMA {
  
  @isTest static void doUpdateRMA() {
    

    TestRestUtilities testRestUtils = new TestRestUtilities();

    Test.startTest();
    RestResponseModel.rma_request_details_t rma_request_details = new RestResponseModel.rma_request_details_t();
    RestResponseModel.submit_rma_response_t response = RestUpdateRMA.doUpdateRMA(rma_request_details); //call with nulls.

    testRestUtils.createAccount();
    testRestUtils.createAccount2();
    testRestUtils.createGoogleAsset();
    testRestUtils.createGoogleAsset2();
    testRestUtils.createRMA1();

    
    RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/UpdateOwner/';
        rReq.httpMethod = 'POST';
        
        RestContext.request = rReq;
        RestContext.response = rRes;

        rma_request_details.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
        rma_request_details.rma_id = testRestUtils.namesMap.get('RMA1_NAME');
        rma_request_details.replacement_cart_link = 'replacement_cart_link';
        rma_request_details.replacement_gpn = 'replacement_gpn';
        rma_request_details.rma_requester_first_name = 'John';
        rma_request_details.rma_requester_last_name = 'Wick';
        rma_request_details.rma_requester_address = new RestResponseModel.address_t();
        rma_request_details.rma_requester_address.address_state_province = 'TX';
        rma_request_details.rma_requester_address.address_country = 'US';
        rma_request_details.rma_requester_address.address_postal_code = '79938';

        response = RestUpdateRMA.doUpdateRMA(rma_request_details);//Call with no matching Account

        //BEGIN TEST FOR RestCancelRMA.cls
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelRMA';
        rReq.httpMethod = 'GET';
        
        RestContext.request = rReq;
        response = RestCancelRMA.doCancelRMA();//Test with Null Values

        rReq.addParameter('rmaId', testRestUtils.namesMap.get('RMA1_NAME'));
        response = RestCancelRMA.doCancelRMA();
        //END TEST FOR RestCancelRMA.cls

        //BEGIN TEST FOR RestSubmitRMA.cls
        RestResponseModel.rma_initiation_details_t requestDetails = new RestResponseModel.rma_initiation_details_t();
        response = RestSubmitRMA.doSubmitRMAs(requestDetails); //Call with Null Values

        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/SubmitRMA/';
        rReq.httpMethod = 'POST';
            
        RestContext.request = rReq;

        requestDetails.unique_id = 'TestGA'; //this unique id must match the Google Asset name in order for the class RestUpdateRMA to fire the account retrieval
        requestDetails.rma_requester_email = 'test@levtest.com'; 
        
        testRestUtils.createOpportunity();
        requestDetails.google_order_id = testRestUtils.namesMap.get('Opportunity1');

        testRestUtils.createLevUser();
        requestDetails.rma_creator = 'standarduser@levementum.com';

        requestDetails.rma_requester_address = new RestResponseModel.address_t();
        requestDetails.rma_requester_address.address_state_province = 'TX';
        requestDetails.rma_requester_address.address_country = 'US';
        requestDetails.rma_requester_address.address_postal_code = '79938';

        testRestUtils.createCodeSet();
        //testRestUtils.createReasonCode();
        testRestUtils.createProductFamily();
        testRestUtils.createDocument();
        testRestUtils.createCountryVariant();
        testRestUtils.createProduct1();

        System.debug('############### requestDetails:' + requestDetails);
        
        response = RestSubmitRMA.doSubmitRMAs(requestDetails);
        //END TEST FOR RestSubmitRMA.cls

        //BEGIN TEST FOR RestGetDeviceFamily.cls
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CancelRMA';
        rReq.httpMethod = 'GET';
        
        RestContext.request = rReq;
        RestResponseModel.device_types_t  response_device_types = RestGetDeviceFamily.getDeviceFamily();//Test with Null Values
        //END TEST FOR RestGetDeviceFamily.cls
  }


  
  
}