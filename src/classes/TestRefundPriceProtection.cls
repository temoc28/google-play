/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRefundPriceProtection 
{
    static Google_Asset__c gAsset;
    static testMethod void myUnitTest() 
    {
        RefundPriceProtection rpp = new RefundPriceProtection(new ApexPages.StandardController(gAsset));
        
        PageReference pageRef = Page.RefundPriceProtection;
    	Test.setCurrentPage(pageRef);    	
        rpp.createPriceProtectionRefund();        
        
        
        PriceProtectionValues__c ppv = new PriceProtectionValues__c(Country__c='US',SKU__c='xxxBBBccc',PriceProtectionStartDate__c=System.now().addDays(-1),PriceProtectionEndDate__c=System.now().addDays(7),FinalDayOfEligibility__c=Date.today(),PercentItemRefund__c=0.02,PercentTaxRefund__c=0.05);
        insert ppv;        
        
        //try and create with final day of eligibility has expired
        rpp.createPriceProtectionRefund();
        
        ppv.FinalDayofEligibility__c=Date.today().addDays(15);
        rpp.createPriceProtectionRefund();
    }
    static
    {   	
    	Account a = new Account();
    	a.Name='TestAccount';
    	insert a;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name='TestOpp';
    	opp.CloseDate=Date.today();
    	opp.StageName='Closed Won';
    	opp.AccountId=a.Id;
    	opp.Country__c='US';
    	opp.State_Province__c='GA';
    	insert opp;
    	
    	Product2 p2 = new Product2(Name='Test',IsActive=true,ProductCode='Test123',Sale_Country__c='US',SKU__c='xxxBBBccc');
    	insert p2;
    	
    	gAsset = new Google_Asset__c();
    	gAsset.Name='46654';
    	gAsset.SKU__c='xxxBBBccc';
    	gAsset.Order_Opportunity__c=opp.Id;
    	gAsset.Line_Number__c=1;
    	gAsset.ProductID__c=p2.Id;
    	insert gAsset;
    	
    	gAsset = [SELECT Id,Sale_Country__c,SKU__c,Order_Opportunity__c,Order_Opportunity__r.CreatedDate,Line_Number__c,Name FROM Google_Asset__c WHERE Id=:gAsset.Id];
    	System.debug('----------------SALECOUNTRY:'+gAsset.Sale_Country__c);
    	
    	RMA__c rma = new RMA__c();
    	rma.Opportunity__c = opp.Id;
    	rma.GoogleAsset__c=gAsset.Id;
    	insert rma;
    }
}