// Copyright 2012 Google Inc. All Rights Reserved.

/**
 * Class providing tests for the SetCaseSecret trigger.
 *
 * @author jongaulding@google.com (Jon Gaulding)
 */
@isTest
private class SetCaseSecretTest {
	
	static testMethod void verifySecretPopulated() {
    Case newCase = new Case();
    insert newCase;
    
    newCase = [SELECT Case_Secret__c
               FROM Case
               WHERE Id=:newCase.Id];
  
    System.assert(newCase.Case_Secret__c != null);
    System.assert(newCase.Case_Secret__c >= 0);
  }
}