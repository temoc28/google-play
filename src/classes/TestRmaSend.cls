/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRmaSend 
{
    static RMA__c rma;
    static testMethod void myUnitTest() 
    {
        Account a = new Account();
        a.FirstName='Test';
        a.LastName='Account';
        a.PersonEmail='Test@test.com';        
        a.PersonMailingCountry='GB';
        a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
        
        Product_Family__c pf = new Product_Family__c(Name='Test');
        insert pf;
        
        Document__c d = new Document__c(Name='Test',Product_Family__c=pf.Id);
        insert d;
        
        Country_Variant__c cv = new Country_Variant__c(Name='GB',Document__c=d.Id);
        insert cv;
        
        Product2 p = new Product2(Name='TEst',ProductCode='Test',SKU__c='xxxBBBccc',IsActive=true,Document__c=d.Id);
        insert p;
        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';
        gAsset.ProductId__c=p.Id;
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;       
        rma.RMA_Category__c='No Longer Wants product';
        rma.RMA_Sub_Category__c='Test';
        rma.Notes__c='Test';        
        insert rma;
        RmaSend rmaSend = new RmaSend(new ApexPages.StandardController(rma));
        System.assertEquals('FALSE', rmaSend.cverror);
    }
    
    static testMethod void myUnitTest2() 
    {
        Account a = new Account();
        a.Name='TestAccount';        
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
                        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';        
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;       
        rma.RMA_Category__c='No Longer Wants product';
        rma.RMA_Sub_Category__c='Test';
        rma.Notes__c='Test';        
        insert rma;
        RmaSend rmaSend = new RmaSend(new ApexPages.StandardController(rma));
        System.assertEquals('TRUE', rmaSend.cverror);
    }
    
    static testMethod void myUnitTest3() 
    {
        Account a = new Account();
        a.Name='TestAccount';        
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
                        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';        
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        insert gAsset;
        
        rma = new RMA__c();
        rma.Status__c='Pending Return';
        rma.Triage_Code__c='C';
        rma.Customer_Induced_Damage__c='Y';
        rma.Type__c='Buyer\'s Remorse';
        rma.Opportunity__c = opp.Id;
        rma.GoogleAsset__c=gAsset.Id;        
        rma.Notes__c='Test';        
        insert rma;
        RmaSend rmaSend = new RmaSend(new ApexPages.StandardController(rma));        
    }
}