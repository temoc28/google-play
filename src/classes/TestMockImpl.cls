/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class TestMockImpl implements WebServiceMock{

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       //docSample.EchoStringResponse_element respElement = new docSample.EchoStringResponse_element();
       //respElement.EchoStringResult = 'Mock response';
       soapPlayHelper.createAllListResponse_element respElement = new soapPlayHelper.createAllListResponse_element();
       soapPlayHelper.AccountObject ao = new soapPlayHelper.AccountObject();
       soapPlayHelper.OpportunityObject oppO = new soapPlayHelper.OpportunityObject();
       soapPlayHelper.OpportunityLineItemObject oliO = new soapPlayHelper.OpportunityLineItemObject();
       soapPlayHelper.RmaObject rO = new soapPlayHelper.RmaObject();
       soapPlayHelper.BundleObject bo = new soapPlayHelper.BundleObject();
       soapPlayHelper.RefundedGA rGa = new soapPlayHelper.RefundedGA();
       soapPlayHelper.AllowFieldTruncationHeader_element ele = new soapPlayHelper.AllowFieldTruncationHeader_element();
       soapPlayHelper.CallOptions_element coEle = new soapPlayHelper.CallOptions_element();
       soapPlayHelper.changeRmaStatus_element crsEle = new soapPlayHelper.changeRmaStatus_element();
       soapPlayHelper.changeRmaStatusResponse_element reEle = new soapPlayHelper.changeRmaStatusResponse_element();
       soapPlayHelper.createAllList_element allEle = new soapPlayHelper.createAllList_element();
       soapPlayHelper.DebuggingHeader_element dEle = new soapPlayHelper.DebuggingHeader_element();
       soapPlayHelper.DebuggingInfo_element dIEle = new soapPlayHelper.DebuggingInfo_element();
       soapPlayHelper.SessionHeader_element sEle = new soapPlayHelper.SessionHeader_element();
       List<soapPlayHelper.BundleObject> boList = new List<soapPlayHelper.BundleObject>();
       boList.add(bo);
       respElement.result=boList;
       response.put('response_x', respElement); 
   }
}