global interface SObjectDataAccess 
{
	void updateObject(sObject obj, ObjectModel objModel);
	//void insertObject(sObject obj, ObjectModel objModel);
	sObject selectObject(Id objId);
	sObject selectObject(String objName);
	void deleteObject(Id objId);
	void deleteObject(String objName);
	sObject insertObject(ObjectModel objectModel);
	sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData);
}