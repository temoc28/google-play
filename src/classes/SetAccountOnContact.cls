public class SetAccountOnContact {
	public static void UpdateValues(List<Contact> TNew){
		Account A;
		List<Account> Accounts = [SELECT Id, Name FROM Account WHERE Name = 'Google Contacts' LIMIT 1];
		if(Accounts.isEmpty()){
			A = new Account(Name = 'Google Contacts');
			insert A;
		}
		else {
			A = Accounts[0];
		}
		
		for(Contact c : TNew){
			if(c.AccountId == NULL){
				c.AccountId = A.Id;
			}
		}
	}
	
	@isTest
    static void myUnitTest() {
        Contact c = new Contact(LastName = 'test');
        insert c;
    }
}