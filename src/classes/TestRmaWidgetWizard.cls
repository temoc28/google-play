/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRmaWidgetWizard 
{
    static RMA__c rma;
    static testMethod void myUnitTest() 
    {
        PageReference pr = Page.RmaWidgetSearch;
        pr.getParameters().put('newRmaId',rma.Id);
        Test.setCurrentPage(pr);
        RmaWidgetWizard widgetWiz = new RmaWidgetWizard();
        widgetWiz.doParameterSearch();
        widgetWiz.searchParam='RMA';
        widgetWiz.doParameterSearch();
        widgetWiz.searchParam=[SELECT Name FROM RMA__c WHERE Id=:rma.Id].Name;
        widgetWiz.doParameterSearch();
        widgetWiz.searchParam='test@test.com';
        widgetWiz.doParameterSearch();
        widgetWiz.searchParam='15731260465432498277.1552240243256561';
        widgetWiz.doParameterSearch();
        
        widgetWiz.searchParam='46654';
        widgetWiz.doParameterSearch();
        
        widgetWiz.goToRma();
        widgetWiz.saveRma();
        widgetWiz.updateRma();
        widgetWiz.cancel();
    }
    static
    {
    	Account a = new Account();
    	a.FirstName='Test';
    	a.LastName='Account';
    	a.PersonEmail='test@test.com';
    	a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
    	a.PersonMailingCountry='US';
    	a.PersonMailingState='GA';
    	insert a;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name='TestOpp';
    	opp.CloseDate=Date.today();
    	opp.StageName='Closed Won';
    	opp.AccountId=a.Id;
    	opp.Type='Standard Order';
    	opp.Country__c='US';
    	opp.State_Province__c='GA';
    	insert opp;
    	
    	Google_Asset__c gAsset = new Google_Asset__c();
    	gAsset.Name='46654';
    	gAsset.SKU__c='xxxBBBccc';
    	gAsset.Order_Opportunity__c=opp.Id;
    	gAsset.Line_Number__c=1;
    	insert gAsset;
    	
    	rma = new RMA__c();
    	rma.Status__c='Pending Return';
    	rma.Triage_Code__c='C';
    	rma.Customer_Induced_Damage__c='Y';
    	rma.Type__c='Buyer\'s Remorse';
    	rma.Opportunity__c = opp.Id;
    	rma.GoogleAsset__c=gAsset.Id;    	
    	insert rma;
    }
}