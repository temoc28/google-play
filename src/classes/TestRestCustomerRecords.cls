@isTest
public class TestRestCustomerRecords 
{
    static testmethod void testRestCustomerRecords0()
    {
        createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    static testmethod void testRestCustomerRecords1()
    {
        Map<String,Id> idsMap = createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', 'test@levtest.com');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.ServiceModel__c='Remorse + DOA + Warranty';
        insert rma;
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    static testmethod void testRestCustomerRecords2()
    {
        createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', 'testOppt');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    static testmethod void testRestCustomerRecords3()
    {
        createTestData();
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', 'TestGA');
        rReq.addParameter('rmaId', '');
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    static testmethod void testRestCustomerRecords4()
    {
        Map<String,Id> idsMap = createTestData();
        
        RMA__c rma = new RMA__c();
        rma.RecordTypeId = [select id from recordtype where sobjecttype = 'RMA__c' and developername = 'Chromecast'].Id;
        rma.Notes__c = 'test notes';
        rma.gCases_ID__c = '5-7327000004804';
        rma.GoogleCustomer__c = idsMap.get('Account1');
        rma.GoogleAsset__c = idsMap.get('Google_Asset__c1');
        rma.ServiceModel__c='Remorse + DOA + Warranty + Repair';
        insert rma;
        
        rma = [select id,name from RMA__c where id = :rma.Id];
        
        RestRequest rReq = new RestRequest();
        RestResponse rRes = new RestResponse();
        
        rReq.addParameter('email', '');
        rReq.addParameter('orderNumber', '');
        rReq.addParameter('assetId', '');
        rReq.addParameter('rmaId', rma.Name);
        
        rReq.requestURI = url.getSalesforceBaseUrl().toExternalForm() + '/CustomerRecords';
        rReq.httpMethod = 'GET';
        RestContext.request = rReq;
        RestContext.response = rRes;
        List<RestResponseModel.customer_records_t> testList = RestCustomerRecords.getCustomerRecords();
        System.assert(testList != null);
    }
    
    static Map<String,Id> createTestData(){
        Map<String,Id> idsMap = new Map<String,Id>();
        
        Account acct = new Account();
        acct.PersonEmail = 'test@levtest.com';
        acct.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'GoogleCustomer'].id;
        acct.FirstName = 'test';
        acct.LastName = 'levtest';
        acct.PersonMailingCountry = 'US';
        acct.PersonMailingState = 'AZ';
        insert acct;
        idsMap.put('Account1',acct.Id);
        
        Account acct2 = new Account();
        acct2.Name = 'TestLevBiz';
        acct2.RecordTypeId = [select id from recordtype where sobjecttype = 'Account' and developername = 'BusinessAccount'].id;
        insert acct2;
        idsMap.put('Account2',acct2.Id);
        
        Return_Reason_Code_Set__c codeSet = new Return_Reason_Code_Set__c();
        codeSet.Name = 'CHromecaseTest';
        insert codeSet;
        idsMap.put('Return_Reason_Code_Set__c1',codeSet.Id);
        
        Return_Reason_Code__c code = new Return_Reason_Code__c();
        code.Name = 'ChromecaseTest';
        code.RMA_Types__c = 'Buyer\'s Remorse';
        code.Return_Reason_Code_Set__c = codeSet.Id;
        insert code;
        idsMap.put('Return_Reason_Code__c1',code.Id);
        
        Product_Family__c prodFam = new Product_Family__c();
        prodFam.Name = 'Chromecast Test';
        insert prodFam;
        idsMap.put('Product_Family__c1',prodFam.Id);
        
        Document__c doc = new Document__c();
        doc.Name = 'Chromecast Test';
        doc.Product_Family__c = prodFam.Id;
        doc.Return_Reason_Code_Set__c = codeset.Id;
        insert doc;
        idsMap.put('Document__c1',doc.Id);
        
        Country_Variant__c cv = new Country_Variant__c();
        cv.Document__c = doc.Id;
        cv.Name = 'US';
        insert cv;
        idsMap.put('Country_Variant__c1',cv.Id);
        
        Product2 prod = new Product2();
        prod.Name = 'Chromecast US Test';
        prod.SKU__c = '86002596-01-Test';
        prod.Sale_Country__c = 'US';
        prod.Unique_ID_Type__c = 'Serial Number';
        prod.RMARecordType__c = 'Chromecast';
        prod.Document__c = doc.Id;
        insert prod;
        idsMap.put('Product21',prod.Id);
        
        Opportunity oppt = new Opportunity();
        oppt.AccountId = acct.Id;
        oppt.OrderIDExternalID__c = '32434902134BNMBd';
        oppt.TransactionDate__c = dateTime.now();
        oppt.Country__c = 'US';
        oppt.State_Province__c = 'AZ';
        oppt.Name = 'testOppt';
        oppt.Type = 'Standard Order';
        oppt.CloseDate = Date.today().addDays(30);
        oppt.StageName = 'Prospecting';
        insert oppt;
        idsMap.put('Opportunity1',oppt.Id);
        
        Opportunity oppt2 = new Opportunity();
        oppt2.AccountId = acct2.Id;
        oppt2.OrderIDExternalID__c = '32434902134BNMBd2';
        oppt2.TransactionDate__c = dateTime.now();
        oppt2.Country__c = 'US';
        oppt2.State_Province__c = 'AZ';
        oppt2.Name = 'testOppt2';
        oppt2.Type = 'Standard Order';
        oppt2.CloseDate = Date.today().addDays(30);
        oppt2.StageName = 'Prospecting';
        insert oppt2;
        idsMap.put('Opportunity2',oppt2.Id);
        
        Google_Asset__c ga = new Google_Asset__c();
        ga.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga.AssetOwner__c = acct.Id;
        ga.SKU__c = '86002596-01-Test';
        ga.Name = 'TestGA';
        ga.Order_Opportunity__c = oppt.Id;
        ga.Line_Number__c = 1;
        ga.ProductID__c = prod.Id;
        insert ga;
        idsMap.put('Google_Asset__c1',ga.Id);
        
        Google_Asset__c ga2 = new Google_Asset__c();
        ga2.Google_Asset_ID_External_ID__c = 'sdf4#fdfrg34';
        ga2.AssetOwner__c = acct2.Id;
        ga2.SKU__c = '86002596-01-Test2';
        ga2.Name = 'TestGA2';
        ga2.Order_Opportunity__c = oppt2.Id;
        ga2.Line_Number__c = 1;
        ga2.ProductID__c = prod.Id;
        insert ga2;
        idsMap.put('Google_Asset__c2',ga2.Id);
        
        return idsMap;
    }
}