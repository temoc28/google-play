/**
 * @author: Levementum LLC
 * @date:   12/18/15
 * @description:    This REST service returns the number of refunds
 *                  associated with orders that are associated with the customer 
 *                  account matching the email address where the refund type == 'Lost/Stolen'
 * @update: 01/21/16 - Changed getNumberOfRefunds() from @HttpPost to @HttpGet
 * @update: 02/01/16 - Added Last 12 months to Refund__c query. Optimized running time
 * 					   complexity when obtaining number of lost/stolen refunds. Used Aggregations instead.
 * @update:	02/23/16 - Changed return error message to '0' if empty or invalid email provided.
 */
 @RestResource(urlMapping='/LostStolenHistory/*')
global class RestLostStolenHistory 
{
    global static RestResponseModel.lost_stolen_history_response_t response;
    
    /**
     * @param:  lostStolenHistory will contain the following parameters:
     * customer_email - Customer Email from a Google Account 
     * agent_ldap - optional and for future use
     * caseId - optional and for future use
     */
    @HttpGet
    global static RestResponseModel.lost_stolen_history_response_t getNumberOfRefunds()
    {
        response = new RestResponseModel.lost_stolen_history_response_t();
        response.failure_reason = '';
        try
        {
            Map<String, String> paramMap = RestContext.request.params;
            String customerEmail = paramMap.get('customer_email');
            String agentLDAP = paramMap.get('agent_ldap');
            String caseId = paramMap.get('caseId');
			system.debug(paramMap);
            if(String.isEmpty(customerEmail))
            {
                response.is_success = true; 
                response.failure_reason = 'Please provide a valid customer email.';
                response.number_lost_stolen_refunds = 0;
                return response;
            }
            
            Integer numberOfLostStolenRefunds = 0;
            List<Account> accts = (List<Account>)getAccounts(customerEmail);
            
            if(accts.size() <= 0)
            {
            	response.is_success = true; 
            	response.failure_reason += ' - Could not find Account with given customer email.'; 
            	response.number_lost_stolen_refunds = 0;
            	return response;
            }
                    
            List<Opportunity> opps = [SELECT Id, AccountId FROM Opportunity WHERE AccountId =: accts.get(0).Id];
            Set<Id> oppIds = new Set<Id>();
            for(Opportunity opp : opps)
            {
                oppIds.add(opp.Id);
            }
            
            for(AggregateResult ar: [SELECT COUNT_DISTINCT(OrderNumber__c) numLostStolenRefunds 
                                     FROM Refund__c 
                                     WHERE RefundReason__c = 'Lost/Stolen' 
                                     AND OrderNumber__c in : (oppIds) 
                                     AND CreatedDate >= LAST_N_DAYS:365])
            {           
                numberOfLostStolenRefunds+=Integer.valueOf(ar.get('numLostStolenRefunds'));
            }
            response.number_lost_stolen_refunds = numberOfLostStolenRefunds;
            response.is_success = true;
            response.customer_id = accts.get(0).Id;
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
            response.is_success = false; 
            response.failure_reason = ex.getMessage() + ' ' + ex.getStackTraceString();
        }
         system.debug(response);
        return response;
    }
    
    /**
     * @description:    Get Account object based on dynamic query. Check if Person Accounts
     *                  is enabled and select corresponding email field.
     */
    global static List<Account> getAccounts(String customerEmail)
    {
        String queryString = '';
        try
        {
            String emailField = '';
            try
            {
                Account acct = new Account();
                acct.get('isPersonAccount');
                emailField = 'PersonEmail ';
            }
            catch(Exception ex)
            {
                emailField = 'Email ';
            }
            queryString += 'SELECT Id, ' + emailField + ' FROM Account WHERE ' + emailField + ' =: customerEmail';
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
            response.is_success = false; 
            response.failure_reason = ex.getMessage() + ' ' + ex.getStackTraceString();
        }
        return Database.query(queryString);
    }
}