//Generated by wsdl2apex

public class ProcessRMAServiceController{
    public class processResponse_element {
        public String result;
        private String[] result_type_info = new String[]{'result','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://corp.google.com/bizapps/ProcessRMAService/ProcessRMAService','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class ProcessRMAService_pt {
        //public String endpoint_x = 'https://soaproxytest01.ext.google.com:443/ProcessRMAService/processrmaservice_client_ep';
        public String endpoint_x = 'https://soaproxyprod01.ext.google.com:443/ProcessRMAService/processrmaservice_client_ep';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://corp.google.com/bizapps/ProcessRMAService/ProcessRMAService', 'ProcessRMAServiceController'};
        public String process(String RMANumber,String RMALineNumber,String RepairPartner,String GoogleAssetId,String IMEI,String PartNumber,String CreationDate,String AltSerialNumber1,String AltSerialNumber2,String AltSerialNumber3,String AltSerialNumber4,String TrackingNumber,String Carrier,String ContactName,String AddressLine1,String AddressLine2,String AddressLine3,String County,String City,String State,String Country,String Zip,String PhoneNumber,String RMAType,String WarrantyExpDate,String CID,String Category,String SubCategory,String Notes,String Status) {
            ProcessRMAServiceController.process_element request_x = new ProcessRMAServiceController.process_element();
            ProcessRMAServiceController.processResponse_element response_x;
            request_x.RMANumber = RMANumber;
            request_x.RMALineNumber = RMALineNumber;
            request_x.RepairPartner = RepairPartner;
            request_x.GoogleAssetId = GoogleAssetId;
            request_x.IMEI = IMEI;
            request_x.PartNumber = PartNumber;
            request_x.CreationDate = CreationDate;
            request_x.AltSerialNumber1 = AltSerialNumber1;
            request_x.AltSerialNumber2 = AltSerialNumber2;
            request_x.AltSerialNumber3 = AltSerialNumber3;
            request_x.AltSerialNumber4 = AltSerialNumber4;
            request_x.TrackingNumber = TrackingNumber;
            request_x.Carrier = Carrier;
            request_x.ContactName = ContactName;
            request_x.AddressLine1 = AddressLine1;
            request_x.AddressLine2 = AddressLine2;
            request_x.AddressLine3 = AddressLine3;
            request_x.County = County;
            request_x.City = City;
            request_x.State = State;
            request_x.Country = Country;
            request_x.Zip = Zip;
            request_x.PhoneNumber = PhoneNumber;
            request_x.RMAType = RMAType;
            request_x.WarrantyExpDate = WarrantyExpDate;
            request_x.CID = CID;
            request_x.Category = Category;
            request_x.SubCategory = SubCategory;
            request_x.Notes = Notes;
            request_x.Status = Status;
            Map<String, ProcessRMAServiceController.processResponse_element> response_map_x = new Map<String, ProcessRMAServiceController.processResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'process',
              'http://corp.google.com/bizapps/ProcessRMAService/ProcessRMAService',
              'process',
              'http://corp.google.com/bizapps/ProcessRMAService/ProcessRMAService',
              'processResponse',
              'ProcessRMAServiceController.processResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
    }
    public class process_element {
        public String RMANumber;
        public String RMALineNumber;
        public String RepairPartner;
        public String GoogleAssetId;
        public String IMEI;
        public String PartNumber;
        public String CreationDate;
        public String AltSerialNumber1;
        public String AltSerialNumber2;
        public String AltSerialNumber3;
        public String AltSerialNumber4;
        public String TrackingNumber;
        public String Carrier;
        public String ContactName;
        public String AddressLine1;
        public String AddressLine2;
        public String AddressLine3;
        public String County;
        public String City;
        public String State;
        public String Country;
        public String Zip;
        public String PhoneNumber;
        public String RMAType;
        public String WarrantyExpDate;
        public String CID;
        public String Category;
        public String SubCategory;
        public String Notes;
        public String Status;
        private String[] RMANumber_type_info = new String[]{'RMANumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] RMALineNumber_type_info = new String[]{'RMALineNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] RepairPartner_type_info = new String[]{'RepairPartner','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] GoogleAssetId_type_info = new String[]{'GoogleAssetId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] IMEI_type_info = new String[]{'IMEI','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PartNumber_type_info = new String[]{'PartNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] CreationDate_type_info = new String[]{'CreationDate','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AltSerialNumber1_type_info = new String[]{'AltSerialNumber1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AltSerialNumber2_type_info = new String[]{'AltSerialNumber2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AltSerialNumber3_type_info = new String[]{'AltSerialNumber3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AltSerialNumber4_type_info = new String[]{'AltSerialNumber4','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TrackingNumber_type_info = new String[]{'TrackingNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Carrier_type_info = new String[]{'Carrier','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ContactName_type_info = new String[]{'ContactName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine1_type_info = new String[]{'AddressLine1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine2_type_info = new String[]{'AddressLine2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine3_type_info = new String[]{'AddressLine3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] County_type_info = new String[]{'County','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] City_type_info = new String[]{'City','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] State_type_info = new String[]{'State','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Country_type_info = new String[]{'Country','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Zip_type_info = new String[]{'Zip','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber_type_info = new String[]{'PhoneNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] RMAType_type_info = new String[]{'RMAType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] WarrantyExpDate_type_info = new String[]{'WarrantyExpDate','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] CID_type_info = new String[]{'CID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Category_type_info = new String[]{'Category','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] SubCategory_type_info = new String[]{'SubCategory','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Notes_type_info = new String[]{'Notes','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Status_type_info = new String[]{'Status','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://corp.google.com/bizapps/ProcessRMAService/ProcessRMAService','true','false'};
        private String[] field_order_type_info = new String[]{'RMANumber','RMALineNumber','RepairPartner','GoogleAssetId','IMEI','PartNumber','CreationDate','AltSerialNumber1','AltSerialNumber2','AltSerialNumber3','AltSerialNumber4','TrackingNumber','Carrier','ContactName','AddressLine1','AddressLine2','AddressLine3','County','City','State','Country','Zip','PhoneNumber','RMAType','WarrantyExpDate','CID','Category','SubCategory','Notes','Status'};
    }
    
    static testMethod void testThisClass()
    {
        Test.startTest(); 
        
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        
        Contact con = new Contact(LastName = 'TestContact', AccountId = acc.Id, MailingStreet = '3300 Fremont Blvd Ste 100', 
            MailingState = 'CA', MailingCity = 'Fremont', MailingPostalCode = '94358');
        insert con;
        
        acc.PersonAcctContactLookup__c = con.Id;
        update acc;
        
        Id rtId = Schema.SObjectType.RMA__c.getRecordTypeInfosByName().get('Chromebook WiFi').getRecordTypeId();
        System.assertNotEquals(rtId, null);
        
        Google_Asset__c ga = new Google_Asset__c (Google_Asset_ID_External_ID__c = '9421341099', Asset_Type__c = 'New',
            Active_Device__c = 'Active', AssetOwner__c = acc.Id, Carrier__c = 'Carrier1', IMEI__c = 'IMEI' );
        insert ga;
        System.assertNotEquals(ga.Id, null);
        
        Order__c ord = new Order__c(Name = 'testorder');
        insert ord;
        System.assertNotEquals(ord.Id, null);
        
        ProcessRMAServiceController.ProcessRMAService_pt prmc = new ProcessRMAServiceController.ProcessRMAService_pt();
        prmc.timeout_x = 120000;
        String response = prmc.process('SF9204', '1', 'Quanta', 'SN10319065',
                     'IMEI', '86001915-13-R', '2012-11-28T14:04:00-08:00', '', '', '', '', '906511', 'Carrier1', 
                     'John Smith','3300 Fremont Blvd', 'Ste 100', '', '', 'Fremont', 'CA', 'US', '94358', 
                     '510-410-5113', 'RR', '2013-10-30T16:59:59-07:00', 'N', '', 'LCD Issue', 
                     'Testing from Salesforce', 'Pending Return');
        
        Test.stopTest(); 
    }
}