/*
*new class to "swap out" the other RefundCourtesy development that was created per Jordan to 
*create a line-item refund insted of the specific-amount refund
*/
public with sharing class RefundCourtesy2 
{
	Opportunity opp;
	public Refund__c refund {get;set;}
	public RefundCourtesy2(ApexPages.StandardController con)
	{
		opp=(Opportunity)con.getRecord();
		refund = new Refund__c();
	}
	public PageReference save()
	{
		Id refundRt = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Line-Item Refund').getRecordTypeId();
		/*List<Refund__c> refundList = new List<Refund__c>([SELECT Id FROM Refund__c WHERE OrderNumber__c=:opp.Id AND RecordTypeId=:sAmountRefundRt]);
		if(refundList.size()>0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'This order already has a specific amount refund'));
			return null;
		}*/		
		refund.OrderNumber__c=opp.Id;		
		refund.RefundStatus__c='Pending';
		refund.ExpectedRefundDate__c=Date.today().addDays(1);
		refund.RecordTypeId=refundRt;
		try
		{
			insert refund;
		}
		catch(DMLException e)
		{
			ApexPages.addMessages(e);
			return null;
		}
		return new PageReference('/'+refund.Id);
	}
}