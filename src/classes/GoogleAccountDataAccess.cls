/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/12/15
 * @email:	cmunoz@levementum.com
 * @description:	General DML Operations for Account (GoogleAccount) object
 */
public with sharing class GoogleAccountDataAccess implements SObjectDataAccess
{
	public static String objectStatus = 'OK';
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/12/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Retrieves a Google Account based on an account id
	 */
	public static Account selectObject(Id accountId)
	{
		Account googleAccount = null;
		try
		{
			googleAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, LastName, FirstName, Salutation, Type, RecordTypeId, ParentId, 
									 BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
									 BillingLongitude, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
									 ShippingLatitude, ShippingLongitude, Phone, Fax, AccountNumber, Website, Sic, Industry, AnnualRevenue, 
									 NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, 
									 LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, 
									 PersonContactId, IsPersonAccount, PersonMailingStreet, PersonMailingCity, PersonMailingState, 
									 PersonMailingPostalCode, PersonMailingCountry, PersonMailingLatitude, PersonMailingLongitude, 
									 PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountry, 
									 PersonOtherLatitude, PersonOtherLongitude, PersonMobilePhone, PersonHomePhone, PersonOtherPhone, 
									 PersonAssistantPhone, PersonEmail, PersonTitle, PersonDepartment, PersonAssistantName, PersonLeadSource, 
									 PersonBirthdate, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonEmailBouncedReason, 
									 PersonEmailBouncedDate, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, Last_Name__c, First_Name__c, 
									 Mobile__c, Other_Phone__c, Salutation__c, LegacySFContactID__c, PersonAcctIDExternalID__c, MIgration__c, 
									 DupFlag__c, PersonAcctContactLookup__c, Customer_Country__c, CID_Exception_Granted__c, 
									 CID_Exception_Granted_Date_Time__c, CID_Exception_on_This_Asset__c, CID_Exception_Granted_Asset__c, 
									 CID_Exception_Granted_Asset_Description__c, CID_Exception_Granted_Asset_Name__c, Primary_Email__pc, 
									 Notes__pc, Email__pc, OtherEmail__pc, Migration__pc
							FROM Account
							WHERE id =: accountId limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return googleAccount;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/24/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Retrieves a Google Account based on an account name
	 */
	public static Account selectObject(String accountName)
	{
		Account googleAccount = null;
		try
		{
			googleAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, LastName, FirstName, Salutation, Type, RecordTypeId, ParentId, 
									 BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
									 BillingLongitude, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
									 ShippingLatitude, ShippingLongitude, Phone, Fax, AccountNumber, Website, Sic, Industry, AnnualRevenue, 
									 NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, 
									 LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, 
									 PersonContactId, IsPersonAccount, PersonMailingStreet, PersonMailingCity, PersonMailingState, 
									 PersonMailingPostalCode, PersonMailingCountry, PersonMailingLatitude, PersonMailingLongitude, 
									 PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountry, 
									 PersonOtherLatitude, PersonOtherLongitude, PersonMobilePhone, PersonHomePhone, PersonOtherPhone, 
									 PersonAssistantPhone, PersonEmail, PersonTitle, PersonDepartment, PersonAssistantName, PersonLeadSource, 
									 PersonBirthdate, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonEmailBouncedReason, 
									 PersonEmailBouncedDate, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, Last_Name__c, First_Name__c, 
									 Mobile__c, Other_Phone__c, Salutation__c, LegacySFContactID__c, PersonAcctIDExternalID__c, MIgration__c, 
									 DupFlag__c, PersonAcctContactLookup__c, Customer_Country__c, CID_Exception_Granted__c, 
									 CID_Exception_Granted_Date_Time__c, CID_Exception_on_This_Asset__c, CID_Exception_Granted_Asset__c, 
									 CID_Exception_Granted_Asset_Description__c, CID_Exception_Granted_Asset_Name__c, Primary_Email__pc, 
									 Notes__pc, Email__pc, OtherEmail__pc, Migration__pc
							FROM Account
							WHERE Name =: accountName limit 1];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}system.debug('??? ' + googleAccount);
		return googleAccount;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
 	 * @date:	04/24/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Updates a Google Account
     */
	public void updateObject(sObject objectToUpdate, ObjectModel updatingData)
	{
		Savepoint sp = Database.setSavepoint();
		try
		{
			if(objectToUpdate == null)
			{
				insert insertObject(updatingData);
			}
			else
			{
				upsert updateModel(objectToUpdate, updatingData);
			}
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			objectStatus = ex.getMessage();
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(String objectName)
	{
		try
		{
			Account accountToDelete = [SELECT id
										 FROM Account
										 WHERE Name =: objectName limit 1];
			delete accountToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public void deleteObject(Id objectId)
	{
		try
		{
			Account accountToDelete = [SELECT id
										 FROM Account
										 WHERE id =: objectId];
			delete accountToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/24/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Inserts a new Google Account/Account
	 */
	public sObject insertObject(ObjectModel objectModel)
	{
		Account accountToInsert = new Account();
		GoogleAccountModel acctModel = (GoogleAccountModel)objectModel; 
		try
		{
			if(accountToInsert != null && acctModel != null)
			{
				//accountToInsert.Name = acctModel.Name;
				accountToInsert.LastName = acctModel.LastName;
				accountToInsert.FirstName = acctModel.FirstName;
				accountToInsert.Salutation = acctModel.Salutation;
				accountToInsert.Type = acctModel.Type;
				accountToInsert.RecordTypeId = acctModel.RecordTypeId;
				accountToInsert.ParentId = acctModel.ParentId;
				accountToInsert.BillingStreet = acctModel.BillingStreet;
				accountToInsert.BillingCity = acctModel.BillingCity;
				accountToInsert.BillingState = acctModel.BillingState;
				accountToInsert.BillingPostalCode = acctModel.BillingPostalCode;
				accountToInsert.BillingCountry = acctModel.BillingCountry;
				accountToInsert.BillingLatitude = acctModel.BillingLatitude;
				accountToInsert.BillingLongitude = acctModel.BillingLongitude;
				accountToInsert.ShippingStreet = acctModel.ShippingStreet;
				accountToInsert.ShippingCity = acctModel.ShippingCity;
				accountToInsert.ShippingState = acctModel.ShippingState;
				accountToInsert.ShippingPostalCode = acctModel.ShippingPostalCode;
				accountToInsert.ShippingCountry = acctModel.ShippingCountry;
				accountToInsert.ShippingLatitude = acctModel.ShippingLatitude;
				accountToInsert.ShippingLongitude = acctModel.ShippingLongitude;
				accountToInsert.Phone = acctModel.Phone;
				accountToInsert.Fax = acctModel.Fax;
				accountToInsert.AccountNumber = acctModel.AccountNumber;
				accountToInsert.Website = acctModel.Website;
				accountToInsert.Sic = acctModel.Sic;
				accountToInsert.Industry = acctModel.Industry;
				accountToInsert.AnnualRevenue = acctModel.AnnualRevenue;
				accountToInsert.NumberOfEmployees = acctModel.NumberOfEmployees;
				accountToInsert.Ownership = acctModel.Ownership;
				accountToInsert.TickerSymbol = acctModel.TickerSymbol;
				accountToInsert.Description = acctModel.Description;
				accountToInsert.Rating = acctModel.Rating;
				accountToInsert.Site = acctModel.Site;
				if(acctModel.OwnerId != null){accountToInsert.OwnerId = acctModel.OwnerId;}
				accountToInsert.CreatedDate = acctModel.CreatedDate;
				accountToInsert.CreatedById = acctModel.CreatedById;
				accountToInsert.LastModifiedDate = acctModel.LastModifiedDate;
				accountToInsert.LastModifiedById = acctModel.LastModifiedById;
				accountToInsert.PersonMailingStreet = acctModel.PersonMailingStreet;
				accountToInsert.PersonMailingCity = acctModel.PersonMailingCity;
				accountToInsert.PersonMailingState = acctModel.PersonMailingState;
				accountToInsert.PersonMailingPostalCode = acctModel.PersonMailingPostalCode;
				if(acctModel.PersonMailingCountry != null){accountToInsert.PersonMailingCountry = acctModel.PersonMailingCountry;}
				accountToInsert.PersonMailingLatitude = acctModel.PersonMailingLatitude;
				accountToInsert.PersonMailingLongitude = acctModel.PersonMailingLongitude;
				accountToInsert.PersonOtherStreet = acctModel.PersonOtherStreet;
				accountToInsert.PersonOtherCity = acctModel.PersonOtherCity;
				accountToInsert.PersonOtherState = acctModel.PersonOtherState;
				accountToInsert.PersonOtherPostalCode = acctModel.PersonOtherPostalCode;
				accountToInsert.PersonOtherCountry = acctModel.PersonOtherCountry;
				accountToInsert.PersonOtherLatitude = acctModel.PersonOtherLatitude;
				accountToInsert.PersonOtherLongitude = acctModel.PersonOtherLongitude;
				accountToInsert.PersonMobilePhone = acctModel.PersonMobilePhone;
				accountToInsert.PersonHomePhone = acctModel.PersonHomePhone;
				accountToInsert.PersonOtherPhone = acctModel.PersonOtherPhone;
				accountToInsert.PersonAssistantPhone = acctModel.PersonAssistantPhone;
				accountToInsert.PersonEmail = acctModel.PersonEmail;
				accountToInsert.PersonTitle = acctModel.PersonTitle;
				accountToInsert.PersonDepartment = acctModel.PersonDepartment;
				accountToInsert.PersonAssistantName = acctModel.PersonAssistantName;
				accountToInsert.PersonLeadSource = acctModel.PersonLeadSource;
				accountToInsert.PersonBirthdate = acctModel.PersonBirthdate;
				accountToInsert.PersonEmailBouncedReason = acctModel.PersonEmailBouncedReason;
				accountToInsert.PersonEmailBouncedDate = acctModel.PersonEmailBouncedDate;
				accountToInsert.Jigsaw = acctModel.Jigsaw;
				accountToInsert.AccountSource = acctModel.AccountSource;
				accountToInsert.SicDesc = acctModel.SicDesc;
				accountToInsert.Last_Name__c = acctModel.Last_Name;
				accountToInsert.First_Name__c = acctModel.First_Name;
				accountToInsert.Mobile__c = acctModel.Mobile;
				accountToInsert.Other_Phone__c = acctModel.Other_Phone;
				accountToInsert.Salutation__c = acctModel.Salutation;
				accountToInsert.LegacySFContactID__c = acctModel.LegacySFContactID;
				accountToInsert.PersonAcctIDExternalID__c = acctModel.PersonAcctIDExternalID;
				if(acctModel.MIgration != null){accountToInsert.MIgration__c = acctModel.MIgration;}
				if(acctModel.DupFlag != null){accountToInsert.DupFlag__c = acctModel.DupFlag;}
				accountToInsert.PersonAcctContactLookup__c = acctModel.PersonAcctContactLookup;
				if(acctModel.CID_Exception_Granted != null){accountToInsert.CID_Exception_Granted__c = acctModel.CID_Exception_Granted;}
				if(acctModel.CID_Exception_Granted_Date_Time != null){accountToInsert.CID_Exception_Granted_Date_Time__c = acctModel.CID_Exception_Granted_Date_Time;}
				if(acctModel.CID_Exception_on_This_Asset != null){accountToInsert.CID_Exception_on_This_Asset__c = acctModel.CID_Exception_on_This_Asset;}
				if(acctModel.CID_Exception_Granted_Asset != null){accountToInsert.CID_Exception_Granted_Asset__c = acctModel.CID_Exception_Granted_Asset;}
				if(acctModel.Primary_Email_pc != null){accountToInsert.Primary_Email__pc = acctModel.Primary_Email_pc;}
				accountToInsert.Notes__pc = acctModel.Notes_pc;
				accountToInsert.Email__pc = acctModel.Email_pc;
				accountToInsert.OtherEmail__pc = acctModel.OtherEmail_pc;
				if(acctModel.Migration_pc != null){accountToInsert.Migration__pc = acctModel.Migration_pc;}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return accountToInsert;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/24/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Updates current Google Account/Account
	 */
	public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
	{
		Account accountToUpdate = (Account)sObjectToUpdate;
		GoogleAccountModel acctModel = (GoogleAccountModel)updatingData;
		try
		{
			if(accountToUpdate != null && updatingData != null)
			{
				if(acctModel.Name != null){accountToUpdate.Name = acctModel.Name;}
				if(acctModel.LastName != null){accountToUpdate.LastName = acctModel.LastName;}
				if(acctModel.FirstName != null){accountToUpdate.FirstName = acctModel.FirstName;}
				if(acctModel.Salutation != null){accountToUpdate.Salutation = acctModel.Salutation;}
				if(acctModel.Type != null){accountToUpdate.Type = acctModel.Type;}
				if(acctModel.RecordTypeId != null){accountToUpdate.RecordTypeId = acctModel.RecordTypeId;}
				if(acctModel.ParentId != null){accountToUpdate.ParentId = acctModel.ParentId;}
				if(acctModel.BillingStreet != null){accountToUpdate.BillingStreet = acctModel.BillingStreet;}
				if(acctModel.BillingCity != null){accountToUpdate.BillingCity = acctModel.BillingCity;}
				if(acctModel.BillingState != null){accountToUpdate.BillingState = acctModel.BillingState;}
				if(acctModel.BillingPostalCode != null){accountToUpdate.BillingPostalCode = acctModel.BillingPostalCode;}
				if(acctModel.BillingCountry != null){accountToUpdate.BillingCountry = acctModel.BillingCountry;}
				if(acctModel.BillingLatitude != null){accountToUpdate.BillingLatitude = acctModel.BillingLatitude;}
				if(acctModel.BillingLongitude != null){accountToUpdate.BillingLongitude = acctModel.BillingLongitude;}
				if(acctModel.ShippingStreet != null){accountToUpdate.ShippingStreet = acctModel.ShippingStreet;}
				if(acctModel.ShippingCity != null){accountToUpdate.ShippingCity = acctModel.ShippingCity;}
				if(acctModel.ShippingState != null){accountToUpdate.ShippingState = acctModel.ShippingState;}
				if(acctModel.ShippingPostalCode != null){accountToUpdate.ShippingPostalCode = acctModel.ShippingPostalCode;}
				if(acctModel.ShippingCountry != null){accountToUpdate.ShippingCountry = acctModel.ShippingCountry;}
				if(acctModel.ShippingLatitude != null){accountToUpdate.ShippingLatitude = acctModel.ShippingLatitude;}
				if(acctModel.ShippingLongitude != null){accountToUpdate.ShippingLongitude = acctModel.ShippingLongitude;}
				if(acctModel.Phone != null){accountToUpdate.Phone = acctModel.Phone;}
				if(acctModel.Fax != null){accountToUpdate.Fax = acctModel.Fax;}
				if(acctModel.AccountNumber != null){accountToUpdate.AccountNumber = acctModel.AccountNumber;}
				if(acctModel.Website != null){accountToUpdate.Website = acctModel.Website;}
				if(acctModel.Sic != null){accountToUpdate.Sic = acctModel.Sic;}
				if(acctModel.Industry != null){accountToUpdate.Industry = acctModel.Industry;}
				if(acctModel.AnnualRevenue != null){accountToUpdate.AnnualRevenue = acctModel.AnnualRevenue;}
				if(acctModel.NumberOfEmployees != null){accountToUpdate.NumberOfEmployees = acctModel.NumberOfEmployees;}
				if(acctModel.Ownership != null){accountToUpdate.Ownership = acctModel.Ownership;}
				if(acctModel.TickerSymbol != null){accountToUpdate.TickerSymbol = acctModel.TickerSymbol;}
				if(acctModel.Description != null){accountToUpdate.Description = acctModel.Description;}
				if(acctModel.Rating != null){accountToUpdate.Rating = acctModel.Rating;}
				if(acctModel.Site != null){accountToUpdate.Site = acctModel.Site;}
				if(accountToUpdate.OwnerId != null){ acctModel.OwnerId = acctModel.OwnerId;}
				if(acctModel.CreatedDate != null){accountToUpdate.CreatedDate = acctModel.CreatedDate;}
				if(acctModel.CreatedById != null){accountToUpdate.CreatedById = acctModel.CreatedById;}
				if(acctModel.LastModifiedDate != null){accountToUpdate.LastModifiedDate = acctModel.LastModifiedDate;}
				if(acctModel.LastModifiedById != null){accountToUpdate.LastModifiedById = acctModel.LastModifiedById;}
				if(acctModel.PersonMailingStreet != null){accountToUpdate.PersonMailingStreet = acctModel.PersonMailingStreet;}
				if(acctModel.PersonMailingCity != null){accountToUpdate.PersonMailingCity = acctModel.PersonMailingCity;}
				if(acctModel.PersonMailingState != null){accountToUpdate.PersonMailingState = acctModel.PersonMailingState;}
				if(acctModel.PersonMailingPostalCode != null){accountToUpdate.PersonMailingPostalCode = acctModel.PersonMailingPostalCode;}
				if(acctModel.PersonMailingCountry != null){accountToUpdate.PersonMailingCountry = acctModel.PersonMailingCountry;}
				if(acctModel.PersonMailingLatitude != null){accountToUpdate.PersonMailingLatitude = acctModel.PersonMailingLatitude;}
				if(acctModel.PersonMailingLongitude != null){accountToUpdate.PersonMailingLongitude = acctModel.PersonMailingLongitude;}
				if(acctModel.PersonOtherStreet != null){accountToUpdate.PersonOtherStreet = acctModel.PersonOtherStreet;}
				if(acctModel.PersonOtherCity != null){accountToUpdate.PersonOtherCity = acctModel.PersonOtherCity;}
				if(acctModel.PersonOtherState != null){accountToUpdate.PersonOtherState = acctModel.PersonOtherState;}
				if(acctModel.PersonOtherPostalCode != null){accountToUpdate.PersonOtherPostalCode = acctModel.PersonOtherPostalCode;}
				if(acctModel.PersonOtherCountry != null){accountToUpdate.PersonOtherCountry = acctModel.PersonOtherCountry;}
				if(acctModel.PersonOtherLatitude != null){accountToUpdate.PersonOtherLatitude = acctModel.PersonOtherLatitude;}
				if(acctModel.PersonOtherLongitude != null){accountToUpdate.PersonOtherLongitude = acctModel.PersonOtherLongitude;}
				if(acctModel.PersonMobilePhone != null){accountToUpdate.PersonMobilePhone = acctModel.PersonMobilePhone;}
				if(acctModel.PersonHomePhone != null){accountToUpdate.PersonHomePhone = acctModel.PersonHomePhone;}
				if(acctModel.PersonOtherPhone != null){accountToUpdate.PersonOtherPhone = acctModel.PersonOtherPhone;}
				if(acctModel.PersonAssistantPhone != null){accountToUpdate.PersonAssistantPhone = acctModel.PersonAssistantPhone;}
				if(acctModel.PersonEmail != null){accountToUpdate.PersonEmail = acctModel.PersonEmail;}
				if(acctModel.PersonTitle != null){accountToUpdate.PersonTitle = acctModel.PersonTitle;}
				if(acctModel.PersonDepartment != null){accountToUpdate.PersonDepartment = acctModel.PersonDepartment;}
				if(acctModel.PersonAssistantName != null){accountToUpdate.PersonAssistantName = acctModel.PersonAssistantName;}
				if(acctModel.PersonLeadSource != null){accountToUpdate.PersonLeadSource = acctModel.PersonLeadSource;}
				if(acctModel.PersonBirthdate != null){accountToUpdate.PersonBirthdate = acctModel.PersonBirthdate;}
				if(acctModel.PersonEmailBouncedReason != null){accountToUpdate.PersonEmailBouncedReason = acctModel.PersonEmailBouncedReason;}
				if(acctModel.PersonEmailBouncedDate != null){accountToUpdate.PersonEmailBouncedDate = acctModel.PersonEmailBouncedDate;}
				if(acctModel.Jigsaw != null){accountToUpdate.Jigsaw = acctModel.Jigsaw;}
				if(acctModel.AccountSource != null){accountToUpdate.AccountSource = acctModel.AccountSource;}
				if(acctModel.SicDesc != null){accountToUpdate.SicDesc = acctModel.SicDesc;}
				if(acctModel.Last_Name != null){accountToUpdate.Last_Name__c = acctModel.Last_Name;}
				if(acctModel.First_Name != null){accountToUpdate.First_Name__c = acctModel.First_Name;}
				if(acctModel.Mobile != null){accountToUpdate.Mobile__c = acctModel.Mobile;}
				if(acctModel.Other_Phone != null){accountToUpdate.Other_Phone__c = acctModel.Other_Phone;}
				if(acctModel.Salutation != null){accountToUpdate.Salutation__c = acctModel.Salutation;}
				if(acctModel.LegacySFContactID != null){accountToUpdate.LegacySFContactID__c = acctModel.LegacySFContactID;}
				if(acctModel.PersonAcctIDExternalID != null){accountToUpdate.PersonAcctIDExternalID__c = acctModel.PersonAcctIDExternalID;}
				if(acctModel.MIgration != null){accountToUpdate.MIgration__c = acctModel.MIgration;}
				if(acctModel.DupFlag != null){accountToUpdate.DupFlag__c = acctModel.DupFlag;}
				if(acctModel.PersonAcctContactLookup != null){accountToUpdate.PersonAcctContactLookup__c = acctModel.PersonAcctContactLookup;}
				if(acctModel.CID_Exception_Granted != null){accountToUpdate.CID_Exception_Granted__c = acctModel.CID_Exception_Granted;}
				if(acctModel.CID_Exception_Granted_Date_Time != null){accountToUpdate.CID_Exception_Granted_Date_Time__c = acctModel.CID_Exception_Granted_Date_Time;}
				if(acctModel.CID_Exception_on_This_Asset != null){accountToUpdate.CID_Exception_on_This_Asset__c = acctModel.CID_Exception_on_This_Asset;}
				if(acctModel.CID_Exception_Granted_Asset != null){accountToUpdate.CID_Exception_Granted_Asset__c = acctModel.CID_Exception_Granted_Asset;}
				if(acctModel.Primary_Email_pc != null){accountToUpdate.Primary_Email__pc = acctModel.Primary_Email_pc;}
				if(acctModel.Notes_pc != null){accountToUpdate.Notes__pc = acctModel.Notes_pc;}
				if(acctModel.Email_pc != null){accountToUpdate.Email__pc = acctModel.Email_pc;}
				if(acctModel.OtherEmail_pc != null){accountToUpdate.OtherEmail__pc = acctModel.OtherEmail_pc;}
				if(acctModel.Migration_pc != null){accountToUpdate.Migration__pc = acctModel.Migration_pc;}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage(); 
		}
		return accountToUpdate;
	}
}