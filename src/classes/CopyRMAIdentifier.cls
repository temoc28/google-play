public class CopyRMAIdentifier
{
	public static void UpdateValues(List<RMA__c> TNew)
	{
		/*Set<Id> RMAIds = new Set<Id>();
		for(RMA__c RMA: TNew)
			RMAIds.add(RMA.Id);
		
		List<RMA__c> NewRMAs = [SELECT Id, Name, RMA_ID_External_ID__c FROM RMA__c WHERE Id IN: RMAIds];
		for(RMA__c R : NewRMAs)
			R.RMA_ID_External_ID__c = R.Name;*/
		List<RMA__c> rmaList = new List<RMA__c>();
		for(RMA__c rma : TNew)
			rmaList.add(new RMA__c(Id=rma.Id,RMA_ID_External_ID__c=rma.Name));
		update rmaList;
		//update NewRMAs;
	}
	
	@isTest
    static void myUnitTest() {
 
		Google_Asset__c ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
        										, Asset_Type__c = 'New'
        										, Active_Device__c = 'Active');
		insert ga;
        RMA__c rma = new RMA__c (GoogleAsset__c = ga.Id);        
        insert rma;        
    }
}