@isTest//(SeeAllData=true)
public with sharing class TestTriggers {
    public static Account a;
    public static Google_Asset__c ga;
    public static Contact c;
    public static Order__c o;
    public static RMA__c rma;
    public static String unique;
    
    public static void setup() {
        Id RecId = [
                SELECT r.Id, r.Name, r.DeveloperName, r.IsPersonType 
                FROM RecordType r 
                WHERE sObjectType = 'Account' AND IsPersonType=True AND DeveloperName='GoogleCustomer'
            ].Id;
        
        a = new Account();
        a.Name = 'Test - ' + unique;
        insert a;
        
        ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
                                                , Asset_Type__c = 'New'
                                                , Active_Device__c = 'Active', AssetOwner__c = a.Id);
        insert ga;
        
        unique = String.valueof(Datetime.now()).replace(' ','_').replace('-','_').replace(':','_') + String.valueOf(Math.random());
        c = new Contact(lastname ='test_' + unique, email = 'test_'+ unique +'@testertest.com', AccountId = a.Id);
        insert c;
        
        Account updatea = new Account(Id = a.Id, RecordTypeId = RecId);
        update updatea;
        
        o = new Order__c(Name = 'test_order_' + unique);
        insert o;
        
        rma = new RMA__c(rma_country__c = 'US'
                                , Type__c = 'Galaxy Nexus - Remorse'
                                , Status__c = 'Pending Return'
                                , Notes__c = 'test-' + unique
                                , Google_Account__c = c.Id
                                , Order__c = o.Id
                                , GoogleAsset__c = ga.Id);
        insert rma;
    }

    static testMethod void testRMATrigger()
    {
        setup();
    
        RMA__c rma2 = new RMA__c(rma_country__c = 'US'
                                , Type__c = 'Galaxy Nexus - Remorse'
                                , Status__c = 'Pending Return'
                                , Notes__c = 'test_' + unique
                                , Google_Account__c = c.Id
                                , Order__c = o.Id
                                , GoogleAsset__c = ga.Id);
        
        Test.startTest();
              
            try
            {
                insert rma2;  
                System.assert(false);
            }
            catch (Exception e)
            {
                // expected
            }                 
                                        
        Test.stopTest();
        
    }
    
    static testMethod void testCaseTrigger() {
        setup();
        
        Case cas = new Case();
        cas.AccountId = a.Id;
        cas.RMA__c = rma.Id;
        cas.Google_Asset__c = ga.Id;
        
        Test.StartTest();
        insert cas;
        Test.StopTest();
    }
}