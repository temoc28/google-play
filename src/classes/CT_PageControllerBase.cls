public virtual class CT_PageControllerBase 
{
	final String CLASSNAME = '\n\n CT_PageControllerBase.METHODNAME()';
	   
    public Profile CurrentUserProfile{
      get;
      private set;
  }
  
    public User CurrentUser{
      get;
      private set;
    }
    
    public String host{
      get {
        return URL.getSalesforceBaseUrl().getHost();
      }
      private set;
    }
    
    public Boolean InProduction{
      get {
      	if(Test.isRunningTest())
      		return true;
  		else
        	return (this.host.contains('na0') || this.host.contains('na1.') || this.host.contains('na2')
		      		|| this.host.contains('na3') || this.host.contains('na4') || this.host.contains('na5')
		      		|| this.host.contains('na6') || this.host.contains('na7') || this.host.contains('na8')
		      		|| this.host.contains('na9') || this.host.contains('na10')|| this.host.contains('na11')
		      		|| this.host.contains('na12') || this.host.contains('na14') || this.host.contains('ap0')
		      		|| this.host.contains('ap1') || this.host.contains('eu0') || this.host.contains('eu1')
	          );
      }
      private set;
    }
    
    public Boolean InSandbox{
      get {
        return !InProduction;
        // return (this.host.contains('tapp0') || this.host.contains('cs1.') || this.host.contains('cs2') || this.host.contains('cs3')
      // || this.host.contains('cs4') || this.host.contains('cs5') || this.host.contains('cs6') || this.host.contains('cs7')
      // || this.host.contains('cs8') || this.host.contains('cs9') || this.host.contains('cs10') || this.host.contains('cs11')
      // || this.host.contains('cs12') || this.host.contains('cs13') || this.host.contains('cs14') || this.host.contains('cs15')
          // );
      }
      private set;
    }

    /*protected void SetCurrentUserInfo()
    {
        for(Profile prof : [SELECT Id, Name, Description, (SELECT Id, FirstName, LastName, Name, PortalRole, IsPortalSelfRegistered, IsPortalEnabled, ContactId FROM Users WHERE IsActive=true) FROM Profile])
        {
            Id ProfileId = prof.Id;
            if(ProfileId == UserInfo.getProfileId())
                CurrentUserProfile = prof; // set CurrentUserProfile
            for(User u : prof.Users){
                if(u.Id == UserInfo.getUserId()){
                    CurrentUser = u; // set CurrentUser
                    break;
                }
            }
        }
    }*/
    
    //@author : CloudTrigger, Inc.
	//@date : 04/19/2012
	//@description : creates new page reference 
	//@paramaters : String path to page 
	//@returns : PageReference
    protected PageReference Redirect(String pageReference){
    	
    	final String METHODNAME = CLASSNAME.replace('@@METHOD','Redirect');
		system.debug(LoggingLevel.INFO, METHODNAME.replace('*** ','*** Inside ') + '\n\n');
		        
        PageReference pageRef = new PageReference(pageReference);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public map<String, String> PageParams{
      get {
        if(PageParams == null || PageParams.size()==0){
            if(system.currentPageReference() != null && system.currentPageReference().getParameters() != null){
               PageParams = system.currentPageReference().getParameters();
            }
        }
        return PageParams;
      }
      private set;
    }

    public String AllQueryString {
        get {
          String QueryStringValue = ''; 
          map<String, String> params = ApexPages.currentPage().getParameters();
          for(String theKey : params.keySet())
          {
              if(QueryStringValue!='')
                  QueryStringValue += '&';
              QueryStringValue = QueryStringValue + theKey + '=' + params.get(theKey);    
          }
          return QueryStringValue;
      }
    }
    
    public Boolean InErrorState {
      get{
        return ApexPages.hasMessages(ApexPages.Severity.FATAL);
      }
    }
    
    public Boolean ExistingMessages {
      get{
        return ApexPages.hasMessages();
      }
    }

    protected void InfoMsg(String pMessage){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, pMessage));
    }

    protected void ErrorMsg(String pMessage){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, pMessage));
    }
    
    protected void WarningMsg(String pMessage){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, pMessage));
    }
    
    
    // List of States
    public list<SelectOption> States {
	    get {
			if( States == null || States.isEmpty() ) {
				States = new list<SelectOption>();
				States.add(new selectOption('Other', 'Other'));
				States.add(new selectOption('AL', 'ALABAMA'));
				States.add(new selectOption('AK', 'ALASKA'));
	            States.add(new selectOption('AZ', 'ARIZONA'));
	            States.add(new selectOption('AR', 'ARKANSAS'));
	            States.add(new selectOption('CA', 'CALIFORNIA'));
	            States.add(new selectOption('CO', 'COLORADO'));
	            States.add(new selectOption('CT', 'CONNECTICUT'));
	            States.add(new selectOption('DE', 'DELAWARE'));
	            States.add(new selectOption('DC', 'DISTRICT OF COLUMBIA'));
	            States.add(new selectOption('FL', 'FLORIDA'));
	            States.add(new selectOption('GA', 'GEORGIA'));
	            States.add(new selectOption('HI', 'HAWAII'));
	            States.add(new selectOption('ID', 'IDAHO'));
	            States.add(new selectOption('IL', 'ILLINOIS'));
	            States.add(new selectOption('IN', 'INDIANA'));
	            States.add(new selectOption('IA', 'IOWA'));
	            States.add(new selectOption('KS', 'KANSAS'));
	            States.add(new selectOption('KY', 'KENTUCKY'));
	            States.add(new selectOption('LA', 'LOUISIANA'));
	            States.add(new selectOption('ME', 'MAINE'));
	            States.add(new selectOption('MD', 'MARYLAND'));
	            States.add(new selectOption('MA', 'MASSACHUSETTS'));
	            States.add(new selectOption('MI', 'MICHIGAN'));
	            States.add(new selectOption('MN', 'MINNESOTA'));
	            States.add(new selectOption('MS', 'MISSISSIPPI'));
	            States.add(new selectOption('MO', 'MISSOURI'));
	            States.add(new selectOption('MT', 'MONTANA'));
	            States.add(new selectOption('NE', 'NEBRASKA'));
	            States.add(new selectOption('NV', 'NEVADA'));
	            States.add(new selectOption('NH', 'NEW HAMPSHIRE'));
	            States.add(new selectOption('NJ', 'NEW JERSEY'));
	            States.add(new selectOption('NM', 'NEW MEXICO'));
	            States.add(new selectOption('NY', 'NEW YORK'));
	            States.add(new selectOption('NC', 'NORTH CAROLINA'));
	            States.add(new selectOption('ND', 'NORTH DAKOTA'));
	            States.add(new selectOption('OH', 'OHIO'));
	            States.add(new selectOption('OK', 'OKLAHOMA'));
	            States.add(new selectOption('OR', 'OREGON'));
	            States.add(new selectOption('PA', 'PENNSYLVANIA'));
	            States.add(new selectOption('PR', 'PUERTO RICO'));
	            States.add(new selectOption('RI', 'RHODE ISLAND'));
	            States.add(new selectOption('SC', 'SOUTH CAROLINA'));
	            States.add(new selectOption('SD', 'SOUTH DAKOTA'));
	            States.add(new selectOption('TN', 'TENNESSEE'));
	            States.add(new selectOption('TX', 'TEXAS'));
	            States.add(new selectOption('UT', 'UTAH'));
	            States.add(new selectOption('VT', 'VERMONT'));
	            States.add(new selectOption('VA', 'VIRGINIA'));
	            States.add(new selectOption('WA', 'WASHINGTON'));
	            States.add(new selectOption('WV', 'WEST VIRGINIA'));
	            States.add(new selectOption('WI', 'WISCONSIN'));
	            States.add(new selectOption('WY', 'WYOMING'));
			}
	        return States;
	    }
	    private set;
    }
    
    public list<SelectOption> Countries{
    get {
      if(Countries == null || Countries.isEmpty()){
            Countries = new list<SelectOption>();
            Countries.add(new selectOption('', '-SELECT-'));
            Countries.add(new selectOption('USA','United States'));
            Countries.add(new selectOption('ABW','Aruba'));
        Countries.add(new selectOption('AFG','Afghanistan'));
        Countries.add(new selectOption('AGO','Angola'));
        Countries.add(new selectOption('AIA','Anguilla'));
        Countries.add(new selectOption('ALA','Åland Islands'));
        Countries.add(new selectOption('ALB','Albania'));
        Countries.add(new selectOption('AND','Andorra'));
        Countries.add(new selectOption('ANT','Netherlands Antilles'));
        Countries.add(new selectOption('ARE','United Arab Emirates'));
        Countries.add(new selectOption('ARG','Argentina'));
        Countries.add(new selectOption('ARM','Armenia'));
        Countries.add(new selectOption('ASM','American Samoa'));
        Countries.add(new selectOption('ATA','Antarctica'));
        Countries.add(new selectOption('ATF','French Southern Territories'));
        Countries.add(new selectOption('ATG','Antigua and Barbuda'));
        Countries.add(new selectOption('AUS','Australia'));
        Countries.add(new selectOption('AUT','Austria'));
        Countries.add(new selectOption('AZE','Azerbaijan'));
        Countries.add(new selectOption('BDI','Burundi'));
        Countries.add(new selectOption('BEL','Belgium'));
        Countries.add(new selectOption('BEN','Benin'));
        Countries.add(new selectOption('BES','Bonaire, Sint Eustatius and Saba'));
        Countries.add(new selectOption('BFA','Burkina Faso'));
        Countries.add(new selectOption('BGD','Bangladesh'));
        Countries.add(new selectOption('BGR','Bulgaria'));
        Countries.add(new selectOption('BHR','Bahrain'));
        Countries.add(new selectOption('BHS','Bahamas'));
        Countries.add(new selectOption('BIH','Bosnia and Herzegovina'));
        Countries.add(new selectOption('BLM','Saint Barthélemy'));
        Countries.add(new selectOption('BLR','Belarus'));
        Countries.add(new selectOption('BLZ','Belize'));
        Countries.add(new selectOption('BMU','Bermuda'));
        Countries.add(new selectOption('BOL','Bolivia, Plurinational State Of'));
        Countries.add(new selectOption('BRA','Brazil'));
        Countries.add(new selectOption('BRB','Barbados'));
        Countries.add(new selectOption('BRN','Brunei Darussalam'));
        Countries.add(new selectOption('BTN','Bhutan'));
        Countries.add(new selectOption('BVT','Bouvet Island'));
        Countries.add(new selectOption('BWA','Botswana'));
        Countries.add(new selectOption('CAF','Central African Republic'));
        Countries.add(new selectOption('CAN','Canada'));
        Countries.add(new selectOption('CCK','Cocos (Keeling) Islands'));
        Countries.add(new selectOption('CHE','Switzerland'));
        Countries.add(new selectOption('CHL','Chile'));
        Countries.add(new selectOption('CHN','China'));
        Countries.add(new selectOption('CIV','Côte DIvoire'));
        Countries.add(new selectOption('CMR','Cameroon'));
        Countries.add(new selectOption('COD','Congo, The Democratic Republic Of The'));
        Countries.add(new selectOption('COG','Congo'));
        Countries.add(new selectOption('COK','Cook Islands'));
        Countries.add(new selectOption('COL','Colombia'));
        Countries.add(new selectOption('COM','Comoros'));
        Countries.add(new selectOption('CPV','Cape Verde'));
        Countries.add(new selectOption('CRI','Costa Rica'));
        Countries.add(new selectOption('CUB','Cuba'));
        Countries.add(new selectOption('CUW','Curaçao'));
        Countries.add(new selectOption('CXR','Christmas Island'));
        Countries.add(new selectOption('CYM','Cayman Islands'));
        Countries.add(new selectOption('CYP','Cyprus'));
        Countries.add(new selectOption('CZE','Czech Republic'));
        Countries.add(new selectOption('DEU','Germany'));
        Countries.add(new selectOption('DJI','Djibouti'));
        Countries.add(new selectOption('DMA','Dominica'));
        Countries.add(new selectOption('DNK','Denmark'));
        Countries.add(new selectOption('DOM','Dominican Republic'));
        Countries.add(new selectOption('DZA','Algeria'));
        Countries.add(new selectOption('ECU','Ecuador'));
        Countries.add(new selectOption('EGY','Egypt'));
        Countries.add(new selectOption('ERI','Eritrea'));
        Countries.add(new selectOption('ESH','Western Sahara'));
        Countries.add(new selectOption('ESP','Spain'));
        Countries.add(new selectOption('EST','Estonia'));
        Countries.add(new selectOption('ETH','Ethiopia'));
        Countries.add(new selectOption('FIN','Finland'));
        Countries.add(new selectOption('FJI','Fiji'));
        Countries.add(new selectOption('FLK','Falkland Islands (Malvinas)'));
        Countries.add(new selectOption('FRA','France'));
        Countries.add(new selectOption('FRO','Faroe Islands'));
        Countries.add(new selectOption('FSM','Micronesia, Federated States Of'));
        Countries.add(new selectOption('GAB','Gabon'));
        Countries.add(new selectOption('GBR','United Kingdom'));
        Countries.add(new selectOption('GEO','Georgia'));
        Countries.add(new selectOption('GGY','Guernsey'));
        Countries.add(new selectOption('GHA','Ghana'));
        Countries.add(new selectOption('GIB','Gibraltar'));
        Countries.add(new selectOption('GIN','Guinea'));
        Countries.add(new selectOption('GLP','Guadeloupe'));
        Countries.add(new selectOption('GMB','Gambia'));
        Countries.add(new selectOption('GNB','Guinea-Bissau'));
        Countries.add(new selectOption('GNQ','Equatorial Guinea'));
        Countries.add(new selectOption('GRC','Greece'));
        Countries.add(new selectOption('GRD','Grenada'));
        Countries.add(new selectOption('GRL','Greenland'));
        Countries.add(new selectOption('GTM','Guatemala'));
        Countries.add(new selectOption('GUF','French Guiana'));
        Countries.add(new selectOption('GUM','Guam'));
        Countries.add(new selectOption('GUY','Guyana'));
        Countries.add(new selectOption('HKG','Hong Kong'));
        Countries.add(new selectOption('HMD','Heard and McDonald Islands'));
        Countries.add(new selectOption('HND','Honduras'));
        Countries.add(new selectOption('HRV','Croatia'));
        Countries.add(new selectOption('HTI','Haiti'));
        Countries.add(new selectOption('HUN','Hungary'));
        Countries.add(new selectOption('IDN','Indonesia'));
        Countries.add(new selectOption('IMN','Isle of Man'));
        Countries.add(new selectOption('IND','India'));
        Countries.add(new selectOption('IOT','British Indian Ocean Territory'));
        Countries.add(new selectOption('IRL','Ireland'));
        Countries.add(new selectOption('IRN','Iran, Islamic Republic Of'));
        Countries.add(new selectOption('IRQ','Iraq'));
        Countries.add(new selectOption('ISL','Iceland'));
        Countries.add(new selectOption('ISR','Israel'));
        Countries.add(new selectOption('ITA','Italy'));
        Countries.add(new selectOption('JAM','Jamaica'));
        Countries.add(new selectOption('JEY','Jersey'));
        Countries.add(new selectOption('JOR','Jordan'));
        Countries.add(new selectOption('JPN','Japan'));
        Countries.add(new selectOption('KAZ','Kazakhstan'));
        Countries.add(new selectOption('KEN','Kenya'));
        Countries.add(new selectOption('KGZ','Kyrgyzstan'));
        Countries.add(new selectOption('KHM','Cambodia'));
        Countries.add(new selectOption('KIR','Kiribati'));
        Countries.add(new selectOption('KNA','Saint Kitts And Nevis'));
        Countries.add(new selectOption('KOR','Korea, Republic of'));
        Countries.add(new selectOption('KWT','Kuwait'));
        Countries.add(new selectOption('LAO','Lao Peoples Democratic Republic'));
        Countries.add(new selectOption('LBN','Lebanon'));
        Countries.add(new selectOption('LBR','Liberia'));
        Countries.add(new selectOption('LBY','Libyan Arab Jamahiriya'));
        Countries.add(new selectOption('LCA','Saint Lucia'));
        Countries.add(new selectOption('LIE','Liechtenstein'));
        Countries.add(new selectOption('LKA','Sri Lanka'));
        Countries.add(new selectOption('LSO','Lesotho'));
        Countries.add(new selectOption('LTU','Lithuania'));
        Countries.add(new selectOption('LUX','Luxembourg'));
        Countries.add(new selectOption('LVA','Latvia'));
        Countries.add(new selectOption('MAC','Macao'));
        Countries.add(new selectOption('MAF','Saint Martin'));
        Countries.add(new selectOption('MAR','Morocco'));
        Countries.add(new selectOption('MCO','Monaco'));
        Countries.add(new selectOption('MDA','Moldova, Republic of'));
        Countries.add(new selectOption('MDG','Madagascar'));
        Countries.add(new selectOption('MDV','Maldives'));
        Countries.add(new selectOption('MEX','Mexico'));
        Countries.add(new selectOption('MHL','Marshall Islands'));
        Countries.add(new selectOption('MKD','Macedonia, the Former Yugoslav Republic Of'));
        Countries.add(new selectOption('MLI','Mali'));
        Countries.add(new selectOption('MLT','Malta'));
        Countries.add(new selectOption('MMR','Myanmar'));
        Countries.add(new selectOption('MNE','Montenegro'));
        Countries.add(new selectOption('MNG','Mongolia'));
        Countries.add(new selectOption('MNP','Northern Mariana Islands'));
        Countries.add(new selectOption('MOZ','Mozambique'));
        Countries.add(new selectOption('MRT','Mauritania'));
        Countries.add(new selectOption('MSR','Montserrat'));
        Countries.add(new selectOption('MTQ','Martinique'));
        Countries.add(new selectOption('MUS','Mauritius'));
        Countries.add(new selectOption('MWI','Malawi'));
        Countries.add(new selectOption('MYS','Malaysia'));
        Countries.add(new selectOption('MYT','Mayotte'));
        Countries.add(new selectOption('NAM','Namibia'));
        Countries.add(new selectOption('NCL','New Caledonia'));
        Countries.add(new selectOption('NER','Niger'));
        Countries.add(new selectOption('NFK','Norfolk Island'));
        Countries.add(new selectOption('NGA','Nigeria'));
        Countries.add(new selectOption('NIC','Nicaragua'));
        Countries.add(new selectOption('NIU','Niue'));
        Countries.add(new selectOption('NLD','Netherlands'));
        Countries.add(new selectOption('NOR','Norway'));
        Countries.add(new selectOption('NPL','Nepal'));
        Countries.add(new selectOption('NRU','Nauru'));
        Countries.add(new selectOption('NZL','New Zealand'));
        Countries.add(new selectOption('OMN','Oman'));
        Countries.add(new selectOption('PAK','Pakistan'));
        Countries.add(new selectOption('PAN','Panama'));
        Countries.add(new selectOption('PCN','Pitcairn'));
        Countries.add(new selectOption('PER','Peru'));
        Countries.add(new selectOption('PHL','Philippines'));
        Countries.add(new selectOption('PLW','Palau'));
        Countries.add(new selectOption('PNG','Papua New Guinea'));
        Countries.add(new selectOption('POL','Poland'));
        Countries.add(new selectOption('PRI','Puerto Rico'));
        Countries.add(new selectOption('PRK','Korea, Democratic Peoples Republic Of'));
        Countries.add(new selectOption('PRT','Portugal'));
        Countries.add(new selectOption('PRY','Paraguay'));
        Countries.add(new selectOption('PSE','Palestinian Territory, Occupied'));
        Countries.add(new selectOption('PYF','French Polynesia'));
        Countries.add(new selectOption('QAT','Qatar'));
        Countries.add(new selectOption('REU','Réunion'));
        Countries.add(new selectOption('ROU','Romania'));
        Countries.add(new selectOption('RUS','Russian Federation'));
        Countries.add(new selectOption('RWA','Rwanda'));
        Countries.add(new selectOption('SAU','Saudi Arabia'));
        Countries.add(new selectOption('SDN','Sudan'));
        Countries.add(new selectOption('SEN','Senegal'));
        Countries.add(new selectOption('SGP','Singapore'));
        Countries.add(new selectOption('SGS','South Georgia and the South Sandwich Islands'));
        Countries.add(new selectOption('SHN','Saint Helena, Ascension and Tristan Da Cunha'));
        Countries.add(new selectOption('SJM','Svalbard And Jan Mayen'));
        Countries.add(new selectOption('SLB','Solomon Islands'));
        Countries.add(new selectOption('SLE','Sierra Leone'));
        Countries.add(new selectOption('SLV','El Salvador'));
        Countries.add(new selectOption('SMR','San Marino'));
        Countries.add(new selectOption('SOM','Somalia'));
        Countries.add(new selectOption('SPM','Saint Pierre And Miquelon'));
        Countries.add(new selectOption('SRB','Serbia'));
        Countries.add(new selectOption('SSD','South Sudan'));
        Countries.add(new selectOption('STP','Sao Tome and Principe'));
        Countries.add(new selectOption('SUR','Suriname'));
        Countries.add(new selectOption('SVK','Slovakia'));
        Countries.add(new selectOption('SVN','Slovenia'));
        Countries.add(new selectOption('SWE','Sweden'));
        Countries.add(new selectOption('SWZ','Swaziland'));
        Countries.add(new selectOption('SXM','Sint Maarten (Dutch part)'));
        Countries.add(new selectOption('SYC','Seychelles'));
        Countries.add(new selectOption('SYR','Syrian Arab Republic'));
        Countries.add(new selectOption('TCA','Turks and Caicos Islands'));
        Countries.add(new selectOption('TCD','Chad'));
        Countries.add(new selectOption('TGO','Togo'));
        Countries.add(new selectOption('THA','Thailand'));
        Countries.add(new selectOption('TJK','Tajikistan'));
        Countries.add(new selectOption('TKL','Tokelau'));
        Countries.add(new selectOption('TKM','Turkmenistan'));
        Countries.add(new selectOption('TLS','Timor-Leste'));
        Countries.add(new selectOption('TON','Tonga'));
        Countries.add(new selectOption('TTO','Trinidad and Tobago'));
        Countries.add(new selectOption('TUN','Tunisia'));
        Countries.add(new selectOption('TUR','Turkey'));
        Countries.add(new selectOption('TUV','Tuvalu'));
        Countries.add(new selectOption('TWN','Taiwan, Province Of China'));
        Countries.add(new selectOption('TZA','Tanzania, United Republic of'));
        Countries.add(new selectOption('UGA','Uganda'));
        Countries.add(new selectOption('UKR','Ukraine'));
        Countries.add(new selectOption('UMI','United States Minor Outlying Islands'));
        Countries.add(new selectOption('URY','Uruguay'));
        Countries.add(new selectOption('UZB','Uzbekistan'));
        Countries.add(new selectOption('VAT','Holy See (Vatican City State)'));
        Countries.add(new selectOption('VCT','Saint Vincent And The Grenedines'));
        Countries.add(new selectOption('VEN','Venezuela, Bolivarian Republic of'));
        Countries.add(new selectOption('VGB','Virgin Islands, British'));
        Countries.add(new selectOption('VIR','Virgin Islands, U.S.'));
        Countries.add(new selectOption('VNM','Viet Nam'));
        Countries.add(new selectOption('VUT','Vanuatu'));
        Countries.add(new selectOption('WLF','Wallis and Futuna'));
        Countries.add(new selectOption('WSM','Samoa'));
        Countries.add(new selectOption('YEM','Yemen'));
        Countries.add(new selectOption('ZAF','South Africa'));
        Countries.add(new selectOption('ZMB','Zambia'));
        Countries.add(new selectOption('ZWE','Zimbabwe'));
        }
          return Countries;
    }
    private set;
    }
    
     public list<SelectOption> DaysOfMonth {
       get {
          if(DaysOfMonth == null || DaysOfMonth.isEmpty()){
            DaysOfMonth = new list<SelectOption>(); 
            DaysOfMonth.add(new selectOption('SELECT', '- Select -')); 
            for(Integer i=1; i<=31; i++){
                DaysOfMonth.add(new SelectOption(String.valueof(i),String.valueof(i)));
            }
          }
          return DaysOfMonth;
      }
    private set;
   }
    
    public list<SelectOption> MonthsOfYear {
      get{    
          if(MonthsOfYear == null || MonthsOfYear.isEmpty()){
            MonthsOfYear = new list<SelectOption>(); 
            MonthsOfYear.add(new selectOption('SELECT', '- Select -')); 
            MonthsOfYear.add(new SelectOption('01','JAN'));
            MonthsOfYear.add(new SelectOption('02','FEB'));
            MonthsOfYear.add(new SelectOption('03','MAR'));
            MonthsOfYear.add(new SelectOption('04','APR'));
            MonthsOfYear.add(new SelectOption('05','MAY')); 
            MonthsOfYear.add(new SelectOption('06','JUN'));
            MonthsOfYear.add(new SelectOption('07','JUL'));       
            MonthsOfYear.add(new SelectOption('08','AUG'));
            MonthsOfYear.add(new SelectOption('09','SEP'));
            MonthsOfYear.add(new SelectOption('10','OCT'));
            MonthsOfYear.add(new SelectOption('11','NOV'));
            MonthsOfYear.add(new SelectOption('12','DEC'));
        }
          return MonthsOfYear;
      }
    private set;
  }

/* =============================================  TEST METHODS  ============================================= */

    static testMethod void test_test()
    {
        Test.startTest();
        Google_Asset__c ga = new Google_Asset__c(SKU__c='test');
        insert ga;
		RMA__c rma = new RMA__c(GoogleAsset__c=ga.Id);
		insert rma;
		ApexPages.StandardController con = new ApexPages.StandardController(rma);
		CT_RMAServices controller = new CT_RMAServices(con);
	    list<SelectOption> mos = controller.MonthsOfYear;
	    system.assert(mos != null);
	    system.assert(mos.size() > 0);
	    list<SelectOption> dys = controller.DaysOfMonth;
	    system.assert(dys != null);
	    system.assert(dys.size() > 0);
	    list<SelectOption> cs = controller.Countries;
	    system.assert(cs != null);
	    system.assert(cs.size() > 0);
	    list<SelectOption> s = controller.States;
	    system.assert(s != null);
	    system.assert(s.size() > 0);
    
    
        Test.stopTest();
    }
}