/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCaseEmailMessageCount 
{
    static Case c;
    static testMethod void myUnitTest() 
    {
        List<EmailMessage> emList = new List<EmailMessage>();
        EmailMessage em = new EmailMessage();
        em.ParentId=c.Id;
        em.FromAddress='testxyz@google.com';
        em.FromName='Test Customer';
        em.Incoming=true;
        emList.add(em);
        
        EmailMessage em2 = new EmailMessage();
        em2.ParentId=c.Id;
        em2.FromAddress='testxyz@google.com';
        em2.FromName='support user';
        em2.Incoming=false;
        emList.add(em2);
        
        insert emList;
        
        c = [Select Id,NumberOfEmailsFromSupportTeam__c,NumberOfEmailsFromCustomer__c, (Select Incoming, FromName From EmailMessages) From Case WHERE RecordType.Name='ChromeOs' and Id = :c.Id];
        System.assertEquals(1,c.NumberofEmailsFromCustomer__c);
        System.assertEquals(1,c.NumberofEmailsFromSupportTeam__c);
        
        List<Case> caseList = new List<Case>();
        caseList.add(c);
        
        BatchCaseEmailCount bEmailCount = new BatchCaseEmailCount();
        bEmailCount.execute(null, caseList);
        bEmailCount.finish(null);
        
        c = [Select Id,NumberOfEmailsFromSupportTeam__c,NumberOfEmailsFromCustomer__c, (Select Incoming, FromName From EmailMessages) From Case WHERE RecordType.Name='ChromeOs' and Id = :c.Id];
        System.assertEquals(1,c.NumberofEmailsFromCustomer__c);
        System.assertEquals(1,c.NumberofEmailsFromSupportTeam__c);
    }
    static
    {
    	c = new Case();
    	c.RecordTypeId = [SELECT Id FROM RecordType where SobjectType='Case' and Name = 'ChromeOs'].Id;
    	c.Status = 'New';
    	insert c;
    }
}