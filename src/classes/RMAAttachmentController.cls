public class RMAAttachmentController {
    private final RMA__c R;
    public List<Google_Asset__c> GAs {get;set;}
    public Boolean HasGA {get;set;}

    //Constructor
    public RMAAttachmentController(ApexPages.StandardController stdController) {
        this.R = [Select Id, GoogleAsset__c, Name, Type__c, RMA_Category__c, RMA_Sub_Category__c, GoogleCustomer__r.PersonAcctIDExternalID__c from RMA__c where Id = :stdController.getId()];
        
        getAssets();
        if(GAs.isEmpty()){
            HasGA = false;
            //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must add at least one RMA Asset to this RMA before generating a PDF.'));
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must populate the Google Asset on this RMA before generating a PDF.'));
        }
        else if(R.Type__c==null || R.RMA_Category__c==null || R.RMA_Sub_Category__c==null || R.GoogleCustomer__r.PersonAcctIDExternalID__c==null){
            HasGA = false;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must populate the RMA Category, RMA Sub-Category, Type and Google Account on this RMA before generating a PDF.'));
        } else {
            HasGA = true;
        }
        
    }
    
    public void getAssets(){
        /*
        Set<Id> AssetIds = new Set<Id>();
        for(RMA_Asset__c RA : [SELECT Id, Google_Asset__c, RMA__c FROM RMA_Asset__c WHERE RMA__c =: R.Id AND Google_Asset__c != NULL]){
            AssetIds.add(RA.Google_Asset__c);
        }
        GAs = [SELECT Id, Name, SKU__c, SKU_Description__c, repair_partner__c, Asset_Type__c FROM Google_Asset__c WHERE Id IN: AssetIds];
        */
        GAs = [SELECT Id, Name, SKU__c, SKU_Description__c, ProductID__r.repairpartner__c, Asset_Type__c FROM Google_Asset__c WHERE Id = :R.GoogleAsset__c];
    }//repair_partner__c,
/*
    public PageReference sendPdf() {
        if(HasGA) {
        // Reference the attachment page, pass in the RMA ID 
        PageReference pdf = Page.RMAAttachment;
        pdf.getParameters().put('id',R.Id);
        pdf.setRedirect(true);
        
        // Take the PDF content 
        Blob body;        
        try {
            body = pdf.getContent();
        } catch (VisualforceException e) {
            body = Blob.valueOf('Error, please try again.');
        }

        // Create the email attachment        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setInline(false);
        attach.Body = body;
        Datetime today = Datetime.now();
        attach.setFileName(today.year()+'-'+today.month()+'-'+today.day() + R.Name + '.pdf');        
 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { R.GoogleCustomer__r.PersonAcctIDExternalID__c });
        mail.setSubject('PDF Email Demo');
        mail.setHtmlBody('Here is the email you requested! Check the attachment!');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
        System.Debug(mail);
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with PDF sent to ' + R.GoogleCustomer__r.PersonAcctIDExternalID__c));
        }
        return null;
    }
*/
    
    @isTest
    static void myUnitTest1() {
        Id RecId = [
            SELECT r.Id, r.Name, r.DeveloperName, r.IsPersonType 
            FROM RecordType r 
            WHERE sObjectType = 'Account' AND IsPersonType=True AND DeveloperName='GoogleCustomer'
        ].Id;
        
        Google_Asset__c ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
                                                , Asset_Type__c = 'New'
                                                , Active_Device__c = 'Active');
        insert ga;
        
        Account acct = new Account(RecordTypeId=RecId, LastName='Last Name', PersonEmail='test@testorg.com', Phone='555-555-5555', PersonMailingCountry='US', PersonMailingState='GA');
        insert acct;
        
        RMA__c RMA = new RMA__c(GoogleAsset__c = ga.Id, Type__c='Test', RMA_Category__c='Test1', RMA_Sub_Category__c='Test2', GoogleCustomer__c=acct.Id, Notes__c='Test Notes');
        insert RMA;

        ApexPages.StandardController testCon = new ApexPages.StandardController(RMA);
        RMAAttachmentController controller = new RMAAttachmentController(testCon);
        controller.getAssets();
        //controller.sendPdf();
    }
    @isTest
    static void myUnitTest2() {
        Google_Asset__c ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
                                                , Asset_Type__c = 'New'
                                                , Active_Device__c = 'Active');
        insert ga;

        RMA__c RMA = new RMA__c(GoogleAsset__c = ga.Id );
        insert RMA;

        ApexPages.StandardController testCon = new ApexPages.StandardController(RMA);
        RMAAttachmentController controller = new RMAAttachmentController(testCon);
        controller.getAssets();
    }
}