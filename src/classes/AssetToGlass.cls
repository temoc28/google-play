public with sharing class AssetToGlass 
{
	public Google_Asset__c ga {get;set;}
	
	public AssetToGlass(ApexPages.StandardController con)
	{
		ga = (Google_Asset__c)con.getRecord();
	}
	public PageReference sendToGlass()
	{
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,PlayToGlass.sendData(ga.Id)));		
		//PlayToGlass.sendDataFuture(new Set<Id>{ga.Id});
		return null;
	}
}