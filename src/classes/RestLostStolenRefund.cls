@RestResource(urlMapping='/LostStolenRefund/*')
global class RestLostStolenRefund {


	global class LostStolenRMARequest{
		public String order_id;
		public String notes;
		public String agent_LDAP;
		public String case_id;
	}

	@HttpPost
	global static RestResponseModel.lostStolenRefundResponse createLostStolenRMA(LostStolenRMARequest refund){

		RestResponseModel.lostStolenRefundResponse response = new RestResponseModel.lostStolenRefundResponse();
		//Instantiate the correct opportunity as the standard controller

		String orderId = refund.order_id;

		List<Opportunity> opp = [SELECT Id, Ship_By_Date__c FROM Opportunity WHERE Name = :orderId];

		if(opp.size() > 0)
		{
			ApexPages.StandardController std = new ApexPages.StandardController(opp.get(0));

			RefundLostStolen controller = new RefundLostStolen(std);

			controller.refund.RefundNotes__c = refund.notes;

			PageReference pageRef = controller.createLostStolenRefund();

			if(pageRef == null)
			{
				response.failure_reason = 'There is already a Total Order Refund created for this Order';
				return response;
			}
			else
			{
				response.refund_id = controller.refund.Id;
				return response;
			}
		}
		else
		{
			response.failure_reason = 'No record found with the following Order Id: ' + orderId;
			return response;
		}
	}
}