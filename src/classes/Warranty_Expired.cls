public class Warranty_Expired {
    public Warranty_Expired(ApexPages.StandardController stdController) {
        asset = (Google_Asset__c)stdController.getRecord();
        asset = [select Sale_Country__c, Warranty_Countdown__c from Google_Asset__c where Id = :asset.Id];
    }
    
    public Warranty_Expired(Google_Asset__c asset) {
        this.asset = [select RecordTypeId, Sale_Country__c, Warranty_Expiration_Date__c, Retail_Sale_Date__c, Warranty_Period__c, Warranty_Countdown__c from Google_Asset__c where Id = :asset.Id];
    }
    
    private Google_Asset__c asset {get; set;}
    
    public String color {get; set;}
    
    public Boolean render {
        get { 
            return message.length() > 0;
        } 
        
        set; 
     }
    
    public String message {
        get {
            if (message == null) {
                // EU countries: 0-6mo=GREEN, message="New to 6 months"
                //               6mo-1year=ORANGE, message="6 months to 1 year"
                //               1 year+=RED, message="YEAR 2"
                //               2 year+=RED, message="WARRANTY EXPIRED"           
                // Other countries: Past 365 days=RED, message ="WARRANTY EXPIRED"
                //System.debug('===================================== Warranty_Countdown__c: ' + asset.Warranty_Countdown__c);
                
                if (asset.Sale_Country__c != null && (asset.Sale_Country__c.equals('UK') ||
                    asset.Sale_Country__c.equals('ES') ||
                    asset.Sale_Country__c.equals('AU') ||
                    asset.Sale_Country__c.equals('IT') ||
                    asset.Sale_Country__c.equals('FR') ||
                    asset.Sale_Country__c.equals('DE'))) {
                        if (asset.Warranty_Countdown__c != null) {
                            if (asset.Warranty_Countdown__c > 547) { //0-6mo
                                message = 'In Warranty';
                                color = 'green';
                            } else if (asset.Warranty_Countdown__c <= 547 && asset.Warranty_Countdown__c > 365) { //6mo-1year
                                message = 'In Warranty';
                                color = 'green';
                            } else if (asset.Warranty_Countdown__c <= 365 && asset.Warranty_Countdown__c > 0) { //1year-2years
                                message = 'In Warranty';
                                color = 'green';
                            } else {
                                message = 'WARRANTY EXPIRED';
                                color = 'red';
                            }
                        } else {
                            message = '';
                        }
                    } else {
                        if (asset.Warranty_Countdown__c != null && asset.Warranty_Countdown__c <= 0) {
                            message = 'WARRANTY EXPIRED';
                            color = 'red';
                        } else {
                            message = '';
                        }
                    }
            }
            return message;
        }
        
        set;
    }
}