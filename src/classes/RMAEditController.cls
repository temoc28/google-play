public class RMAEditController 
{
	@TestVisible
	private Map<String,Map<String,List<String>>> rmaCategoryMap {get;set;}
	
	public List<SelectOption> optionsType {get; set;}
	public List<SelectOption> optionsCat {get; set;}
	public List<SelectOption> optionsSubCat {get; set;}
	public Id codeToUse {get; set;}
	public Rma__c rmaToEdit {get; set;}
	public ApexPages.StandardController controller {get; set;}
	public boolean googlerUserOnly {get; set;}
	public boolean novaTablet {get; set;}
		
	public RMAEditController(ApexPages.StandardController std) 
	{
		try
		{
			googlerUserOnly = false;
			controller = std;
			rmaToEdit = (Rma__c)std.getRecord();
			rmaCategoryMap = new Map<String,Map<String,List<String>>>();
			if(rmaToEdit.Id != null)
			{
				String rmaId = rmaToEdit.Id;
				SObjectType soType = Schema.getGlobalDescribe().get('RMA__c');
   				Map<String,Schema.SObjectField> currentFields = soType.getDescribe().fields.getMap();
   				String queryRMA = 'SELECT ';
   				for(String currentField : currentFields.keySet())
   				{
   					queryRMA += currentField + ', ';
   				}
   				queryRMA += 'GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c FROM RMA__c WHERE Id =: rmaId Limit 1';
   				rmaToEdit = Database.query(queryRMA);
   				if(rmaToEdit.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c != null)
   				{
   					codeToUse = rmaToEdit.GoogleAsset__r.ProductID__r.Document__r.Return_Reason_Code_Set__c;
   				}
				getOptionMap();
			}
			else
			{
				optionsType= new List<SelectOption>();
				optionsCat= new List<SelectOption>();
				optionsSubCat = new List<SelectOption>();
				optionsType.add(new SelectOption('--None--', '--None--'));
				optionsCat.add(new SelectOption('--None--', '--None--'));
				optionsSubCat.add(new SelectOption('--None--', '--None--'));
			}
			if(rmaToEdit.recordTypeId == null && ApexPages.currentPage().getParameters().get('recordType') != null)
			{
				rmaToEdit.recordTypeId = ApexPages.currentPage().getParameters().get('recordType');
			}
			
			googlerUserOnly = isRecordTypeProcessed('Googler User Only');
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}
	
	/**
	 * @date: 			02/04/2016
	 * @description:	Updates both category and subcategory pick lists when the RMA object
	 *					is of Record Type 'Googler User Only.' See Googler User Only custom 
	 *					setting (RMATypeCat__c) for more details.	
	 */
	public boolean isRecordTypeProcessed(String recordTypeName)
	{
		try
		{
			Id rmaRecordTypeId = RmaTrgHandler.getRMARecordType('RMA__c', recordTypeName);
			if(rmaRecordTypeId != null)
			{
				List<RMATypeCat__c> rmaTypeCats = [SELECT Category__c, SubCategory__c, Type__c, Service_Model__c
	 					   	   					   FROM RMATypeCat__c
	 					       					   WHERE Name =: recordTypeName];
	 			if(rmaTypeCats.size() > 0 && rmaRecordTypeId == rmaToEdit.recordTypeId)
	 			{
	 				populateOptions(rmaTypeCats);	
	 				return true;
	 			}
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
		return false;
	}
	
	/**
	 * @date:			02/05/16
	 * @description:	Re-populates picklist values for types, categories, and subcategories
	 *
	 */
	private void populateOptions(List<RMATypeCat__c> rmaTypeCats)
	{
		try
		{
			optionsType= new List<SelectOption>();
			optionsCat= new List<SelectOption>();
			optionsSubCat = new List<SelectOption>();
			
			if(rmaTypeCats.get(0).Type__c != null && rmaTypeCats.get(0).Type__c != '')
			{
				List<String> typesSplitter = rmaTypeCats.get(0).Type__c.split(';');
				for(String typeS : typesSplitter)
				{
					optionsType.add(new SelectOption(typeS, typeS));
				}
			}
			
			if(rmaTypeCats.get(0).Category__c != null && rmaTypeCats.get(0).Category__c != '')
			{
				List<String> categoriesSplitter = rmaTypeCats.get(0).Category__c.split(';');
				for(String cats : categoriesSplitter)
				{
					optionsCat.add(new SelectOption(cats, cats));
				}
			}
			
			if(rmaTypeCats.get(0).SubCategory__c != null && rmaTypeCats.get(0).SubCategory__c != '')
			{
				List<String> subcategoriesSplitter = rmaTypeCats.get(0).SubCategory__c.split(';');
				for(String subcats : subcategoriesSplitter)
				{
					optionsSubCat.add(new SelectOption(subcats, subcats));
				}
			}
			
			if(rmaTypeCats.get(0).Service_Model__c != null && rmaTypeCats.get(0).Service_Model__c != '')
			{
				List<String> serviceModelsSplitter = rmaTypeCats.get(0).Service_Model__c.split(';');
				for(String service : serviceModelsSplitter)
				{
					optionsSubCat.add(new SelectOption(service, service));
				}
			}	
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
	}
		
	public void checkAsset()
	{
		try
		{
			if(googlerUserOnly)return;
			
			if(rmaToEdit.GoogleAsset__c != null && String.isNotEmpty(rmaToEdit.GoogleAsset__c))
			{
				Google_Asset__c rmaCode = ([SELECT ProductID__r.Document__r.Return_Reason_Code_Set__c FROM Google_Asset__c WHERE Id =: rmaToEdit.GoogleAsset__c]);
				if(rmaCode.ProductID__r.Document__r.Return_Reason_Code_Set__c != null)
				{
					codeToUse = rmaCode.ProductID__r.Document__r.Return_Reason_Code_Set__c;
				}
				getOptionMap();
			}
			else
			{
				rmaToEdit.Type__c = '--None--';
				rmaToEdit.RMA_Category__c = '--None--';
				rmaToEdit.RMA_Sub_Category__c = '--None--';
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}
	
	public void getOptionMap()
	{
		try
		{
			List<Return_Reason_Code_Set__c> reasonCodes = ([SELECT Name, Id
									FROM Return_Reason_Code_Set__c
									WHERE Id =: codeToUse limit 1]);
			if(reasonCodes.size() > 0)
			{
				Return_Reason_Code_Set__c reasonCode = reasonCodes.get(0);
				List<Return_Reason_Code__c> rCodes = [SELECT Name, Category__c, CID__c, RMA_Types__c, Return_Reason_Code_Set__c
										FROM Return_Reason_Code__c
										WHERE Return_Reason_Code_Set__c =: reasonCode.Id];
				for(Return_Reason_Code__c rrc : rCodes)
				{
					if(rrc.RMA_Types__c != null && rrc.RMA_Types__c.length() > 0)
					{
						String[] typesSplitter = rrc.RMA_Types__c.split(';');
						if(typesSplitter.size() <= 0)
						{
							typesSplitter = new String[1];
							typesSplitter[0] = rrc.RMA_Types__c;
						}
						for(String rmaType : typesSplitter)
						{
							if(!rmaCategoryMap.containsKey(rmaType))
							{
								Map<String,List<String>> mapcat = new Map<String,List<String>>();
								List<String> nameToadd = new List<String>();
								if(rrc.Name != null)
								{
									nameToadd.add(rrc.Name);
								}
								if(rrc.Category__c != null)
								{
									mapcat.put(rrc.Category__c, nameToadd);
								}
								rmaCategoryMap.put(rmaType, mapcat);
							}
							else
							{
								Map<String,List<String>> mapcat = rmaCategoryMap.get(rmaType);
								if(mapcat.containsKey(rrc.Category__c))
								{
									List<String> nameToadd = mapcat.get(rrc.Category__c);
									if(rrc.Name != null && rrc.Name.length() >0)
									{
										nameToadd.add(rrc.Name);
									}
									mapcat.put(rrc.Category__c, nameToadd);
									rmaCategoryMap.put(rmaType, mapcat);
								}
								else
								{
									List<String> nameToadd = new List<String>();
									if(rrc.Name != null && rrc.Name.length() >0)
									{
										nameToadd.add(rrc.Name);
									}
									if(rrc.Category__c != null && rrc.Category__c.length() > 0)
									{
										mapcat.put(rrc.Category__c, nameToadd);
										rmaCategoryMap.put(rmaType, mapcat);
									}
								}
							}
						}
					}
				}
			}
			getAllOptions();
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
	}

	public void getAllOptions()
	{
		try
		{
			// Check if Googler User Only Record Type
			if(googlerUserOnly){return;}
		
			optionsType = getTypes();
			optionsCat = getCategories();
			optionsSubCat = getSubCategories();
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber() + ' ' + e.getStackTraceString());
		}
	}
	
	public Map<String, String> filter()
	{
		Map<String, String> filterMap = new Map<String, String>();
		try
		{
			if(rmaToEdit.ServiceModel__c == 'Remorse Only')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Remorse + DOA + Warranty')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				filterMap.put('Warranty DOA', 'Warranty DOA');
				filterMap.put('Warranty Regular', 'Warranty Regular');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Remorse + DOA + Warranty + EW')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				filterMap.put('Warranty DOA', 'Warranty DOA');
				filterMap.put('Warranty Regular', 'Warranty Regular');
				filterMap.put('Extended Warranty', 'Extended Warranty');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Remorse + DOA + Warranty + Repair')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				filterMap.put('Warranty DOA', 'Warranty DOA');
				filterMap.put('Warranty Regular', 'Warranty Regular');
				filterMap.put('Repair', 'Repair');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Remorse + DOA')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				filterMap.put('Warranty DOA', 'Warranty DOA');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Remorse + Warranty Refund')
			{
				filterMap.put('Buyer\'s Remorse', 'Buyer\'s Remorse');
				filterMap.put('Warranty Refund',  'Warranty Refund');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Warranty Only')
			{
				filterMap.put('Warranty Regular', 'Warranty Regular');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Warranty + Repair')
			{
				filterMap.put('Warranty Regular', 'Warranty Regular');
				filterMap.put('Repair', 'Repair');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'Repair Only')
			{
				filterMap.put('Repair', 'Repair');
				return filterMap;
			}
			if(rmaToEdit.ServiceModel__c == 'DOA + Warranty')
			{
				filterMap.put('Warranty DOA', 'Warranty DOA');
				filterMap.put('Warranty Regular', 'Warranty Regular');
				return filterMap;
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
		}
		return filterMap;
	}
	
	public List<SelectOption> getTypes()
	{
		List<SelectOption> options = new List<SelectOption>();
		Map<String, String> filterMap = new Map<String, String>();
		filterMap = filter();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0)
			{
				for(String s : rmaCategoryMap.keySet())
				{
					if(filterMap.containsKey(s))
					{
						options.add(new SelectOption(s,s));
					}
				}
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber() + ' ' + e.getStackTraceString());
		}
		return options;
	}
	
	public void changeModel()
	{
		rmaToEdit.Type__c = '--None--';
		rmaToEdit.RMA_Category__c = '--None--';
		rmaToEdit.RMA_Sub_Category__c = '--None--';
		getAllOptions();
	}
	
	public List<SelectOption> getCategories()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0 && rmaCategoryMap.containsKey(rmaToEdit.Type__c) && rmaCategoryMap.get(rmaToEdit.Type__c).size() >0)
			{
				for(String s : rmaCategoryMap.get(rmaToEdit.Type__c).keySet())
				{
					options.add(new SelectOption(s,s));
				}
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber() + ' ' + e.getStackTraceString());
		}
		return options;
	}
	
	public List<SelectOption> getSubCategories()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--None--', '--None--'));
		try
		{
			if(rmaCategoryMap != null && rmaCategoryMap.size() > 0 && rmaCategoryMap.containsKey(rmaToEdit.Type__c) && rmaCategoryMap.get(rmaToEdit.Type__c).size() > 0 &&  rmaCategoryMap.get(rmaToEdit.Type__c).containsKey(rmaToEdit.RMA_Category__c) &&  rmaCategoryMap.get(rmaToEdit.Type__c).get(rmaToEdit.RMA_Category__c).size() > 0)
			{
				for(String s : rmaCategoryMap.get(rmaToEdit.Type__c).get(rmaToEdit.RMA_Category__c))
				{
					options.add(new SelectOption(s,s));
				}
				system.debug('Sub options ' + options);
			}
		}
		catch(Exception e)
		{
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber() + ' ' + e.getStackTraceString());
		}
		return options;
	}
	
	public PageReference saveRecord()
	{
		try
		{
			if(rmaToEdit.Type__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Type: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Category: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Sub_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Sub-Category: Validation Error: Value is required.'));
				return null;
			}
			upsert rmaToEdit;
			return new PageReference('/'+ rmaToEdit.Id);
		}
		catch(System.DMLException e) {
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, e.getDMLMessage(0)));
			return null;
		}
	}
	
	public PageReference saveNew()
	{
		try
		{
			if(rmaToEdit.Type__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Type: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Category: Validation Error: Value is required.'));
				return null;
			}
			if(rmaToEdit.RMA_Sub_Category__c == '--None--')
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'RMA Sub-Category: Validation Error: Value is required.'));
				return null;
			}
			upsert rmaToEdit;
			return new PageReference('/'+controller.getRecord().getSObjectType().getDescribe().getKeyPrefix()+'/e?recordType=' +  rmaToEdit.recordTypeId);
		}
		catch(System.DMLException e) {
			System.debug(e.getMessage() + ' in line '+ e.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, e.getDMLMessage(0)));
			return null;
		}
	}
}