/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestGoogleAssetTrg 
{
    static Google_Asset__c ga;
    static testMethod void myUnitTest() 
    {
        ga.Retail_Sale_Date__c=Date.today();
        ga.Name='testproduct2zzz';
        ga.SKUUpsert__c='noProductToBeFound';
        ga.SKU__c=null;
        update ga;
    }
    static
    {
    	CountryRules__c cr  = new CountryRules__c(Name='US',DOAPeriod__c=25,MinimumStandardWarranty__c=90,RemorsePeriod__c=25,MinimumReplacementWarranty__c=30,ExpectedSalesDate__c=30);
    	insert cr;
    	
    	Account a = new Account(FirstName='Test',LastName='User',PersonMailingCountry='US',PersonMailingState='GA',RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id);
    	insert a;
    	
    	Opportunity opp = new Opportunity(AccountId=a.Id,Name='TestXYX123',Country__c='US',State_Province__c='GA',Type='Standard Order',StageName='Closed Won',CloseDate=Date.today());
    	insert opp;
    	
    	Product2 p2 = new Product2(Name='TestProduct',ProductCode='testSKUZZZ',SKU__c='testSKUZZZ',IsActive=true);
    	insert p2;
    	
    	ga = new Google_Asset__c(Name='testSKUZZZ',SKU__c='testSKUZZZ',Order_Opportunity__c=opp.Id);
    	insert ga;
    }
}