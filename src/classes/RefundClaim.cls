public without sharing class RefundClaim 
{	
	public List<Refund__c> refundList {get;set;}
	private Refund__c refund;
	private final Id currentUserId;
	private boolean showPage;
	
	public RefundClaim(ApexPages.StandardController con)
	{
		showPage=false;		
		currentUserId = UserInfo.getUserId();
		refund = (Refund__c)con.getRecord();
		refundList = new List<Refund__c>([SELECT Id, Owner.Type,OwnerId,Name FROM Refund__c where Id=:refund.Id]);
		//refundList.add(refund);
		checkOwnership(refundList);		
	}
	public RefundClaim(ApexPages.StandardSetController setCon)
	{
		showPage=false;
		currentUserId = UserInfo.getUserId();
		refundList = new List<Refund__c>([SELECT Id, Owner.Type,OwnerId,Name FROM Refund__c where Id in :setCon.getRecords()]);
		checkOwnership(refundList);		
	}
	private void checkOwnership(List<Refund__c> refundList)
	{
		for(Refund__c refund : refundList)
		{
			if(refund.Owner.Type=='User' && refund.OwnerId!=currentUserId)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Are you sure you want to take ownership of this Refund from another Support Rep?'));
				showPage=true;
				break;
			}
		}
	}
	public PageReference checkClaim()
	{
		if(showPage)
			return null;
		else
			return claim();
	}
	public PageReference claim()
	{		
		for(Refund__c refund : refundList)
			refund.OwnerId = currentUserId;	
					
		update refundList;	
		
		if(refundList.size()==1)
			return new PageReference('/'+refundList.get(0).Id);
		else
			return new PageReference('/a0H');
	}
}