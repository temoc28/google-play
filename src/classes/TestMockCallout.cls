/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMockCallout 
{
    @isTest static void testCreateAll() 
    {              
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TestMockImpl());
        
        // Call the method that invokes a callout
        GlassDataToPlayWS.AccountObject ao = new GlassDataToPlayWS.AccountObject();
        ao.email='test@test.com';        
        ao.firstName='awesome';
		ao.lastName='dude';		
		ao.mailingCity='Calhoun';
		ao.mailingCountry='US';			
		ao.mailingPostalCode='30701';
		ao.mailingState='GA';
		ao.mailingStreet='103';
		ao.phone='7066026963';
        GlassDataToPlayWS.OpportunityObject oppObj = new GlassDataToPlayWS.OpportunityObject();
        oppObj.name='TestOpp';
		oppObj.closeDate=Date.today();		
		oppObj.stageName='Closed Won';
		oppObj.shipmentDate=Date.today();
		oppObj.typeOfOrder='Device Swap';
        GlassDataToPlayWS.AssetObject assetObj = new GlassDataToPlayWS.AssetObject(); 
        assetObj.name='testAsset';
        assetObj.sku='12345xyz';
        assetObj.serialNumber='axw123';       
        GlassDataToPlayWS.RmaObject rmaObj = new GlassDataToPlayWS.RmaObject();
        rmaObj.typeOfOrder='Device Swap';
		rmaObj.name='TestRMA';        
        System.debug(GlassDataToPlayWS.createAll(ao,oppObj,assetObj,rmaObj));
        Test.stopTest();
    }
    @isTest static void testChangeStatus() 
    {              
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TestMockImpl());
        
        // Call the method that invokes a callout
        List<GlassDataToPlayWS.RmaObject> rmaObjList = new List<GlassDataToPlayWS.RmaObject>();
        GlassDataToPlayWS.RmaObject rmaObj = new GlassDataToPlayWS.RmaObject();
        rmaObj.typeOfOrder='Device Swap';
		rmaObj.name='TestRMA'; 
		rmaObjList.add(rmaObj);
        GlassDataToPlayWS.changeRmaStatus(rmaObjList);
        Test.stopTest();
    }
    static
    {
    	Product2 p2 = new Product2(Name='TestProduct',ProductCode='12345xyz',SKU__c='12345xyz',IsActive=true);
    	insert p2;
    }
}