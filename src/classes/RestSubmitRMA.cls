@RestResource(urlMapping='/SubmitRMA/*')
global class RestSubmitRMA 
{   
    @HttpPost
    global static RestResponseModel.submit_rma_response_t doSubmitRMAs(RestResponseModel.rma_initiation_details_t requestDetails)
    {
        RestResponseModel.submit_rma_response_t submit_rma_response = new RestResponseModel.submit_rma_response_t();
        
        Double startTime = System.currentTimeMillis();
        List<Google_Asset__c> gaList = new List<Google_Asset__c>([SELECT Id,RecordType.Name,Order_Opportunity__c,Line_Number__c FROM Google_Asset__c WHERE Name=:requestDetails.unique_id]);
        system.debug('Google_Asset__c RT: ' + (System.currentTimeMillis() - startTime));
        
        boolean queryOpp=true;
        Google_Asset__c ga;
        Id oppId;
        if(gaList.size()!=1)
        {
            submit_rma_response.failure_reason='Device not found';
            return submit_rma_response;
        }
        else 
        {
            ga = gaList.get(0);
            oppId=ga.Order_Opportunity__c;
            if(ga.RecordType.Name=='Google Play Asset' && oppId==null)
            {
                submit_rma_response.failure_reason='This is a Google Play Asset but there is not a related order.';
                submit_rma_response.is_success=false;
                return submit_rma_response;
            }
        }
        startTime = System.currentTimeMillis();
        List<Account> aList = new List<Account>([SELECT Id FROM Account WHERE PersonEmail=:requestDetails.rma_requester_email]);
        system.debug('Account RT: ' + (System.currentTimeMillis() - startTime));
        if(aList.size()!=1)
        {
            submit_rma_response.failure_reason='Customer not found';
            submit_rma_response.is_success=false;
            return submit_rma_response;
        }
        
        /*
        removed in V1 - per Jordan
        if(ga.RecordType.Name!='Google Play Asset')
        {
            List<Opportunity> oppList = new List<Opportunity>([SELECT Id FROM Opportunity WHERE Name=:requestDetails.google_order_id]);
            if(oppList.size()!=1)
            {
                submit_rma_response.failure_reason='Order not found';
                submit_rma_response.is_success=false;
                return submit_rma_response;
            }
            else
                oppId=oppList.get(0).Id;
        }*/        
        String ownerId;
        startTime = System.currentTimeMillis();
        List<User> uList = new List<User>([SELECT Id FROM User WHERE IsActive=true AND LDAP__c=:requestDetails.rma_creator]);
        system.debug('User RT: ' + (System.currentTimeMillis() - startTime));
        
        if(uList.size()==1)
            ownerId=uList.get(0).Id;
        if(ownerId==null)
        {
            submit_rma_response.failure_reason='No user found for '+requestDetails.rma_creator;
            submit_rma_response.is_success=false;
            return submit_rma_response;
        }
        System.savePoint sp;
        try
        {
            //validation against line number to update          
            if(ga.RecordType.Name!='Retail Asset' && ga.Line_Number__c==null && (requestDetails.rma_type=='Buyer\'s Remorse'||requestDetails.rma_type=='Warranty Refund'))
            {
                ga.Line_Number__c=0;
                    
                startTime = System.currentTimeMillis();                
                update ga;
                system.debug('Update Google Asset RT: ' + (System.currentTimeMillis() - startTime));              
            }
            RMA__c rma = new RMA__c();
            rma.GoogleCustomer__c=aList[0].Id;
            rma.Opportunity__c=oppId;
            rma.GoogleAsset__c=gaList[0].Id;
            rma.Type__c=requestDetails.rma_type;
            rma.RMA_Category__c=requestDetails.rma_category;
            rma.RMA_Sub_Category__c=requestDetails.rma_sub_category;
            rma.Notes__c=requestDetails.rma_notes;
            rma.gCases_ID__c=requestDetails.case_id;
            rma.OwnerId=ownerId;
            rma.RMA_Action__c=requestDetails.rma_action;
            sp=Database.setSavePoint();
            
            startTime = System.currentTimeMillis();
            insert rma;
            system.debug('Insert RMA RT: ' + (System.currentTimeMillis() - startTime));
            
            startTime = System.currentTimeMillis();
            rma = [SELECT Name,GoogleAsset__r.ProductID__r.Document__c,RMA_Country_Workflow__c,Type__c,RMA_Action__c,GoogleAsset__c FROM RMA__c WHERE Id=:rma.Id];
            system.debug('Select RMA RT: ' + (System.currentTimeMillis() - startTime));
/*
            submit_rma_response.rma_success_details = new RestResponseModel.rma_success_details_t();
            submit_rma_response.rma_success_details.rma_id=rma.Name;
            List<Country_Variant__c> cvList = new List<Country_Variant__c>([SELECT Id,Requires_Shipping_Label__c,
            Buyer_s_Remorse_Article_ID__c,DOA_Article_ID__c,Warranty_Article_ID__c,Warranty_Refund_Article_ID__c,
            AR_Method__c,Return_Center_Id__c,DOA_Replacement_GPN__c,Warranty_Replacement_GPN__c,Doa_Replacement_Doc_ID__c,Warranty_Replacement_Doc_ID__c
            FROM Country_Variant__c WHERE Name=:rma.RMA_Country_Workflow__c AND Document__c=:rma.GoogleAsset__r.ProductID__r.Document__c]);         
            Country_Variant__c cv;

            // jescamilla@levementum.com 12/2/14 Description: added for test coverage.
            if(Test.isRunningTest()){
                cvList.add(new Country_Variant__c());
            }
*/
            submit_rma_response.rma_success_details = new RmaDetailsBuilder()
                    .setCV(rma.RMA_Country_Workflow__c,rma.GoogleAsset__r.ProductID__r.Document__c)
                    .setRMA(rma)
                    .stampParameter()
                    .getRMASuccessDetail();

            submit_rma_response.rma_success_details.rma_id=rma.Name;

            /*if(cvList.size()==1)
            {
                cv = cvList.get(0);
                submit_rma_response.rma_success_details.shipping_label_needed=cv.Requires_Shipping_Label__c;
                if(rma.Type__c=='Buyer\'s Remorse')
                    submit_rma_response.rma_success_details.canned_response=cv.Buyer_s_Remorse_Article_ID__c;
                else if(rma.Type__c=='Warranty DOA')
                {
                    submit_rma_response.rma_success_details.canned_response=cv.DOA_Article_ID__c;
                    submit_rma_response.rma_success_details.exchange_gpn=cv.DOA_Replacement_GPN__c;
                    submit_rma_response.rma_success_details.exchange_doc_id = cv.DOA_Replacement_Doc_ID__c;
                }
                else if(rma.Type__c=='Warranty Regular')
                {
                    submit_rma_response.rma_success_details.canned_response=cv.Warranty_Article_ID__c;
                    submit_rma_response.rma_success_details.exchange_gpn=cv.Warranty_Replacement_GPN__c;
                    submit_rma_response.rma_success_details.exchange_doc_id = cv.Warranty_Replacement_Doc_ID__c;
                }
                else if(rma.Type__c=='Warranty Refund')
                    submit_rma_response.rma_success_details.canned_response=cv.Warranty_Refund_Article_ID__c;
                
                submit_rma_response.rma_success_details.ar_method=cv.AR_Method__c;
                submit_rma_response.rma_success_details.shipping_label_needed=cv.Requires_Shipping_Label__c;    
                //submit_rma_response.rma_success_details.exchange_doc_id=cv.Doc_ID__c;
                if(cv.Requires_Shipping_Label__c)
                {
                    List<Shipping_Label_Method_Rule__c> slRuleList;
                    if(requestDetails.rma_action == 'ADVANCED_EXCHANGE') slRuleList = new List<Shipping_Label_Method_Rule__c>([SELECT Shipping_Label_Method__c FROM Shipping_Label_Method_Rule__c WHERE Return_Center__r.Shipping_Label_Service_Code__c=:cv.Return_Center_Id__c AND RMA_Method__c=:cv.AR_Method__c AND Action__c=:requestDetails.rma_action]);
                    else slRuleList = new List<Shipping_Label_Method_Rule__c>([SELECT Shipping_Label_Method__c FROM Shipping_Label_Method_Rule__c WHERE Return_Center__r.Shipping_Label_Service_Code__c=:cv.Return_Center_Id__c AND Action__c=:requestDetails.rma_action]);
                    if(slRuleList.size()==1)
                        submit_rma_response.rma_success_details.shipping_method=slRuleList.get(0).Shipping_Label_Method__c;
                }
                if(cv.Return_Center_Id__c!=null)
                {                   
                    Return_Center__c rc = [SELECT Address_Line_1__c,Address_Line_2__c,Address_Line_3__c,City__c,State__c,Postal_Code__c,Country__c,Shipping_Label_Service_Code__c,Name FROM Return_Center__c WHERE Shipping_Label_Service_Code__c=:cv.Return_Center_Id__c];
                    submit_rma_response.rma_success_details.shipping_destination_address = new RestResponseModel.address_t();
                    submit_rma_response.rma_success_details.shipping_destination_address.address_street=rc.Address_Line_1__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_street_2=rc.Address_Line_2__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_street_3=rc.Address_Line_3__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_city=rc.City__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_state_province=rc.State__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_country=rc.Country__c;
                    submit_rma_response.rma_success_details.shipping_destination_address.address_postal_code=rc.Postal_Code__c;
                    submit_rma_response.rma_success_details.shipping_center_id=rc.Shipping_Label_Service_Code__c;
                    submit_rma_response.rma_success_details.shipping_center_name=rc.Name;
                }
            }*/
            
            Account a = aList.get(0);           
            a.FirstName=requestDetails.rma_requester_first_name;
            a.LastName=requestDetails.rma_requester_last_name;
            a.PersonMailingStreet=requestDetails.rma_requester_address.address_street;
            a.PersonMailingCity=requestDetails.rma_requester_address.address_city;
            a.PersonMailingState=requestDetails.rma_requester_address.address_state_province;
            a.PersonMailingCountry=requestDetails.rma_requester_address.address_country;
            a.PersonMailingPostalCode=requestDetails.rma_requester_address.address_postal_code;
            //Date rmaCreateDate = null;
            //Datetime exceptionDateTime = null;
            //if(requestDetails.rma_creation_date != null && String.valueOf(requestDetails.rma_creation_date) != ''){
            //    List<String> dateSplit = String.valueOf(requestDetails.rma_creation_date).split('-');
            //    rmaCreateDate = Date.parse(dateSplit[2]+'/'+dateSplit[1]+'/'+dateSplit[0]);
            //}
            //if(a.CID_Exception_Granted_Date_Time__c!=null){
            //    exceptionDateTime = a.CID_Exception_Granted_Date_Time__c;
            //}
            //if(requestDetails.rma_category=='1x Exception' && rmaCreateDate != null && (exceptionDateTime==null || exceptionDateTime.date()<rmaCreateDate)){
            //    a.CID_Exception_Granted__c=true;
            //    a.CID_Exception_Granted_Asset__c=rma.GoogleAsset__c;
            //    a.CID_Exception_Granted_Date_Time__c=datetime.now();
            //    a.CID_Exception_on_This_Asset__c=requestDetails.rma_category;
            //}
            startTime = System.currentTimeMillis();
            update a;
            system.debug('Update Account RT: ' + (System.currentTimeMillis() - startTime) + ' ' + a);
            submit_rma_response.is_success=true;
        }
        catch(Exception e)
        {
            submit_rma_response.failure_reason=e.getMessage();
            submit_rma_response.is_success=false;
            submit_rma_response.rma_success_details=null;
            Database.rollBack(sp);
        }
        //system.debug('RUNNING TIME IN MS: ' + (System.currentTimeMillis() - startTime));
        return submit_rma_response;
    }
}