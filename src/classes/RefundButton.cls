public with sharing class RefundButton 
{
	Refund__c refund;
	public RefundButton(ApexPages.StandardController con)
	{
		refund = (Refund__c)con.getRecord();
	}
	public PageReference doAction()
	{
		String action = ApexPages.currentPage().getParameters().get('action');
		RMA__c rma;
		if(refund.OwnerId != UserInfo.getUserId())
		{
			refund.addError('You are not allowed to take this action unless you are the owner.  Please claim the record');
			return null;
		}
		if(action=='close')
		{
			if(refund.RMA__c!=null)
			{
				rma = new RMA__c(Id=refund.RMA__c);
				rma.Status__c='Closed';
			}
			refund.RefundStatus__c='Failure - Closed';
		}		
		else if(action=='retry')
		{
			refund.ManualRetry__c=true;
			refund.RefundStatus__c='Pending';
		}
		else if(action=='escalate')
			refund.OwnerId=Label.ManualRefundReviewEscalationQueue;
		
		SavePoint sp;
		try
		{
			sp = Database.setSavepoint();
			update refund;
			if(rma!=null)
				update rma;
		}
		catch(Exception e)
		{
			Database.rollback(sp);
		}
		return new PageReference('/'+refund.Id);
	}
}