/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/13/15
 * @email:	cmunoz@levementum.com
 * @description:	General DML Operations for Google Asset custom object
 */
public with sharing class GoogleAssetDataAccess 
{
	public static String objectStatus = 'OK';
	private static String namePrefix = 'Google_Asset_';
	private static String serialPrefix = 'Serial_';
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/13/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Retrieves a Google Asset based on an asset id
	 */
	public static List<Google_Asset__c> selectGoogleAsset(Id assetId)
	{
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			googleAssets = [SELECT Active_Device__c, AlternateSerialNumber1__c, AlternateSerialNumber2__c, AlternateSerialNumber3__c, 
								   AlternateSerialNumber4__c, cssn__c, AssetOwner__c, Asset_Type__c, Associated_with_a_Safety_report__c, 
								   Buyers_Remorse_destination__c, CSN__c, Carrier__c, Chrome_S_N__c, Converted_to_Chromecast_R__c, 
								   country_sold__c, CreatedById, CreatedDate, DOA_Days_Left__c, DOA_Period__c, IsDeleted, 
								   RMA_Exceptions__c, Expected_Sale_Days__c, GlassError__c, GlassErrorMessage__c, GlassSent2__c, 
								   Name, Google_Asset_ID_External_ID__c, Order_Opportunity__c, IMEI__c, Last_Date_Buyer_s_Remorse__c, 
								   Last_Date_DOA__c, Last_Date_Spare_Parts__c, LastModifiedById, LastModifiedDate, Line_Number__c, 
								   MTR_ID__c, Mac_Address__c, Manufacture_Date__c, Minimum_Replacement_Warranty_Period__c, 
								   No_Warranty_Support__c, OrderLegacy__c, Order_ID_Name__c, Order_Type__c, Original_Retail_Store_Name__c, 
								   OwnerId, ProductID__c, ProductSKUDescription__c, ProductSKU__c, ProductFamily__c, Proof_of_Purchase_gCases_ID__c, 
								   Purchaser_Email__c, Recently_modified_by_user__c, Id, RecordTypeId, Remorse_Days_Left__c, Remorse_Period__c, 
								   bricking__c, RemoteDisable__c, Repair_Partner_Legacy__c, RepairPartner__c, Replacement_Warranty__c, 
								   Retail_Country__c, Retail_Sale_Date__c, SKU__c, SKU_Description__c, SKUUpsert__c, Sale_Country__c, 
								   Sale_Date__c, SaleDateOpportunity__c, SaleDateOrder__c, Sale_Datevalue__c, Serial__c, Service_Model__c, 
								   Shipment_Date__c, Shipping_Adjustment__c, Shopping_Express__c, SystemModstamp, Tracking_Number__c, 
								   Tracking_Link__c, Tracking_URL__c, Unique_ID_Type_from_product__c, Unique_ID_Type__c, VIP__c, 
								   Warranty_Countries_Legacy__c, WarrantyCountries__c, Warranty_Countdown__c, Warranty_Expiration_Date__c, 
								   Warranty_Period__c
						    FROM Google_Asset__c 
			                WHERE id =: assetId];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/13/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Retrieves a Google Asset based on an asset id
	 */
	public static List<Google_Asset__c> selectGoogleAsset(String assetId)
	{
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			if(assetId == null)
			{
				return googleAssets;
			}
			googleAssets = [SELECT Active_Device__c, AlternateSerialNumber1__c, AlternateSerialNumber2__c, AlternateSerialNumber3__c, 
								   AlternateSerialNumber4__c, cssn__c, AssetOwner__c, Asset_Type__c, Associated_with_a_Safety_report__c, 
								   Buyers_Remorse_destination__c, CSN__c, Carrier__c, Chrome_S_N__c, Converted_to_Chromecast_R__c, 
								   country_sold__c, CreatedById, CreatedDate, DOA_Days_Left__c, DOA_Period__c, IsDeleted, 
								   RMA_Exceptions__c, Expected_Sale_Days__c, GlassError__c, GlassErrorMessage__c, GlassSent2__c, 
								   Name, Google_Asset_ID_External_ID__c, Order_Opportunity__c, IMEI__c, Last_Date_Buyer_s_Remorse__c, 
								   Last_Date_DOA__c, Last_Date_Spare_Parts__c, LastModifiedById, LastModifiedDate, Line_Number__c, 
								   MTR_ID__c, Mac_Address__c, Manufacture_Date__c, Minimum_Replacement_Warranty_Period__c, 
								   No_Warranty_Support__c, OrderLegacy__c, Order_ID_Name__c, Order_Type__c, Original_Retail_Store_Name__c, 
								   OwnerId, ProductID__c, ProductSKUDescription__c, ProductSKU__c, ProductFamily__c, Proof_of_Purchase_gCases_ID__c, 
								   Purchaser_Email__c, Recently_modified_by_user__c, Id, RecordTypeId, Remorse_Days_Left__c, Remorse_Period__c, 
								   bricking__c, RemoteDisable__c, Repair_Partner_Legacy__c, RepairPartner__c, Replacement_Warranty__c, 
								   Retail_Country__c, Retail_Sale_Date__c, SKU__c, SKU_Description__c, SKUUpsert__c, Sale_Country__c, 
								   Sale_Date__c, SaleDateOpportunity__c, SaleDateOrder__c, Sale_Datevalue__c, Serial__c, Service_Model__c, 
								   Shipment_Date__c, Shipping_Adjustment__c, Shopping_Express__c, SystemModstamp, Tracking_Number__c, 
								   Tracking_Link__c, Tracking_URL__c, Unique_ID_Type_from_product__c, Unique_ID_Type__c, VIP__c, 
								   Warranty_Countries_Legacy__c, WarrantyCountries__c, Warranty_Countdown__c, Warranty_Expiration_Date__c, 
								   Warranty_Period__c
						    FROM Google_Asset__c 
			                WHERE Name =: assetId];
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/13/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Updates current Google Asset
	 * @update:	04/24/15. Autogenerate both Google Asset name and serial if not provided in body
	 */
	public static List<Google_Asset__c> updateGoogleAsset(List<Google_Asset__c> googleAssetModelsToBeUpdated, GoogleAssetModel updatingData)
	{
		Savepoint sp = Database.setSavepoint();
		List<Google_Asset__c> googleAssets = new List<Google_Asset__c>();
		try
		{
			if(googleAssetModelsToBeUpdated.size() <= 0)
			{
				Google_Asset__c googleAssetToInsert = insertGoogleAsset(updatingData);
				insert googleAssetToInsert;
				
				// autogenerate both Name and serial for Google Asset
				googleAssetToInsert.Name = namePrefix + googleAssetToInsert.id;
				googleAssetToInsert.Serial__c = serialPrefix + googleAssetToInsert.id;
				update googleAssetToInsert;
				googleAssets.add(googleAssetToInsert);
			}
			else
			{
				for(Google_Asset__c ga : googleAssetModelsToBeUpdated)
				{
					googleAssets.add(updateGoogleAssetFields(ga, updatingData));
				}
				upsert googleAssets;
			}
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			objectStatus = ex.getMessage();
			system.debug(ex.getMessage());
		}
		return googleAssets;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/17/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Inserts a brand new Google Asset absed on the response sent via @HttpPost
	 */
	private static Google_Asset__c insertGoogleAsset(GoogleAssetModel googleAssetModel)
	{
		Google_Asset__c googleAssetToInsert = new Google_Asset__c();
		try
		{
			// REQUIRED FIELDS
			googleAssetToInsert.RecordTypeId = googleAssetModel.recordTypeId;
			googleAssetToInsert.AssetOwner__c = googleAssetModel.assetOwner;
			googleAssetToInsert.Name = namePrefix;  // record id not available yet, and since this is required, it must be populated
			googleAssetToInsert.Order_Opportunity__c = googleAssetModel.order_opportunity; // If Google Asset Record Type only
			googleAssetToInsert.Line_Number__c = googleAssetModel.Line_Number; // If Google Asset Record Type only
			googleAssetToInsert.SKU__c = googleAssetModel.SKU;
			
			googleAssetToInsert.SKU_Description__c = googleAssetModel.sku_description;
			googleAssetToInsert.SKUUpsert__c = googleAssetModel.skuUpsert;
			googleAssetToInsert.Sale_Date__c = googleAssetModel.sale_date;
			googleAssetToInsert.Serial__c = serialPrefix;
			googleAssetToInsert.Shopping_Express__c = googleAssetModel.shopping_express != null ? googleAssetModel.shopping_express : false;
			googleAssetToInsert.Tracking_Number__c = googleAssetModel.tracking_number;
			googleAssetToInsert.Tracking_URL__c = googleAssetModel.tracking_url;
			googleAssetToInsert.Unique_ID_Type__c = googleAssetModel.unique_id_type;
			googleAssetToInsert.VIP__c = googleAssetModel.vip != null ? googleAssetModel.vip : false;
			googleAssetToInsert.Warranty_Countries_Legacy__c = googleAssetModel.warranty_countries_legacy;
			googleAssetToInsert.Warranty_Period__c = googleAssetModel.warranty_period;
			googleAssetToInsert.Remorse_Period__c = googleAssetModel.remorse_period;
			googleAssetToInsert.bricking__c = googleAssetModel.bricking;
			googleAssetToInsert.Repair_Partner_Legacy__c = googleAssetModel.repair_partner_legacy;
			googleAssetToInsert.Replacement_Warranty__c = googleAssetModel.replacement_warranty;
			googleAssetToInsert.Retail_Country__c = googleAssetModel.retail_country;
			googleAssetToInsert.Retail_Sale_Date__c = googleAssetModel.retail_sale_date;
			googleAssetToInsert.Mac_Address__c = googleAssetModel.mac_address;
			googleAssetToInsert.Manufacture_Date__c = googleAssetModel.manufacture_date;
			googleAssetToInsert.Minimum_Replacement_Warranty_Period__c = googleAssetModel.minimum_replacement_warranty_period;
			googleAssetToInsert.No_Warranty_Support__c = googleAssetModel.no_warranty_support != null ? googleAssetModel.no_warranty_support : false;
			googleAssetToInsert.OrderLegacy__c = googleAssetModel.orderLegacy;
			googleAssetToInsert.Original_Retail_Store_Name__c = googleAssetModel.original_retail_store_name;
			googleAssetToInsert.OwnerId = googleAssetModel.ownerId;
			googleAssetToInsert.ProductID__c = googleAssetModel.productId;
			googleAssetToInsert.Proof_of_Purchase_gCases_ID__c = googleAssetModel.proof_of_purchase_gCases_id;
			googleAssetToInsert.IMEI__c = googleAssetModel.IMEI;
			googleAssetToInsert.LastModifiedById = googleAssetModel.lastModifiedById;
			googleAssetToInsert.LastModifiedDate = googleAssetModel.lastModifiedDate;
			googleAssetToInsert.Google_Asset_ID_External_ID__c = googleAssetModel.google_asset_id_external_id;
			googleAssetToInsert.Asset_Type__c = googleAssetModel.asset_type;
			googleAssetToInsert.Associated_with_a_Safety_report__c = googleAssetModel.associated_with_a_safety_report != null ? googleAssetModel.associated_with_a_safety_report : false; 
			googleAssetToInsert.Buyers_Remorse_destination__c = googleAssetModel.buyers_remorse_destination;
			googleAssetToInsert.CSN__c = googleAssetModel.CSN;
			googleAssetToInsert.Carrier__c = googleAssetModel.carrier;
			googleAssetToInsert.Chrome_S_N__c = googleAssetModel.chrome_s_n;
			googleAssetToInsert.Converted_to_Chromecast_R__c = googleAssetModel.converted_to_chromecast_r != null ? googleAssetModel.converted_to_chromecast_r : false; 
			googleAssetToInsert.country_sold__c = googleAssetModel.country_sold;
			googleAssetToInsert.CreatedById = googleAssetModel.createdById;
			googleAssetToInsert.CreatedDate = googleAssetModel.createdDate;
			googleAssetToInsert.DOA_Period__c = googleAssetModel.DOA_period;
			googleAssetToInsert.Expected_Sale_Days__c = googleAssetModel.Expected_Sale_Days;
			googleAssetToInsert.GlassError__c = googleAssetModel.glassError != null ? googleAssetModel.glassError : false;
			googleAssetToInsert.GlassErrorMessage__c = googleAssetModel.glassErrorMessage;
			googleAssetToInsert.GlassSent2__c = googleAssetModel.glassSent2 != null ? googleAssetModel.glassSent2 : false;
			googleAssetToInsert.Active_Device__c = googleAssetModel.active_device;
 			googleAssetToInsert.AlternateSerialNumber1__c = googleAssetModel.alternateSerialNumber1;
			googleAssetToInsert.AlternateSerialNumber2__c = googleAssetModel.alternateSerialNumber2;
			googleAssetToInsert.AlternateSerialNumber3__c = googleAssetModel.alternateSerialNumber3;
			googleAssetToInsert.AlternateSerialNumber4__c = googleAssetModel.alternateSerialNumber4;
			googleAssetToInsert.cssn__c = googleAssetModel.cssn;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage();
		}
		return googleAssetToInsert;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/13/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Updates current Google Asset
	 * @update:	04/23/15: Addded following fields: doa_period__c, expected_sale_days__c, remorse_period__c, 
	 *					  minimum_replacement_warranty_period__c, and replacement_warranty__c.
	 */
	private static Google_Asset__c updateGoogleAssetFields(Google_Asset__c googleAssetModelToUpdate, GoogleAssetModel updatingData)
	{
		Google_Asset__c ga = googleAssetModelToUpdate;
		try
		{
			if(ga != null && updatingData != null)
			{
				ga.Id = updatingData.Id;
				ga.OwnerId = updatingData.OwnerId;
				ga.Name = updatingData.Name;
				ga.RecordTypeId = updatingData.RecordTypeId;
				ga.CreatedDate = updatingData.CreatedDate;
				ga.CreatedById = updatingData.CreatedById;
				ga.LastModifiedDate = updatingData.LastModifiedDate;
				ga.LastModifiedById = updatingData.LastModifiedById;
				ga.OrderLegacy__c = updatingData.OrderLegacy;
				ga.Serial__c = updatingData.Serial;
				ga.Manufacture_Date__c = updatingData.Manufacture_Date;
				ga.Sale_Date__c = updatingData.Sale_Date;
				ga.IMEI__c = updatingData.IMEI;
				ga.SKU__c = updatingData.SKU;
				ga.Active_Device__c = updatingData.Active_Device;
				ga.Asset_Type__c = updatingData.Asset_Type;
				ga.Unique_ID_Type__c = updatingData.Unique_ID_Type;
				ga.SKU_Description__c = updatingData.SKU_Description;
				ga.Repair_Partner_Legacy__c = updatingData.Repair_Partner_Legacy;
				ga.Warranty_Countries_Legacy__c = updatingData.Warranty_Countries_Legacy;
				ga.bricking__c = updatingData.bricking;
				ga.Tracking_Number__c = updatingData.Tracking_Number;
				ga.Google_Asset_ID_External_ID__c = updatingData.Google_Asset_ID_External_ID;
				ga.cssn__c = updatingData.cssn;
				ga.country_sold__c = updatingData.country_sold;
				ga.Buyers_Remorse_destination__c = updatingData.Buyers_Remorse_destination;
				ga.ProductID__c = updatingData.ProductID;
				ga.AssetOwner__c = updatingData.AssetOwner;
				ga.Chrome_S_N__c = updatingData.Chrome_S_N;
				ga.Order_Opportunity__c = updatingData.Order_Opportunity;
				ga.AlternateSerialNumber1__c = updatingData.AlternateSerialNumber1;
				ga.AlternateSerialNumber2__c = updatingData.AlternateSerialNumber2;
				ga.AlternateSerialNumber3__c = updatingData.AlternateSerialNumber3;
				ga.AlternateSerialNumber4__c = updatingData.AlternateSerialNumber4;
				ga.SKUUpsert__c = updatingData.SKUUpsert;
				if(updatingData.VIP != null){ga.VIP__c = updatingData.VIP;}
				ga.Tracking_URL__c = updatingData.Tracking_URL;
				ga.Carrier__c = updatingData.Carrier;
				ga.Line_Number__c = updatingData.Line_Number;
				ga.Remorse_Period__c = updatingData.Remorse_Period;
				ga.DOA_Period__c = updatingData.DOA_Period;
				if(updatingData.No_Warranty_Support != null){ga.No_Warranty_Support__c = updatingData.No_Warranty_Support;}
				ga.Expected_Sale_Days__c = updatingData.Expected_Sale_Days;
				ga.Minimum_Replacement_Warranty_Period__c = updatingData.Minimum_Replacement_Warranty_Period;
				ga.Proof_of_Purchase_gCases_ID__c = updatingData.Proof_of_Purchase_gCases_ID;
				ga.Replacement_Warranty__c = updatingData.Replacement_Warranty;
				ga.Retail_Country__c = updatingData.Retail_Country;
				ga.Retail_Sale_Date__c = updatingData.Retail_Sale_Date;
				ga.Warranty_Period__c = updatingData.Warranty_Period;
				ga.Original_Retail_Store_Name__c = updatingData.Original_Retail_Store_Name;
				ga.Mac_Address__c = updatingData.Mac_Address;
				if(updatingData.Associated_with_a_Safety_report != null){ga.Associated_with_a_Safety_report__c = updatingData.Associated_with_a_Safety_report;}
				if(updatingData.Shopping_Express != null){ga.Shopping_Express__c = updatingData.Shopping_Express;}
				if(updatingData.GlassError != null){ga.GlassError__c = updatingData.GlassError;}
				ga.GlassErrorMessage__c = updatingData.GlassErrorMessage;
				if(updatingData.GlassSent2 != null){ga.GlassSent2__c = updatingData.GlassSent2;}
				if(updatingData.Converted_to_Chromecast_R != null){ga.Converted_to_Chromecast_R__c = updatingData.Converted_to_Chromecast_R;}
				ga.CSN__c = updatingData.CSN;
			}
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
			objectStatus = ex.getMessage(); 
		}
		return ga;
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/23/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Deletes a Google Asset by name
	 */
	public static void deleteObject(String objectName)
	{
		try
		{
			Google_Asset__c assetToDelete = [SELECT id
										 FROM Google_Asset__c
										 WHERE Name =: objectName limit 1];
			delete assetToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	/**
	 * @author:	Cuauhtemoc Munoz
	 * @date:	04/23/15
	 * @email:	cmunoz@levementum.com
	 * @description:	Deletes a Google Asset by id
	 */
	public static void deleteObject(Id objectId)
	{
		try
		{
			Google_Asset__c assetToDelete = [SELECT id
										 FROM Google_Asset__c
										 WHERE id =: objectId];
			delete assetToDelete;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
}