public without sharing class CT_RMAServices extends CT_PageControllerBase {

    final String CLASSNAME = '\n\n CT_RMAServices.@@METHODNAME()';

    public String queryStringParams{get; private set;}
    public String incomingURL{get; private set;}
    public String destinationURL{get; private set;}
    public String host{get; private set;}
    public String targetSObjectPrefix{get; private set;}
    private PageReference DestinationPageReference;
    public String incomingReturnUrl{get; private set;}
    public String incomingRecordId{get; private set;}
    public String GoogleAssetId{get; private set;}
    String caseSObjectPrefix{
        get{
            return Case.sObjectType.getDescribe().getKeyPrefix();
        }
    }
    String googleAssetSObjectPrefix{
        get{
            return Google_Asset__c.sObjectType.getDescribe().getKeyPrefix();
        }
    }

    public CT_RMAServices(ApexPages.StandardController controller)
    {
        String METHODNAME = CLASSNAME.replace('@@METHODNAME', 'CT_RMAServices');
        system.debug(LoggingLevel.INFO, METHODNAME.replace('**** ', '**** Inside ') + ' - constructor\n\n');

        this.host = URL.getSalesforceBaseUrl().getHost();
        if(controller != null)
            this.incomingRecordId = controller.getId();

        this.queryStringParams = ''+ this.PageParams +'';
        this.incomingURL = (Test.isRunningTest()) ? '' : system.Encodingutil.urlDecode(system.currentPageReference().getUrl(), 'UTF-8');
        
        if(this.incomingRecordId == '' || this.incomingRecordId == null){
            if(this.PageParams.containsKey('sourceId')){
                try {
                    String s = this.PageParams.get('sourceId');
                    system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: this.PageParams.get(\'sourceId\') : ' + s + '.\n\n');
                    if( s.startsWith(caseSObjectPrefix) || s.startsWith(googleAssetSObjectPrefix) ){
                        this.incomingRecordId = s;
                    }else{
                        String soql = 'SELECT Id FROM Google_Asset__c WHERE IsDeleted=false and Name =\''+ s +'\'';
                        system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: soql = ' + soql + '.\n\n');
                        Google_Asset__c ga = Database.query(soql);
                        system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: Google_Asset__c = ' + ga + '.\n\n');
                        this.incomingRecordId = (ga != null) ? ga.Id : '';
                    }

                }catch(system.StringException e) {
                    system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: EXCEPTION: invalid \'sourceId\' param.\n\n');
                    this.ErrorMsg('The \'sourceId\' parameter is invalid. ' + this.PageParams.get('sourceId'));
                }
            }
        }
    }
        
    public PageReference redirect()
    {
        String METHODNAME = CLASSNAME.replace('@@METHODNAME', 'redirect');
        system.debug(LoggingLevel.INFO, METHODNAME.replace('**** ', '**** Inside ') + '\n\n');
        String gaName = '';
        if( this.InErrorState )
        {
            this.InfoMsg('\'InErrorState\' is TRUE.');
            system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: \'InErrorState\' is TRUE.\n\n');
            return null;
        }else{
            this.targetSObjectPrefix = RMA__c.sObjectType.getDescribe().getKeyPrefix();
            String dest;
            system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: this.incomingRecordId = ' + this.incomingRecordId + '.\n\n');
            system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: caseSObjectPrefix = ' + caseSObjectPrefix + '.\n\n');
            system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: googleAssetSObjectPrefix = ' + googleAssetSObjectPrefix + '.\n\n');
            try{
                if(this.incomingRecordId.startsWith(caseSObjectPrefix)){
                    dest = CreateCaseRMA(this.incomingRecordId);
                }else if(this.incomingRecordId.startsWith(googleAssetSObjectPrefix)){
                    dest = CreateGoogleAssetRMA(this.incomingRecordId);
                }else{
                    gaName = this.incomingRecordId; // the Google_Asset__c.Name value
                    dest = CreateGoogleAssetRMA(this.incomingRecordId);
    //              this.InfoMsg('\'this.incomingRecordId\' is '+ this.incomingRecordId +'.');
    //              system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: \'this.incomingRecordId\' is '+ this.incomingRecordId +'.\n\n');
                }
            }catch(Exception ex){
                if(ex instanceof CT_BaseApplicationException)
                    this.ErrorMsg(((CT_BaseApplicationException)ex).Message);
                else{
                    system.Debug(LoggingLevel.ERROR, METHODNAME + ' :: Exception : '+ ex.getMessage() +'\n\n');
                    this.ErrorMsg(ex.getMessage());
                }
                return null;
            }
            if( dest == null || dest == '' ){
                return null;
            }
            
            DestinationPageReference = new PageReference(dest);
    
            if(this.PageParams.containsKey('retURL')){
                try{
                    this.incomingReturnUrl = this.PageParams.get('retURL');
                }catch(system.StringException e) {
                    system.debug(LoggingLevel.DEBUG, METHODNAME + ' :: EXCEPTION: invalid \'retURL\' param.\n\n');
                    this.ErrorMsg('The \'retURL\' parameter is invalid. ' + this.PageParams.get('retURL'));
                    return null;
                }
            }
            
            //if(this.PageParams.get('retURL').contains(gaName)){
            //    this.PageParams.put('retURL', this.GoogleAssetId);
            //}
            
            // add the existing/incoming querystring params to PageReference EXCEPT the 'save_new' param in order to avoid 
            // the "The page you submitted was invalid for your session" error
            for(String p: this.PageParams.keyset()){
                if( p != 'save_new' && p != 'sourceId')
                    DestinationPageReference.getParameters().put(p, this.PageParams.get(p));
            }
            
            // EXPLANATION:
            // The 'New' button on the RMA__c objects has been overridden to point to Visualforce pages
            // that use this class as the Controller.  If you try to redirect the user to the "New RMA" page now, SFDC will simply intercept it,
            // redirect back to this VF page SO... 
            // you MUST include this querystring param in order to prevent that infinite loop from occurring
            DestinationPageReference.getParameters().put('nooverride', '1');
    
            //this.destinationURL = DestinationPageReference.getUrl(); 
            DestinationPageReference.setRedirect(true);
            return DestinationPageReference;
        }
    }

    private String CreateGoogleAssetRMA(String googleAssetId){
        String METHODNAME = CLASSNAME.replace('@@METHODNAME()', 'CreateGoogleAssetRMA(String googleAssetId)');
        system.debug(LoggingLevel.INFO, METHODNAME.replace('\n\n ', '\n\n Inside ') + ' :: googleAssetId = ' + googleAssetId +'\n\n');
        
        if(googleAssetId != null){          
            Google_Asset__c ga = Database.query('SELECT Service_Model__c, ProductID__r.Document__r.RMARecordType__c, cssn__c, country_sold__c, bricking__c, Warranty_Expiration_Date__c, Unique_ID_Type__c, Tracking_Number__c, SystemModstamp, Serial__c, Sale_Date__c, SaleDateOrder__c, SKU__c, SKU_Description__c, RemoteDisable__c, RecordTypeId, Purchaser_Email__c, ProductSKU__c, ProductSKUDescription__c, ProductID__c, OwnerId, Order_Opportunity__c, OrderLegacy__c, Name, Manufacture_Date__c, Last_Date_Spare_Parts__c, Last_Date_DOA__c, Last_Date_Buyer_s_Remorse__c, Id, IMEI__c, Google_Asset_ID_External_ID__c, Chrome_S_N__c, Buyers_Remorse_destination__c, Asset_Type__c, AssetOwner__c, AlternateSerialNumber4__c, AlternateSerialNumber3__c, AlternateSerialNumber2__c, AlternateSerialNumber1__c, Active_Device__c FROM Google_Asset__c WHERE IsDeleted=false and Id =\''+ googleAssetId +'\''); // Repair_Partner__c, RepairPartner__c,Warranty_Countries__c, WarrantyCountries__c,             
            return BuildUrl(ga);
        }else{
            return '';
        }
    }
    
    private String CreateCaseRMA(String caseId){
        String METHODNAME = CLASSNAME.replace('@@METHODNAME()', 'CreateCaseRMA(String caseId)');
        system.debug(LoggingLevel.INFO, METHODNAME.replace('\n\n ', '\n\n Inside ') + ' :: caseId = ' + caseId +'\n\n');

        Case theCase = Database.query('SELECT Id, Google_Asset__c FROM Case WHERE IsDeleted=false and Id =\''+ caseId +'\'');
        if(theCase.Google_Asset__c != null){
            list<Google_Asset__c> ga = Database.query('SELECT Service_Model__c, ProductID__r.Document__r.RMARecordType__c, cssn__c, country_sold__c, bricking__c, Warranty_Expiration_Date__c, Unique_ID_Type__c, Tracking_Number__c, SystemModstamp, Serial__c, Sale_Date__c, SaleDateOrder__c, SKU__c, SKU_Description__c, RemoteDisable__c, RecordTypeId, Purchaser_Email__c, ProductSKU__c, ProductSKUDescription__c, ProductID__c, OwnerId, Order_Opportunity__c, OrderLegacy__c, Name, Manufacture_Date__c, Last_Date_Spare_Parts__c, Last_Date_DOA__c, Last_Date_Buyer_s_Remorse__c, Id, IMEI__c, Google_Asset_ID_External_ID__c, Chrome_S_N__c, Buyers_Remorse_destination__c, Asset_Type__c, AssetOwner__c, AlternateSerialNumber4__c, AlternateSerialNumber3__c, AlternateSerialNumber2__c, AlternateSerialNumber1__c, Active_Device__c FROM Google_Asset__c WHERE IsDeleted=false and Id =\''+ theCase.Google_Asset__c +'\'');  // Repair_Partner__c, RepairPartner__c,Warranty_Countries__c, WarrantyCountries__c,          
            if( ga != null && ga.isEmpty()==false ){
                this.GoogleAssetId = ga[0].Id;
                return BuildUrl(ga[0]);
            }else 
                return '';          
        }else{
            this.ErrorMsg('RMA was not created. Please provide a valid Google Asset.');
            return '';
        }
    }
    
    private String BuildUrl(Google_Asset__c ga)
    {
        String METHODNAME = CLASSNAME.replace('@@METHODNAME()', 'BuildUrl(Google_Asset__c ga)');
        system.debug(LoggingLevel.INFO, METHODNAME.replace('\n\n ', '\n\n Inside ') + ' :: Google_Asset__c = ' + ga +'\n\n');
        String rt = '';
        if(ga.ProductID__r.Document__r.RMARecordType__c != null)
            rt = ga.ProductID__r.Document__r.RMARecordType__c;
        String sm = '';
        if(ga.Service_Model__c != null)
            sm = ga.Service_Model__c;
        String googleAcct = '';
        if(ga.AssetOwner__c != null)
            googleAcct = ga.AssetOwner__c;
        String ord = '';
        if(ga.Order_Opportunity__c != null)
            ord = ga.Order_Opportunity__c;
        
        RMA__c rma = new RMA__c();
        rma.GoogleAsset__c = ga.Id;
        rma.GoogleCustomer__c = ga.AssetOwner__c;
        rma.ServiceModel__c = ga.Service_Model__c;//sm;
        rma.Opportunity__c = ga.Order_Opportunity__c;//ord;
        if(rt == '')
            throw CT_BaseApplicationException.NewException(METHODNAME, 'A problem has occurred - the Product on the Google Asset is missing the RMARecordType value.');
        System.debug('About to query RT:::'+rt);
        rma.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = :rt].Id;
        System.debug('About to query RT2:::'+rt);
        system.debug(LoggingLevel.INFO, METHODNAME + ' :: RMA__c record to insert = ' + rma +'\n\n');
        
        try{
            insert rma;
            this.destinationUrl = ('/' + this.targetSObjectPrefix + '/e?id=' + rma.Id + '&retURL=%2F' + rma.Id);
        }catch(DMLException dmle){
            system.Debug(LoggingLevel.ERROR, METHODNAME + ' :: ******************************** DML EXCEPTION BEG ********************************\n');
            for (Integer i=0; i < dmle.getNumDml(); i++){
                system.Debug(LoggingLevel.ERROR,'\n' + dmle.getDmlMessage(i) + '\n');
            }
            system.Debug(LoggingLevel.ERROR, METHODNAME + ' :: ******************************** DML EXCEPTION END ********************************\n');
            //throw CT_BaseApplicationException.NewException(METHODNAME, dmle.getMessage(),'A problem has occurred while trying to insert a new RMA record - please contact your Administrator to review the logs for additional details.');
            //this.ErrorMsg(dmle.getMessage());
            return null;
        }
        catch(Exception ex){
            if(ex instanceof CT_BaseApplicationException)
                throw ex;
            else{
                system.Debug(LoggingLevel.ERROR, METHODNAME + ' :: Exception : '+ ex.getMessage() +'\n\n');
                //throw CT_BaseApplicationException.NewExceptionWithInnerException(METHODNAME, ex.getMessage(),'A problem has occurred while trying to insert a new RMA record - please contact your Administrator to review the logs for additional details.', ex);
                //this.ErrorMsg(ex.getMessage());
            }
            return null;
        }
        return destinationUrl;
    }
    
    /*private boolean InProduction(){
        return (URL.getSalesforceBaseUrl().getHost().contains('na0') || URL.getSalesforceBaseUrl().getHost().contains('na1.') || URL.getSalesforceBaseUrl().getHost().contains('na2')
      || URL.getSalesforceBaseUrl().getHost().contains('na3') || URL.getSalesforceBaseUrl().getHost().contains('na4') || URL.getSalesforceBaseUrl().getHost().contains('na5')
      || URL.getSalesforceBaseUrl().getHost().contains('na6') || URL.getSalesforceBaseUrl().getHost().contains('na7') || URL.getSalesforceBaseUrl().getHost().contains('na8')
      || URL.getSalesforceBaseUrl().getHost().contains('na9') || URL.getSalesforceBaseUrl().getHost().contains('na10')|| URL.getSalesforceBaseUrl().getHost().contains('na11')
      || URL.getSalesforceBaseUrl().getHost().contains('na12') || URL.getSalesforceBaseUrl().getHost().contains('na14') || URL.getSalesforceBaseUrl().getHost().contains('ap0')
      || URL.getSalesforceBaseUrl().getHost().contains('ap1') || URL.getSalesforceBaseUrl().getHost().contains('eu0') || URL.getSalesforceBaseUrl().getHost().contains('eu1')
          );
    }*/
}