public without sharing class PlayToGlass 
{
	static String session;
	
	@future(callout=true)
	public static void sendDataFuture(Set<Id> gaIdSet)
	{	
		List<Google_Asset__c> gaList = new List<Google_Asset__c>([SELECT Id,Name,ProductId__r.SKU__c,Serial__c,
		Order_Opportunity__r.Id,Order_Opportunity__r.Name,Order_Opportunity__r.CloseDate,Order_Opportunity__r.StageName,Order_Opportunity__r.ShipmentDate__c,Order_Opportunity__r.Type,
		AssetOwner__r.Name,AssetOwner__r.PersonEmail,AssetOwner__r.PersonMailingCity,AssetOwner__r.PersonMailingCountry,AssetOwner__r.PersonMailingPostalCode,AssetOwner__r.PersonMailingState,AssetOwner__r.PersonMailingStreet,AssetOwner__r.Phone
		FROM Google_Asset__c WHERE Id in :gaIdSet AND ProductFamily__c='Glass' ORDER BY AssetOwner__r.PersonEmail]);
		
		List<Google_Asset__c> updateList = new List<Google_Asset__c>();		
		for(soapPlayHelper.BundleObject bo : sendList(gaList))
		{
			if(bo.hasError!=null)
				updateList.add(new Google_Asset__c(Id=bo.playAssetId,GlassError__c=true,GlassSent2__c=false,GlassErrorMessage__c=bo.errorMessage));
			else
				updateList.add(new Google_Asset__c(Id=bo.playAssetId,GlassError__c=false,GlassSent2__c=true,GlassErrorMessage__c=''));
		}
		if(!updateList.isEmpty())
			update updateList;
	}
	@future(callout=true)
	public static void refundedGa(Set<Id> gaIdSet)
	{		
		soapPlayHelper.refundedGa rGa;
		List<soapPlayHelper.refundedGa> rGaList = new List<soapPlayHelper.refundedGa>();
		List<soapPlayHelper.refundedGa> returnList = new List<soapPlayHelper.refundedGa>();
		Map<String,Id> serialToIdMap = new Map<String,Id>();
		for(Google_Asset__c ga : [SELECT Id,Name FROM Google_Asset__c WHERE Id in :gaIdSet])
		{
			System.debug('found GA'+ga);
			rGa = new soapPlayHelper.refundedGa();
			rGa.serialNumber=ga.Name;
			serialToIdMap.put(ga.Name,ga.Id);
			rGaList.add(rGa);
		}
		if(session==null)
			session = soapPlayHelper.login();
		
		soapPlayHelper.PlayDataToGlassWS toGlass = new soapPlayHelper.PlayDataToGlassWS();
		toGlass.SessionHeader = new soapPlayHelper.SessionHeader_element();
		toGlass.SessionHeader.sessionId=session;
		returnList = toGlass.refundedGa(rGaList);
		System.debug('returnList'+returnList);
		List<Google_Asset__c> gaList = new List<Google_Asset__c>();
		for(soapPlayHelper.refundedGa ra : returnList)
		{
			if(ra.hasError)
				gaList.add(new Google_Asset__c(Id=serialToIdMap.get(ra.SerialNumber),GlassError__c=true,GlassErrorMessage__c=ra.errorMessage));
		}
		if(!gaList.isEmpty())
			update gaList;
	}
	@future(callout=true)
	public static void changeRmaStatus(Set<Id> rmaIdSet)
	{
		Map<Id,RMA__c> rmaMap = new Map<Id,RMA__c>([SELECT Id,Status__c,GlassRmaId__c FROM RMA__c WHERE Id in :rmaIdSet]);
		Map<Id,Id> glassIdToPlayIdMap = new Map<Id,Id>();
		for(RMA__c rma : rmaMap.values())
			glassIdToPlayIdMap.put(rma.GlassRmaId__c,rma.Id);
		if(session==null)
			session = soapPlayHelper.login();
		
		soapPlayHelper.PlayDataToGlassWS toGlass = new soapPlayHelper.PlayDataToGlassWS();
		toGlass.SessionHeader = new soapPlayHelper.SessionHeader_element();
		toGlass.SessionHeader.sessionId=session;
		List<soapPlayHelper.RmaObject> rmaObjList = new List<soapPlayHelper.RmaObject>();
		soapPlayHelper.RmaObject rmaObj;
		for(RMA__c rma : rmaMap.values())
		{
			 rmaObj = new soapPlayHelper.RmaObject();
			 rmaObj.rmaId=rma.GlassRmaId__c;
			 rmaObj.status=rma.Status__c;
			 rmaObjList.add(rmaObj);
		}
		rmaObjList = toGlass.changeRmaStatus(rmaObjList);
		List<RMA__c> updateList = new List<RMA__c>();
		
		for(soapPlayHelper.RmaObject rmaObj2 : rmaObjList)
		{
			if(rmaObj2.hasError!=null)
				updateList.add(new RMA__c(Id=glassIdToPlayIdMap.get(rmaObj2.rmaId),GlassError__c=true,GlassErrorMessage__c=rmaObj2.errorMessage));
			else
				updateList.add(new RMA__c(Id=glassIdToPlayIdMap.get(rmaObj2.rmaId),GlassError__c=false,GlassErrorMessage__c=''));
		}
		if(!updateList.isEmpty())
			update updateList;
	}
	public static String sendData(Id gaId)
	{	
		Google_Asset__c ga = [SELECT Id,Name,ProductId__r.SKU__c,Serial__c,
		Order_Opportunity__r.Id,Order_Opportunity__r.Name,Order_Opportunity__r.CloseDate,Order_Opportunity__r.StageName,Order_Opportunity__r.ShipmentDate__c,Order_Opportunity__r.Type,
		AssetOwner__r.Name,AssetOwner__r.PersonEmail,AssetOwner__r.PersonMailingCity,AssetOwner__r.PersonMailingCountry,AssetOwner__r.PersonMailingPostalCode,AssetOwner__r.PersonMailingState,AssetOwner__r.PersonMailingStreet,AssetOwner__r.Phone
		FROM Google_Asset__c WHERE Id=:gaId AND ProductFamily__c='Glass'];
		String returnMessage='Success!';
		/*String returnMessage=send(ga);
		
		if(returnMessage!='Success!')
			ga.GlassError__c=true;
		else
			ga.GlassError__c=false;
		
		
		update ga;
		return returnMessage;*/
		List<Google_Asset__c> updateList = new List<Google_Asset__c>();
		for(soapPlayHelper.BundleObject bo : sendList(new List<Google_Asset__c>{ga}))
		{
			if(bo.hasError!=null)
			{
				ga.GlassError__c=true;
				ga.GlassErrorMessage__c=bo.errorMessage;
				ga.GlassSent2__c=false;
				returnMessage=bo.errorMessage;
			}
			else
			{
				ga.GlassSent2__c=true;
				ga.GlassError__c=false;
				ga.GlassErrorMessage__c='';
			}
		}
		update ga;
		return returnMessage;
	}
	
	private static List<soapPlayHelper.BundleObject> sendList(List<Google_Asset__c> gaList)
	{
		if(session==null)
		{
			if(!Test.isRunningTest())
				session=soapPlayHelper.login();
			else
			 	session='asdf';
		}
		soapPlayHelper.PlayDataToGlassWS toGlass = new soapPlayHelper.PlayDataToGlassWS();
		toGlass.timeout_x=120000;
		toGlass.SessionHeader = new soapPlayHelper.SessionHeader_element();
		toGlass.SessionHeader.sessionId=session;
		List<soapPlayHelper.BundleObject> boList = new List<soapPlayHelper.BundleObject>();
		for(Google_Asset__c ga : gaList)
		{
			soapPlayHelper.AccountObject ao = new soapPlayHelper.AccountObject();
			ao.name=ga.AssetOwner__r.Name;
			ao.email=ga.AssetOwner__r.PersonEmail;
			ao.mailingCity=ga.AssetOwner__r.PersonMailingCity;
			ao.mailingCountry=ga.AssetOwner__r.PersonMailingCountry;
			ao.mailingPostalCode=ga.AssetOwner__r.PersonMailingPostalCode;
			ao.mailingState=ga.AssetOwner__r.PersonMailingState;
			ao.mailingStreet=ga.AssetOwner__r.PersonMailingStreet;			
			ao.phone=ga.AssetOwner__r.Phone;
			
			soapPlayHelper.OpportunityObject oppObj = new soapPlayHelper.OpportunityObject();
			oppObj.playOpportunityId=ga.Order_Opportunity__r.Id;
			oppObj.name=ga.Order_Opportunity__r.Name;					
			oppObj.closeDate=ga.Order_Opportunity__r.CloseDate;		
			oppObj.stageName=ga.Order_Opportunity__r.StageName;
			oppObj.shipmentDate=ga.Order_Opportunity__r.ShipmentDate__c;
			oppObj.typeOfOrder=ga.Order_Opportunity__r.Type;			
			
			soapPlayHelper.OpportunityLineItemObject oliObj = new soapPlayHelper.OpportunityLineItemObject();
			oliObj.playOpportunityLineItemId=ga.Id;
			oliObj.sku=ga.ProductId__r.SKU__c;
			//oliObj.serialNumber=ga.Serial__c;
			oliObj.serialNumber=ga.Name;
			soapPlayHelper.BundleObject bo = new soapPlayHelper.BundleObject();
			bo.playAssetId=ga.Id;
			bo.ao = ao;
			bo.oppObj = oppObj;
			bo.oliObj = oliObj;
			boList.add(bo);
		}
		if(Test.isRunningTest())
			return boList;
		else
			return toGlass.createAllList(boList);
	}
	
	/*private static String send(Google_Asset__c ga)
	{
		String session = soapPlayHelper.login();
		soapPlayHelper.PlayDataToGlassWS toGlass = new soapPlayHelper.PlayDataToGlassWS();
		toGlass.SessionHeader = new soapPlayHelper.SessionHeader_element();
		toGlass.SessionHeader.sessionId=session;
		
		soapPlayHelper.AccountObject ao = new soapPlayHelper.AccountObject();
		//ao.firstName=rma.Google_Account__r.FirstName;
		//ao.lastName=rma.Google_Account__r.LastName;
		ao.email=ga.AssetOwner__r.PersonEmail;
		ao.mailingCity=ga.AssetOwner__r.PersonMailingCity;
		ao.mailingCountry=ga.AssetOwner__r.PersonMailingCountry;
		ao.mailingPostalCode=ga.AssetOwner__r.PersonMailingPostalCode;
		ao.mailingState=ga.AssetOwner__r.PersonMailingState;			
		ao.phone=ga.AssetOwner__r.Phone;
		
		soapPlayHelper.OpportunityObject oppObj = new soapPlayHelper.OpportunityObject();
		oppObj.playOpportunityId=ga.Order_Opportunity__r.Id;
		oppObj.name=ga.Order_Opportunity__r.Name;					
		oppObj.closeDate=ga.Order_Opportunity__r.CloseDate;		
		oppObj.stageName=ga.Order_Opportunity__r.StageName;
		oppObj.shipmentDate=ga.Order_Opportunity__r.ShipmentDate__c;
		oppObj.typeOfOrder=ga.Order_Opportunity__r.Type;			
		
		soapPlayHelper.OpportunityLineItemObject oliObj = new soapPlayHelper.OpportunityLineItemObject();
		oliObj.playOpportunityLineItemId=ga.Id;
		oliObj.sku=ga.ProductId__r.SKU__c;
		oliObj.serialNumber=ga.Serial__c;
		return toGlass.createAll(ao, oppObj, oliObj);
	}*/
}