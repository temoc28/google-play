public with sharing class RefundPriceProtection 
{
	Google_Asset__c ga;
	private List<PriceProtectionValues__c> ppvList;
	public RefundPriceProtection(ApexPages.StandardController con)
	{
		ga = (Google_Asset__c)con.getRecord();
	}
	public PageReference createPriceProtectionRefund()
	{
		String message = isEligible();
		if(message=='')
		{
			Id lineItemRefundRt = Schema.SObjectType.Refund__c.getRecordTypeInfosByName().get('Line-Item Refund').getRecordTypeId();
			List<Refund__c> refundList = new List<Refund__c>([SELECT Id FROM Refund__c WHERE OrderNumber__c=:ga.Order_Opportunity__c AND GoogleAsset__c=:ga.Id AND RecordTypeId=:lineItemRefundRt]);
			if(refundList.size()>0)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,'This asset already has a price protection refund'));
				return null;
			}
			Refund__c refund = new Refund__c(
			GoogleAsset__c=ga.Id,
			PercentItemRefund__c=ppvList.get(0).PercentItemRefund__c,
			PercentTaxRefund__c=ppvList.get(0).PercentTaxRefund__c,
			OrderNumber__c=ga.Order_Opportunity__c,
			RefundStatus__c='Pending',
			ExpectedRefundDate__c=Date.today().addDays(1),
			RecordTypeId=lineItemRefundRt,
			RefundReason__c='Price Protection',
			LineNumber__c=String.valueOf(ga.Line_Number__c));
			try
			{
				insert refund;
			}
			catch(Exception e)
			{
				ApexPages.addMessages(e);
				return null;
			}
			//return new PageReference('/'+refund.Id);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'Refund was created.'));
			return null;
		}
		else
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,message));
			return null;
		}
	}
	private String isEligible()
	{		
		String message = '';
		ppvList = new List<PriceProtectionValues__c>([SELECT Id,PriceProtectionStartDate__c,PriceProtectionEndDate__c,FinalDayofEligibility__c,PercentItemRefund__c,PercentTaxRefund__c FROM PriceProtectionValues__c WHERE SKU__c=:ga.SKU__c AND Country__c=:ga.Sale_Country__c]);
		if(ppvList.size()==1)
		{
			//we found one record for the sku/country, now check the date
			PriceProtectionValues__c ppv = ppvList.get(0);
			if(ga.Order_Opportunity__r.CreatedDate>=ppv.PriceProtectionStartDate__c && ga.Order_Opportunity__r.CreatedDate<=ppv.PriceProtectionEndDate__c)
			{				
				if(Date.today()>ppv.FinalDayOfEligibility__c)				
					message='SKU was found but the final day of eligibility has passed('+ppv.FinalDayofEligibility__c.format()+')';
			}
			else
				message='This order was not placed during the eligibility period. If you believe this is not actually the case, please escalate to your supervisor.';
				//message='The order was not created between '+ppv.PriceProtectionStartDate__c.format()+' and '+ppv.PriceProtectionEndDate__c.format();
		}
		else if(ppvList.size()>1)
		{
			//should we throw error alerting the user to contact sys admin?
			message = 'More than one record was found under price protection.  Please contact your system administrator.';			
		}
		else
			message = 'There is no price protection for this SKU('+ga.SKU__c+')';
		
		return message;
	}
}