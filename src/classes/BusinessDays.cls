public with sharing class BusinessDays 
{
	private static Boolean isWeekendDay(Date dateParam)
    {
       boolean result = false;       
       //Recover the day of the week
       Date startOfWeek = dateParam.toStartOfWeek();
       Integer dayOfWeek  = dateParam.day() - startOfWeek.day();
       result = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
       return result;
    } 
    public static Date addBusinessDays(Date StartDate, integer BusinessDaysToAdd )
    {
       //Add or decrease in BusinessDaysToAdd days 
       Date finalDate = StartDate;       
       integer direction = BusinessDaysToAdd < 0 ? -1 : 1;
       while(BusinessDaysToAdd != 0)
       {
            finalDate = finalDate.AddDays(direction);       
            if (!isWeekendDay(finalDate))
                BusinessDaysToAdd -= direction;
       }
       return finalDate;
    }
}