/**
 * @author:	Levementum LLC
 * @date:	12/18/15
 * @description:	Test Class for RestLostStolenHistory web service
 */
 @isTest
public class TestRestLostStolenHistory 
{
	public static RestRequest req;
	public static RestResponse res;

	public static void setup()
	{
		try
		{
			req = new RestRequest();
    		res = new RestResponse();
    			
    		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/LostStolenHistory';
    		req.httpMethod = 'GET';
    			
    		RestContext.request = req;
    		RestContext.response = res;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
		}
	}
	
    public static testMethod void testNullCustomerEmail()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 
  				 tr.createRefund();
  				 
				 req.addParameter('customer_email', null);
  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testInvalidCustomerEmail()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 
  				 tr.createRefund();

				 req.addParameter('customer_email', 'test@test.com');
  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testEmptyCustomerEmail()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 
  				 tr.createRefund();

				 req.addParameter('customer_email', '');
  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testValidCustomerEmailNoLostStolenRefund()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				 system.assert(acctId != null);
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 system.assert(oppId != null);
  				 
  				 Refund__c ref = tr.createRefund();
  				 ref.RefundReason__c = 'None';
  				 ref.OrderNumber__c = oppId;

  				 req.addParameter('customer_email', [SELECT Id, PersonEmail FROM Account WHERE Id =: acctId limit 1].PersonEmail);
  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testValidCustomerEmailLostStolenRefund()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				 system.assert(acctId != null);
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 system.assert(oppId != null);
  				 
  				 Refund__c ref = tr.createRefund();
  				 ref.RefundReason__c = 'Lost/Stolen';
  				 ref.OrderNumber__c = oppId;
  				 update ref;
  				 				
  				 req.addParameter('customer_email', [SELECT Id, PersonEmail FROM Account WHERE Id =: acctId limit 1].PersonEmail);
  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testNullResponseModel()
    {
    	try
    	{
    		Test.startTest();
  				 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				 system.assert(acctId != null);
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 system.assert(oppId != null);
  				 
  				 Refund__c ref = tr.createRefund();
  				 ref.RefundReason__c = 'Lost/Stolen';
  				 ref.OrderNumber__c = oppId;
  				 update ref;

  				 RestLostStolenHistory.getNumberOfRefunds();
  				 
  				 system.assertEquals(RestLostStolenHistory.response.is_success, true);	  		
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
    
    public static testMethod void testRefundsWithinLast12Months()
    {
    	try
    	{
    		Test.startTest();
    			 setup();
  				 TestRestUtilities tr = new TestRestUtilities();
  				 tr.createAccount();
  				 Id acctId = tr.idsMap.get('Account1');  
  				 system.assert(acctId != null);
  				    
  				 tr.createOpportunity();
  				 Id oppId = tr.idsMap.get('Opportunity1'); 
  				 system.assert(oppId != null);
  				 
  				 Refund__c ref = tr.createRefund();
  				 ref.RefundReason__c = 'Lost/Stolen';
  				 ref.OrderNumber__c = oppId;
  				 update ref;

				 List<Account> accts = [SELECT PersonEmail FROM Account WHERE id =: tr.idsMap.get('Account1')];
				 if(accts.size() > 0)
				 {
					 req.addParameter('customer_email', accts.get(0).PersonEmail);
	  				 RestResponseModel.lost_stolen_history_response_t result = RestLostStolenHistory.getNumberOfRefunds();
	  				 
	  				 // For the number n provided, starts 12:00:00 of the last day of the previous month and continues for 
	  				 // the past n months.
	  				 //system.assertEquals(RestLostStolenHistory.response.number_lost_stolen_refunds, 0);
				 }
    		Test.stopTest();
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
    	}
    }
}