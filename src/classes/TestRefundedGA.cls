/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRefundedGA 
{
	static set<Id> gaIdSet = new Set<Id>();
    static testMethod void myUnitTest() 
    {
        Test.startTest();
        PlayToGlass.refundedGa(gaIdSet);
        Test.stopTest();
    }
    static
    {
    	GlassIntegration__c gi = new GlassIntegration__c(domain__c='test',name='test@test.com',glass__c='asdf');
    	insert gi;    
    	
    	Account a = new Account();
        a.Name='TestAccount';        
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name='TestOpp';
        opp.CloseDate=Date.today();
        opp.StageName='Closed Won';
        opp.AccountId=a.Id;
        opp.Type='Standard Order';
        opp.Country__c='UK';
        opp.State_Province__c='GA';
        insert opp;
                        
        Google_Asset__c gAsset = new Google_Asset__c();
        gAsset.Name='46654';        
        gAsset.SKU__c='xxxBBBccc';
        gAsset.Order_Opportunity__c=opp.Id;
        gAsset.Line_Number__c=1;
        insert gAsset;
        gaidSet.add(gAsset.Id);
    }
}