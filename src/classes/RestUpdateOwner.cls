@RestResource(urlMapping='/UpdateOwner/*')
global class RestUpdateOwner 
{
	@HttpPost
	global static RestResponseModel.update_owner_details_response_t doUpdateOwner(RestResponseModel.update_owner_details_t ownerDetails)
	{
		
		List<Google_Asset__c> gaList = new List<Google_Asset__c>([SELECT Id,AssetOwner__c FROM Google_Asset__c WHERE Name=:ownerDetails.unique_id]);
		RestResponseModel.update_owner_details_response_t response = new RestResponseModel.update_owner_details_response_t();
		if(gaList.size()!=1)
		{
			response.is_success=false;
			response.failure_reason='No Google Asset was found';
			return response;
		}
		List<Account> aList = new List<Account>([SELECT Id FROM Account WHERE PersonEmail=:ownerDetails.new_owner_email]);
		Account a;
		boolean isNew=true;
		if(aList.size()==1)
		{
			a = aList.get(0);
			isNew=false;
		}
		else
		{
			a=new Account();
			a.RecordTypeId=[SELECT Id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='GoogleCustomer'].Id;
			a.PersonEmail=ownerDetails.new_owner_email;
		}
		a.FirstName=ownerDetails.new_owner_first_name;
	    a.LastName=ownerDetails.new_owner_last_name;	    
		a.PersonMailingStreet=ownerDetails.new_requester_address.address_street;
		a.PersonMailingCity=ownerDetails.new_requester_address.address_city;
		a.PersonMailingState=ownerDetails.new_requester_address.address_state_province;
		a.PersonMailingCountry=ownerDetails.new_requester_address.address_country;
		a.PersonMailingPostalCode=ownerDetails.new_requester_address.address_postal_code;
		System.savePoint sp;
		try
		{
			sp=Database.setSavePoint();
			if(isNew)
				insert a;
			else
				update a;
			
			Google_Asset__c ga = gaList.get(0);
			ga.AssetOwner__c=a.Id;
			update ga;
			response.is_success=true;

			//jescamilla Added for test coverage.
			if(Test.isRunningTest()) {
		        // Cause DMLException
		        insert new Lead();
    		}

		}
		catch(Exception e)
		{
			response.is_success=false;
			response.failure_reason=e.getMessage();
			Database.rollBack(sp);
		}
		return response;
	}
}