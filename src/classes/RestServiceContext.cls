public class RestServiceContext 
{
	public SObjectDataAccess sObjectDA {get; set;}
	
	public RestServiceContext(SObjectDataAccess sObjectDA)
	{
		this.sObjectDA = sObjectDA;
	}
	
	public void updateObject(sObject obj, ObjectModel objModel)
	{
		this.SObjectDA.updateObject(obj, objModel);
	}
	
	public sObject selectObject(Id objId)
	{
		return this.sObjectDA.selectObject(objId);
	}
	
	public sObject selectObject(String objName)
	{
		return this.sObjectDA.selectObject(objName);
	}
	
	public void deleteObject(Id objId)
	{
		this.deleteObject(objId);
	}
	
	public void deleteObject(String objName)
	{
		this.deleteObject(objName);
	}
	
	public sObject insertObject(ObjectModel objectModel)
	{
		return this.sObjectDA.insertObject(objectModel);
	}
	
	public sObject updateModel(sObject sObjectToUpdate, ObjectModel updatingData)
	{
		return this.sObjectDA.updateModel(sObjectToUpdate, updatingData);
	}
}