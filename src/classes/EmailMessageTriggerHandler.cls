public without sharing class EmailMessageTriggerHandler 
{
	public static void onBeforeInsert(EmailMessage[] trgNew)
	{
		Map<Id,Case> idToCaseMap = new Map<Id,Case>();
		Set<Id> caseIdSet = new Set<Id>();
		Case c;
		RecordType rt = [SELECT Id FROM RecordType where SobjectType='Case' and Name = 'ChromeOs'];
		for(EmailMessage em : trgNew)
		{
			if(em.ParentId!=null)
				caseIdSet.add(em.ParentId);
		}
		for(Case c2 : [SELECT Id, NumberofEmailsFromCustomer__c, NumberofEmailsFromSupportTeam__c FROM Case WHERE Id in :caseIdSet AND RecordTypeId=:rt.Id])
			idToCaseMap.put(c2.Id,c2);
			
		
		for(EmailMessage em : trgNew)
		{
			if(idToCaseMap.containsKey(em.ParentId))
			{
				c = idToCaseMap.get(em.ParentId);
				//if(!em.FromName.containsIgnoreCase('support'))
				if(em.Incoming)
    			{
    				if(c.NumberOfEmailsFromCustomer__c==null)
    					c.NumberOfEmailsFromCustomer__c=0;
    				c.NumberofEmailsFromCustomer__c+=1;
    			}
    			//else if(em.FromName.containsIgnoreCase('support'))
    			else if(!em.Incoming)
    			{
    				//if it's not incoming and the name contains support, increment
    				if(c.NumberOfEmailsFromSupportTeam__c==null)
    					c.NumberOfEmailsFromSupportTeam__c=0;    				
    				c.NumberofEmailsFromSupportTeam__c+=1;    				
    			}
			}
		}
		if(!idToCaseMap.isEmpty())
			update idToCaseMap.values();
	}
}