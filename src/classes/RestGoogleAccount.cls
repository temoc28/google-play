/**
 * @author:	Cuauhtemoc Munoz
 * @date:	04/24/15
 * @email:	cmunoz@levementum.com
 * @description:	This REST service exposes the GoogleAccount (Account) object from Salesforce to be used
 * during integration testing at Google.
 * @update:	04/27/15. Added Strategy and context
 */
@RestResource(urlMapping='/GoogleAccountTest/*')
global class RestGoogleAccount
{
	public static RestServiceContext dataAccessContext = new RestServiceContext(new GoogleAccountDataAccess());
	
	@HttpPost
	global static ObjectModel updateObject(GoogleAccountModel accountModel)
	{
		GoogleAccountModel response = null;
		try
		{
			Account accountToUpdate;
			Account updatedAccount;
			
			if(accountModel.Id != null)
			{
				accountToUpdate = (Account)dataAccessContext.selectObject(accountModel.Id);
			}
			else
			{
				accountToUpdate = (Account)dataAccessContext.selectObject(accountModel.Name);
			}			
			dataAccessContext.updateObject(accountToUpdate, accountModel);
			
			// REFRESH FORMULA FIELDS
			if(accountModel.Id != null)
			{
				updatedAccount = (Account)dataAccessContext.selectObject(accountModel.Id);
			}
			else
			{
				updatedAccount = (Account)dataAccessContext.selectObject(accountModel.Name); 
			}
			
    		response = (GoogleAccountModel)updateModel(updatedAccount);
    		response.status = GoogleAccountDataAccess.objectStatus;
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
		return response;
	}
	
	@HttpGet
	global static ObjectModel getObject()
	{
    	GoogleAccountModel response = new GoogleAccountModel();
    	try
    	{
    		Map<String, String> paramMap = RestContext.request.params;
			String accountName = paramMap.get('accountName');
			
			if(String.isNotEmpty(accountName))
			{
				Account currentAccount = (Account)dataAccessContext.selectObject(accountName);			
				response = (GoogleAccountModel)updateModel(currentAccount);
			}
    	}		
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	response.status = GoogleAccountDataAccess.objectStatus;
        return response;
    }
	
	@HttpDelete
	global static void deleteObject()
	{
		try
		{
    		Map<String, String> paramMap = RestContext.request.params;
			String accountName = paramMap.get('accountName');	
			dataAccessContext.deleteObject(accountName);
		}
		catch(Exception ex)
		{
			system.debug(ex.getMessage());
		}
	}
	
	public static ObjectModel updateModel(sObject updatedsObject)
    {
    	GoogleAccountModel accountModel = new GoogleAccountModel();
    	Account accountSObject = (Account)updatedsObject;
    	try
    	{
    		if(accountSObject != null && accountModel != null)
    		{
	    		accountModel.Id = accountSObject.Id;
				accountModel.IsDeleted = accountSObject.IsDeleted;
				accountModel.MasterRecordId = accountSObject.MasterRecordId;
				accountModel.Name = accountSObject.Name;
				accountModel.LastName = accountSObject.LastName;
				accountModel.FirstName = accountSObject.FirstName;
				accountModel.Salutation = accountSObject.Salutation;
				accountModel.Type = accountSObject.Type;
				accountModel.RecordTypeId = accountSObject.RecordTypeId;
				accountModel.ParentId = accountSObject.ParentId;
				accountModel.BillingStreet = accountSObject.BillingStreet;
				accountModel.BillingCity = accountSObject.BillingCity;
				accountModel.BillingState = accountSObject.BillingState;
				accountModel.BillingPostalCode = accountSObject.BillingPostalCode;
				accountModel.BillingCountry = accountSObject.BillingCountry;
				accountModel.BillingLatitude = accountSObject.BillingLatitude;
				accountModel.BillingLongitude = accountSObject.BillingLongitude;
				accountModel.ShippingStreet = accountSObject.ShippingStreet;
				accountModel.ShippingCity = accountSObject.ShippingCity;
				accountModel.ShippingState = accountSObject.ShippingState;
				accountModel.ShippingPostalCode = accountSObject.ShippingPostalCode;
				accountModel.ShippingCountry = accountSObject.ShippingCountry;
				accountModel.ShippingLatitude = accountSObject.ShippingLatitude;
				accountModel.ShippingLongitude = accountSObject.ShippingLongitude;
				accountModel.Phone = accountSObject.Phone;
				accountModel.Fax = accountSObject.Fax;
				accountModel.AccountNumber = accountSObject.AccountNumber;
				accountModel.Website = accountSObject.Website;
				accountModel.Sic = accountSObject.Sic;
				accountModel.Industry = accountSObject.Industry;
				accountModel.AnnualRevenue = accountSObject.AnnualRevenue;
				accountModel.NumberOfEmployees = accountSObject.NumberOfEmployees;
				accountModel.Ownership = accountSObject.Ownership;
				accountModel.TickerSymbol = accountSObject.TickerSymbol;
				accountModel.Description = accountSObject.Description;
				accountModel.Rating = accountSObject.Rating;
				accountModel.Site = accountSObject.Site;
				accountModel.OwnerId = accountSObject.OwnerId;
				accountModel.CreatedDate = accountSObject.CreatedDate;
				accountModel.CreatedById = accountSObject.CreatedById;
				accountModel.LastModifiedDate = accountSObject.LastModifiedDate;
				accountModel.LastModifiedById = accountSObject.LastModifiedById;
				accountModel.SystemModstamp = accountSObject.SystemModstamp;
				accountModel.LastActivityDate = accountSObject.LastActivityDate;
				accountModel.LastViewedDate = accountSObject.LastViewedDate;
				accountModel.LastReferencedDate = accountSObject.LastReferencedDate;
				accountModel.PersonContactId = accountSObject.PersonContactId;
				accountModel.IsPersonAccount = accountSObject.IsPersonAccount;
				accountModel.PersonMailingStreet = accountSObject.PersonMailingStreet;
				accountModel.PersonMailingCity = accountSObject.PersonMailingCity;
				accountModel.PersonMailingState = accountSObject.PersonMailingState;
				accountModel.PersonMailingPostalCode = accountSObject.PersonMailingPostalCode;
				accountModel.PersonMailingCountry = accountSObject.PersonMailingCountry;
				accountModel.PersonMailingLatitude = accountSObject.PersonMailingLatitude;
				accountModel.PersonMailingLongitude = accountSObject.PersonMailingLongitude;
				accountModel.PersonOtherStreet = accountSObject.PersonOtherStreet;
				accountModel.PersonOtherCity = accountSObject.PersonOtherCity;
				accountModel.PersonOtherState = accountSObject.PersonOtherState;
				accountModel.PersonOtherPostalCode = accountSObject.PersonOtherPostalCode;
				accountModel.PersonOtherCountry = accountSObject.PersonOtherCountry;
				accountModel.PersonOtherLatitude = accountSObject.PersonOtherLatitude;
				accountModel.PersonOtherLongitude = accountSObject.PersonOtherLongitude;
				accountModel.PersonMobilePhone = accountSObject.PersonMobilePhone;
				accountModel.PersonHomePhone = accountSObject.PersonHomePhone;
				accountModel.PersonOtherPhone = accountSObject.PersonOtherPhone;
				accountModel.PersonAssistantPhone = accountSObject.PersonAssistantPhone;
				accountModel.PersonEmail = accountSObject.PersonEmail;
				accountModel.PersonTitle = accountSObject.PersonTitle;
				accountModel.PersonDepartment = accountSObject.PersonDepartment;
				accountModel.PersonAssistantName = accountSObject.PersonAssistantName;
				accountModel.PersonLeadSource = accountSObject.PersonLeadSource;
				accountModel.PersonBirthdate = accountSObject.PersonBirthdate;
				accountModel.PersonLastCURequestDate = accountSObject.PersonLastCURequestDate;
				accountModel.PersonLastCUUpdateDate = accountSObject.PersonLastCUUpdateDate;
				accountModel.PersonEmailBouncedReason = accountSObject.PersonEmailBouncedReason;
				accountModel.PersonEmailBouncedDate = accountSObject.PersonEmailBouncedDate;
				accountModel.Jigsaw = accountSObject.Jigsaw;
				accountModel.JigsawCompanyId = accountSObject.JigsawCompanyId;
				accountModel.AccountSource = accountSObject.AccountSource;
				accountModel.SicDesc = accountSObject.SicDesc;
				accountModel.Last_Name = accountSObject.Last_Name__c;
				accountModel.First_Name = accountSObject.First_Name__c;
				accountModel.Mobile = accountSObject.Mobile__c;
				accountModel.Other_Phone = accountSObject.Other_Phone__c;
				accountModel.Salutation = accountSObject.Salutation__c;
				accountModel.LegacySFContactID = accountSObject.LegacySFContactID__c;
				accountModel.PersonAcctIDExternalID = accountSObject.PersonAcctIDExternalID__c;
				accountModel.MIgration = accountSObject.MIgration__c;
				accountModel.DupFlag = accountSObject.DupFlag__c;
				accountModel.PersonAcctContactLookup = accountSObject.PersonAcctContactLookup__c;
				accountModel.Customer_Country = accountSObject.Customer_Country__c;
				accountModel.CID_Exception_Granted = accountSObject.CID_Exception_Granted__c;
				accountModel.CID_Exception_Granted_Date_Time = accountSObject.CID_Exception_Granted_Date_Time__c;
				accountModel.CID_Exception_on_This_Asset = accountSObject.CID_Exception_on_This_Asset__c;
				accountModel.CID_Exception_Granted_Asset = accountSObject.CID_Exception_Granted_Asset__c;
				accountModel.CID_Exception_Granted_Asset_Description = accountSObject.CID_Exception_Granted_Asset_Description__c;
				accountModel.CID_Exception_Granted_Asset_Name = accountSObject.CID_Exception_Granted_Asset_Name__c;
				accountModel.Primary_Email_pc = accountSObject.Primary_Email__pc;
				accountModel.Notes_pc = accountSObject.Notes__pc;
				accountModel.Email_pc = accountSObject.Email__pc;
				accountModel.OtherEmail_pc = accountSObject.OtherEmail__pc;
				accountModel.Migration_pc = accountSObject.Migration__pc;
    		}
    	}
    	catch(Exception ex)
    	{
    		system.debug(ex.getMessage());
    	}
    	return accountModel;
    }
}