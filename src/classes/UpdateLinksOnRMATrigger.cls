public class UpdateLinksOnRMATrigger {
	
	public static void UpdateValues(list<RMA__c> TNew){
		list<Id> CaseIdList = new list<Id>(); //List of all Cases associated with RMAs
		map<Id, RMA__c> CaseIdRMAMap = new map<Id, RMA__c>(); //Map of Case ids to RMAs
		
		//Populate a list of the Emails and Orders associated to the RMAs and Map them
		for(RMA__c R : TNew){
			if(R.Case__c != NULL){
				CaseIdList.Add(R.Case__c);
				CaseIdRMAMap.put(R.Case__c,R);
			}
		}
		
		//Update the account of the Case to the account of the Email or the Account
		for(Case C : [SELECT Id, ContactId, Order__c FROM Case WHERE Id IN: CaseIdList]){
			if(CaseIdRMAMap.containsKey(C.Id)){
				if(CaseIdRMAMap.get(C.Id).Google_Account__c == NULL){
					CaseIdRMAMap.get(C.Id).Google_Account__c = C.ContactId;
				}
				
				if(CaseIdRMAMap.get(C.Id).Order__c == NULL){
					CaseIdRMAMap.get(C.Id).Order__c = C.Order__c;
				}
			}
		}
		return;
	}
	
	@isTest
    static void myUnitTest() {
        String unique = String.valueof(Datetime.now()).replace(' ','_').replace('-','_').replace(':','_') + String.valueOf(Math.random());
        Contact c = new Contact(LastName = 'test_' + unique);
        insert c;
        
		Google_Asset__c ga = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891011'
        										, Asset_Type__c = 'New'
        										, Active_Device__c = 'Active');
		insert ga;
		Google_Asset__c ga2 = new Google_Asset__c(Google_Asset_ID_External_ID__c = '1234567891012'
        										, Asset_Type__c = 'New'
        										, Active_Device__c = 'Active');
		insert ga2;
		
        RMA__c r = new RMA__c(Google_Account__c = c.Id, GoogleAsset__c = ga.Id);
        insert r;
        
        Order__c O = new Order__c(Google_Account__c = c.Id);
        insert O;
        
        Case ca = new Case(ContactId=c.Id, Order__c=O.id);
        
        r = new RMA__c(Case__c = ca.Id, GoogleAsset__c = ga2.Id);
        insert r;
    }
}