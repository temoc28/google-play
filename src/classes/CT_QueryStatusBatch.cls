global class CT_QueryStatusBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{

    global final string query;
        
    global CT_QueryStatusBatch(String q){
        query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){        
        if(scope.size() == 0) return;
        CT_QueryStatusUtility.ProcessRMAs(scope);        
    }

    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'sumit.shingavi@cloudsherpas.com'});
        mail.setReplyTo('donotreply@cloudsherpas.com');
        mail.setSenderDisplayName('RMA Batch Processing');
        mail.setSubject('RMA Query Status Batch Process Completed');
        mail.setPlainTextBody('RMA Query Status Batch Process Completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
       
    public static testMethod void testThisClass() {
        Test.startTest();        
        
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        
        Contact con = new Contact(LastName = 'TestContact', AccountId = acc.Id, MailingStreet = '3300 Fremont Blvd Ste 100', 
            MailingState = 'CA', MailingCity = 'Fremont', MailingPostalCode = '94358');
        insert con;
        
        acc.PersonAcctContactLookup__c = con.Id;
        update acc;
        
        Id rtId = Schema.SObjectType.RMA__c.getRecordTypeInfosByName().get('Chromebook WiFi').getRecordTypeId();
        System.assertNotEquals(rtId, null);
        
        Google_Asset__c ga = new Google_Asset__c (Google_Asset_ID_External_ID__c = '9421341099', Asset_Type__c = 'New',
            Active_Device__c = 'Active', AssetOwner__c = acc.Id, Carrier__c = 'Carrier1', IMEI__c = 'IMEI' );
        insert ga;
        System.assertNotEquals(ga.Id, null);
        
        Order__c ord = new Order__c(Name = 'testorder');
        insert ord;
        System.assertNotEquals(ord.Id, null);
        
        RMA__c rma = new RMA__c(rma_country__c = 'US', Type__c = 'Galaxy Nexus - Remorse', Status__c = 'Pending Return', 
            Notes__c = 'test-notes',
             GoogleCustomer__c = acc.Id, Order__c = ord.Id, GoogleAsset__c = ga.Id,
            RecordTypeId = rtId, Line_Number__c = 1, Return_Shipping_Tracking_Number__c = 'F2012', 
            Customer_Induced_Damage__c = 'N', Created_on_Google_Server__c = true, Ready_to_Submit__c = true, RMA_Sub_Category__c = 'LCD Issue', 
            Google_Account__c = con.Id);
        insert rma;
        System.assertNotEquals(rma.Id, null);              
        
        String strSOQL = 'SELECT id, Name, Ready_to_Submit__c, Status__c, Line_Number__c FROM RMA__c WHERE Ready_to_Submit__c = true AND Status__c = \'Pending Return\'';
        CT_QueryStatusBatch qsb = new CT_QueryStatusBatch(strSOQL);
        ID batchprocessid = Database.executeBatch(qsb,100);
        
        Test.stoptest();
    }    
}