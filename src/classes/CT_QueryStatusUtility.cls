global class CT_QueryStatusUtility
{
    public static void ProcessRMAs(List<sObject> scope)
    {   
        //Map<String, sObject> scopeMap = new Map<String, sObject>();
        Map<String, RMA__c> scopeMap = new Map<String, RMA__c>();
        List<sObject> UpdatedRMAs = new List<sObject>();
        QueryRMAStatusController.QueryRMAStatus_pt qrsc = new QueryRMAStatusController.QueryRMAStatus_pt();
        List<QueryRMAStatusController.RMAInputType> RMAInput = new List<QueryRMAStatusController.RMAInputType>();        
        List<QueryRMAStatusController.RMADetailsType> responseList = new List<QueryRMAStatusController.RMADetailsType>();
                
        System.debug(LoggingLevel.INFO, 'scope == '+scope);                
        RMA__c rma;
        try
        {            
            if(scope.size() > 0)
            {                
                Set<Id> orderIdSet = new Set<Id>();
                for(sObject s : scope)
                {     
                   rma = (RMA__c)s; 
                   //scopeMap.put(String.valueOf(s.get('Name')), s);
                   scopeMap.put(rma.Name, rma);
                   orderIdSet.add(rma.Opportunity__r.Id);
                   //orderIdSet.add((Id)String.valueOf(s.get('Opportunity__r.Id')));
                }                   
                //Add all the total order refunds so we can mark them as complete
                //instead of received so the trigger doesn't try and create refunds
                Map<Id,Refund__c> refundMap = new Map<Id,Refund__c>();
                for(Refund__c refund : [SELECT Id,OrderNumber__c FROM Refund__c WHERE RecordType.Name='Total Order Refund' AND OrderNumber__c in :orderIdSet])
				{	
					if(!refundMap.containsKey(refund.OrderNumber__c))
						refundMap.put(refund.OrderNumber__c,refund);
				}
                
                QueryRMAStatusController.RMAInputType rmait = new QueryRMAStatusController.RMAInputType();                
                String lineNo = '';
                for(sObject obj : scope)
                {
                    rmait.RMANumber = String.valueOf(obj.get('Name'));
                    rmait.RMALineNumber = (String.valueOf(obj.get('Line_Number__c')) != null) ? String.valueOf(obj.get('Line_Number__c')) : '0';
                    rmait.Vendor = '';
                    rmait.FromDate = '';
                    rmait.ToDate = '';
                    RMAInput.add(rmait);
                    rmait = new QueryRMAStatusController.RMAInputType();                
                }                
                
                qrsc.timeout_x = 120000;
                responseList = qrsc.process(RMAInput);
                
                for(QueryRMAStatusController.RMADetailsType r : responseList)
                {
                    if(r.RMADetailsStatus == 'VENDOR_RECEIVED') //if(r.RMADetailsStatus == 'OPEN')                    
                    {
                        System.debug('=='+r.RMANumber+'==ReturnConditionCode==' + r.ReturnConditionCode);
                        System.debug('=='+r.RMANumber+'==ReturnConditionNotes==' + r.ReturnConditionNotes);
                        System.debug('=='+r.RMANumber+'==RMADetailsStatus==' + r.RMADetailsStatus);
                        
                        System.debug('==scopeMap==' + scopeMap);
                        
                        if(scopeMap != null && r.RMANumber != null 
                        && r.RMANumber != '' && scopeMap.ContainsKey(r.RMANumber))
                        {              
                            //sObject s = scopeMap.get(r.RMANumber);
                            rma = scopeMap.get(r.RMANumber); 
                            //if(refundMap.containsKey((Id)s.get('Opportunity__r.Id')))
	                            //s.put('Status__c', 'Closed');
	                        if(refundMap.containsKey(rma.Opportunity__r.Id))
	                            rma.Status__c='Closed';
	                        else
	                        	rma.Status__c='Received';
	                        	//s.put('Status__c', 'Received');
                            //s.put('Query_Status_Processed__c', true);
                            rma.Query_Status_Processed__c=true;
                            //UpdatedRMAs.add(s);
                            UpdatedRMAs.add(rma);
                        }
                    }    
                }                
            }         
        }
        catch(Exception e)
        {
            System.debug('Exception: ' + e.getMessage());
        }   
        
        /*try{            
            update UpdatedRMAs;
            System.debug(LoggingLevel.INFO, 'final updated UpdatedRMAs== '+UpdatedRMAs);            
        }catch(Exception e){
            System.Debug(LoggingLevel.Info, 'Exception : '+e.getMessage());
        } */
        List<Database.Saveresult> srList = Database.update(UpdatedRMAs,false);
        integer i=0;
        for(Database.Saveresult sr : srList)
        {
        	if(!sr.isSuccess())
        		System.debug(LoggingLevel.Error,'RecordId:'+UpdatedRMAs.get(i).Id+' did not update:::::'+sr.getErrors());
        	i++;
        }   
        RMAInput = new List<QueryRMAStatusController.RMAInputType>();        
        responseList = new List<QueryRMAStatusController.RMADetailsType>();
    }
}