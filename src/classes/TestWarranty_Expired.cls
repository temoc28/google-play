@isTest
public class TestWarranty_Expired {
    public static testMethod void testNewTo6Months() {
        Date dateToday = Date.today();
        
        // 0-6mo
        Google_Asset__c asset = createAsset(dateToday.addDays(-29));
        Warranty_Expired we = new Warranty_Expired(asset);
        System.assertEquals('In Warranty', we.message);
    }
    
    public static testMethod void test6MonthsTo1Year() {
        Date dateToday = Date.today();
        // 6mo-1year
        Google_Asset__c asset = createAsset(dateToday.addDays(-314));
        Warranty_Expired we = new Warranty_Expired(asset);
        System.assertEquals('In Warranty', we.message);
    }
    
    public static testMethod void testYear2() {
        Date dateToday = Date.today();
        // 1year-2years
        Google_Asset__c asset = createAsset(dateToday.addDays(-400));
        Warranty_Expired we = new Warranty_Expired(asset);
        System.assertEquals('In Warranty', we.message);
    }
    
    public static testMethod void testWarrantyExpired() {
        Date dateToday = Date.today();
        // Over 2years
        Google_Asset__c asset = createAsset(dateToday.addDays(-750));
        Warranty_Expired we = new Warranty_Expired(asset);
        System.assertEquals('WARRANTY EXPIRED', we.message);
    }
    
    public static testMethod void testRender() {
        Google_Asset__c asset = createAsset(Date.today());
        Warranty_Expired we = new Warranty_Expired(asset);
        System.assert(we.render);
    }
    
    private static Google_Asset__c createAsset(Date saleDate) {
      Account a = new Account();
      a.LastName='TestWarranty';
      a.Email__pc = 'new@new.com'; 
      a.PersonMailingCountry = 'UK';
      insert a;
      
      Opportunity opp = new Opportunity();
      opp.Name='TestOpp';
      opp.CloseDate=saleDate;
      opp.TransactionDate__c = saleDate;
      opp.StageName='Closed Won';
      opp.AccountId=a.Id;
      opp.Type='Standard Order';
      opp.Country__c='UK';
      insert opp;
        
      Product2 p = new Product2();
      p.Name = 'Test Product';
      p.SKU__c = '123456';
      p.SKUDescription__c = 'Testing';
      p.Finsky_Doc_ID__c = '1';
      p.Sale_Country__c = 'UK';
      p.ServiceModel__c = 'Remorse + DOA + Warranty';
      p.RepairPartner__c = 'Asus APAC (HK)';
      p.Retail_Channel__c = 'Mixed';  // "Play Only" sets the record type for associated
                                      // Google_Assets to "Google Play Asset"
      insert p;
      
      Google_Asset__c gAsset = new Google_Asset__c();
      gAsset.Name='4665';
      gAsset.SKU__c='123456';
      gAsset.Order_Opportunity__c=opp.Id;
      gAsset.Line_Number__c=1;
      gAsset.Sale_Date__c = saleDate;
      gAsset.Retail_Sale_Date__c = saleDate;
      gAsset.RecordTypeId = [select Id from RecordType where Name = 'Retail Asset'][0].Id;
      gAsset.Warranty_Period__c = 730;
      insert gAsset;
        
      return gAsset;
    }
        
}